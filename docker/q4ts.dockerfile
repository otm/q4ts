FROM msecher/q4ts:debian_t-m_main_11-21-2022_salome_9.9

USER root

RUN mkdir -m755 -p /etc/apt/keyrings  # not needed since apt version 2.4.0 like Debian 12 and Ubuntu 22 or newer

RUN apt install -y wget lsb-release
RUN wget -O /etc/apt/keyrings/qgis-archive-keyring.gpg https://download.qgis.org/downloads/qgis-archive-keyring.gpg

RUN echo "Types: deb deb-src\n\
URIs: https://qgis.org/debian/\n\
Suites: $(lsb_release -cs)\n\
Architectures: amd64\n\
Components: main\n\
Signed-By: /etc/apt/keyrings/qgis-archive-keyring.gpg"\
>> /etc/apt/sources.list.d/qgis.sources

RUN apt update

RUN apt install -y qgis

USER tm
