# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 0.10.0 - 2024-03-05

This version contains bugfixes related with salome script use. Now Fiona is used instead of GDAL.

`slf2med` processing can also use other extension than .slf for SELAFIN mesh (can also be .srf, .res or other).

Fixes :

- Bug: Interpolate function can only modify BOTTOM value #214
- Bug still with Extend Mesh #211
- Contour pb with Create Mesh #210
- Bug with Extend Mesh: does'nt recognize INPUT Meshes #209
-Extend Mesh: Bug when adding a river (as a refine layer) in the extension at the jonction old / new river #201

## 0.9.1 - 2023-12-12

This version contains mainly bugfixes for contour generation and some enhancement for snapping of extend mesh used contour.

Features:

- feat(ui): add snapping button on extend mesh contour layer !179

Fixes:

- Create Boundary : Creating the contour, I can't see it #200

## 0.9.0 - 2023-12-06

This version contains new processings for mesh extension and bug fixes.

This is the first version available on the official QGIS repository.

Features:

- Add extent mesh processing #50 #51
- Update create mesh processing for new parameters #176 #177 #181 #198
- Add logs for processings #187 #189
- Distribution of Q4TS on the official repository of QGIS #165
- feat(alter): add rotate option !169

Fixes:

- Documentation Interpolation PointCloud : bug if contour of the mesh bigger than the point clouds data. #167
- Name of a refine layer shape filled in the attributs table turns to be NULL when we open the table #194
- Bug: Create Boundary when creating the contour #197

## 0.8.0 - 2024-10-18

This version contains mainly bugfixes for TELEMAC-MASCARET v8p5 use.

Features:

- salome_script: add growth_ratio as user parameter for create mesh (+ correction for MGCadSurf) !160
- Huge speedup of create_boundary with big meshes  (commit 9b3cbf7ffd92efd6dc4cc58fb103f429c912c038)

Fixes:

- Fix problem of field on Homard refinement !161
- Fix overconstrained triangles treatment in create_mesh !157
- fix(salome_script): remove toggle_precision duplicate !158
- feat(test): add flags for telemac-mascaret v8p5 use !159
- fix spelling mistake for alter script (commit b9ddfed8dd6123b8c0abf0aebce2f07cbbf4d947)
- Boundary Condition : Bug of display of the nodes inside a boundary condition : depends on contour_mesh displayed or not #166
- Fix conversion of med to serafin file to have double precision on coordinates (commit aaaaf2f3de479094d954b30718fd17685f4fc29a)

## 0.7.0 - 2023-11-24

This version contains mainly bugfixes for mesh contour display.

Features:

- Change the style of Mesh Contour #160
- Change of value of Default value in Interpolation point Cloud and Interpolation Field #151
- Automatic SCR definition from project for layer without SCR !151 !152
- Update default style of segment during the snapping #23
- Move friction table symbology to friction table widget !155

## 0.6.0 - 2023-10-13

Final version for 0.6.0.

Features:

- Change strickler to friction to table edition #153
- update mesh layer symbology at load !143

## 0.6.0-rc2 - 2022-12-02

Release candidate 2 6 for 0.6.0.

Features:

- Add test of salome script and QGIS processing in CI !135 !136
- Use Q4TS geopackage folder for default output path #138
- Force tracing in snapping option for boundaries layers creation #139
- Use .ui file for refine layer attributes definition #137

Fix:

- Can't run interpolation field if "Use available value." option used #141

## 0.6.0-rc1 - 2022-11-15

Release candidate 6 for 0.6.0.

Features:

- Use of a geopackage file to store all layer created by Q4TS  #135
- ask for deletion of layer in Q4TS geopackage when remove in QGIS !130
- add stbtel option for refine !125 !120
- Add option in create boundary widget to update snapping options #133
- Update boundaries and contour style for boundary creation widget #132
- Small GUI refactoring #131 #130 #128 #119 #117
- Mesh Transform: Add check all / uncheck all for variable #96
- Use return code from telemac-mascaret and salome scripts #121
- Ask user for layer save before processing run #127

Fix:

- Interpolation field and point cloud : invalid dataset style updated with several dataset available #129
- Mesh transform: legend ok but min/max not in propriety #116
- Mesh Transform : bug on times #115
- Interpolation field : Default value = 0 doesn't work #113
- Can't use domains on refine with SALOME #88
- if mesh for refine have field before min_size, invalid value can be used if fiona not available !129

## 0.5.0 - 2022-10-07

Release 0.5.0.

- add plugin icon
- fix definition of bottom friction style after clc interpolation if several dataset available in input mesh layer

## 0.5.0-rc6 - 2022-09-30

Release candidate 6 for 0.5.0.

- Error in variable name extracted by MDAL : variable name is changed  #99
- add some icons
- update default value for strickler table #104
- fix multiple boundary condition use #100

## 0.5.0-rc5 - 2022-09-13

Release candidate 5 for 0.5.0.

- fix times definition in alter script #89
- restore output definition for boundaries creation #93
- Windows version : only one variable can be selected in mesh transform processing #92
- Change the style of the interpolation point cloud and field mesh result #45

## 0.5.0-rc4 - 2022-09-02

Release candidate 4 for 0.5.0.

- update module display order #91
- add processing widget in main window #84
- fix multiple variable selection on Windows #92
- remove stderr and stdout distinction when launching salome or telemac (allow to keep log order and fix some issues on Windows)
- fix environnement variable on Windows to remove `GDAL_DRIVER_PATH` #85
- update loaded mesh style after a refine #87

## 0.5.0-rc3 - 2022-08-30

Release candidate 3 for 0.5.0.

- update order of boundaries limit style
- update island and contour style
- fix invalid selafin filename for input
- fix python call on Windows

## 0.5.0-rc2 - 2022-07-04

Release candidate 2 for 0.5.0.

Fix windows version

### Added

- fix .ui for CLC interpolation on Windows

## 0.5.0-rc1 - 2022-07-01

Release candidate 1 for 0.5.0.

Introduce new processing and a strickler table edition GUI.

### Added

- fix execution right for salome scripts in telemac environnement on installed version
- handle case of CLC code in CLC interpolation
- add style for boundary conditions
- processing and GUI for mesh export and translation/filtering
- GUI for strickler table edition and export
- processing and GUI for mesh refine
- use a color ramp to create QGIS style for interpolated CLC mesh

### Changed

- remove output option for boundaries creation GUI, using input as output

## 0.4.0 - 2022-04-05

First version with experimental windows support.

### Added

- Online documentation
- Windows support (experimental feature)
- Enable processing run in GUI only if layers are not in edition mode
- Allow null value definition for default value for interpolation processing

### Changed

- Fix multiple layer use in create_mesh GUI (removed layer were still used)
- Fix snapping option configuration when editing boundaries layer

## 0.3.1 - 2022-01-31

Fix version of 0.3.0

### Added

- Fix crash on processing launch from GUI due to code refactor
- .shp file created on QGIS 3.16 were not editable

## 0.3.0 - 2022-01-19

Third version with GUI refactoring and new options for `create_mesh`

### Added

- No use of temporary layers, all created layer from GUI are now stored in `$HOME/.q4ts`
- Add group in layer tree for each layer type
- Created mesh layer name is defined from file name if no temporary output used
- Add option to use several layers (island / constraints lines/ refinement area) for `create_mesh` processing
- Add `--record` option for `interp_field` processing
- Automatic snapping options definition when creating boundaries layer
- Remove file path size limitation for `create_mesh` GUI
- Add option to stop processings
- Add tests for `create_mesh` processing
- Review of `create_boundary` GUI
- Finalization of sonarcloud analysis
- Creation of static documentation : <https://otm.gitlab-pages.pam-retd.fr/salome4telemac/>

## 0.2.0 - 2022-01-11

Second version with interpolation scripts integration and GUI refactoring

### Added

- Integration of interpolation field script `field`
- Integration of interpolation Corine Land Cover script `clc`
- Integration of interpolation from cloud point `interpolate`
- Integration of Island / Contraints line /  Redefinition area in `create_mesh`
- Add specific GUI to define boundary condition type
- Add pytest script for QGIS processing
- Initialization of CI/CD jobs
- Initialization of documentation
- Initialization of sonarcloud analysis
- Add several option in GUI (zoom on layer, refresh layer style)
- Add option to add docked widget for each processing
- Add option to check boundaries layer with contour layer contents

## 0.1.1 - 2021-12-17

Fix version

### Added

- Processing logs in GUI
- Check for processing result layer before load
- Define min/max/default values for processing parameters
- Display mesh with default style

## 0.1.0 - 2021-12-15

First version of Q4TS QGIS plugin.

### Added

- `create_mesh` processing and GUI
- `create_boundaries` processing and GUI
