---
myst:
  substitutions:
    img_salome_windows: |-
      :::{image} /images/salome_window.png
      :scale: 70 %
      :::
    img_salome_tools: |-
      :::{image} /images/salome_tools.png
      :scale: 100 %
      :::
    img_layer_selection: |-
      :::{image} /images/layer_selection.png
      :scale: 100 %
      :::
    img_multiple_layer_selection: |-
      :::{image} /images/multiple_layer_selection.png
      :scale: 100 %
      :::
    img_add_button: |-
      :::{image} /images/add_button.png
      :scale: 100 %
      :::
    img_delete_button: |-
      :::{image} /images/delete_button.png
      :scale: 100 %
      :::
    img_refresh_button: |-
      :::{image} /images/refresh_button.png
      :scale: 100 %
      :::
    img_zoom_button: |-
      :::{image} /images/zoom_button.png
      :scale: 100 %
      :::
    img_select_button: |-
      :::{image} /images/select_button.png
      :scale: 100 %
      :::
    img_run_active_button: |-
      :::{image} /images/run_active_button.png
      :scale: 100 %
      :::
    img_run_disabled_button: |-
      :::{image} /images/run_disabled_button.png
      :scale: 100 %
      :::
    img_stop_button: |-
      :::{image} /images/stop_button.png
      :scale: 100 %
      :::
    img_log_widget: |-
      :::{image} /images/log_widget.png
      :scale: 70 %
      :::
    img_create_mesh: |-
      :::{image} /images/create_mesh.png
      :scale: 100 %
      :::
    img_extend_mesh: |-
      :::{image} /images/extend_mesh.png
      :scale: 100 %
      :::
    img_create_boundaries: |-
      :::{image} /images/create_boundaries.png
      :scale: 80 %
      :::
    img_boundaries_python_macros: |-
      :::{image} /images/boundaries_python_macros.png
      :scale: 80 %
      :::
    img_boundaries_cb_feature: |-
      :::{image} /images/boundaries_cb_feature.png
      :scale: 80 %
      :::
    img_boundaries_custom_value: |-
      :::{image} /images/boundaries_custom_value.png
      :scale: 80 %
      :::
    img_projection_field: |-
      :::{image} /images/projection_field.png
      :scale: 100 %
      :::
    img_field_value_feature: |-
      :::{image} /images/field_value_feature.png
      :scale: 80 %
      :::
    img_interp_clc: |-
      :::{image} /images/interp_clc.png
      :scale: 100 %
      :::
    img_interp_point_cloud: |-
      :::{image} /images/interp_point_cloud.png
      :scale: 100 %
      :::
    img_interp_point_cloud_concave: |-
      :::{image} /images/interpolate_point_cloud_concave.jpg
      :scale: 100 %
      :::
    img_interp_point_cloud_convex: |-
      :::{image} /images/interpolate_point_cloud_convex.jpg
      :scale: 100 %
      :::  
    img_refine: |-
      :::{image} /images/refine.png
      :scale: 100 %
      :::  
    img_alter: |-
      :::{image} /images/alter.png
      :scale: 100 %
      :::  
    img_friction_table_edition: |-
      :::{image} /images/strickler_table_edition.png
      :scale: 100 %
      :::  
    img_snapping_icon: |-
      :::{image} /images/snapping_icon.png
      :scale: 100 %
      :::  
    img_custom_clc_style: |-
      :::{image} /images/custom_clc_style.png
      :scale: 100 %
      :::  
    img_save_button: |-
      :::{image} /images/save_button.png
      :scale: 100 %
      :::  
    img_save_as_button: |-
      :::{image} /images/save_as_button.png
      :scale: 100 %
      :::
    img_remove_layer_confirmation: |-
      :::{image} /images/remove_layer_confirmation.png
      :scale: 100 %
      :::
---

# Graphical User Interface

Q4TS processing can be used with a Graphical User Interface that helps user to define needed input.

There are 2 ways to display these interfaces :

- Click on Q4TS button in QGIS toolbar

A windows will be display over QGIS with all tools :

{{ img_salome_windows }}

- Select one of the tools

{{ img_salome_tools }}

The tool will be displayed as a dockable widget.

:::{warning}
Tools displayed in Q4TS and tools displayed as dockable widget are not synchronized.
:::

## Q4TS Geopackage

When creating a layer with Q4TS GUI, user will be asked to define a path to a geopackage to store all created layers. A QGIS project will also be stored in the layer.

If a project from a Q4TS Geopackage is loaded in QGIS, layer created with Q4TS will be added to the Geopackage.

When removing a layer in QGIS, if a Q4TS Geopackage is loaded and the layer is contained in the Geopackage, user can force the removal of the layer from the Geopackage:

{{ img_remove_layer_confirmation }}

## Common components

### Layer selection and creation

{{ img_layer_selection }}

This component allows user to :

- select a layer from a combobox
- {{ img_add_button }} create a new layer
- {{ img_refresh_button }} refresh selected layer style
- {{ img_zoom_button }} zoom to selected layer
- {{ img_select_button }} select layer from file

:::{warning}
Created layer will use current QGIS project CRS. Make sure to use a metric system or to adapt the default values for sizes.
:::

### Multiple layer selection and creation

{{ img_multiple_layer_selection }}

This component provides several component for layer selection and creation

A new layer can be added to list with left add button {{ img_add_button }}.
The added layer can be removed with left delete button {{ img_delete_button }}

:::{note}
Layer are not deleted from QGIS project with left delete button {{ img_delete_button }}
:::

### Log and processing status

{{ img_log_widget }}

At the bottom of all tool there is a log widget that display logs from SALOME / Telemac call.

Above this widget, user can define result file and run or stop the processing.

Run button can have 3 states :

- Run is enabled {{ img_run_active_button }}
- Run is disabled {{ img_run_disabled_button }}
- Processing is running and can be stopped {{ img_stop_button }}

:::{note}
A tooltip can be displayed to indicate why run is disable.
For all tools, layers used must be saved before processing run.
:::

## Create mesh

{{ img_create_mesh }}

```{include} ../../q4ts/resources/help/create_mesh.md
```

## Extend mesh

{{ img_extend_mesh }}

```{include} ../../q4ts/resources/help/extend_mesh.md
```

### Contour creation

For this processing, exact mesh contour must be used for correct extend. This contour can be created from input mesh and other mesh with {{ img_add_button }} from contour selection component.

### Contour snapping options

To maintains vertex on extent contour, each vertex of the input mesh contour must be defined in extend contour or refinement area.

To help user configure QGIS to have the correct snapping options for layer definition, {{ img_snapping_icon }} button can be used to force define snapping option for selected layer.

:::{warning}
For correct snapping be careful that the contour mesh layer is displayed.

Snapping option defined with {{ img_snapping_icon }} button will force display of the layer.
:::

## Create boundary

{{ img_create_boundaries }}

```{include} ../../q4ts/resources/help/create_boundary.md
```

### Contour creation

For this processing, exact mesh contour must be used. This contour can be created from input mesh with {{ img_add_button }} from contour selection component.

### Contour snapping options

To avoid holes in the resulting boundaries file, each vertex of the mesh contour must match a vertex in the input boundaries layer.

To help user configure QGIS to have the correct snapping options for boundaries layer definition, the options are defined each time a contour layer is selected.

If the snapping option was updated by user, {{ img_snapping_icon }} button can be used to force correct snapping option for selected layer.

:::{warning}
For correct snapping be careful that the contour mesh layer is displayed before creating the boundaries.

Snapping option defined with {{ img_snapping_icon }} button will force display of the layer.
:::

### Boundaries code definition

A custom widget is used for boundaries code definition.

{{ img_boundaries_cb_feature }}

You can define a custom value by checking Custom checkbox.

{{ img_boundaries_custom_value }}

:::{note}
At first use, user must enable python macro for correct boundaries code use

{{ img_boundaries_python_macros }}

Select `Don't Ask Anymore` to always remove this warning.
:::

## Projection field

{{ img_projection_field }}

```{include} ../../q4ts/resources/help/projection_field.md
```

:::{note}
With GUI, depending on the variable name, a custom QGIS style is applied to the created mesh.

:::

### Area field value definition

When using defaut layer creation, a widget is displayed for each feature added.

It's used to defined area value:

{{ img_field_value_feature }}

## Projection Corine Land Cover

{{ img_interp_clc }}

```{include} ../../q4ts/resources/help/projection_corine_land_cover.md
```

## Interpolation point cloud

{{ img_interp_point_cloud }}

```{include} ../../q4ts/resources/help/interp_cloud_point.md
```

:::{note}
With GUI, depending on the variable name, a custom QGIS style is applied to the created mesh.

:::

If a mesh is bigger than the points cloud (this case shouldn't occur in theory), the interpolation depends on the form of the points cloud.
In the convex part of the points cloud, where the mesh is outside the points cloud, the mesh won't be interpolated.

{{ img_interp_point_cloud_convex }}

In the concave part, where the mesh is outside the points cloud, it will.

{{ img_interp_point_cloud_concave }}

## Refine

{{ img_refine }}

```{include} ../../q4ts/resources/help/refine.md
```

## Mesh transformation

{{ img_alter }}

```{include} ../../q4ts/resources/help/alter.md
```

QGIS use MDAL internally to manage selafin files (see <https://www.mdal.xyz/drivers/selafin.html>).

When parsing variable name and unit, MDAL need to do some conversion of initial variable name and unit.

Some options are available in Q4TS to reconstruct the initial variable name to be able to use telemac-mascaret python scripts :

- Upper variable name : all variable name are converted to lowercase in MDAL, this option allow user to convert them to uppercase
- Vector dataset split :

In MDAL if a variable can be represented as a vector u,v (velocity for example), it's stored as a single dataset with both values.

In selafin files this will be represented by 2 variables. There are several convention for this variable naming (along / suivant / uv), this option allow user to define how the selafin variable name will be created for QGIS vector dataset.

:::{note}
When Time from / Time end / Time step options are used, the resulting times are checked on the time selection table.
:::

## Friction Table Edition

{{ img_friction_table_edition }}

A friction table file can be selected with {{ img_select_button }} button. The default file of q4ts plugin will be selected.

You can choose a different file but please note that the selected file is not synchronized with the file with in Corine Land Cover projection tool.

You must save the file by using {{ img_save_button }} button or {{ img_save_as_button }} button if you want to define a new file path.

There is also an option to select a color ramp and define a custom QGIS style to a selected mesh layer from value in friction table.

{{ img_custom_clc_style }}
