---
myst:
  substitutions:
    img_options: |-
      :::{image} /images/options.png
      :::
    img_processing_options: |-
      :::{image} /images/processing_options.png
      :::
    img_conf_ubuntu: |-
      :::{image} /images/conf_ubuntu.png
      :scale: 100 %
      :::
    img_conf_windows: |-
      :::{image} /images/conf_windows.png
      :scale: 100 %
      :::
---

# User guide

## Preferences
In order to use Q4TS QGIS plugin, some variable must be defined for Salome / Telemac environnement.

These variables are defined in `Settings > Options` dialog in `Processing` panel.

{{ img_options }}

You must define `Providers > Q4TS` variables

{{ img_processing_options }}

- `Salome executable` : full path to Salome executable
- `Telemac environnement script` : full path to Telemac environnement script needed for Telemac call
- `Python Salome (only for windows)` : full path to python available in Salome installation dir

:::{warning}
It's recommended to be on the last version of main branch of telemac-mascaret to be able to run all available processing.

There is currently no check of the version used before running telemac-mascaret scripts. 

When reporting issue, please indicate the version of telemac-mascaret used or the commit of the main branch.

:::

### Linux example
{{ img_conf_ubuntu }}

### Windows example
{{ img_conf_windows }}

:::{warning}
Windows support is still experimental and some crashes can occurs.
:::
