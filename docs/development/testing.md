# Unit tests

## Requirements

- 3.26 < QGIS < 3.99

```bash
python -m pip install -U -r requirements/testing-qgis.txt
```

## Run unit tests

```bash
# run all tests with PyTest and Coverage report
python -m pytest

# run a specific test module using standard unittest
python -m unittest tests.test_plg_metadata

# run a specific test function using standard unittest
python -m unittest tests.test_plg_metadata.TestPluginMetadata.test_version_semver
```
