# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: Q4TS
maxdepth: 2
---
user_manual/user_manual
user_manual/processings
user_manual/gui
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```
