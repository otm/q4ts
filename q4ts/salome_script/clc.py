""" functions to set clc value on mesh """
from os import path, remove, environ
import shutil

import geopandas as gpd
import shapely as shp

from data_manip.extraction.telemac_file import TelemacFile

from script_clc18 import read_corresp_table, set_strickler, \
                         get_clc_18_polygons


def clc_parser(subparser):
    """
    Add arguments for a clc

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser('clc',
                                  help='Define Strickler values using Corine \
                                  Land Cover data and a corresponding table')
    parser.add_argument(
        "input_mesh",
        help="Name of the input mesh file")

    parser.add_argument(
        "output_mesh",
        help="Name of the output mesh file")

    parser.add_argument(
        "clc_tab_name",
        help="Name of the clc <-> Strickler corespondance table file")


    parser.add_argument(
        "--clc_data_file",
        default=False,
        help="CLC data file if not avaiable as CLC_DATA_FILE environnement variable")

    parser.add_argument(
        "--write-clc-code",
        action="store_true",
        default=False,
        help="If true an additiona field called CODE CLC will be written \
        containing the CLC code (-1 for custom defined area)")

    poly_clc = parser.add_argument_group("Local Strickler",
                                         "Localised strickler")
    poly_clc.add_argument(
        "--zone",
        default=None,
        help="Geopackage file containing zones and values with strickler \
        values")

    poly_clc.add_argument(
        "--zone-attribute",
        default=None,
        help="Name of the field in the zone file containing the strickler \
        value")

    poly_clc.add_argument(
        "--zone-offset",
        action="store_true",
        default=False,
        help="Option shoulbe added if the zone file is in the same \
              projection as the mesh file.")

    offset_clc = parser.add_argument_group("Mesh localization",
                                           "Mesh localization")
    offset_clc.add_argument(
        "--offset",
        default=None,
        nargs=2,
        type=float,
        help="Offset to apply to mesh coordinates")

    return subparser


def clc(input_mesh, output_mesh, clc_tab_name, clc_data_file=None, zone_clc=None,
        zone_clc_attribute=None, offset=None, write_clc_code=False,
        zone_offset=False):
    """
    Cerate a field containt strickler value based on CLC and custom values

    @param input_mesh (str) Name of the mesh to derefine
    @param output_mesh (str) Name of the derefined mesh
    @param clc_tab_name (str) Corresponding table for CLC
    @param clc_data_file (str) CLC data file if not avaiable as CLC_DATA_FILE environnement variable
    @param points_file (str) File containing x y z to clc
    @param zone_clc (str) zone in which user value is defined instead of CLC
    @param zone_clc_attribute (str) Attribute un zone_clc that containe the
    strickler value
    @param offset ([float, float]) X, Y offset to apply to the mesh
    @param write_clc_code (bool) If true writing also a field containing the CLC code
    @param zone_offset (bool) If true writing also a field containing the CLC code
    """

    if input_mesh != output_mesh:
        if path.exists(output_mesh):
            remove(output_mesh)

        print(" "*6 + '{} -> {}'.format(input_mesh, output_mesh))
        shutil.copy2(input_mesh, output_mesh)

    # Getting bounding box from model extent
    if offset is None:
        offset = [0.0, 0.0]
    res = TelemacFile(output_mesh)
    coord_extent = (min(res.meshx+offset[0]), min(res.meshy+offset[1]),
                    max(res.meshx+offset[0]), max(res.meshy+offset[1]))
    res.close()

    bound_box = ((coord_extent[0], coord_extent[1]),
                 (coord_extent[2], coord_extent[1]),
                 (coord_extent[2], coord_extent[3]),
                 (coord_extent[0], coord_extent[3]))

    print(" " * 6 + " ~> Reading file")
    gpkg_mesh = clc_data_file
    if gpkg_mesh:
        if not path.exists(gpkg_mesh):
            raise Exception("The file set in clc_data_file option does not exists: {}" \
                            .format(gpkg_mesh))
    else:
        gpkg_mesh = environ["CLC_DATA_FILE"]
        if not path.exists(gpkg_mesh):
            raise Exception("The file set in CLC_DATA_FILE does not exists: {}" \
                            .format(gpkg_mesh))

    df_clc_18 = gpd.read_file(gpkg_mesh)

    print(" "*6+" ~> Extracting polygon")
    square = shp.geometry.Polygon(bound_box)

    clc_polygons = get_clc_18_polygons(df_clc_18, square)

    print(" "*6+" ~> Building correspondace table")
    corresp = read_corresp_table(clc_tab_name)

    print(" "*6 + " ~> Setting strickler")
    code_clc_name = "CODE CLC" if write_clc_code else None

    set_strickler(output_mesh, clc_polygons, corresp,
                  shp_exclude_file=zone_clc,
                  shp_exclude_local=zone_offset,
                  attribute_table_field=zone_clc_attribute,
                  code_clc_varname=code_clc_name,
                  shift_x=offset[0], shift_y=offset[1])
