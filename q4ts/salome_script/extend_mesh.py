import os

from create_mesh import create_bc_file, create_geom_face, mesh_face


def extend_mesh_parser(subparser):
    """
    Add arguments for mesh extension

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser(
        "extend_mesh",
        help="Extend a mesh from a shapefile and constraint lines",
    )

    parser.add_argument("input_mesh", help="Name of the input mesh file")

    parser.add_argument(
        "input_contour_shp",
        help="File name of the input mesh contour (ESRI shapefile)",
    )

    parser.add_argument(
        "extended_contour_shp",
        help="File name of the extended mesh contour (ESRI shapefile)",
    )

    parser.add_argument(
        "-dx",
        "--phy_size",
        dest="phy_size",
        type=float,
        help="Physical size of a mesh element",
    )

    parser.add_argument(
        "-min",
        "--min_size",
        dest="min_size",
        type=float,
        help="Minimum size of a mesh element",
    )

    parser.add_argument(
        "-max",
        "--max_size",
        dest="max_size",
        type=float,
        help="Maximum size of a mesh element (unused in NETGEN 1D-2D)",
    )

    parser.add_argument(
        "-gr",
        "--growth_rate",
        dest="growth_rate",
        default=1.3,
        type=float,
        help="define the growth rate allowed between cells. The meaning (and \
              default value), differ for Netgen1D2D and for MGCadSurf. For \
              NETGEN1D2D, it defines how much the linear dimensions of two \
              adjecent cells can differ (e.g. 0.3 which is default value \
              means 30%). For MGCadSurf, it is the maximum ratio between two \
              adjacent edges (default value is 1.3)",
    )

    parser.add_argument("output_mesh", help="Name of the output mesh file")

    parser.add_argument(
        "--other-mesh",
        dest="other_mesh",
        default="",
        help="Name of an optional second input mesh file to join to the first one",
    )

    parser.add_argument(
        "--other-contour-shp",
        dest="other_contour_shp",
        default="",
        help="File name of a second input mesh contour (ESRI shapefile)",
    )

    parser.add_argument(
        "--mesh_engine",
        dest="mesh_engine",
        default="netgen_1d_2d",
        choices=["netgen_1d_2d", "mg_cadsurf"],
        help="Option to choose the mesh generator engine between NETGEN1D2D \
              and MG-CADSURF (warning, MG-CADSURF requires a licence)",
    )

    parser.add_argument(
        "--constraints",
        default=[],
        help="Files containing constraint lines (ESRI shapefile format)",
        nargs="+",
    )

    parser.add_argument(
        "--isles",
        default=[],
        help="Files containing isles polygons (ESRI shapefile format)",
        nargs="+",
    )

    parser.add_argument(
        "--refine",
        default=[],
        help="Files containing refinement polygon "
        "(must contain attribute "
        "(min_size, phy_size, max_size)) (ESRI shapefile format)",
        nargs="+",
    )

    return subparser


def extend_mesh(
    input_mesh,
    input_contour_shp,
    extended_contour_shp,
    phy_size,
    min_size,
    max_size,
    mesh_engine,
    output_mesh,
    other_mesh="",
    other_contour_shp="",
    constraints_files=[],
    growth_rate="default",
    refine_files=[],
    island_files=[],
):
    """
    Extend a given mesh from contours and constraint lines using GEOM and SMESH

    @param input_mesh (str) Name of the mesh to extend
    @param input_contour_shp (str) Name of the shapefile containing the polygon
                                   contour of the input mesh
    @param extended_contour_shp (str) Name of the shapefile containing the
                                      polygon contour from which the mesh is to
                                      be extended
    @param phy_size (float) Physical size of a mesh element
    @param min_size (float) Minimum size of a mesh element
    @param max_size (float) Maximum size of a mesh element
    @param mesh_engine (str) Name of the mesh engine to use
    @param output_mesh (str) Filename of the output mesh
    @param other_mesh (str) Name on optional secondary mesh that should be
                            merged with the first one. If both meshes overlap
                            the secondary one is cut before the junction.
    @param other_contour_shp (str) Name of a shapefile containing the polygon
                                   contour of the secondary mesh.
    @param constraints_files (list) list of shapefile name for constraint lines
    @param growth_rate (float) Define the growth rate allowed between cells.
                               The meaning (and default value), differ for
                               Netgen1D2D and for MGCadSurf. For NETGEN1D2D,
                               it defines how much the linear dimensions of
                               two adjecent cells can differ (e.g. 0.3 which
                               is default value means 30%). For MGCadSurf, it
                               is the maximum ratio between two adjacent
                               edges (default value is 1.3)
    @param refine_files (list) List of refine files
    @param island_files (list) List of shapefile(s) to create island()s
    """
    from salome.geom import geomBuilder
    from salome.smesh import smeshBuilder

    geompy = geomBuilder.New()
    smesh = smeshBuilder.New()

    input_contours_shp = [input_contour_shp]
    if other_contour_shp != "":
        input_contours_shp.append(other_contour_shp)

    model, refine_zones = create_geom_face(
        geompy,
        extended_contour_shp,
        constraints_files,
        refine_files,
        island_files,
        input_contours_shp,
    )

    use_netgen = mesh_engine == "netgen_1d_2d"
    mesh = mesh_face(
        smesh,
        model,
        phy_size,
        min_size,
        max_size,
        refine_zones,
        growth_rate,
        use_netgen=use_netgen,
    )

    # Load the input mesh and set the list of meshes to concatenate
    ([original_mesh], _) = smesh.CreateMeshesFromMED(input_mesh)
    meshes = [original_mesh, mesh]

    # Load the other mesh if any, and add it to the list of meshes
    if other_mesh != "":
        ([second_mesh], _) = smesh.CreateMeshesFromMED(other_mesh)
        meshes.append(second_mesh)

    # Merge the input meshes with the extension
    mesh = smesh.Concatenate(
        meshes,
        uniteIdenticalGroups=True,
        mergeNodesAndElements=True,
    )

    mesh.ExportMED(output_mesh)
    bnd_file = os.path.splitext(output_mesh)[0] + ".bnd"
    create_bc_file(bnd_file)
    print(f"Created {output_mesh}")
