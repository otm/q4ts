""" functions to create mesh boundary file """

PROP_BND_TYPE="bnd_type"

def create_boundary_parser(subparser):
    """
    Add arguments for a create_boundary

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser('create_boundary',
                                  help='Create boundary on a mesh from polygon')
    parser.add_argument(
        "input_mesh",
        help="Name of the input mesh file")

    parser.add_argument(
        "output_mesh",
        help="Name of the output mesh file")

    parser.add_argument(
        "contour",
        help="Name of shape file containing the contour of the mesh")

    parser.add_argument(
            "--boundaries",
            default=None,
            help="GeoPackage containing boundary zones and boundary value for each zone (huvt) the rest will be set to default-bnd (default None)")

    parser.add_argument(
            "--default-bnd",
            default=2222,
            help="Default boundary value (default 2222)")

    return subparser


def create_boundary(input_mesh, output_mesh, contour, default_bnd=2222, boundaries=None):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and hydro

    @param input_mesh (str) Name of the mesh for which to create boundary
    @param output_mesh (str) Name of the create_boundaryd mesh
    @param boundaries (str) Geopackage file containing where to create boundaries and there values
    """
    from itertools import chain
    from os import path, remove

    import fiona
    import numpy as np
    from scipy.spatial import cKDTree

    from data_manip.extraction.shapefile_reader import read_shape_data
    from data_manip.extraction.telemac_file import TelemacFile

    geo = TelemacFile(input_mesh)

    # Name of boundary file
    ext = ".bnd" if geo.fformat == "MED" else ".cli"
    bnd_file = path.splitext(output_mesh)[0] + ext

    # Remove output files if they already exist
    if path.exists(output_mesh):
        remove(output_mesh)
    if path.exists(bnd_file):
        remove(bnd_file)

    res = TelemacFile(output_mesh, bnd_file=bnd_file, access="w")
    # Identify boundary points from contour
    boundary_points = read_shape_data(contour)[0]
    # In case of polygon, the closing point can be counted twice
    for index, boundary in enumerate(boundary_points):
        if boundary[0] == boundary[-1]:
            boundary_points[index] = boundary_points[index][:-1]

    # Build a KDTree to find closest points
    vertices = np.column_stack((geo.meshx[:geo.npoin2], geo.meshy[:geo.npoin2]))
    kd_tree = cKDTree(vertices)

    list_bnd_points = list(chain.from_iterable(boundary_points))
    npoints = len(list_bnd_points)
    _, ikle_bnd = kd_tree.query(list_bnd_points)
    nbor = dict(zip(ikle_bnd, range(npoints)))

    h, u, v, t = [int(a) for a in str(default_bnd)]
    lihbor = [h]*len(ikle_bnd)
    liubor = [u]*len(ikle_bnd)
    livbor = [v]*len(ikle_bnd)
    litbor = [t]*len(ikle_bnd)

    # Get the list of points from shapefile and code from attribute table
    if boundaries is not None:
        with fiona.open(boundaries) as f:
            for polyline in f:
                try:
                    code_bnd = polyline["properties"][PROP_BND_TYPE]
                except KeyError:
                    raise Exception("Missing attribute {} in shape file"\
                                    .format(PROP_BND_TYPE))
                h, u, v, t = [int(a) for a in str(code_bnd)]
                list_bnd_points = polyline['geometry']['coordinates']
                # If the shape is closed the closing point is counted twice
                if list_bnd_points[0] == list_bnd_points[-1]:
                    list_bnd_points = list_bnd_points[:-1]

                # Updating boundary values for each point in the poly line
                _, point_ids = kd_tree.query(list_bnd_points)
                for id in point_ids:
                    lihbor[nbor[id]] = h
                    liubor[nbor[id]] = u
                    livbor[nbor[id]] = v
                    litbor[nbor[id]] = t

    #TODO: Not working doing only the add_bnd (boundary_file is neither defined nor opened)
    ##     Temporary work around copying mesh
    res.add_header(geo.title, date=geo.datetime)
    res.add_mesh(geo.meshx, geo.meshy, geo.ikle2)
    res.add_bnd(ikle_bnd, lihbor, liubor, livbor, litbor=litbor)
    res.write()
    res.close()
    geo.close()
