from pathlib import Path

import ss_utils.shapefile as shapefile

try:
    import fiona
    has_fiona = fiona.__version__ >= "1.9"
except ImportError:
    has_fiona = False


def read_using_fiona(features_file: str, get_names: bool):
    """
    Extract feature coordinates or names from a vector features file using
    Fiona.
    """

    extensions = [".shp", ".geojson", ".json", ".gpkg"]
    ext = Path(features_file).suffix
    if ext not in extensions:
        raise Exception("Unsupported vector dataset extension: {}".format(ext))

    try:
        layer = fiona.open(features_file, "r")
    except:
        raise Exception("Could not open {}".format(features_file))

    if get_names:
        names = []
        for feature in layer:
            if 'name' in feature.properties:
                names.append(feature['name'])
            else:
                names.append(None)
        return names

    res = []
    for feature in layer:
        if feature.geometry is None:
            continue
        coordinates = feature.geometry.coordinates
        if feature.geometry.type in ("Point", "LineString"):
            res.append(coordinates)
        elif feature.geometry.type == "Polygon":
            res.append(coordinates[0])
        elif feature.geometry.type in ("MultiPoint", "MultiLineString"):
            for i in range(len(coordinates)):
                res.append(coordinates[i])
        elif feature.geometry.type == "MultiPolygon":
            for i in range(len(coordinates)):
                res.append(coordinates[i][0])
        else:
            raise Exception("Unsupported feature geometry type: {}".format(feature.geometry.type))

    return res


def read_shape_data(features_file: str, get_names: bool=False):
    """
    Extract feature coordinates or names from a vector features file.

    @param features_file (str) Path to the vector dataset.
    @param get_names (bool) If True returns a list of name of each feature.

    @return (list) A list representing the polygon (list of 2-uple)
                   Or the list of the names of the polygon (if get_names=True)
    """
    if has_fiona:
        print("Using Fiona to read {}".format(features_file))
        return read_using_fiona(features_file, get_names)

    # Read the file using the internal shapefile reader
    print(
        "Using Q4TS shapefile reader to read {}".format(features_file)
    )
    ext = Path(features_file).suffix
    if ext != ".shp":
        raise Exception("Unsupported file extension: {}".format(ext))

    if get_names:
        root = Path(features_file).name
        shape = shapefile.Reader(features_file, dbf=root+'.dbf')
    else:
        shape = shapefile.Reader(features_file)

    if get_names:
        names = []
        for record in shape.iterRecords():
            if record != [None]:
                names.append(record[1])
        return names

    res = []
    # first feature of the shapefile
    for feature in shape.shapeRecords():
        first = feature.shape.__geo_interface__
        if first['type'] not in [("LineString"), ("Polygon"),
                                ("Point")]:
            raise IOError(
                "No linestrings, polygons or points in the shapefile\n"
                "type: {} not handled".format(first['type']))

        if first['type'] == ("LineString"):
            res.append(list(first['coordinates']))
        elif first['type'] == ("Polygon"):
            res.append(list(first['coordinates'][0][:-1]))
        elif first['type'] == ("Point"):
            res.append(list(first['coordinates']))
    return res
