""" functions to interpolate points+valaue on mesh """
from data_manip.extraction.telemac_file import TelemacFile
import numpy as np
import shutil
from ss_utils.read_shape_data import read_shape_data
from os import path

def interpolate_parser(subparser):
    """
    Add arguments for a interpolate

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser('interpolate',
                                  help='interpolate a xyz on the whole mesh or within a polygon')
    parser.add_argument(
            "input_mesh",
            type=str,
            help="Name of the input mesh file")

    parser.add_argument(
            "output_mesh",
            type=str,
            help="Name of the output mesh file")

    parser.add_argument(
            "--varname",
            default='BOTTOM',
            type=str,
            help="Name of the variable in the file")

    parser.add_argument(
            "--varunit",
            default='M',
            type=str,
            help="Unit of the variable in the file")

    #TODO: More explicit name for laz
    parser.add_argument(
            "points_file",
            type=str,
            help="Name of the xyz like file or laz file that contains the points and value to \
                    interpolate")

    parser.add_argument(
            "--delimiter",
            default=';',
            type=str,
            help="Delimiter of columns in the points file")

    parser.add_argument(
            "--kind",
            default="nearest",
            type=str,
            help="Type of interpolation (nearest, linear, cubic)")

    parser.add_argument(
            "--zone",
            default=None,
            type=str,
            help="If given shapefile containing zone in which to interpolate")

    #parser.add_argument(
    #        "--zone",
    #        default=None,
    #        help="GeoPackage containing zone in which to interpolate")

    parser.add_argument(
            "--record",
            type=int,
            default=-1,
            help="Number of the record (time step) to change in the mesh \
                  (-1 is the last record)")

    parser.add_argument(
            "--shift-x",
            type=float,
            default=0.0,
            help="Translation along with x-axis to consider to match \
                  the geo-coordinates")

    parser.add_argument(
            "--shift-y",
            type=float,
            default=0.0,
            help="Translation along with y-axis to consider to match \
                  the geo-coordinates")

    parser.add_argument(
            "--outside-zone",
            action='store_true',
            help="If a zone polygon file is given, the interpolation is done by default inside the polygone \
                  zone. If this option is activated, the interpolation is done outside the polygone zone")

    parser.add_argument(
            "--default-value",
            default=None,
            type=float,
            help="This value will be applied to points not within/without the polygon if a \
                  polygon is given")

    parser.add_argument(
            "--mmap",
            action='store_true',
            help="If activated, it converts the points file into a numpy object and create a \
                  memory map to avoid a full loading of the file in the memory")

    parser.add_argument(
            "--chunk-interp",
            action='store_true',
            help="This option allow to interpolate by chunk which is usefull for large datasets \
                  which lead to memory problems")

    parser.add_argument(
            "--chunk-x-partitions",
            default=None,
            type=int,
            help="Number of partitions in x-direction. If None, a default is calculated \
                according to the number of blockpts")

    parser.add_argument(
            "--chunk-y-partitions",
            default=None,
            type=int,
            help="Number of partitions in y-direction. If None, a default is calculated \
                according to the number of blockpts")

    parser.add_argument(
            "--chunk-overlap",
            default=0.25,
            type=float,
            help="Proportion of block length to overlap on other blocks. \
                For example, if pe=0.25, the block will be extended 25% on both the \
                left and right sides of px to overlap on successive blocks.")

    parser.add_argument(
            "--blockpts",
            default=300*300,
            type=int,
            help="Approximate number of interpolation points within each partition block. \
                  Defaults to 300*300. blockpts is used to automatically size either \
                  chunk x partitions or chunk y partitions if these are set to None")

    return subparser

def interpolate(input_mesh,
                output_mesh,
                points_file,
                delimiter=';',
                varname='BOTTOM',
                varunit='M',
                kind="nearest",
                zone=None,
                record=-1,
                shift_x=0.0,
                shift_y=0.0,
                outside_zone=False,
                default_value=None,
                mmap=False,
                chunk_interp=False,
                chunk_x_partitions=None,
                chunk_y_partitions=None,
                chunk_overlap=0.25,
                blockpts=300*300):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and hydro

    @param input_mesh (str) Name of the mesh to derefine
    @param output_mesh (str) Name of the derefined mesh
    @param delimiter (str) Delimiter of columns in the points file
    @param varname (str) Name of the variable to write in the result file (not yet implemented)
    @param varunit (str) Unit of the variable to write in the result file (not yet implemented)
    @param points_file (str) File containing x y z to interpolate
    @param kind (str) Type of interpolation (nearest, linear, cubic)
    @param zone (str) If given only interpolate within zone defile by polygon file
    @param record (int) Number of the record (time step) to change in the mesh
    @param shift_x (real) Translation along with x-axis to consider to match
            the geo-coordinates
    @param shift_y (real) Translation along with y-axis to consider to match
            the geo-coordinates
    @param inside_zone (bool) If a zone polygon file is given, option to define if the
            interpolation is done inside the polygone zone (True) or outside the polygone
            zone (False)"
    @param default_value (float) If using zone, value set to points outside/inside the zone
    @param mmap (bool) If set to True, it converts the points file into a numpy object and create a
            memory map to avoid a full loading of the file in the memory
    @param chunk_interp (bool) This option allow to interpolate by chunk which is usefull for
            large datasets which lead to memory problems
    @param chunk_x_partitions (int) Number of partitions in x-direction. If None, a default is
            calculated according to the number of blockpts
    @param chunk_x_partitions (int) Number of partitions in x-direction. If None, a default is
            calculated according to the number of blockpts
    @param chunk_overlap (float) Proportion of block length to overlap on other blocks.
            For example, if pe=0.25, the block will be extended 25% on both the
            left and right sides of px to overlap on successive blocks."
    @param blockpts (int) Approximate number of interpolation points within each partition block.
            Defaults to 300*300. blockpts is used to automatically size either
            chunk x partitions or chunk y partitions if these are set to None.
    """

    print('{} -> {}'.format(input_mesh, output_mesh))
    shutil.copy2(input_mesh, output_mesh)

    if mmap == True:
        xyz_name = path.split(path.splitext(points_file)[-2])[-1]
        xyz = np.loadtxt(points_file, delimiter=delimiter)
        xyz[:, 0] = xyz[:, 0] - shift_x
        xyz[:, 1] = xyz[:, 1] - shift_y
        np.save(xyz_name + "_xyz.npy", xyz)
        del(xyz)
        xyz = np.load(xyz_name + "_xyz.npy", mmap_mode='r')
    else:
        xyz = np.loadtxt(points_file, delimiter=delimiter)
        xyz[:, 0] = xyz[:, 0] - shift_x
        xyz[:, 1] = xyz[:, 1] - shift_y

    if zone != None:
        poly = read_shape_data(zone)
        poly = np.array(poly)[0]
        poly[:, 0] = poly[:, 0] - shift_x
        poly[:, 1] = poly[:, 1] - shift_y
    else:
        poly = None

    res = TelemacFile(output_mesh, access='rw')
    res.read()

    var_exists = False
    if varname in res.varnames:
        var_exists = True
    else:
        res.add_variable(varname, varunit)

    # If default value is none and poly defined, use available value (if it exists)
    if default_value is None and poly is not None and var_exists:
        default_value = res.get_data_value(varname, 0)

    # If no default value defined, use 0.0
    if default_value is None:
        default_value = 0.0

    if outside_zone == True:
        loc = 'outside'
    else:
        loc = 'inside'

    if chunk_interp == False:
        chunk_x_partitions = None
        chunk_y_partitions = None

    else:
        ptnum = len(xyz[:, 0])
        blocknum = ptnum / blockpts + 1
        if chunk_x_partitions == None:
            if chunk_y_partitions == None:
                chunk_x_partitions = int(np.sqrt(blocknum))
                chunk_y_partitions = int(blocknum / chunk_x_partitions)
            else:
                chunk_y_partitions = int(blocknum / chunk_y_partitions)

    interp_data = res.interpolate_xyz_on_mesh(xyz,
                                              kind=kind,
                                              poly=poly,
                                              loc=loc,
                                              init=default_value,
                                              px=chunk_x_partitions,
                                              py=chunk_y_partitions,
                                              pe=chunk_overlap,
                                              rescale=True)

    res.add_data_value(varname, record, interp_data)
    res.write()
    res.close()
