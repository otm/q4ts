TELEMAC_FILE = file.[*|slf|res|srf|sel|med]
options for selafin double precision ?

when input and output are the same replace file ? (pass by temporary file and move at the end ?)


## Lot 2: mesh creation

geopackage contains:
constraint lines
shape on domain + isles

TODO: List mesh parameters

salome-script.py create_mesh --shp geopackage -o TELEMAC_FILE


## Lot 4: refine/derefine

## Addtional parameter (mesher/homard parameters):
element size Min/Max

Those information can be set in the geopackage ?

salome-script.py refine mesh.[slf|med] --shp geopackage -o refine_mesh.[slf|med] --method=[1:remesh|2:homard]

TODO: Ask gerald if homard can derefine
salome-script.py derefine mesh.[slf|med] --shp geopackage -o derefine_mesh.[slf|med]

## Lot 5: Setting boundary automatically

geopackage contains:

shape + bnd value huvt ({[0-9]}4) boundary will be applied in order of shape and the remaining points/segments will be set to 2222.

salome-script.py create_bnd --shp geopackage TELEMAC_FILE -o TELEMAC_FILE+BND

Name of the bnd file will be name of TELEMAC_FILE with extension .bnd for med and .cli for selafin


## Lot 8: Interpolation of xyz/laz on mesh

Interpolating list of points on whole mesh or on specific contour (option shp)

salome-script.py interp TELEMAC_FILE --points points_file.[xyz|laz] -o TELEMAC_FILE --varname VARNAME (--shp geopackage)

## Lot 9: Friction coefficient

TODO: Specify variable name ? or just limit to french vs english ?

geopackage contains:
list of shape + friction value
Order is important friction wil be applied in the order of shapes
TODO: Check that order is conserved in geopackage.


salome-script.py clc TELEMAC_FILE --year year --clc-name -o TELEMAC_FILE --shp geopackage

## Lot 10: Modification on field

Same as in Lot 9.

salome-script.py field TELEMAC_FILE -o TELEMAC_FILE --varname VARNAME --shp geopackage

## Lot 11: Modification of file

Mapping on run_telfile.py alter
