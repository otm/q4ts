"""
Extracting stricker using geopandas for clc 2018
"""
import re

import geopandas as gpd
import shapely as shp
import numpy as np
from data_manip.extraction.telemac_file import TelemacFile

def get_clc_18_polygons(geo_dataframe,
                        zone_polygon,
                        plot=False,
                        fig_name=None,
                        plot_bnd=None):
    """
    Extract polygons informations

    @param geo_dataframe (geopandas package) TODO
    @param zone_polygon TODO
    @param plot (bool) If true plotting the polygons
    @param fig_name (str) If given save figure instead of showing it
    @param plot_bnd (list) If given change xmin, xmax, ymin, ymax of the plot
                               (format: [xmin, ymin, xmax, ymax])
    """

    test_bool = []
    for i in range(len(geo_dataframe)):
        test_bool.append(zone_polygon.intersects(geo_dataframe['geometry'][i]))

    clc_polygons = geo_dataframe[test_bool]

    if plot:
        import matplotlib.pyplot as plt

        try:
            clc_polygons.plot(column='code_18')
        except:
            clc_polygons.plot(column='CODE_18')

        if plot_bnd is not None:
            plt.xlim(plot_bnd[0], plot_bnd[2])
            plt.ylim(plot_bnd[1], plot_bnd[3])

        if fig_name is not None:
            plt.savefig(fig_name)
        else:
            plt.show()

    return clc_polygons


def get_code_18_name(data_frame):
    """
    Identify the name matchin code_18 (ignoring case in the geo data frame)
    Some time it is not code_18 but CODE_18

    @param data_frame () The data frame to search in

    @returns (str) Name of the column in data_frame
    """
    gdf_columns = data_frame.keys()

    for name in gdf_columns:
        if name.lower() == 'code_18':
            return name

    raise Exception(\
        "Could not find code_18 or CODE_18 in your clc file:\n"
        "Available names {}".format(gdf_columns))


def set_strickler(mesh_file,
                  clc_polygons,
                  corresp,
                  varname='BOTTOM FRICTION',
                  varunit='M1/3/S',
                  code_clc_varname="CODE CLC",
                  shift_x=0.0,
                  shift_y=0.0,
                  shp_exclude_file=None,
                  attribute_table_field=None,
                  shp_exclude_local=False):
    """
    Setting strickler in file

    @param mesh_file (np.array) Name of the mesh file
    @param clc_polygons (geodataframe) geodataframe (geopandas object)
                                       containing CLC polygon (return of
                                       clc_polygons)
    @param corresp (dict) dictionnary containing correspondance between CLC
                          code and friction coefficient
    @param varname (str) Name of the variable in the mesh_file that will
                         contain the clc data
    @param shift_x (float) x shift to apply to mesh_x
    @param shift_y (float) y shift to apply to mesh_y
    @param shp_exclude_file (str) path+name of a ESRI shp file containing
                                  polygon where the friction coefficients are
                                  not set
    @param attribute_table_field (str) Name of a field of the attribute table
                                       of shp_exclude_file ; the polygons of
                                       this shape file are exclude polygons if
                                       they contain a value in this field.
    @shp_exclude_local (bool) this option has to be True if the shp_exclude_file
                              is in the same geographic projection than the mesh
                              file (shift_x and shift_y are not applied)

    """


    multipoints = []

    res = TelemacFile(mesh_file, access='rw')
    res.read()

    friction = np.zeros(res.npoin2)
    code_clc = np.zeros(res.npoin2)


    if varname not in res.varnames:
        res.add_variable(varname, varunit)
        res.add_data_value(varname, -1, friction)

    if code_clc_varname not in res.varnames and \
       code_clc_varname is not None:
        res.add_variable(code_clc_varname, ' ')
        res.add_data_value(code_clc_varname, -1, code_clc)


    res.write()

    for x, y in zip(res.meshx, res.meshy):
        multipoints.append(shp.geometry.Point((x + shift_x, y + shift_y)))

    points_serie = gpd.GeoSeries(multipoints)
    gdf_points_serie = gpd.GeoDataFrame(points_serie)
    gdf_points_serie = gdf_points_serie.rename(
        columns={0:'geometry'}).set_geometry('geometry')
    gdf_points_serie_joined = gpd.sjoin(gdf_points_serie,
                                        clc_polygons,
                                        how="left")

    if shp_exclude_file is not None:
        print("shape exclude")
        gdf_points_serie_shp = gdf_points_serie

        if shp_exclude_local:
            multipoints_shp = []
            for x, y in zip(res.meshx, res.meshy):
                multipoints_shp.append(shp.geometry.Point((x, y)))

            points_serie = gpd.GeoSeries(multipoints_shp)
            gdf_points_serie_shp = gpd.GeoDataFrame(points_serie)
            gdf_points_serie_shp = gdf_points_serie_shp.rename(
                columns={0:'geometry'}).set_geometry('geometry')

        gpdf_exclude_shp_file = gpd.read_file(shp_exclude_file)
        gdf_points_exclude_serie_joined = gpd.sjoin(gdf_points_serie_shp,
                                                    gpdf_exclude_shp_file,
                                                    how="left")
        friction = res.get_data_value(varname, -1)

        column_name = get_code_18_name(gdf_points_serie_joined)
        print("column_name", column_name)

        for i in range(res.npoin2):
            if np.isnan(gdf_points_exclude_serie_joined[\
                attribute_table_field].iloc[i]):
                friction[i] = corresp[int(gdf_points_serie_joined[
                    column_name].iloc[i])]['value']

            else:
                friction[i] = gdf_points_exclude_serie_joined[
                    attribute_table_field].iloc[i]

            code_clc[i] = int(gdf_points_serie_joined[column_name].iloc[i])

    else:
        column_name = get_code_18_name(gdf_points_serie_joined)

        try:
            for i in range(res.npoin2):
                friction[i] = corresp[int(gdf_points_serie_joined[
                    column_name].iloc[i])]['value']
                code_clc[i] = int(gdf_points_serie_joined[column_name].iloc[i])
        except ValueError:
            raise Exception(\
                "Your clc shape should cover the whole mesh\n"
                "Point {} (X={}, Y={}) is not covered"
                .format(i, res.meshx[i], res.meshy[i]))

    res.add_data_value(varname, -1, friction)

    if code_clc_varname in res.varnames:
        res.add_data_value(code_clc_varname, -1, code_clc)

    res.write()
    res.close()

    return friction

def read_corresp_table(table_name):
    """
    Read the corresponding file

    @param table (str) table used

    @returns (dict) Corecponding table (code->(name, ref, value))
    """
    corresp = {}
    with open(table_name, 'r') as f:
        f.readline()
        for line in f.readlines():
            match = re.match(
                r'"(?P<name>[^"]+)"\s+(?P<value>[^ ]+)\s+(?P<ref>[^ ]+)'\
                r'\s+(?P<code>[^ ]+)', line)
            corresp[int(match.group('code'))] = {\
                'name':match.group('name'),
                'ref':match.group('ref'),
                'value':match.group('value')}

    return corresp
