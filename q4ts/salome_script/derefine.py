""" functions to derefine mesh from salome """

def derefine_parser(subparser):
    """
    Add arguments for a derefinement

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser('derefine',
                                  help='derefine a whole mesh or within a polygon')
    parser.add_argument(
        "input_mesh",
        help="Name of the input mesh file")

    parser.add_argument(
        "output_mesh",
        help="Name of the output mesh file")

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
            "--domain",
            default=None,
            help="GeoPackage containing zone to derefine and parameter")

    group.add_argument(
            "--mesh-size",
            type=int,
            nargs=2,
            help="Min and Max of elements in mesh")

    return subparser


def derefine(input_mesh, output_mesh, domain=None, mesh_size=None):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and hydro

    @param input_mesh (str) Name of the mesh to derefine
    @param output_mesh (str) Name of the derefined mesh
    @param domain (str) Geopackage file containing where to derefine
    @param mesh_size (float,float) Min and max size of mesh parameters for refinement (used only if doamine is None)
    """
    if geopackage is not None:
         _ = extract_from_geopackage(geopackage)
     ## TODO: Create derefine script (Using MG-Adapt see examples in SMESH)
     ## TODO: Get MG-Adapt parameters
