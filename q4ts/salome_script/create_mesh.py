""" Functions to create a TELEMAC mesh using SALOME """
from collections import OrderedDict, defaultdict
from os import path

import numpy as np
from ss_utils.read_shape_data import read_shape_data

DATA = {'intersect':{}, 'poly':{}, 'face':{}, 'box':{}, 'group':{},
        'edge':{}, 'vertices':{}, 'fixed_edges': []}

PROP_MIN_SIZE = "min_size"
PROP_PHY_SIZE = "phy_size"
PROP_MAX_SIZE = "max_size"
PROP_GROWTH_RATE = "gro_rate"


def points_to_geom_face(shortname, points, geominst):
    """
    @brief Create a geom polyline and face from a list of points

    @param shorname Name of the polygon and face
    @param points list of points
    @param geominst GEOM instance
    """
    # Creation of the geom polyline based on vertices
    # The boolean True in MakePolyline close polyline
    polyline = geominst.MakePolyline(points, True)
    # Creation of the geom face based on polyline
    face = geominst.MakeFaceWires([polyline], True)
    DATA["poly"][shortname] = polyline
    DATA["face"][shortname] = face


def shapefile_to_geom_face(input_contour, geompy, other_contours=[]):
    """
    @brief Create GEOM polylines and faces from input contours defined in a
           shapefile. 

    @param input_contour (str) Shapefile to read the contours from
    @param geompy (geomBuilder) Instance of SALOME geomBuilder
    @param other_contours (list) List of contours, each defined by a pair of
                                 (x,y) coordinates representing a polyline that
                                  may intersect the input contours
    """
    # Read the input contours from the given shapefile to extract a list
    # containing the (x,y) coordinates of each contour
    contours = read_shape_data(input_contour)

    polylines = []
    faces = []
    vertex_array = []

    for contour in contours:
        poly = np.asarray(contour)
        # Create a list of GEOM vertices
        vertices = []
        for i in range(len(poly[:, 0])):
            vertices.append(geompy.MakeVertex(poly[i, 0], poly[i, 1], 0))

        # Create the corresponding GEOM polyline and cut it if necessary
        polyline = geompy.MakePolyline(
            vertices, True
        )  # True to close the polyline
        face = geompy.MakeFaceWires([polyline], True)

        if other_contours:
            # Find the intersection points between the input contour and each
            # external contour and create a dictionary of intersected edges
            # from the input contour with the corresponding intersection points
            intersected_edges = defaultdict(list)
            for other_contour in other_contours:
                other_points = np.asarray(other_contour)
                other_vertices = []
                for i in range(len(other_points[:, 0])):
                    other_vertices.append(
                        geompy.MakeVertex(
                            other_points[i, 0], other_points[i, 1], 0
                        )
                    )
                other_polyline = geompy.MakePolyline(other_vertices, True)
                intersection = geompy.MakeSection(polyline, other_polyline)
                vertices = geompy.ExtractShapes(
                    intersection, geompy.ShapeType["VERTEX"], True
                )
                for vertex in vertices:
                    edge = geompy.GetEdgeNearPoint(polyline, vertex)
                    edge_id = geompy.GetSubShapeID(polyline, edge)
                    intersected_edges[edge_id].append(vertex)

            # Flatten the external contours array
            external_contour = np.concatenate(
                [np.array(l) for l in other_contours]
            )

            if intersected_edges:
                # Recreate the vertex array from the input contour edges
                # with the addition of new vertices close to the intersection
                # points but located on the external contour(s)
                vertices = []
                edges = geompy.ExtractShapes(polyline, geompy.ShapeType["EDGE"])
                for edge in edges:
                    nodes = geompy.ExtractShapes(
                        edge, geompy.ShapeType["VERTEX"]
                    )
                    vertex = nodes[0]
                    x, y, _ = geompy.PointCoordinates(vertex)
                    coord = np.array([x, y])
                    vertices.append(geompy.MakeVertex(coord[0], coord[1], 0))
                    # For any intersection point, append the closest vertex
                    # from the external contour
                    edge_id = geompy.GetSubShapeID(polyline, edge)
                    if edge_id in intersected_edges:
                        for vertex in intersected_edges[edge_id]:
                            x, y, _ = geompy.PointCoordinates(vertex)
                            coord = np.array([x, y])
                            # If the vertex is not on the external contour, find
                            # the closest node from the external contour and
                            # move the vertex to its position
                            if not any(
                                np.all(external_contour == coord, axis=1)
                            ):
                                distances = np.linalg.norm(
                                    external_contour - coord, axis=1
                                )
                                closest_index = np.argmin(distances)
                                coord = external_contour[closest_index]
                            vertices.append(
                                geompy.MakeVertex(coord[0], coord[1], 0)
                            )

                polyline = geompy.MakePolyline(vertices, True)
                face = geompy.MakeFaceWires([polyline], True)

        polylines.append(polyline)
        faces.append(face)
        vertex_array.append(vertices)

    return polylines, faces, vertex_array


def shapefile_to_geom_face_contour(
    input_contour, geompy, inner_contours=[], cutting_contours=[]
):
    """
    @brief Create a GEOM polyline and face from an input contour defined
           in a shapefile

    @param input_contour (str) Shapefile to read the contour from
    @param geompy (geomBuilder) Instance of SALOME geomBuilder
    @param inner_contours (list) Shapefile(s) containing inner contours
    @param cutting_contours (list) List of contours, each defined by a list of
                                   (x,y) coordinates representing a polyline
                                   used to cut the input contour
    """
    # Read the input contours from the given shapefile to extract
    # a list containing the (x,y) coordinates of each contour
    contours = read_shape_data(input_contour)

    # Please note that only one contour should be specified in the shapefile
    contour = np.asarray(contours[0])
    # Create a list of GEOM vertices
    vertices = []
    for i in range(len(contour[:, 0])):
        vertices.append(geompy.MakeVertex(contour[i, 0],
                                          contour[i, 1],
                                          0))

    # Create the corresponding GEOM polyline and cut it if necessary
    polyline = geompy.MakePolyline(vertices, True) # True to close the polyline
    polylines = []
    polylines.append(polyline)

    # Create a GEOM polyline for each inner contour
    for inner_contour in inner_contours:
        contours = read_shape_data(inner_contour)
        contour_points = np.asarray(contours[0])
        inner_vertices = []
        for i in range(len(contour_points[:, 0])):
            inner_vertices.append(
                geompy.MakeVertex(contour_points[i, 0], contour_points[i, 1], 0)
            )
        polyline = geompy.MakePolyline(inner_vertices, True)
        polylines.append(polyline)

    # Create the GEOM face based on the polylines
    face = geompy.MakeFaceWires(polylines, True)

    if cutting_contours:
        # Create a cutting face from each cutting contour
        cutting_faces = []
        for cutting_contour in cutting_contours:
            cutting_points = np.asarray(cutting_contour)
            cutting_vertices = []
            for i in range(len(cutting_points[:, 0])):
                cutting_vertices.append(
                    geompy.MakeVertex(cutting_points[i, 0], cutting_points[i, 1], 0)
                )
            cutting_polyline = geompy.MakePolyline(cutting_vertices, True)
            cutting_faces.append(geompy.MakeFaceWires(cutting_polyline, True))

        # Cut the face and retrieve its polylines
        face = geompy.MakeCutList(face, cutting_faces)
        polylines = geompy.ExtractShapes(face, geompy.ShapeType["WIRE"])

        # Flatten the cutting contours array
        cutting_contour = np.concatenate([np.array(l) for l in cutting_contours])

        # Recreate the vertices array from the external contour edges
        vertices = []
        recreate_face = False
        edges = geompy.ExtractShapes(polylines[0], geompy.ShapeType["EDGE"])
        for edge in edges:
            nodes = geompy.ExtractShapes(edge, geompy.ShapeType["VERTEX"])
            vertex = nodes[0]
            x, y, _ = geompy.PointCoordinates(vertex)
            coord = np.array([x, y])
            # Adjust the vertices intersecting the cutting contour if needed
            # All vertices should be on the input or the cutting contour
            if not (
                np.any(np.all(contour == coord, axis=1))
                or np.any(np.all(cutting_contour == coord, axis=1))
            ):
                recreate_face = True
                # Find the closest node from the cutting contour and move the
                # vertex to its position
                distances = np.linalg.norm(cutting_contour - coord, axis=1)
                closest_index = np.argmin(distances)
                coord = cutting_contour[closest_index]
            vertices.append(geompy.MakeVertex(coord[0], coord[1], 0))

        if recreate_face:
            polylines[0] = geompy.MakePolyline(vertices, True)
            face = geompy.MakeFaceWires(polylines, True)

    return polylines[0], face, vertices


def shapefile_to_geom_edge(filename, geominst):
    """
    @brief Create a geom polyline from a shape file

    @param filename is the name of the shape file to read
    @param geomist is the instance of salome geomBuilder
    @param constraint logical for open or closed line
    """
    #Read the file.shp, be careful file.df is mandatory
    my_polys = read_shape_data(filename)
    # my poly is a list of list containing the (x,y) coordinate
    # Be careful only one poly is supposed in the shape file
    polylines = []
    for my_poly in my_polys:
        my_poly_1 = np.array(my_poly)
        # Creation of the list of geom vertices
        vertex = []
        for i in range(len(my_poly_1[:, 0])):
            vertex.append(geominst.MakeVertex(my_poly_1[i, 0],
                                              my_poly_1[i, 1],
                                              0))
        # Creation of the geom polyline based on vertices
        # The boolean True in MakePolyline close polyline
        polyline = geominst.MakePolyline(vertex, False)
        polylines.append(polyline)
    return polylines

def mesh_face(smesh,
              face,
              physize,
              minsize,
              maxsize,
              refine,
              growth_rate='default',
              use_netgen=False,
              aspect_ratio=2.0):
    """
    @brief Create a mesh from a salome GEOM face

    @param smesh (smeshBuilder) Instance of SALOME smeshBuilder
    @param face is the geometry to mesh
    @param physize is the physical lenght of the edge
    @param minsize is minimum lenght of the edge
    @param maxsize is maximum lenght of the edge
    @param refine (dict) shape: [minsize,physize,maxsize]
    @param growth_rate (float) define the growth rate allowed between cells.
                                The meaning (and default value), differ for
                                Netgen1D2D and for MGCadSurf. For NETGEN1D2D,
                                it defines how much the linear dimensions of
                                two adjecent cells can differ (e.g. 0.3 which
                                is default value means 30%). For MGCadSurf, it
                                 is the maximum ratio between two adjacent
                                edges (default value is 1.3)
    @param use_netgen (bool) option to use Netgen1D2D instead of MG_CADSurf
    @param aspect_ratio (float) to define a group of faces with
                                aspect ratio >= aspect_ratio
    @param fixed_edges (list) List of GEOM edges that should not be split
    @return (smeshBuilder.Mesh) A mesh instance
    """
    #Mesh
    import SMESH
    from salome.smesh import smeshBuilder

    mesh = smesh.Mesh(face)
    if use_netgen:
        if growth_rate == 'default':
            growth_rate = 0.3
        else :
            growth_rate -= 1.0
        netgen_1d_2d = mesh.Triangle(algo=smeshBuilder.NETGEN_1D2D)
        netgen_2d_parameters = netgen_1d_2d.Parameters()
        netgen_2d_parameters.SetMaxSize(maxsize)
        netgen_2d_parameters.SetMinSize(minsize)
        netgen_2d_parameters.SetGrowthRate(growth_rate)
        netgen_2d_parameters.SetSecondOrder(0)
        netgen_2d_parameters.SetOptimize(1)
        netgen_2d_parameters.SetFineness(5)
        netgen_2d_parameters.SetUseSurfaceCurvature(1)
        netgen_2d_parameters.SetFuseEdges(1)
        netgen_2d_parameters.SetQuadAllowed(0)

        cpt = 0
        netgen_1d_2d_1 = []
        netgen_1d_2d_parameters_1 = []
        for key, data in refine.items():
            value = data[2]
            netgen_1d_2d_1.append(mesh.Triangle(\
                algo=smeshBuilder.NETGEN_1D2D,
                geom=DATA["group"][key]))
            netgen_1d_2d_parameters_1.append(netgen_1d_2d_1[cpt].Parameters())
            netgen_1d_2d_parameters_1[cpt].SetMaxSize(value[2])
            netgen_1d_2d_parameters_1[cpt].SetMinSize(value[0])
            if len(value) < 4:
                netgen_1d_2d_parameters_1[cpt].SetGrowthRate(growth_rate)
            else:
                netgen_1d_2d_parameters_1[cpt].SetGrowthRate(value[3] - 1.0)
            netgen_1d_2d_parameters_1[cpt].SetSecondOrder(0)
            netgen_1d_2d_parameters_1[cpt].SetOptimize(1)
            netgen_1d_2d_parameters_1[cpt].SetFineness(5)
            netgen_1d_2d_parameters_1[cpt].SetUseSurfaceCurvature(1)
            netgen_1d_2d_parameters_1[cpt].SetFuseEdges(1)
            netgen_1d_2d_parameters_1[cpt].SetQuadAllowed(0)
            cpt += 1

        sub_mesh = []
        for i in range(cpt):
            sub_mesh.append(netgen_1d_2d_1[i].GetSubMesh())
            smesh.SetName(netgen_1d_2d_1[i].GetAlgorithm(),
                          'NETGEN_1D_2D_1'+str(i))
            smesh.SetName(netgen_1d_2d_parameters_1[i],
                          'NETGEN_1D_2D_parameters_1'+str(i)),
            smesh.SetName(sub_mesh[i], 'sub_mesh'+str(i))
        # Set names of Mesh objects
        smesh.SetName(netgen_1d_2d.GetAlgorithm(), 'NETGEN 1D-2D')
        smesh.SetName(netgen_2d_parameters, 'NETGEN 2D Parameters')
        smesh.SetName(mesh.GetMesh(), 'Mesh')
    else:
        if growth_rate == 'default':
            growth_rate = 1.3
        mg_cadsurf = mesh.Triangle(algo=smeshBuilder.MG_CADSurf)
        mg_cadsurf_parameters = mg_cadsurf.Parameters()
        mg_cadsurf_parameters.SetPhySize(physize)
        mg_cadsurf_parameters.SetMinSize(minsize)
        mg_cadsurf_parameters.SetMaxSize(maxsize)
        mg_cadsurf_parameters.SetGradation(growth_rate)
        mg_cadsurf_parameters.SetChordalError(physize/2)

        cpt = 0
        mg_cadsurf_1 = []
        mg_cadsurf_parameters_1 = []
        for key, data in refine.items():
            value = data[2]
            mg_cadsurf_1.append(mesh.Triangle(\
                algo=smeshBuilder.MG_CADSurf,
                geom=DATA["group"][key]))
            mg_cadsurf_parameters_1.append(mg_cadsurf_1[cpt].Parameters())
            mg_cadsurf_parameters_1[cpt].SetPhySize(value[1])
            mg_cadsurf_parameters_1[cpt].SetMinSize(value[0])
            mg_cadsurf_parameters_1[cpt].SetMaxSize(value[2])
            if len(value) < 4:
                mg_cadsurf_parameters_1[cpt].SetGradation(growth_rate)
            else:
                mg_cadsurf_parameters_1[cpt].SetGradation(value[3])
            mg_cadsurf_parameters_1[cpt].SetChordalError(value[1]/2)
            cpt += 1

        sub_mesh = []
        for i in range(cpt):
            sub_mesh.append(mg_cadsurf_1[i].GetSubMesh())
            smesh.SetName(mg_cadsurf_1[i].GetAlgorithm(), 'MG_CADSurf_1'+str(i))
            smesh.SetName(mg_cadsurf_parameters_1[i],
                          'MG_CADSurf_Parameters_1'+str(i))
            smesh.SetName(sub_mesh[i], 'sub_mesh'+str(i))

        ## Set names of Mesh objects
        smesh.SetName(mg_cadsurf.GetAlgorithm(), 'MG-CADSurf')
        smesh.SetName(mg_cadsurf_parameters, 'MG-CADSurf Parameters')
        smesh.SetName(mesh.GetMesh(), 'Mesh')

    # Set group on mesh according to geom Group
    for key, val in DATA["group"].items():
        mesh.GroupOnGeom(val, key, SMESH.EDGE)

    # Set the fixed edges number of segments to 1 to prevent the mesher from
    # splitting them
    for edge in DATA["fixed_edges"]:
        mesh.Segment(geom=edge).NumberOfSegments(1)

    mesh.Compute()

    # Clean the mesh of invalid elements, i.e. elements that are not triangles
    elements = mesh.GetElementsByType(SMESH.ALL)
    invalid_elements = [
        elem
        for elem in elements
        if mesh.GetElementGeomType(elem) != SMESH.Entity_Triangle
    ]
    mesh.RemoveElements(invalid_elements)

    # Reorient the direction of all elements
    mesh.Reorient2D(mesh, [0, 0, 1], [0, 0, 0])

    # change overConstrained Triangles
    # Prepare groups for cleaning overConstrained Triangles
    # 1 - Create group containing free borders
    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.EDGE, SMESH.FT_FreeBorders, SMESH.FT_Undefined, 0)
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(mesh.GetMesh())
    free_borders = mesh.GroupOnFilter(SMESH.EDGE, 'free_borders', aFilter)

    # 2 - Create group containing all edges
    edges = mesh.CreateEmptyGroup(SMESH.EDGE, 'edges')
    edges.AddFrom(mesh.GetMesh())

    # 3 - remove useless edges (edges - free_borders)
    useless_edges = mesh.GetMesh().CutListOfGroups([edges], [free_borders], 'useless_edges')
    mesh.RemoveGroupWithContents(useless_edges)

    # Cleaning overConstrained Triangles
    a_criteria = []
    a_criterion = smesh.GetCriterion(\
        SMESH.FACE,
        SMESH.FT_OverConstrainedFace,
        SMESH.FT_Undefined,
        0)
    a_criteria.append(a_criterion)
    a_filter = smesh.GetFilterFromCriteria(a_criteria)
    a_filter.SetMesh(mesh.GetMesh())
    mesh.GroupOnFilter(SMESH.FACE, 'Group_1', a_filter)
    over_constrained_triangles_ids = mesh.GetIdsFromFilter(a_filter)
    for id_tri in over_constrained_triangles_ids:
        node_ids = mesh.GetElemNodes(id_tri)
        shared_node_ids = []
        for node_id in node_ids:
            face_ids = mesh.GetNodeInverseElements(node_id,
                                                   elemType=SMESH.FACE)
            if len(face_ids) > 1:
                shared_node_ids.append(node_id)
        mesh.InverseDiag(shared_node_ids[0], shared_node_ids[1])

    # find elements with a bad aspect ratio
    ar_margin = aspect_ratio
    a_filter = smesh.GetFilter(SMESH.FACE,
                               SMESH.FT_AspectRatio,
                               SMESH.FT_MoreThan,
                               ar_margin)
    an_ids = mesh.GetIdsFromFilter(a_filter)
    if len(an_ids):
        a_group = mesh.CreateEmptyGroup(SMESH.FACE,
                                        "Aspect Ratio > " + repr(ar_margin))
        a_group.Add(an_ids)

    return mesh


def geom_from_shape_file(filename, geompy, other_contours=[]):
    """
    @brief Generate GEOM polylines and faces from a shapefile defining the
           outline of one or more areas.

    An optional list of external contours can be provided, in which case any
    node of an external contour close to its intersection with an input contour
    will be added to the polyline defining the latter. This helps to avoid
    non-conforming triangulations.

    @param filename (str) Shapefile to read the contour from
    @param geompy (geomBuilder) Instance of SALOME geomBuilder
    @param other_contours (list) List of existing contours that may intersect
                                 the input ones
    """
    shortname, _ = path.splitext(path.basename(filename))
    polys, faces, vertices = shapefile_to_geom_face(
        filename, geompy, other_contours
    )

    zones = []
    for i, poly in enumerate(polys):
        polygon_name = shortname+"_{}".format(i)
        DATA["poly"][polygon_name] = poly
        DATA["face"][polygon_name] = faces[i]
        DATA["vertices"][polygon_name] = vertices[i]

        zones.append(polygon_name)

    return zones


def geom_edge_from_shape_file(filename, geominst):
    """
    @brief Generate polyline and edge from shape files
           defining the edge of constraint lines

    @param filename is the name of the shape file to read
    @param geomist is the instance of salome geomBuilder

    @returns (list) List of constraint names added to geom
    """
    shortname, _ = path.splitext(path.basename(filename))
    poly = shapefile_to_geom_edge(filename, geominst)

    list_constraints = []

    for i, poly in enumerate(poly):
        constraint_name = shortname+"_{}".format(i)
        print("Adding contraint {} from {}".format(constraint_name, shortname))
        DATA["edge"][constraint_name] = poly

        list_constraints.append(constraint_name)

    return list_constraints


def check_new_points(geompy, shape_name, points, min_dist):
    """
    @brief method to remove points in contour shape if the points are too close
           of the intersection of boundary zone and contour shape (which
           generate a new node)

    @param geompy is the instance of salome geomBuilder
    @shape_name name of polygon shape
    @points points of polygon shape
    @min_dist criterion (minimal distance) to detect points too close
    """
    orig_points = DATA["vertices"][shape_name]
    ipn = None # index of new point corresponding to first original point
    for i, point in enumerate(points):
        dist = geompy.MinDistance(orig_points[0], point)
        if dist < 1.e-5:
            ipn = i
            break

    inserted_short = []
    # index offest corresponding to the number of inserted points already found
    deltai = 0
    for i, orig_point in enumerate(orig_points):
        if i+ipn+deltai < len(points):
            i_1 = i+ipn+deltai
        else:
            i_1 = i+ipn+deltai -len(points)
        dist = geompy.MinDistance(orig_point, points[i_1])
        if dist > 1.e-5: # a point as been inserted here
            deltai = deltai+1
            if i_1 > 0:
                i_m = i_1-1
            else:
                i_m = len(points)-1
            if i_1 < len(points)-1:
                i_p = i_1+1
            else:
                i_p = 0
            d_0 = geompy.MinDistance(points[i_1], points[i_m])
            d_1 = geompy.MinDistance(points[i_1], points[i_p])
            if d_0 < min_dist:
                inserted_short.append((i_m, i_m-deltai+1))
            if d_1 < min_dist:
                inserted_short.append((i_p, i_p-deltai))
    for i, j in inserted_short:
        print("point to remove", i, j, geompy.PointCoordinates(orig_points[j]))
    return inserted_short


def group_for_submesh_face(geompy, model, refine):
    """
    @brief Make face partition and generate group face for submesh
           Return new partitioned model

    @param geompy (geomBuilder) Instance of SALOME geomBuilder
    @param model (GEOM.GEOM_Object) The model geometry
    @param refine (dict) Informations on refinement zones
    @return (GEOM.GEOM_Object) The updated model
    """
    import GEOM

    poly = []
    for key in refine:
        poly.append(DATA["poly"][key])

    # Creation of the partition based on curve and face domain
    model = geompy.MakePartition(
        [model],
        poly,
        [],
        [],
        geompy.ShapeType["FACE"],
        0,
        [],
        1,
        "domain_partition",
    )

    for key in refine:
        intersection = geompy.MakeSection(
            model, DATA["face"][key], True, f"{key}_section"
        )
        DATA["intersect"][key] = intersection

        box = geompy.MakeThickSolid(DATA["face"][key], 1, [])
        DATA["edge"][key] = box

    # Get the ID of the created partition face
    for key in refine:
        list_submesh = geompy.GetShapesOnShapeIDs(
            DATA["edge"][key], model, geompy.ShapeType["FACE"], GEOM.ST_ONIN
        )

        submesh = geompy.CreateGroup(model, geompy.ShapeType["FACE"])

        # Create group with the ID
        DATA["group"][key] = submesh
        geompy.UnionIDs(submesh, list_submesh)

    return model


def constraint_partition(geompy, contour_shape, constraints):
    """
    @brief Generate partition from constraint line

    @param contour_shape string corresponding to the shape name file containing
    the model edge
    @param list of the constraint shape files
    @parma constraints (list) List of constraint lines
    """

    edges = [DATA["edge"][key] for key in constraints]
    partition = geompy.MakePartition([contour_shape],
                                     edges,
                                     [],
                                     [],
                                     geompy.ShapeType["FACE"],
                                     0,
                                     [],
                                     1,
                                     "constraints_partition")

    return partition


def bc_group_from_model(geompy, model):
    """
    @brief Generate boundary group of the geometry

    @param geometry of the model
    @param constraint logical if there are constraint lines
    """
    import GEOM
    (_, closed_free_boundaries, _) = geompy.GetFreeBoundary(model)
    # When we have island closed_free_boundary contains a shape for each
    closed_free_boundary_ids = []
    for closed_free_boundary in closed_free_boundaries:
        closed_free_boundary_edges = geompy.ExtractShapes(\
            closed_free_boundary,
            geompy.ShapeType["EDGE"],
            True)
        tmp = geompy.GetSubShapesIDs(\
            model,
            closed_free_boundary_edges)
        closed_free_boundary_ids += tmp

    for key in DATA["box"]:
        list_bc = geompy.GetShapesOnShapeIDs(DATA["box"][key],
                                             model,
                                             geompy.ShapeType["EDGE"],
                                             GEOM.ST_ONIN)
        bc_ids = [e for e in list_bc if e in closed_free_boundary_ids]
        bnd = geompy.CreateGroup(model, geompy.ShapeType["EDGE"])
        if len(bc_ids) == 0:
            raise Exception("Issue with boundary {}:\n"\
                            "Change node_options for that boundary"\
                            .format(key))

        DATA["group"][key] = bnd
        geompy.UnionIDs(bnd, bc_ids)
        for k in bc_ids:
            closed_free_boundary_ids.remove(k)

    wall = geompy.CreateGroup(model, geompy.ShapeType["EDGE"])
    DATA["group"]["wall"] = wall
    geompy.UnionIDs(wall, closed_free_boundary_ids)


def create_bc_file(boundfile):
    """
    Generates the boundary conditions file

    @param boundfile filename of the output bc file
    """
    bc_file = open(boundfile, "w")
    bc_file.write("2 2 2 2 wall")
    bc_file.close()


def define_refine_group(model, refine, geompy):
    """
    Define group used by submesh

    @param model Geometry model
    @param refine dict of refinement zones
    @param geompy geom instance
    """
    import GEOM

    # Get the ID of the created partition face
    for key in refine:
        list_submesh = geompy.GetShapesOnShapeIDs(\
            DATA["edge"][key],
            model,
            geompy.ShapeType["FACE"],
            GEOM.ST_ONIN)
        submesh = geompy.CreateGroup(model,
                                     geompy.ShapeType["FACE"])
        # Create group with the ID
        DATA["group"][key] = submesh
        geompy.UnionIDs(submesh, list_submesh)


def create_geom_face(geompy,
                     input_contour,
                     constraint_lines=[],
                     refine_contours=[],
                     island_contours=[],
                     cutting_contours=[]):
    """Create a GEOM face from shapefiles and constraint lines.

    @param geompy (geomBuilder) An instance of SALOME geomBuilder
    @param input_contour (str) Shapefile containing the contour used to create
                               the face
    @param constraint_lines (list) Shapefile(s) containing constraint lines
    @param refine_contours (list) Shapefile(s) containing contour(s) to define
                                  refinement zones
    @param island_contours (list) Shapefile(s) containing contour(s) to define
                                  islands
    @param cutting_contours (str) Shapefile(s) used to cut the contour
    """
    cutting_coordinates = []
    for contour in cutting_contours:
        # Read the cutting contour vertices
        cutting_coords = read_shape_data(contour)
        cutting_coordinates.append(cutting_coords[0])

    # Create the GEOM shape from the input contour
    poly, face, vertices = shapefile_to_geom_face_contour(
        input_contour, geompy, island_contours, cutting_coordinates
    )

    shortname, _ = path.splitext(path.basename(input_contour))
    DATA["poly"][shortname] = poly
    DATA["face"][shortname] = face
    DATA["vertices"][shortname] = vertices

    # Add refinement zones
    refine_zones = {}
    if refine_contours:
        data_refine = build_refine_dict(refine_contours)
        for namefile in data_refine:
            refine_zone = geom_from_shape_file(namefile, geompy, cutting_coordinates)
            for i, zone in enumerate(refine_zone):
                refine_zones[zone] = [namefile, i, data_refine[namefile][i]]

    model = group_for_submesh_face(geompy, face, refine_zones)

    # Add the GEOM object to the salome study
    listtyp = ["poly", "face", "intersect", "box"]
    for typ in listtyp:
        for key, val in DATA[typ].items():
            geompy.addToStudy(val, key)

    for key, val in DATA["group"].items():
        geompy.addToStudyInFather(model, val, key)

    constraints2file = {}
    if constraint_lines is not None:
        for namefile in constraint_lines:
            constraints = geom_edge_from_shape_file(namefile, geompy)
            for i, constraint in enumerate(constraints):
                constraints2file[constraint] = [namefile, i]
        for key, val in DATA["edge"].items():
            # Discarding edges that comes from refine
            skip = False
            for ref_key in refine_zones:
                if key == ref_key:
                    skip = True
            if skip:
                print("Skipping {}".format(key))
                continue
            print("Adding {}".format(key))
            geompy.addToStudyInFather(model, val, key)
        model = constraint_partition(geompy, model, constraints2file)

    bc_group_from_model(geompy, model)

    define_refine_group(model, refine_zones, geompy)

    geompy.addToStudy(model, 'model')

    # Set the list of fixed edges if there is a cutting contour
    edges = geompy.SubShapeAll(model, geompy.ShapeType["EDGE"])
    for contour_points in cutting_coordinates:
        for edge in edges:
            vertices = geompy.SubShapeAll(edge, geompy.ShapeType["VERTEX"])
            x1, y1, _ = geompy.PointCoordinates(vertices[0])
            coord1 = np.array([x1, y1])
            x2, y2, _ = geompy.PointCoordinates(vertices[1])
            coord2 = np.array([x2, y2])
            # A fixed edge is located on a cutting contour
            if np.any(np.all(contour_points == coord1, axis=1)) and np.any(
                np.all(contour_points == coord2, axis=1)
            ):
                DATA["fixed_edges"].append(edge)

    return model, refine_zones


def create_mesh(
    contour_shp,
    phy_size,
    min_size,
    max_size,
    mesh_engine,
    output_mesh,
    constraints_files=[],
    growth_rate="default",
    refine_files=[],
    island_files=[],
):
    """
    Create a mesh from shapes and constraint lines using GEOM and SMESH.

    @param contour_shp (str) Name of the shapefile containing the contour
                             polygon from which to create the mesh
    @param phy_size (float) Physical size of a mesh element
    @param min_size (float) Minimum size of a mesh element
    @param max_size (float) Maximum size of a mesh element
    @param mesh_engine (str) Name of the mesh engine to use
    @param output_mesh (str) Filename of the output mesh
    @param dico_boundaries (dict) path_to_shape :
                         {type : [4, 5, 5, 5], node_option : 0}
                         (O: simple geometric partition,
                          1: removal of points in boundary geom shape,
                          2: select meshes edges in box)
    @param constraints_files (list) list of shapefile name for constraint lines
    @param growth_rate (float) Define the growth rate allowed between cells.
                               The meaning (and default value), differ for
                               Netgen1D2D and for MGCadSurf. For NETGEN1D2D,
                               it defines how much the linear dimensions of
                               two adjecent cells can differ (e.g. 0.3 which
                               is default value means 30%). For MGCadSurf, it
                               is the maximum ratio between two adjacent
                               edges (default value is 1.3)
    @param refine_files (list) List of refine files
    @param island_files (list) List of shapefile(s) to create island(s)

    """
    from salome.geom import geomBuilder
    from salome.smesh import smeshBuilder

    geompy = geomBuilder.New()
    smesh = smeshBuilder.New()

    model, refine_zones = create_geom_face(
        geompy,
        contour_shp,
        constraints_files,
        refine_files,
        island_files,
    )

    use_netgen = mesh_engine == "netgen_1d_2d"
    mesh = mesh_face(
        smesh,
        model,
        phy_size,
        min_size,
        max_size,
        refine_zones,
        growth_rate,
        use_netgen=use_netgen,
    )

    # Export the mesh to the given path
    mesh.ExportMED(output_mesh)
    bnd_file = path.splitext(output_mesh)[0] + ".bnd"
    create_bc_file(bnd_file)
    print(f"Created {output_mesh}")


def read_shape_attr(shape_file, attr_names, default_attr=[]):
    """
    Get attribute from a shape file

    @param shape_file (str) Name of shape file to read from
    @param attr_names (list) List of attributes to read
    @param default_attr (list of lists) List of attributes to read with
                                        default values if they are missing in
                                        the GIS file
    @returns (list) List of values for each polygon in the file
    """
    from copy import deepcopy
    try:
        import fiona
        has_fiona = True
    except ImportError:
        has_fiona = False
        import ss_utils.shapefile as shapefile

    attr_values = [0]*(len(attr_names) + len(default_attr))

    values = []

    if has_fiona:

        with fiona.open(shape_file) as f:
            for polygon in f:
                for i, attr in enumerate(attr_names):
                    try:
                        attr_values[i] = polygon["properties"][attr]
                    except KeyError:
                        raise Exception("Missing attribute {} in shape file"\
                                        .format(attr))
                for j, (attr, value) in enumerate(default_attr):
                    try:
                        attr_values[i + j + 1] = polygon["properties"][attr]
                    except KeyError:
                        attr_values[i + j + 1] = value

                values.append(deepcopy(attr_values))

        return values

    # Use shapefile implementation
    shape = shapefile.Reader(shape_file)

    # DeletionFlag field is not available in records
    fieldnames = [f[0] for f in shape.fields if f[0] != 'DeletionFlag']

    # values for each feature of the shapefile
    for feature in shape.shapeRecords():
        for i, attr in enumerate(attr_names):
            try:
                index = fieldnames.index(attr)
                attr_values[i] = feature.record[index]
            except ValueError:
                raise Exception("Missing attribute {} in shape file".format(attr))
        for j, (attr, value) in enumerate(default_attr):
            try:
                attr_values[i + j + 1] = feature.record[index]
            except KeyError:
                attr_values[i + j + 1] = value

        values.append(deepcopy(attr_values))

    return values


def create_mesh_parser(subparser):
    """
    Add arguments for a mesh creation

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser(
        'create_mesh',
        help='Creating a mesh from shape and constraint lines')

    #TODO: describe geopackage format
    #parser.add_argument(
    #    "geopackage",
    #    help="GeoPackage containing ... ")

    parser.add_argument(
        "contour_shp",
        help="File name of the mesh contour (ESRI shapefile format)")

    parser.add_argument(
        "-dx", "--phy_size",
        dest="phy_size", type=float,
        help="Physical size of a mesh element")

    parser.add_argument(
        "-min", "--min_size",
        dest="min_size", type=float,
        help="Minimum size of a mesh element")

    parser.add_argument(
        "-max", "--max_size",
        dest="max_size", type=float,
        help="Maximum size of a mesh element (unused in NETGEN 1D-2D)")

    parser.add_argument(
        "-gr", "--growth_rate",
        dest="growth_rate",
        default=1.3,
        type=float,
        help="define the growth rate allowed between cells. The meaning (and \
              default value), differ for Netgen1D2D and for MGCadSurf. For \
              NETGEN1D2D, it defines how much the linear dimensions of two \
              adjecent cells can differ (e.g. 0.3 which is default value \
              means 30%). For MGCadSurf, it is the maximum ratio between two \
              adjacent edges (default value is 1.3)")

    parser.add_argument(
        "--mesh_engine",
        dest="mesh_engine",
        default="netgen_1d_2d",
        choices=["netgen_1d_2d", "mg_cadsurf"],
        help="Option to choose the mesh generator engine between NETGEN1D2D \
              and MG-CADSURF (warning, MG-CADSURF requires a licence)")

    parser.add_argument(
        "output_mesh",
        help="Name of the output mesh file")

    parser.add_argument(
        "--constraints",
        default=[],
        help="Files containing constraint lines (ESRI shapefile format)", nargs="+")

    parser.add_argument(
        "--isles",
        default=[],
        help="Files containing isles polygons (ESRI shapefile format)", nargs="+")

    parser.add_argument(
        "--refine",
        default=[],
        help="Files containing refinement polygon "\
              "(must contain attribute "\
              "(min_size, phy_size, max_size)) (ESRI shapefile format)",
        nargs="+")


    return subparser


def extract_geopackage(geopackage):
    """
    Extract information from geopackage

    @param (str) Name of the geopackage file
    """
    import fiona
    import geopandas as gpd
    from shapely.geometry.multilinestring import MultiLineString
    from shapely.geometry.polygon import Polygon

    data_gpd = OrderedDict()
    data_fiona = OrderedDict()

    for layer in fiona.listlayers(geopackage):
        print("Handling layer fiona:", layer)
        # Example of reading data using fiona to get coordinates of Polygon and lines
        lay = fiona.open(geopackage, layer=layer)
        data_fiona[layer] = []
        for key, val in lay.items():
            print("Reading key ", key)
            if 'geometry' in val and val['geometry'] is not None:
                geo_type = val['geometry']['type']
            else:
                print("No geometry...")
                continue
            print("keys", list(val['properties'].keys()))
            print("type", val['type'])
            # To reach attribute table go into val['properties'][attr_name]

            if geo_type == "Polygon":
                coords = val['geometry']['coordinates']
                print("reading {} polygons".format(len(coords)))
            elif geo_type == "MultiLineString":
                coords = val['geometry']['coordinates']
                print("reading {} lines".format(len(coords)))
            for coord in coords:
                data_fiona[layer].append(coord)

        # Example of reading data using fiona to get coordinates of Polygon and lines
        gpd_file = gpd.read_file(geopackage, layer=layer)
        data_gpd[layer] = []

        print("Handling layer geopandas:", layer)
        print("gpd_file", gpd_file.get('Type', None))
        # to reach attribute it is directly accessible through dpf_file[attr_name]
        if 'geometry' in gpd_file:
            geo = gpd_file['geometry']
            for data in geo:
                if isinstance(data, Polygon):
                    print("Adding polygon")
                    xs, ys = list(data.exterior.coords.xy)
                    coords = [ (x, y) for x, y in zip(xs, ys)]
                    data_gpd[layer].append(coords)
                elif isinstance(data, MultiLineString):
                    print("Adding Line String")
                    for line in data:
                        xs, ys = line.coords.xy
                        coords = [ (x, y) for x, y in zip(xs, ys)]
                        data_gpd[layer].append(coords)
                else:
                    print("Unknown type", type(data))

    return True

def build_refine_dict(refine_files):
    """
    Extract refine informations from refinement files

    @param refine_files (dict) List of shape files

    @returns (dict) {file_name:[min_size, phy_size, max_size]}
    """

    refine_dict = {}
    for shp_file in refine_files:
        list_attr = [PROP_MIN_SIZE, PROP_PHY_SIZE, PROP_MAX_SIZE]
        default_attr = [[PROP_GROWTH_RATE, 1.3]]
        refine_dict[shp_file] = read_shape_attr(shp_file,
                                                list_attr,
                                                default_attr=default_attr)

    from pprint import pprint
    pprint(refine_dict)

    return refine_dict
