#!/usr/bin/env python3
""" Script providing all Q4TS meshing functions """

import importlib.util
import os
import sys
from argparse import ArgumentParser

try:
    # TELEMAC Python modules
    from pretel.extract_contour import extract_contour, write_gis_file
    from pretel.manip_telfile import alter
    from run_telfile import alter_parser, contour_parser

    HAS_TELEMAC = True
except ImportError:
    HAS_TELEMAC = False

if HAS_TELEMAC:
    if sys.platform.startswith("win"):
        # Workaround in case TELEMAC uses Python embeddable package for Windows
        sys.path.append(os.path.dirname(os.path.abspath(__file__)))

    from clc import clc, clc_parser
    from field import field, field_parser
    from interpolate import interpolate, interpolate_parser

from create_boundary import create_boundary, create_boundary_parser
from create_mesh import create_mesh, create_mesh_parser
from cut_mesh import cut_mesh, cut_mesh_parser
from derefine import derefine, derefine_parser
from extend_mesh import extend_mesh, extend_mesh_parser
from refine import refine, refine_parser

HAS_SALOME = importlib.util.find_spec("salome") is not None


def main():
    """
    Main function of the converter
    """

    parser = ArgumentParser()
    subparser = parser.add_subparsers(help="command to do", dest="command")
    if HAS_SALOME:
        subparser = create_mesh_parser(subparser)
        subparser = cut_mesh_parser(subparser)
        subparser = extend_mesh_parser(subparser)

    subparser = create_boundary_parser(subparser)
    subparser = refine_parser(subparser)
    subparser = derefine_parser(subparser)

    if HAS_TELEMAC:
        subparser = clc_parser(subparser)
        subparser = field_parser(subparser)
        subparser = contour_parser(subparser)
        subparser = alter_parser(subparser)
        subparser = interpolate_parser(subparser)

    options = parser.parse_args()

    # Command switch
    if options.command == "create_mesh":
        create_mesh(
            options.contour_shp,
            options.phy_size,
            options.min_size,
            options.max_size,
            options.mesh_engine,
            options.output_mesh,
            growth_rate=options.growth_rate,
            constraints_files=options.constraints,
            refine_files=options.refine,
            island_files=options.isles,
        )
    elif options.command == "contour":
        domains_bnd = extract_contour(options.mesh_file)
        write_gis_file(domains_bnd, options.shp_file)
    elif options.command == "create_boundary":
        create_boundary(
            options.input_mesh,
            options.output_mesh,
            options.contour,
            options.default_bnd,
            options.boundaries,
        )
    elif options.command == "refine":
        refine(
            options.input_mesh,
            options.output_mesh,
            options.refinement_level,
            domain=options.domain,
            method=options.method,
        )
    elif options.command == "derefine":
        derefine(options.input_mesh, options.output_mesh, options.domain)
    elif options.command == "field":
        field(
            options.input_mesh,
            options.output_mesh,
            options.varname,
            varunit=options.varunit,
            zone_shp=options.zone_shp,
            field_value=options.field_value,
            attr_name=options.attr_name,
            default_value=options.default_value,
            epsg=options.epsg,
            record=options.record,
            shift_x=options.shift_x,
            shift_y=options.shift_y,
        )
    elif options.command == "interpolate":
        interpolate(
            options.input_mesh,
            options.output_mesh,
            options.points_file,
            delimiter=options.delimiter,
            varname=options.varname,
            varunit=options.varunit,
            kind=options.kind,
            record=options.record,
            shift_x=options.shift_x,
            shift_y=options.shift_y,
            zone=options.zone,
            outside_zone=options.outside_zone,
            default_value=options.default_value,
            mmap=options.mmap,
            chunk_interp=options.chunk_interp,
            chunk_x_partitions=options.chunk_x_partitions,
            chunk_y_partitions=options.chunk_y_partitions,
            chunk_overlap=options.chunk_overlap,
            blockpts=options.blockpts,
        )
    elif options.command == "cut_mesh":
        cut_mesh(options.input_mesh, options.cutting_contour_shp, options.output_mesh)
    elif options.command == "extend_mesh":
        extend_mesh(
            options.input_mesh,
            options.input_contour_shp,
            options.extended_contour_shp,
            options.phy_size,
            options.min_size,
            options.max_size,
            options.mesh_engine,
            options.output_mesh,
            options.other_mesh,
            options.other_contour_shp,
            growth_rate=options.growth_rate,
            constraints_files=options.constraints,
            refine_files=options.refine,
            island_files=options.isles,
        )
    elif options.command == "clc":
        clc(
            options.input_mesh,
            options.output_mesh,
            options.clc_tab_name,
            clc_data_file=options.clc_data_file,
            zone_clc=options.zone,
            zone_clc_attribute=options.zone_attribute,
            zone_offset=options.zone_offset,
            offset=options.offset,
            write_clc_code=options.write_clc_code,
        )
    elif options.command == "alter":
        if options.sph2ll != [None, None]:
            sph2ll = True
            longitude = options.sph[0]
            latitude = options.sph[1]
        else:
            sph2ll = False
            longitude = None
            latitude = None

        if options.ll2sph != [None, None]:
            ll2sph = True
            longitude = options.sph[0]
            latitude = options.sph[1]
        else:
            ll2sph = False
            longitude = None
            latitude = None

        if options.ll2utm is not None:
            ll2utm = True
            zone = int(options.ll2utm[:-1])
            zone_letter = options.ll2utm[-1]
        else:
            ll2utm = False
            zone = None
            zone_letter = None

        if options.utm2ll is not None:
            utm2ll = True
            zone = int(options.utm2ll[:-1])
            zone_letter = options.utm2ll[-1]
        else:
            utm2ll = False
            zone = None
            zone_letter = None

        alter(
            options.input_file,
            options.output_file,
            bnd_file=options.bnd_file,
            title=options.title,
            in_datetime=options.datetime,
            toggle_endian=options.toggle_endian,
            toggle_precision=options.toggle_precision,
            add_x=options.add_x,
            mul_x=options.mul_x,
            add_y=options.add_y,
            mul_y=options.mul_y,
            orig=options.orig,
            rotate=options.rotate,
            rot_pt=options.rot_pt,
            center=options.center,
            proj=options.proj,
            sph2ll=sph2ll,
            ll2sph=ll2sph,
            longitude=longitude,
            latitude=latitude,
            utm2ll=utm2ll,
            ll2utm=ll2utm,
            zone=zone,
            zone_letter=zone_letter,
            in_vars=options.xvars,
            rename_var=options.rename_var,
            modif_var=options.modif_var,
            add_var=options.add_var,
            mul_var=options.mul_var,
            times=options.times,
            tfrom=options.tfrom,
            tstep=options.tstep,
            tend=options.tend,
            reset_time=options.reset_time,
            add_time=options.add_time,
            mul_time=options.mul_time,
            force=options.force,
        )
    else:
        parser.print_help()

    sys.exit(0)


if __name__ == "__main__":
    main()
