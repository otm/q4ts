""" functions to set field value on mesh """
import shutil
import geopandas as gpd
import shapely as shp
import numpy as np
from data_manip.extraction.telemac_file import TelemacFile
from os import path

def field_parser(subparser):
    """
    Add arguments for a field

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser('field',
                                  help='field a xyz on the whole mesh or within a polygon')
    parser.add_argument(
        "input_mesh",
        help="Name of the input mesh file")

    parser.add_argument(
        "output_mesh",
        help="Name of the output mesh file")

    parser.add_argument(
            "--varname",
            help="Name of the variable in the file")

    parser.add_argument(
            "--varunit",
            help="Unit of the variable in the file")

    parser.add_argument(
            "--zone-shp",
            default=None,
            help="ESRI shp file containing zones and values to define field")

    parser.add_argument(
            "--field-value",
            type=float,
            default=None,
            help="Field value used to define the field within the polygon(s) \
                  if there is no attributes in the shp file")

    parser.add_argument(
            "--attr-name",
            default=None,
            help="Name of the attribute to consider in the shapefile")

    parser.add_argument(
            "--default-value",
            default=None,
            type=float,
            help="In case the varible does not already exists in the file this \
                 value will be applied to points not within the polygons if \
                 polygons were given")

    parser.add_argument(
            "--epsg",
            default=None,
            help="EPSG code of the input mesh file")

    parser.add_argument(
            "--record",
            type=int,
            default=-1,
            help="Number of the record (time step) to change in the mesh \
                  (-1 is the last record)")

    parser.add_argument(
            "--shift-x",
            type=float,
            default=0.0,
            help="Translation along with x-axis to consider to match \
                  the geo-coordinates")

    parser.add_argument(
            "--shift-y",
            type=float,
            default=0.0,
            help="Translation along with y-axis to consider to match \
                  the geo-coordinates")

    return subparser


def field(input_mesh,
          output_mesh,
          varname,
          varunit='',
          zone_shp=None,
          field_value=-999.0,
          attr_name=None,
          default_value=None,
          epsg="epsg:2154",
          record=-1,
          shift_x=0.0,
          shift_y=0.0):
    """
    Define values of a field in the mesh with a polygon shape

    @param input_mesh (str) Name of the mesh to derefine
    @param output_mesh (str) Name of the derefined mesh
    @param varname (str) Name of the variable to write in the result file
    @param varunit (str) Unit of the variable to write in the result file
    @param field_value (real) Field value used to define the field within
                              the polygon(s) if there is no attributes in
                              the shp file
    @param zone_shp (str) If given only field within zone define by polygon file
    @param default_value (float) If using zone and variable does not already
                                 exists in output_mesh value set to points out
                                 of zone
    @param epsg (str) EPSG code of the input mesh file
    @param attr_name (str) Name of the attribute to consider in the shapefile
    @param record (int) Number of the record (time step) to change in the mesh
    @param shift_x (real) Translation along with x-axis to consider to match
    the geo-coordinates
    @param shift_y (real) Translation along with y-axis to consider to match
    the geo-coordinates
    """
    print('{} -> {}'.format(input_mesh, output_mesh))
    shutil.copy2(input_mesh, output_mesh)

    res = TelemacFile(output_mesh, access='rw')
    res.read()

    mesh_x = res.meshx
    mesh_y = res.meshy
    if zone_shp is not None:
        gpdf_shp_file = gpd.read_file(zone_shp)
        if attr_name is None:
            attr_name = "field_value"
            gpdf_shp_file[attr_name] = field_value

    multipoints = []

    if default_value is not None:
        user_default_value = default_value
    else:
        user_default_value = 0.0

    if varname not in res.varnames:
        champ = np.full(len(mesh_x), user_default_value)
        res.add_variable(varname, varunit)
    elif default_value is None:
        champ = res.get_data_value(varname, -1)
    else:
        champ = np.full(len(mesh_x), user_default_value)

    if zone_shp is not None:
        for j, (x, y) in enumerate(zip(mesh_x, mesh_y)):
            multipoints.append(shp.geometry.Point((x + shift_x, y + shift_y)))

        points_serie = gpd.GeoSeries(multipoints)
        gdf_points_serie = gpd.GeoDataFrame(points_serie)
        gdf_points_serie = gdf_points_serie.rename(
                columns={0: 'geometry'}).set_geometry('geometry')
        gdf_points_serie.crs = epsg
        gdf_points_serie_joined = gpd.sjoin(gdf_points_serie, gpdf_shp_file,
                                            how="left")

        for i in range(len(mesh_x)):
            if not np.isnan(float(gdf_points_serie_joined[attr_name].iloc[i])):
                champ[i] = gdf_points_serie_joined[attr_name].iloc[i]

    res.add_data_value(varname, record, champ)
    res.write()
    res.close()
