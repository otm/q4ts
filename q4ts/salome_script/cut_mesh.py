import os
import shutil

from create_mesh import shapefile_to_geom_face


def cut_mesh_parser(subparser):
    """
    Add arguments to cut a mesh with another mesh

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser(
        "cut_mesh",
        help="Cut a mesh with another mesh",
    )

    parser.add_argument(
        "input_mesh",
         help="Name of the input mesh file to be cut"
    )

    parser.add_argument(
        "cutting_contour_shp",
        help="Name of the shapefile containing the polygon defining the "
             "cutting contour",
    )

    parser.add_argument("output_mesh", help="Name of the output mesh file")

    return subparser


def cut_mesh(
    input_mesh,
    cutting_contour_shp,
    output_mesh,
):
    """
    Cut a mesh with another mesh. This temove elements from the input mesh that
    overlap the cutting mesh.

    @param input_mesh (str) Name of the input mesh file to be cut
    @param cutting_contour_shp (str) Name of the shapefile containing the polygon
                                     defining the cutting contour
    @param output_mesh (str) Name of the output mesh file
    """
    import SMESH
    from salome.geom import geomBuilder
    from salome.smesh import smeshBuilder

    geompy = geomBuilder.New()
    smesh = smeshBuilder.New()

    ([original_mesh], _) = smesh.CreateMeshesFromMED(input_mesh)

    # Get the cutting face
    _, faces, _ = shapefile_to_geom_face(cutting_contour_shp, geompy)
    cutting_face = faces[0]

    # Get all elements where at least one node of each lies on the cutting face
    filter = smesh.GetFilter(SMESH.FACE, SMESH.FT_LyingOnGeom, cutting_face)
    element_ids = original_mesh.GetIdsFromFilter(filter)
    # Get the corresponding nodes to also remove a second range of elements
    group = original_mesh.MakeGroupByIds("Overlapping elements", SMESH.FACE, element_ids)
    node_ids = group.GetNodeIDs()

    # Remove them from the input mesh
    _ = original_mesh.RemoveNodes(node_ids)

    original_mesh.ExportMED(output_mesh)
    # Copy the boundary conditions file if any
    input_bnd_file = os.path.splitext(input_mesh)[0] + ".bnd"
    if os.path.exists(input_bnd_file):
        output_bnd_file = os.path.splitext(output_mesh)[0] + ".bnd"
        shutil.copy2(input_bnd_file, output_bnd_file)
    print(f"Cut mesh saved to {output_mesh}")
