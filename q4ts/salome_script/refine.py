""" functions to refine a mesh with salome """
from os import path, remove
import shutil
import numpy as np
import matplotlib.path as mpltPath

from ss_utils.read_shape_data import read_shape_data

def refine_parser(subparser):
    """
    Add arguments for a refinement

    @param subparser (argumentParser) argument parser

    @return (argumentParser) the updated argument parser
    """
    parser = subparser.add_parser(
        'refine',
        help='Refine a whole mesh or within a polygon')
    parser.add_argument(
        "input_mesh",
        help="Name of the input mesh file")

    parser.add_argument(
        "output_mesh",
        help="Name of the output mesh file")

    parser.add_argument(
        "--domain",
        default=None,
        help="Shapefile of the domain to refine")

    parser.add_argument(
        "-l", "--refinement-level",
        type=int,
        help="Refinement level")

    parser.add_argument(
        "--method",
        default="homard",
        choices=["homard", "stbtel", "smesh"],
        help="Method to refine:\nhomard: local refinement (dividing triangle in 4)\
        \nstbtel: local refinement (same as homard but with TELEMAC and limited \
        to one level of refinement if the refinement is limited in a polygon)\
        \nsmesh: remove element in polygon and remesh zone")

    return subparser

def refine_homard_global(input_mesh, output_mesh, refinement_level):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and hydro

    @param input_mesh (str) Name of the mesh to refine (absolute path)
    @param output_mesh (str) Name of the refined mesh
    @param refinement level (int) Number of run of the refinement
    """
    import salome
    salome.salome_init()
    import salome_notebook
    notebook = salome_notebook.NoteBook()
    import HOMARD
    import medcoupling as mc
    homard = salome.lcc.FindOrLoadComponent('FactoryServer', 'HOMARD')
    homard.UpdateStudy()
    #
    # Creation of the hypotheses
    # ==========================
    # Creation of the hypothesis Hypo_1
    Hypo_1 = homard.CreateHypothesis("HypoUnif")
    Hypo_1.SetUnifRefinUnRef(1)
    # check if there is fields in the mesh to add interpolation of field to
    #hypothesis
    if mc.GetAllFieldNames(input_mesh) != ():
        Hypo_1.SetTypeFieldInterp(1)
    #
    # Creation of the cases
    # =====================
    # Creation of the case Case_1
    Case_1 = homard.CreateCase("Case_1", "MESH", input_mesh)
    dir_name = path.dirname(input_mesh)
    Case_1.SetDirName(dir_name)
    Case_1.SetConfType(0)
    Case_1.SetExtType(0)
    #
    # Creation of the iterations
    # ==========================
    # Creation of the iteration Iter_1
    if refinement_level < 1:
        raise Exception("Refinement level must be higher or equal to 1\n"\
                        "Actual value: {}".format(refinement_level))

    for ilevel in range(refinement_level):
        if ilevel == 0:
            iter_1 = Case_1.NextIteration("iter_{}".format(ilevel))
        else:
            iter_1 = iter_1.NextIteration("iter_{}".format(ilevel))
        iter_1.SetFieldFile(input_mesh)
        iter_1.AssociateHypo("HypoUnif")
        iter_1.SetMeshName("MESH")
        tmp_mesh = path.join(dir_name, "maill_{}.med".format(ilevel))
        iter_1.SetMeshFile(tmp_mesh)
        codret = iter_1.Compute(1, 1)
        if codret != 0:
            raise Exception(\
                "Hormard could not compute Iteration {}".format(ilevel))

    shutil.copy2(tmp_mesh, output_mesh)


def refine_homard_local(input_mesh, output_mesh, refinement_level, domain):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and hydro

    @param input_mesh (str) Name of the mesh to refine
    @param output_mesh (str) Name of the refined mesh
    @param refinement level (int) Number of run of the refinement
    @param domain (str) File containing array to refine and their parameter if None is given will refine the whole mesh
    """
    import salome
    salome.salome_init()
    import salome_notebook
    notebook = salome_notebook.NoteBook()
    import medcoupling as mc

    import HOMARD
    homard = salome.lcc.FindOrLoadComponent('FactoryServer', 'HOMARD')
    homard.UpdateStudy()

    field_name = "refine_field"
    #TODO: Identify what they correspond to
    hypo_name = "HypoLocal"
    type_thres=1
    threshold=0.5
    use_field=0

    # ==========================
    # Creation of the hypothesis Hypo
    hypo = homard.CreateHypothesis(hypo_name)
    hypo.SetField("refine_field")
    hypo.SetUseField(use_field)
    hypo.SetUseComp(0)
    hypo.AddComp(field_name)
    hypo.SetRefinThr(type_thres, threshold)
    # check if there is fields in the mesh to add interpolation of field to
    #hypothesis
    if mc.GetAllFieldNames(input_mesh) != ():
        Hypo_1.SetTypeFieldInterp(1)
    # Creation of the case case
    case_1 = homard.CreateCase("case_1", "MESH", input_mesh)
    dir_name = path.dirname(input_mesh)
    case_1.SetDirName(dir_name)
    case_1.SetConfType(0)
    case_1.SetExtType(0)

    field_mesh = path.join(dir_name, "field_mesh.med")
    shutil.copy2(input_mesh, field_mesh)

    for ilevel in range(refinement_level):
        if ilevel == 0:
            iter_1 = case_1.NextIteration("iter_{}".format(ilevel))
        else:
            iter_1 = iter_1.NextIteration("iter_{}".format(ilevel))
        iter_1.SetFieldFile(input_mesh)
        iter_1.AssociateHypo(hypo_name)
        iter_1.SetMeshName("MESH")

        tmp_mesh = path.join(dir_name, "maill_{}.med".format(ilevel))
        iter_1.SetMeshFile(tmp_mesh)

        # Using previously computed mesh as field mesh
        add_refinement_field(field_mesh, domain, field_name=field_name)

        iter_1.SetFieldFile(field_mesh)

        codret = iter_1.Compute(1, 1)

        if codret != 0:
            raise Exception(\
                "Hormard could not compute Iteration {}".format(ilevel))

        field_mesh = tmp_mesh

    shutil.copy2(tmp_mesh, output_mesh)


def add_refinement_field(mesh_file, domain, field_name="refine"):
    """
    Add a field name refine_field that contains 1 for each point in the domain file

    @param mesh_file (str) Initial mesh file
    @param domain (str) Shapefile containing the area in which to refine
    @param output_file (str) Path of the file containing the field
    """
    import medcoupling as mc
    import MEDLoader as ml

    res = ml.ReadMeshFromFile(mesh_file)

    # detecting polygon inner nodes
    field = np.zeros((res.getNumberOfNodes()), dtype=np.float64)
    poly = read_shape_data(domain)[0]
    path0 = mpltPath.Path(poly)
    coord_x = [res.getCoords()[i, 0] for i, _ in enumerate(res.getCoords()[:, 0])]
    coord_y = [res.getCoords()[i, 1] for i, _ in enumerate(res.getCoords()[:, 1])]
    indic = path0.contains_points(list(zip(coord_x, coord_y)))
    field += indic

    # creating a field containing 1 for nodes inside polygon and 0 for the others
    med_field = mc.MEDCouplingFieldDouble.New(mc.ON_NODES, mc.ONE_TIME)
    med_field.setMesh(res)
    mc_array = mc.DataArrayDouble(field.tolist())
    med_field.setTime(0.0, 0, -1)
    med_field.setArray(mc_array)
    med_field.setName(field_name)

    # writting the field
    mc.WriteFieldUsingAlreadyWrittenMesh(mesh_file, med_field)

def refine_stbtel(input_mesh, output_mesh, refinement_level, domain):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and hydro

    @param input_mesh (str) Name of the mesh to refine
    @param output_mesh (str) Name of the refined mesh
    @param domain (str) File containing array to refine and their parameter
        if None is given will refine the whole mesh
    """
    from pretel.extract_contour import extract_contour, write_gis_file
    from create_boundary import create_boundary
    from pretel.stbtel_refine import run_refine
    from os import environ

    #create a file because stbtel must have one to run (even if it is empty)
    open("mock.cli", "w")
    for i in range(refinement_level):
        root_dir = environ.get('HOMETEL')
        run_refine(input_mesh, output_mesh, root_dir, "mock.cli",
                   zone_gis_file=domain)

        remove(".".join(output_mesh.split(".")[:-1]) + ".cli")
        #handle input and output mesh for each step of refinement
        if refinement_level > 1:
            if path.exists("temp_file.slf"):
                remove("temp_file.slf")
            shutil.copy2(output_mesh, "temp_file.slf")
            input_mesh = "temp_file.slf"
            if i != refinement_level - 1:
                remove(output_mesh)

    remove("mock.cli")
    if refinement_level > 1:
        remove("temp_file.slf")


def refine(input_mesh, output_mesh, refinement_level=1, domain=None,
           method="homard"):
    """
    Creating a mesh from shapes and constraint lines through geom and smesh and
    hydro

    @param input_mesh (str) Name of the mesh to refine
    @param output_mesh (str) Name of the refined mesh
    @param refinement level (int) Number of run of the refinement
    @param method (str) refinement method (homard, smesh)
    @param domain (str) File containing array to refine and their parameter
            if None is given will refine the whole mesh
    """
    if method == "homard":
        if domain is None:
            refine_homard_global(path.abspath(input_mesh),
                                 output_mesh, refinement_level)
        else:
            refine_homard_local(path.abspath(input_mesh),
                                output_mesh, refinement_level, domain)
    elif method == "stbtel":
        if domain is not None and refinement_level > 1:
            raise Exception("Refinement level with stbtel in a polygon is"\
                    "limited to 1\nActual value: {}".format(refinement_level))
        refine_stbtel(path.abspath(input_mesh), output_mesh,
                      refinement_level, domain=domain)

