"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
)
from qgis.PyQt.QtCore import QCoreApplication

from q4ts.processing.qgis_utils import (
    get_short_string,
    get_user_manual_url,
    run_python_script_telemac_env,
)


class Slf2MedAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    INPUT_BND = "INPUT_BND"
    OUTPUT_MESH = "OUTPUT_MESH"

    def tr(self, string):
        return QCoreApplication.translate("Convert .slf mesh to .med mesh", string)

    def createInstance(self):
        return Slf2MedAlgorithm()

    def name(self):
        return "slf2med"

    def displayName(self):
        return self.tr("slf2med")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Convert .slf mesh to .med mesh")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_MESH,
                self.tr("Input .slf mesh"),
                QgsProcessingParameterFile.File,
            )
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_BND,
                self.tr("Input boundaries file"),
                QgsProcessingParameterFile.File,
                "cli",
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output .med mesh"), "Mesh layer (*.med)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_file = self.parameterAsFile(parameters, self.INPUT_MESH, context)
        output_mesh_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH, context
        )
        commands = [
            "srf2med",
            input_mesh_file,
        ]

        if self.INPUT_BND in parameters:
            input_bnd_file = self.parameterAsFile(parameters, self.INPUT_BND, context)
            commands.append("-b")
            commands.append(input_bnd_file)

        commands.append(output_mesh_file)
        telemac_return_code = run_python_script_telemac_env(
            "converter.py", commands, feedback
        )
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when converting mesh from slf to med. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh_file}
