
# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""


class SalomeHydroSettings(object):
    """
    SalomeSettings's key names
    """
    # Path to salome exe
    SALOME_BIN = "SALOME_BIN"
    PYTHON_SALOME = "PYTHON_SALOME"
    TELEMAC_ENV_SCRIPT = "TELEMAC_ENV_SCRIPT"

    @staticmethod
    def keys():
        return [
            SalomeHydroSettings.SALOME_BIN,
            SalomeHydroSettings.PYTHON_SALOME,
            SalomeHydroSettings.TELEMAC_ENV_SCRIPT,
        ]
