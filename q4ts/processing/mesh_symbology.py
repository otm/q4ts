"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterMeshLayer,
)
from qgis.PyQt.QtCore import QCoreApplication

from q4ts.processing.qgis_utils import get_short_string, get_user_manual_url
from q4ts.processing.variables_utils import VariablesUtils


class MeshSymbologyAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    CUSTOM_STYLE = "CUSTOM_STYLE"

    def tr(self, string):
        return QCoreApplication.translate("Q4TS mesh symbology processing", string)

    def createInstance(self):
        return MeshSymbologyAlgorithm()

    def name(self):
        return "mesh_symbology"

    def displayName(self):
        return self.tr("Mesh symbology")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Apply Q4TS symbology to mesh layer")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.CUSTOM_STYLE,
                self.tr("Custom style"),
                VariablesUtils().get_variable_custom_style(),
                defaultValue=0,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        mesh_layer = self.parameterAsMeshLayer(parameters, self.INPUT_MESH, context)
        idx = self.parameterAsEnum(parameters, self.CUSTOM_STYLE, context)
        custom_style = VariablesUtils().get_variable_custom_style()[idx]
        VariablesUtils().apply_mesh_symbology(mesh_layer, custom_style)
        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {}
