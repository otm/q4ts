"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterNumber,
    QgsProcessingParameterString,
    QgsProcessingUtils,
)
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    add_shift_parameters,
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_telemac_env,
)


class AlterAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    VARS = "VARS"

    TIMES = "TIMES"
    TIME_FROM = "TFROM"
    TIME_END = "TEND"
    TIME_STEP = "TSTEP"

    SHIFT_X = "SHIFT_X"
    SHIFT_Y = "SHIFT_Y"
    ROTATE = "ROTATE"

    OUTPUT_MESH = "OUTPUT_MESH"

    ROTATE_MIN_VALUE = 0
    ROTATE_MAX_VALUE = 360

    def tr(self, string):
        return QCoreApplication.translate("Mesh alteration", string)

    def createInstance(self):
        return AlterAlgorithm()

    def name(self):
        return "alter"

    def displayName(self):
        return self.tr("alter")

    def icon(self) -> QIcon:
        return QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "alter-icon.png"))

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Mesh alteration")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.VARS,
                self.tr("Variables"),
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.TIMES,
                self.tr("Times"),
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.TIME_FROM,
                self.tr("Time from"),
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.TIME_END,
                self.tr("Time end"),
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.TIME_STEP,
                self.tr("Time step"),
                optional=True,
            )
        )

        add_shift_parameters(self)

        self.addParameter(
            QgsProcessingParameterNumber(
                self.ROTATE,
                self.tr("Angle of rotation (counter clockwise) to apply in degree"),
                type=QgsProcessingParameterNumber.Double,
                optional=True,
                minValue=self.ROTATE_MIN_VALUE,
                maxValue=self.ROTATE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "SERAFIN files (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        output_mesh = self.parameterAsFileOutput(parameters, self.OUTPUT_MESH, context)
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)

        vars = self.parameterAsString(parameters, self.VARS, context)

        times = self.parameterAsString(parameters, self.TIMES, context)
        time_from = self.parameterAsInt(parameters, self.TIME_FROM, context)
        time_end = self.parameterAsInt(parameters, self.TIME_END, context)
        time_step = self.parameterAsInt(parameters, self.TIME_STEP, context)

        shift_x = self.parameterAsDouble(parameters, self.SHIFT_X, context)
        shift_y = self.parameterAsDouble(parameters, self.SHIFT_Y, context)
        rotate = self.parameterAsDouble(parameters, self.ROTATE, context)

        commands = ["alter"]

        if vars:
            commands += ["--vars", vars]
        if times:
            commands += ["--times"]
            # Need to split values so no quote is added with windows .bat
            times_val = times.split(" ")
            for time in times_val:
                commands += [time]

        if self.TIME_FROM in parameters:
            commands += ["-f", str(time_from)]
        if self.TIME_END in parameters:
            commands += ["-s", str(time_end)]
        if self.TIME_STEP in parameters:
            commands += ["-d", str(time_step)]

        if shift_x:
            commands += ["--X+?", str(shift_x)]
        if shift_y:
            commands += ["--Y+?", str(shift_y)]
        if rotate:
            commands += ["--rotate", str(rotate)]

        commands += ["--force"]
        commands += [input_mesh_path, output_mesh]

        telemac_return_code = run_q4ts_script_telemac_env(commands, feedback)
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when altering mesh. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        context.addLayerToLoadOnCompletion(
            output_mesh,
            QgsProcessingContext.LayerDetails(
                "Output mesh field alter",
                context.project(),
                "Alter",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))

        return {"OUTPUT_MESH": output_mesh}
