"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterVectorDestination,
)
from qgis.PyQt.QtCore import QCoreApplication

from q4ts.processing.qgis_utils import (
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_telemac_env,
)


class CountourAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    OUTPUT_COUNTOUR = "OUTPUT_COUNTOUR"

    def tr(self, string):
        return QCoreApplication.translate("Create countour from mesh", string)

    def createInstance(self):
        return CountourAlgorithm()

    def name(self):
        return "contour"

    def displayName(self):
        return self.tr("create contour")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Create a contour from a mesh layer")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT_COUNTOUR, self.tr("Output countour")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        output_countour = self.parameterAsOutputLayer(
            parameters, self.OUTPUT_COUNTOUR, context
        )
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)

        commands = ["contour", input_mesh_path, output_countour]

        telemac_return_code = run_q4ts_script_telemac_env(commands, feedback)
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when creating contour for mesh. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {self.OUTPUT_COUNTOUR: output_countour}
