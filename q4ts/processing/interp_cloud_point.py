"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterDefinition,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterNumber,
    QgsProcessingParameterString,
    QgsProcessingUtils,
)
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    add_shift_parameters,
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_telemac_env,
    save_as_temp_shp,
)
from q4ts.processing.variables_utils import VariablesUtils


class InterpolationCloudPointAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    CLOUD_POINT_FILE = "CLOUD_POINT_FILE"
    VAR_NAME = "VAR_NAME"
    VAR_UNIT = "VAR_UNIT"
    KIND = "KIND"
    SHIFT_X = "SHIFT_X"
    SHIFT_Y = "SHIFT_Y"
    ZONE_SHP = "ZONE_SHP"
    DEFAULT_VALUE = "DEFAULT_VALUE"
    OUTPUT_MESH = "OUTPUT_MESH"
    KIND_ENUM = ["linear", "nearest", "cubic"]
    DELIMITER = "DELIMITER"
    RECORD = "RECORD"
    OUTSIDE_ZONE = "OUTSIDE_ZONE"
    NMAP = "NMAP"
    CHUNK_INTERP = "CHUNK_INTERP"
    CHUNK_X_PARTITION = "CHUNK_X_PARTITION"
    CHUNK_Y_PARTITION = "CHUNK_Y_PARTITION"
    CHUNK_OVERLAP = "CHUNK_OVERLAP"
    BLOCK_PTS = "BLOCK_PTS"

    def tr(self, string):
        return QCoreApplication.translate(
            "Mesh interpolation from cloud point for Q4TS", string
        )

    def createInstance(self):
        return InterpolationCloudPointAlgorithm()

    def name(self):
        return "interp_cloud_point"

    def displayName(self):
        return self.tr("interpolation cloud point")

    def icon(self) -> QIcon:
        return QIcon(
            str(
                DIR_PLUGIN_ROOT / "resources" / "images" / "interp-point-cloud-icon.png"
            )
        )

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(
            self.name(), "Mesh interpolation from cloud point for Q4TS"
        )

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.CLOUD_POINT_FILE,
                self.tr("Input point cloud"),
                fileFilter="Point cloud file (*.xyz)",
            )
        )
        self.addParameter(
            QgsProcessingParameterString(
                self.DELIMITER, self.tr("Delimiter"), defaultValue=";"
            )
        )

        VariablesUtils.addProcessingParameterStringCustomWidget(
            self,
            self.VAR_NAME,
            self.tr("Variable name"),
            VariablesUtils.VariableNameWidget,
        )

        VariablesUtils.addProcessingParameterStringCustomWidget(
            self,
            self.VAR_UNIT,
            self.tr("Variable unit"),
            VariablesUtils.VariableUnitWidget,
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.ZONE_SHP,
                self.tr("Area layer"),
                [QgsProcessing.TypeVectorPolygon],
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.OUTSIDE_ZONE, self.tr("Apply outside area")
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.DEFAULT_VALUE,
                self.tr("Default value"),
                optional=True,
                type=QgsProcessingParameterNumber.Double,
                minValue=VariablesUtils.VAL_MIN_VALUE,
                maxValue=VariablesUtils.VAL_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.KIND, self.tr("Interpolation"), self.KIND_ENUM, defaultValue=0
            )
        )

        add_shift_parameters(self)

        param = QgsProcessingParameterNumber(
            self.RECORD,
            self.tr("Record"),
            type=QgsProcessingParameterNumber.Integer,
            defaultValue=-1,
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(
            self.NMAP, self.tr("Use memory map numpy")
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(
            self.CHUNK_INTERP, self.tr("Chunk interpolation")
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        param = QgsProcessingParameterNumber(
            self.CHUNK_X_PARTITION,
            self.tr("Chunk x partition"),
            optional=True,
            type=QgsProcessingParameterNumber.Integer,
            minValue=0.0,
            maxValue=VariablesUtils.INT_MAX_VALUE,
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)
        param = QgsProcessingParameterNumber(
            self.CHUNK_Y_PARTITION,
            self.tr("Chunk y partition"),
            optional=True,
            type=QgsProcessingParameterNumber.Integer,
            minValue=0.0,
            maxValue=VariablesUtils.INT_MAX_VALUE,
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        param = QgsProcessingParameterNumber(
            self.CHUNK_OVERLAP,
            self.tr("Chunk overlap"),
            optional=True,
            type=QgsProcessingParameterNumber.Double,
            minValue=0.0,
            maxValue=VariablesUtils.VAL_MAX_VALUE,
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        param = QgsProcessingParameterNumber(
            self.BLOCK_PTS,
            self.tr("Block size"),
            optional=True,
            type=QgsProcessingParameterNumber.Integer,
            minValue=0.0,
            maxValue=VariablesUtils.INT_MAX_VALUE,
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH,
                self.tr("Output mesh"),
                "SERAFIN files (*.slf)",
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        cloud_point_path = self.parameterAsFile(
            parameters, self.CLOUD_POINT_FILE, context
        )
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        var_name = self.parameterAsString(parameters, self.VAR_NAME, context)
        var_unit = self.parameterAsString(parameters, self.VAR_UNIT, context)
        zone_shp = self.parameterAsVectorLayer(parameters, self.ZONE_SHP, context)

        default_value = None
        if self.DEFAULT_VALUE in parameters:
            default_value = self.parameterAsDouble(
                parameters, self.DEFAULT_VALUE, context
            )

        shift_x = self.parameterAsDouble(parameters, self.SHIFT_X, context)
        shift_y = self.parameterAsDouble(parameters, self.SHIFT_Y, context)
        output_mesh = self.parameterAsFileOutput(parameters, self.OUTPUT_MESH, context)
        kind = self.KIND_ENUM[self.parameterAsEnum(parameters, self.KIND, context)]
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)
        delimiter = self.parameterAsString(parameters, self.DELIMITER, context)
        record = self.parameterAsInt(parameters, self.RECORD, context)
        outside_zone = self.parameterAsBool(parameters, self.OUTSIDE_ZONE, context)
        nmap = self.parameterAsBool(parameters, self.NMAP, context)
        chunk_interp = self.parameterAsBool(parameters, self.CHUNK_INTERP, context)

        commands = [
            "interpolate",
            input_mesh_path,
            output_mesh,
            cloud_point_path,
            "--delimiter",
            delimiter,
        ]

        commands += ["--varname", var_name]
        commands += ["--varunit", var_unit]
        commands += ["--kind", kind]
        commands += ["--record", str(record)]

        if zone_shp:
            temp_shp_file = save_as_temp_shp(zone_shp)
            commands += ["--zone", temp_shp_file]

        if outside_zone:
            commands += ["--outside-zone"]

        if default_value is not None:
            commands += ["--default-value", str(default_value)]

        if shift_x:
            commands += ["--shift-x", str(shift_x)]
        if shift_y:
            commands += ["--shift-y", str(shift_y)]

        if nmap:
            commands += ["--mmap"]
        elif chunk_interp:
            commands += ["--chunk-interp"]
            block_pts = self.parameterAsInt(parameters, self.BLOCK_PTS, context)
            if block_pts:
                commands += ["--blockpts", str(block_pts)]
            else:
                commands += [
                    "--chunk-x-partitions",
                    str(
                        self.parameterAsInt(parameters, self.CHUNK_X_PARTITION, context)
                    ),
                ]
                commands += [
                    "--chunk-y-partitions",
                    str(
                        self.parameterAsInt(parameters, self.CHUNK_Y_PARTITION, context)
                    ),
                ]
                commands += [
                    "--chunk-overlap",
                    str(
                        self.parameterAsDouble(parameters, self.CHUNK_OVERLAP, context)
                    ),
                ]

        telemac_return_code = run_q4ts_script_telemac_env(commands, feedback)
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when interpolating point cloud for mesh. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        context.addLayerToLoadOnCompletion(
            output_mesh,
            QgsProcessingContext.LayerDetails(
                "Output mesh cloud point interpolation",
                context.project(),
                "Cloud point interpolation",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh}
