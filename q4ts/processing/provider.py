from processing.core.ProcessingConfig import ProcessingConfig, Setting
from qgis.core import QgsProcessingProvider

from q4ts.processing.alter import AlterAlgorithm
from q4ts.processing.contour import CountourAlgorithm
from q4ts.processing.create_boundary import CreateBoundaryAlgorithm
from q4ts.processing.create_mesh import CreateMeshAlgorithm
from q4ts.processing.cut_mesh import CutMeshAlgorithm
from q4ts.processing.extend_mesh import ExtendMeshAlgorithm
from q4ts.processing.interp_cloud_point import InterpolationCloudPointAlgorithm
from q4ts.processing.med2slf import Med2SlfAlgorithm
from q4ts.processing.mesh_symbology import MeshSymbologyAlgorithm
from q4ts.processing.projection_clc import ProjectionCorineLandCoverAlgorithm
from q4ts.processing.projection_field import ProjectionFieldAlgorithm
from q4ts.processing.refine import RefineAlgorithm
from q4ts.processing.settings import SalomeHydroSettings
from q4ts.processing.slf2med import Slf2MedAlgorithm


class Provider(QgsProcessingProvider):
    def load(self):
        group = self.name()
        ProcessingConfig.addSetting(
            Setting(
                group, SalomeHydroSettings.SALOME_BIN, self.tr("Salome executable"), ""
            )
        )
        ProcessingConfig.addSetting(
            Setting(
                group,
                SalomeHydroSettings.PYTHON_SALOME,
                self.tr("Python Salome (only for windows)"),
                "",
            )
        )
        ProcessingConfig.addSetting(
            Setting(
                group,
                SalomeHydroSettings.TELEMAC_ENV_SCRIPT,
                self.tr("Telemac " "environnement " "script"),
                "",
            )
        )
        ProcessingConfig.readSettings()
        self.refreshAlgorithms()
        return True

    def unload(self):
        for setting in SalomeHydroSettings.keys():
            ProcessingConfig.removeSetting(setting)

    def loadAlgorithms(self, *args, **kwargs):
        self.addAlgorithm(CreateMeshAlgorithm())
        self.addAlgorithm(CreateBoundaryAlgorithm())
        self.addAlgorithm(CountourAlgorithm())
        self.addAlgorithm(InterpolationCloudPointAlgorithm())
        self.addAlgorithm(ProjectionCorineLandCoverAlgorithm())
        self.addAlgorithm(Med2SlfAlgorithm())
        self.addAlgorithm(ProjectionFieldAlgorithm())
        self.addAlgorithm(AlterAlgorithm())
        self.addAlgorithm(Slf2MedAlgorithm())
        self.addAlgorithm(RefineAlgorithm())
        self.addAlgorithm(MeshSymbologyAlgorithm())
        self.addAlgorithm(CutMeshAlgorithm())
        self.addAlgorithm(ExtendMeshAlgorithm())

    def id(self, *args, **kwargs):
        """The ID of your plugin, used for identifying the provider.

        This string should be a unique, short, character only string,
        eg "qgis" or "gdal". This string should not be localised.
        """
        return "q4ts"

    def name(self, *args, **kwargs):
        """The human friendly name of your plugin in Processing.

        This string should be as short as possible (e.g. "Lastools", not
        "Lastools version 1.0.1 64-bit") and localised.
        """
        return self.tr("Q4TS")

    def icon(self):
        """Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QgsProcessingProvider.icon(self)
