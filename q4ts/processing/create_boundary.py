"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
from qgis import processing
from qgis.core import (
    QgsFeature,
    QgsField,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterNumber,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_telemac_env,
    save_as_temp_shp,
)


class CreateBoundaryAlgorithm(QgsProcessingAlgorithm):
    BOUNDARIES_LAYER = "BOUNDARIES_LAYER"
    CONTOUR_LAYER = "CONTOUR_LAYER"
    DEFAULT_BND = "DEFAULT_BND"
    CHECK_BOUNDARIES = "CHECK_BOUNDARIES"
    INPUT_MESH = "INPUT_MESH"
    OUTPUT_MESH = "OUTPUT_MESH"

    BOUNDARIES_FIELDS = [QgsField("bnd_type", QVariant.Int)]

    DEFAULT_BND_MIN_VAL = 1000
    DEFAULT_BND_MAX_VAL = 9999
    DEFAULT_BND_INIT_VAL = 2222

    def tr(self, string):
        return QCoreApplication.translate("Create boundary for Q4TS", string)

    def createInstance(self):
        return CreateBoundaryAlgorithm()

    def name(self):
        return "create_boundary"

    def displayName(self):
        return self.tr("create boundary")

    def icon(self) -> QIcon:
        return QIcon(
            str(DIR_PLUGIN_ROOT / "resources" / "images" / "create-boundary-icon.png")
        )

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(
            self.name(), "Create a boundary mesh layer for Q4TS use"
        )

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.CONTOUR_LAYER,
                self.tr("Contour layer"),
                [QgsProcessing.TypeVectorPolygon],
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.BOUNDARIES_LAYER,
                self.tr("Boundaries layer"),
                [QgsProcessing.TypeVectorLine],
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.DEFAULT_BND,
                self.tr("Default bound value"),
                defaultValue=self.DEFAULT_BND_INIT_VAL,
                minValue=self.DEFAULT_BND_MIN_VAL,
                maxValue=self.DEFAULT_BND_MAX_VAL,
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.CHECK_BOUNDARIES,
                self.tr("Check boundaries layer"),
                defaultValue=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "SERAFIN files (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        contour_layer = self.parameterAsVectorLayer(
            parameters, self.CONTOUR_LAYER, context
        )
        boundary_layer = self.parameterAsVectorLayer(
            parameters, self.BOUNDARIES_LAYER, context
        )
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        default_bnd = self.parameterAsInt(parameters, self.DEFAULT_BND, context)
        output_mesh = self.parameterAsFileOutput(parameters, self.OUTPUT_MESH, context)
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)

        if self.parameterAsBool(parameters, self.CHECK_BOUNDARIES, context):
            self._check_boundary_layer(boundary_layer, contour_layer)

        temp_contour_shp_file = save_as_temp_shp(contour_layer)
        temp_boundary_shp_file = save_as_temp_shp(boundary_layer)

        commands = [
            "create_boundary",
            input_mesh_path,
            output_mesh,
            temp_contour_shp_file,
            "--boundaries",
            temp_boundary_shp_file,
            "--default-bnd",
            str(default_bnd),
        ]
        telemac_return_code = run_q4ts_script_telemac_env(commands, feedback)
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when creating boundaries for mesh. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh}

    def _check_boundary_layer(
        self, boundary_layer: QgsVectorLayer, contour_layer: QgsVectorLayer
    ):
        self._check_boundary_layer_fields(boundary_layer)
        self._check_boundary_layer_feature(boundary_layer, contour_layer)

    def _check_boundary_layer_fields(self, boundary_layer: QgsVectorLayer):
        fields = boundary_layer.fields()
        for field in self.BOUNDARIES_FIELDS:
            if fields.indexFromName(field.name()) == -1:
                raise QgsProcessingException(
                    self.tr(
                        f"Field {field.name()} is not a available in boundary layer."
                    )
                )

    def _check_boundary_layer_feature(
        self, boundary_layer: QgsVectorLayer, contour_layer: QgsVectorLayer
    ):
        result = processing.run(
            "native:polygonstolines",
            {"INPUT": contour_layer, "OUTPUT": "TEMPORARY_OUTPUT"},
        )

        line_layer = result["OUTPUT"]

        # TODO : distance depends on CRS. 1 could be too large if CRS in WGS84
        result = processing.run(
            "native:buffer",
            {
                "INPUT": line_layer,
                "DISTANCE": 1,
                "SEGMENTS": 5,
                "END_CAP_STYLE": 0,
                "JOIN_STYLE": 0,
                "MITER_LIMIT": 2,
                "DISSOLVE": False,
                "OUTPUT": "TEMPORARY_OUTPUT",
            },
        )
        buffer_layer = result["OUTPUT"]

        features = boundary_layer.getFeatures()
        for feature in features:
            feature_layer = self._clone_layer_with_feature(boundary_layer, feature)
            self._check_feature_layer(buffer_layer, feature_layer)

    def _check_feature_layer(self, buffer_layer, feature_layer):
        result = processing.run(
            "native:extractvertices",
            {"INPUT": feature_layer, "OUTPUT": "TEMPORARY_OUTPUT"},
        )
        point_layer = result["OUTPUT"]
        nb_points = point_layer.featureCount()

        result = processing.run(
            "native:countpointsinpolygon",
            {
                "POLYGONS": buffer_layer,
                "POINTS": point_layer,
                "WEIGHT": "",
                "CLASSFIELD": "",
                "FIELD": "NUMPOINTS",
                "OUTPUT": "TEMPORARY_OUTPUT",
            },
        )
        count_layer = result["OUTPUT"]

        self._check_nb_point_in_count_layer(count_layer, nb_points)

    def _check_nb_point_in_count_layer(self, count_layer, nb_points):
        feature_contained = False
        for result_poly in count_layer.getFeatures():
            numpoints = result_poly.attribute("NUMPOINTS")
            if numpoints == nb_points:
                feature_contained = True
                break
        if not feature_contained:
            raise QgsProcessingException(
                self.tr(
                    "Invalid feature for boundary line. A line point is not contained in contour layer.\nCheck "
                    "if layer have correct CRS."
                )
            )

    @staticmethod
    def _clone_layer_with_feature(
        boundary_layer: QgsVectorLayer, feature: QgsFeature
    ) -> QgsVectorLayer:
        feature_layer = QgsVectorLayer("MultiLinestring", "temp", "memory")
        feature_layer.setCrs(boundary_layer.crs())
        feature_layer.startEditing()
        data = feature_layer.dataProvider()
        data.addFeatures([feature])
        feature_layer.commitChanges()
        return feature_layer
