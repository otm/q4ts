"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
import sys

from qgis import processing
from qgis.core import (
    QgsField,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterMultipleLayers,
    QgsProcessingParameterNumber,
    QgsProcessingUtils,
    QgsVectorLayer,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    get_layer,
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_salome_env,
    save_as_temp_shp,
)


class ExtendMeshAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    CONTOUR_LAYER = "CONTOUR_LAYER"
    EXTENDED_CONTOUR_LAYER = "EXTENDED_CONTOUR_LAYER"

    OTHER_MESH = "OTHER_MESH"
    OTHER_CONTOUR_LAYER = "OTHER_CONTOUR_LAYER"
    ISLAND_LAYER = "ISLAND_LAYER"
    CONSTRAINT_LINE_LAYER = "CONSTRAINT_LINE_LAYER"
    REFINE_LAYER = "REFINE_LAYER"
    MESH_MIN_SIZE = "MESH_MIN_SIZE"
    MESH_MAX_SIZE = "MESH_MAX_SIZE"
    MESH_SIZE = "MESH_SIZE"
    MESH_GROWTH_RATE = " MESH_GROWTH_RATE"
    MESH_ENGINE = "MESH_ENGINE"
    OUTPUT_MESH = "OUTPUT_MESH"
    TEMP_MED = "TEMP_MED"
    OTHER_TEMP_MED = "OTHER_TEMP_MED"

    NAME_FIELD = [QgsField("name", QVariant.String)]

    REFINE_FIELDS = [
        QgsField("name", QVariant.String),
        QgsField("min_size", QVariant.Double),
        QgsField("phy_size", QVariant.Double),
        QgsField("max_size", QVariant.Double),
        QgsField("growth_rate", QVariant.Double),
    ]

    MESH_SIZE_MIN_VALUE = 1e-6
    MESH_SIZE_MAX_VALUE = sys.float_info.max
    MESH_SIZE_DECIMAL = 6
    MESH_SIZE_DEFAULT_VALUE = 50
    MESH_MIN_SIZE_DEFAULT_VALUE = 25.0
    MESH_MAX_SIZE_DEFAULT_VALUE = 100.0
    MESH_GROWTH_RATE_MIN_VALUE = 1.0
    MESH_GROWTH_RATE_MAX_VALUE = sys.float_info.max
    MESH_GROWTH_RATE_DEFAULT_VALUE = 1.3

    MESH_ENGINE_ENUM = (
        ["netgen_1d_2d", "mg_cadsurf"] if sys.platform == "linux" else ["netgen_1d_2d"]
    )

    def __init__(self):
        super().__init__()
        self.input_layers = {
            self.EXTENDED_CONTOUR_LAYER: {
                "display": self.tr("Extended contour layer"),
                "type": [QgsProcessing.TypeVectorPolygon],
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "Contour_v2.qml",
                "directory": "contour",
                "optional": False,
            },
            self.CONTOUR_LAYER: {
                "display": self.tr("Input mesh contour layer"),
                "type": [QgsProcessing.TypeVectorPolygon],
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "Contour_v2.qml",
                "directory": "contour",
                "optional": False,
            },
            self.OTHER_CONTOUR_LAYER: {
                "display": self.tr("Other mesh contour layer"),
                "type": [QgsProcessing.TypeVectorPolygon],
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "Contour_v2.qml",
                "directory": "contour",
                "optional": True,
            },
        }
        self.multiple_input_layers = {
            self.ISLAND_LAYER: {
                "display": self.tr("Island layer"),
                "type": QgsProcessing.TypeVectorPolygon,
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "Ile_v2.qml",
                "optional": True,
                "directory": "island",
                "fields": self.NAME_FIELD,
            },
            self.CONSTRAINT_LINE_LAYER: {
                "display": self.tr("Constraint lines layer"),
                "type": QgsProcessing.TypeVectorLine,
                "type_wkb": QgsWkbTypes.LineString,
                "style": "LignesDeContrainte_v2.qml",
                "optional": True,
                "directory": "constraint_line",
                "fields": self.NAME_FIELD,
            },
            self.REFINE_LAYER: {
                "display": self.tr("Refinement area"),
                "type": QgsProcessing.TypeVectorPolygon,
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "ZoneRaffinement-Deraffinement_v2.qml",
                "optional": True,
                "directory": "refinement",
                "fields": self.REFINE_FIELDS,
                "edit_form": "refine_layer_feature_widget.ui",
            },
        }

    def tr(self, string):
        return QCoreApplication.translate("Extend mesh", string)

    def createInstance(self):
        return ExtendMeshAlgorithm()

    def name(self):
        return "extend_mesh"

    def displayName(self):
        return self.tr("extend mesh")

    def icon(self) -> QIcon:
        return QIcon(
            str(DIR_PLUGIN_ROOT / "resources" / "images" / "create-mesh-icon.png")
        )

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Extend a mesh layer")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )
        self.addParameter(
            QgsProcessingParameterMeshLayer(
                self.OTHER_MESH, self.tr("Other mesh"), optional=True
            )
        )
        for layer_name, input_layer in self.input_layers.items():
            self.addParameter(
                QgsProcessingParameterFeatureSource(
                    layer_name,
                    input_layer["display"],
                    input_layer["type"],
                    optional=input_layer["optional"],
                )
            )

        for layer_name, input_layer in self.multiple_input_layers.items():
            self.addParameter(
                QgsProcessingParameterMultipleLayers(
                    layer_name,
                    input_layer["display"],
                    input_layer["type"],
                    optional=input_layer["optional"],
                )
            )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_SIZE,
                self.tr("Mesh size"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_SIZE_DEFAULT_VALUE,
                minValue=self.MESH_SIZE_MIN_VALUE,
                maxValue=self.MESH_SIZE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_MIN_SIZE,
                self.tr("Mesh min. size"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_MIN_SIZE_DEFAULT_VALUE,
                minValue=self.MESH_SIZE_MIN_VALUE,
                maxValue=self.MESH_SIZE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_MAX_SIZE,
                self.tr("Mesh max. size"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_MAX_SIZE_DEFAULT_VALUE,
                minValue=self.MESH_SIZE_MIN_VALUE,
                maxValue=self.MESH_SIZE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_GROWTH_RATE,
                self.tr("Mesh growth rate"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_GROWTH_RATE_DEFAULT_VALUE,
                minValue=self.MESH_GROWTH_RATE_MIN_VALUE,
                maxValue=self.MESH_GROWTH_RATE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.MESH_ENGINE,
                self.tr("Mesh engine"),
                self.MESH_ENGINE_ENUM,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.TEMP_MED, self.tr("Temporary .med file"), "Mesh layer (*.med)"
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OTHER_TEMP_MED,
                self.tr("Temporary .med file"),
                "Mesh layer (*.med)",
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "Mesh layer (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)

        layers = {}
        for layer_name, input_layer in self.input_layers.items():
            layers[layer_name] = get_layer(
                self, layer_name, context, parameters, optional=input_layer["optional"]
            )

        for layer_name, input_layer in self.multiple_input_layers.items():
            layers[layer_name] = self.parameterAsLayerList(
                parameters, layer_name, context
            )
            if not layers[layer_name] and not input_layer["optional"]:
                raise QgsProcessingException(
                    self.invalidSourceError(parameters, layer_name)
                )

        temp_med_file = self.parameterAsFileOutput(parameters, self.TEMP_MED, context)
        output_mesh_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH, context
        )

        # Convert to .med
        processing.run(
            "q4ts:slf2med",
            {"INPUT_MESH": input_mesh_path, "OUTPUT_MESH": temp_med_file},
            context=context,
            feedback=feedback,
        )

        input_extent = temp_med_file
        output_extent = temp_med_file

        mesh_size = self.parameterAsDouble(parameters, self.MESH_SIZE, context)
        mesh_min_size = self.parameterAsDouble(parameters, self.MESH_MIN_SIZE, context)
        mesh_max_size = self.parameterAsDouble(parameters, self.MESH_MAX_SIZE, context)
        mesh_engine = self.MESH_ENGINE_ENUM[
            self.parameterAsEnum(parameters, self.MESH_ENGINE, context)
        ]
        growth_rate = self.parameterAsDouble(parameters, self.MESH_GROWTH_RATE, context)

        countour_shp_file = save_as_temp_shp(layers[self.CONTOUR_LAYER])
        extended_countour_shp_file = save_as_temp_shp(
            layers[self.EXTENDED_CONTOUR_LAYER]
        )

        cmd = [
            "extend_mesh",
            input_extent,
            countour_shp_file,
            extended_countour_shp_file,
            "-dx",
            mesh_size,
            "-min",
            mesh_min_size,
            "-max",
            mesh_max_size,
            "-gr",
            growth_rate,
            output_extent,
            "--mesh_engine",
            mesh_engine,
        ]

        other_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.OTHER_MESH, context
        )
        if other_mesh_layer:
            other_temp_med_file = self.parameterAsFileOutput(
                parameters, self.OTHER_TEMP_MED, context
            )

            other_contour_layer = layers[self.OTHER_CONTOUR_LAYER]

            # Check if mesh intersect by checking contour layers
            intersection_result = processing.run(
                "qgis:intersection",
                {
                    "INPUT": layers[self.CONTOUR_LAYER],
                    "OVERLAY": other_contour_layer,
                    "OUTPUT": "TEMPORARY_OUTPUT",
                },
                context=context,
                feedback=feedback,
            )["OUTPUT"]

            if intersection_result.featureCount() == 0:
                other_mesh_path = get_selafin_layer_file_name(other_mesh_layer)
                # Convert to .med
                processing.run(
                    "q4ts:slf2med",
                    {"INPUT_MESH": other_mesh_path, "OUTPUT_MESH": other_temp_med_file},
                    context=context,
                    feedback=feedback,
                )
            else:
                context_cut = QgsProcessingContext()
                # Need to cut other mesh to keep input mesh data
                cut_result = processing.run(
                    "q4ts:cut_mesh",
                    {
                        "INPUT_MESH": other_mesh_layer,
                        "INPUT_CONTOUR": layers[self.CONTOUR_LAYER],
                        "TEMP_MED": "TEMPORARY_OUTPUT",
                        "OUTPUT_MESH": "TEMPORARY_OUTPUT",
                        "OUTPUT_MESH_MED": other_temp_med_file,
                    },
                    context=context_cut,
                    feedback=feedback,
                )
                # Create cut mesh contour for extend input
                other_contour_layer_tmp_path = processing.run(
                    "q4ts:contour",
                    {
                        "INPUT_MESH": cut_result["OUTPUT_MESH"],
                        "OUTPUT_COUNTOUR": "TEMPORARY_OUTPUT",
                    },
                    context=context,
                    feedback=feedback,
                )["OUTPUT_COUNTOUR"]
                other_contour_layer = QgsVectorLayer(other_contour_layer_tmp_path)

            cmd += ["--other-mesh"]
            cmd.append(other_temp_med_file)

            cmd += ["--other-contour-shp"]
            cmd.append(save_as_temp_shp(other_contour_layer))

        if layers[self.CONSTRAINT_LINE_LAYER]:
            cmd += ["--constraints"]
            for layer in layers[self.CONSTRAINT_LINE_LAYER]:
                cmd.append(save_as_temp_shp(layer))

        if layers[self.ISLAND_LAYER]:
            cmd += ["--isles"]
            for layer in layers[self.ISLAND_LAYER]:
                cmd.append(save_as_temp_shp(layer))

        if layers[self.REFINE_LAYER]:
            cmd += ["--refine"]
            for layer in layers[self.REFINE_LAYER]:
                cmd.append(save_as_temp_shp(layer))

        salome_return_code = run_q4ts_script_salome_env(cmd, feedback)
        if salome_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by salome when extending mesh. "
                    "Check logs for more informations.".format(salome_return_code)
                )
            )

        if not feedback.isCanceled():
            context_conversion = QgsProcessingContext()
            processing.run(
                "q4ts:med2slf",
                {"INPUT_MESH": output_extent, "OUTPUT_MESH": output_mesh_file},
                context=context_conversion,
                feedback=feedback,
            )

        context.addLayerToLoadOnCompletion(
            output_mesh_file,
            QgsProcessingContext.LayerDetails(
                "Extend slf mesh",
                context.project(),
                "Extend slf mesh",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh_file}
