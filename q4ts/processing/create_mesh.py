"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
import sys

from qgis import processing
from qgis.core import (
    QgsField,
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMultipleLayers,
    QgsProcessingParameterNumber,
    QgsWkbTypes,
)
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    get_layer,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_salome_env,
    save_as_temp_shp,
)


class CreateMeshAlgorithm(QgsProcessingAlgorithm):
    CONTOUR_LAYER = "CONTOUR_LAYER"
    ISLAND_LAYER = "ISLAND_LAYER"
    CONSTRAINT_LINE_LAYER = "CONSTRAINT_LINE_LAYER"
    REFINE_LAYER = "REFINE_LAYER"
    MESH_MIN_SIZE = "MESH_MIN_SIZE"
    MESH_MAX_SIZE = "MESH_MAX_SIZE"
    MESH_SIZE = "MESH_SIZE"
    MESH_GROWTH_RATE = " MESH_GROWTH_RATE"
    MESH_ENGINE = "MESH_ENGINE"
    OUTPUT_MESH = "OUTPUT_MESH"
    TEMP_MED = "TEMP_MED"

    NAME_FIELD = [QgsField("name", QVariant.String)]

    REFINE_FIELDS = [
        QgsField("name", QVariant.String),
        QgsField("min_size", QVariant.Double),
        QgsField("phy_size", QVariant.Double),
        QgsField("max_size", QVariant.Double),
        QgsField("growth_rate", QVariant.Double),
    ]

    MESH_SIZE_MIN_VALUE = 1e-6
    MESH_SIZE_MAX_VALUE = sys.float_info.max
    MESH_SIZE_DECIMAL = 6
    MESH_SIZE_DEFAULT_VALUE = 50
    MESH_MIN_SIZE_DEFAULT_VALUE = 25.0
    MESH_MAX_SIZE_DEFAULT_VALUE = 100.0
    MESH_GROWTH_RATE_MIN_VALUE = 1.0
    MESH_GROWTH_RATE_MAX_VALUE = sys.float_info.max
    MESH_GROWTH_RATE_DEFAULT_VALUE = 1.3

    MESH_ENGINE_ENUM = (
        ["netgen_1d_2d", "mg_cadsurf"] if sys.platform == "linux" else ["netgen_1d_2d"]
    )

    def __init__(self):
        super().__init__()
        self.input_layers = {
            self.CONTOUR_LAYER: {
                "display": self.tr("Contour layer"),
                "type": [QgsProcessing.TypeVectorPolygon],
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "Contour_v2.qml",
                "directory": "contour",
                "optional": False,
            }
        }
        self.multiple_input_layers = {
            self.ISLAND_LAYER: {
                "display": self.tr("Island layer"),
                "type": QgsProcessing.TypeVectorPolygon,
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "Ile_v2.qml",
                "optional": True,
                "directory": "island",
                "fields": self.NAME_FIELD,
            },
            self.CONSTRAINT_LINE_LAYER: {
                "display": self.tr("Constraint lines layer"),
                "type": QgsProcessing.TypeVectorLine,
                "type_wkb": QgsWkbTypes.LineString,
                "style": "LignesDeContrainte_v2.qml",
                "optional": True,
                "directory": "constraint_line",
                "fields": self.NAME_FIELD,
            },
            self.REFINE_LAYER: {
                "display": self.tr("Refinement area"),
                "type": QgsProcessing.TypeVectorPolygon,
                "type_wkb": QgsWkbTypes.Polygon,
                "style": "ZoneRaffinement-Deraffinement_v2.qml",
                "optional": True,
                "directory": "refinement",
                "fields": self.REFINE_FIELDS,
                "edit_form": "refine_layer_feature_widget.ui",
            },
        }

    def tr(self, string):
        return QCoreApplication.translate("Create mesh for Q4TS", string)

    def createInstance(self):
        return CreateMeshAlgorithm()

    def name(self):
        return "create_mesh"

    def displayName(self):
        return self.tr("create mesh")

    def icon(self) -> QIcon:
        return QIcon(
            str(DIR_PLUGIN_ROOT / "resources" / "images" / "create-mesh-icon.png")
        )

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Create a mesh layer for Q4TS use")

    def initAlgorithm(self, config=None):
        for layer_name, input_layer in self.input_layers.items():
            self.addParameter(
                QgsProcessingParameterFeatureSource(
                    layer_name,
                    input_layer["display"],
                    input_layer["type"],
                    optional=input_layer["optional"],
                )
            )

        for layer_name, input_layer in self.multiple_input_layers.items():
            self.addParameter(
                QgsProcessingParameterMultipleLayers(
                    layer_name,
                    input_layer["display"],
                    input_layer["type"],
                    optional=input_layer["optional"],
                )
            )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_SIZE,
                self.tr("Mesh size"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_SIZE_DEFAULT_VALUE,
                minValue=self.MESH_SIZE_MIN_VALUE,
                maxValue=self.MESH_SIZE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_MIN_SIZE,
                self.tr("Mesh min. size"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_MIN_SIZE_DEFAULT_VALUE,
                minValue=self.MESH_SIZE_MIN_VALUE,
                maxValue=self.MESH_SIZE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_MAX_SIZE,
                self.tr("Mesh max. size"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_MAX_SIZE_DEFAULT_VALUE,
                minValue=self.MESH_SIZE_MIN_VALUE,
                maxValue=self.MESH_SIZE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.MESH_ENGINE,
                self.tr("Mesh engine"),
                self.MESH_ENGINE_ENUM,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.MESH_GROWTH_RATE,
                self.tr("Mesh growth rate"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=self.MESH_GROWTH_RATE_DEFAULT_VALUE,
                minValue=self.MESH_GROWTH_RATE_MIN_VALUE,
                maxValue=self.MESH_GROWTH_RATE_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.TEMP_MED, self.tr("Temporary .med file"), "Mesh layer (*.med)"
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "Mesh layer (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        layers = {}
        for layer_name, input_layer in self.input_layers.items():
            layers[layer_name] = get_layer(
                self, layer_name, context, parameters, optional=input_layer["optional"]
            )

        for layer_name, input_layer in self.multiple_input_layers.items():
            layers[layer_name] = self.parameterAsLayerList(
                parameters, layer_name, context
            )
            if not layers[layer_name] and not input_layer["optional"]:
                raise QgsProcessingException(
                    self.invalidSourceError(parameters, layer_name)
                )

        temp_med_file = self.parameterAsFileOutput(parameters, self.TEMP_MED, context)
        output_mesh_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH, context
        )
        mesh_size = self.parameterAsDouble(parameters, self.MESH_SIZE, context)
        mesh_min_size = self.parameterAsDouble(parameters, self.MESH_MIN_SIZE, context)
        mesh_max_size = self.parameterAsDouble(parameters, self.MESH_MAX_SIZE, context)
        mesh_engine = self.MESH_ENGINE_ENUM[
            self.parameterAsEnum(parameters, self.MESH_ENGINE, context)
        ]
        growth_rate = self.parameterAsDouble(parameters, self.MESH_GROWTH_RATE, context)

        countour_shp_file = save_as_temp_shp(layers[self.CONTOUR_LAYER])

        cmd = [
            "create_mesh",
            countour_shp_file,
            "-dx",
            mesh_size,
            "-min",
            mesh_min_size,
            "-max",
            mesh_max_size,
            "-gr",
            growth_rate,
            temp_med_file,
            "--mesh_engine",
            mesh_engine,
        ]

        if layers[self.CONSTRAINT_LINE_LAYER]:
            cmd += ["--constraints"]
            for layer in layers[self.CONSTRAINT_LINE_LAYER]:
                cmd.append(save_as_temp_shp(layer))

        if layers[self.ISLAND_LAYER]:
            cmd += ["--isles"]
            for layer in layers[self.ISLAND_LAYER]:
                cmd.append(save_as_temp_shp(layer))

        if layers[self.REFINE_LAYER]:
            cmd += ["--refine"]
            for layer in layers[self.REFINE_LAYER]:
                cmd.append(save_as_temp_shp(layer))

        salome_return_code = run_q4ts_script_salome_env(cmd, feedback)
        if salome_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by salome when creating mesh. "
                    "Check logs for more informations.".format(salome_return_code)
                )
            )

        if not feedback.isCanceled():
            processing.run(
                "q4ts:med2slf",
                {"INPUT_MESH": temp_med_file, "OUTPUT_MESH": output_mesh_file},
                context=context,
                feedback=feedback,
            )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh_file}
