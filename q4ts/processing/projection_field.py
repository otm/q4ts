"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterCrs,
    QgsProcessingParameterDefinition,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterField,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterNumber,
    QgsProcessingUtils,
)
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    add_shift_parameters,
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_telemac_env,
    save_as_temp_shp,
)
from q4ts.processing.variables_utils import VariablesUtils


class ProjectionFieldAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    VAR_NAME = "VAR_NAME"
    VAR_UNIT = "VAR_UNIT"
    ZONE_SHP = "ZONE_SHP"
    ATTR_NAME = "ATTR_NAME"
    FIELD_VALUE = "FIELD_VALUE"
    DEFAULT_VALUE = "DEFAULT_VALUE"
    EPSG = "EPSG"
    SHIFT_X = "SHIFT_X"
    SHIFT_Y = "SHIFT_Y"
    RECORD = "RECORD"
    OUTPUT_MESH = "OUTPUT_MESH"

    def tr(self, string):
        return QCoreApplication.translate(
            "Mesh projection from input field for Q4TS", string
        )

    def createInstance(self):
        return ProjectionFieldAlgorithm()

    def name(self):
        return "projection_field"

    def displayName(self):
        return self.tr("Projection field")

    def icon(self) -> QIcon:
        return QIcon(
            str(DIR_PLUGIN_ROOT / "resources" / "images" / "projection-field-icon.png")
        )

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(
            self.name(), "Mesh projection from input field for Q4TS"
        )

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        VariablesUtils.addProcessingParameterStringCustomWidget(
            self,
            self.VAR_NAME,
            self.tr("Variable name"),
            VariablesUtils.VariableNameWidget,
        )

        VariablesUtils.addProcessingParameterStringCustomWidget(
            self,
            self.VAR_UNIT,
            self.tr("Variable unit"),
            VariablesUtils.VariableUnitWidget,
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.ZONE_SHP,
                self.tr("Area layer"),
                [QgsProcessing.TypeVectorPolygon],
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterField(
                self.ATTR_NAME,
                self.tr("Layer field"),
                None,
                self.ZONE_SHP,
                QgsProcessingParameterField.Any,
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.FIELD_VALUE,
                self.tr("Field value"),
                type=QgsProcessingParameterNumber.Double,
                optional=True,
                minValue=VariablesUtils.VAL_MIN_VALUE,
                maxValue=VariablesUtils.VAL_MAX_VALUE,
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.DEFAULT_VALUE,
                self.tr("Default value"),
                type=QgsProcessingParameterNumber.Double,
                optional=True,
                minValue=VariablesUtils.VAL_MIN_VALUE,
                maxValue=VariablesUtils.VAL_MAX_VALUE,
            )
        )
        self.addParameter(QgsProcessingParameterCrs(self.EPSG, self.tr("CRS")))

        add_shift_parameters(self)

        param = QgsProcessingParameterNumber(
            self.RECORD,
            self.tr("Record"),
            type=QgsProcessingParameterNumber.Integer,
            defaultValue=-1,
        )
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "SERAFIN files (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        var_name = self.parameterAsString(parameters, self.VAR_NAME, context)
        var_unit = self.parameterAsString(parameters, self.VAR_UNIT, context)
        zone_shp = self.parameterAsVectorLayer(parameters, self.ZONE_SHP, context)
        attr_name = self.parameterAsString(parameters, self.ATTR_NAME, context)
        field_value = self.parameterAsDouble(parameters, self.FIELD_VALUE, context)

        default_value = None
        if self.DEFAULT_VALUE in parameters:
            default_value = self.parameterAsDouble(
                parameters, self.DEFAULT_VALUE, context
            )

        shift_x = self.parameterAsDouble(parameters, self.SHIFT_X, context)
        shift_y = self.parameterAsDouble(parameters, self.SHIFT_Y, context)
        epsg = self.parameterAsCrs(parameters, self.EPSG, context)
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        output_mesh = self.parameterAsFileOutput(parameters, self.OUTPUT_MESH, context)
        record = self.parameterAsInt(parameters, self.RECORD, context)
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)

        commands = ["field", input_mesh_path, output_mesh]
        commands += ["--varname", var_name]
        commands += ["--varunit", var_unit]
        commands += ["--record", str(record)]

        if zone_shp:
            temp_shp_file = save_as_temp_shp(zone_shp)
            commands += ["--zone-shp", temp_shp_file]
            if attr_name:
                commands += ["--attr-name", attr_name]
        if field_value:
            commands += ["--field-value", str(field_value)]

        if default_value is not None:
            commands += ["--default-value", str(default_value)]

        commands += ["--epsg", epsg.authid()]
        if shift_x:
            commands += ["--shift-x", str(shift_x)]
        if shift_y:
            commands += ["--shift-y", str(shift_y)]

        telemac_return_code = run_q4ts_script_telemac_env(commands, feedback)
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when interpolating field for mesh. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        context.addLayerToLoadOnCompletion(
            output_mesh,
            QgsProcessingContext.LayerDetails(
                "Output mesh field projection",
                context.project(),
                "Field projection",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh}
