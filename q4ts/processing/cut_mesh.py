"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis import processing
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingUtils,
)
from qgis.PyQt.QtCore import QCoreApplication

from q4ts.processing.qgis_utils import (
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_salome_env,
    save_as_temp_shp,
)


class CutMeshAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    INPUT_CONTOUR = "INPUT_CONTOUR"
    OUTPUT_MESH = "OUTPUT_MESH"
    OUTPUT_MESH_MED = "OUTPUT_MESH_MED"
    TEMP_MED = "TEMP_MED"

    def tr(self, string):
        return QCoreApplication.translate("Cut Mesh", string)

    def createInstance(self):
        return CutMeshAlgorithm()

    def name(self):
        return "cut_mesh"

    def displayName(self):
        return self.tr("cut mesh")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Cut mesh from a input contour")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_CONTOUR,
                self.tr("Contour layer"),
                [QgsProcessing.TypeVectorPolygon],
                optional=False,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.TEMP_MED, self.tr("Temporary .med file"), "Mesh layer (*.med)"
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH_MED, self.tr("Output mesh .med"), "Mesh layer (*.med)"
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "Mesh layer (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)
        temp_med_file = self.parameterAsFileOutput(parameters, self.TEMP_MED, context)
        output_mesh_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH, context
        )

        output_mesh_med_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH_MED, context
        )
        # Convert to .med
        processing.run(
            "q4ts:slf2med",
            {"INPUT_MESH": input_mesh_path, "OUTPUT_MESH": temp_med_file},
            context=context,
            feedback=feedback,
        )

        contour_layer = self.parameterAsVectorLayer(
            parameters, self.INPUT_CONTOUR, context
        )
        countour_shp_file = save_as_temp_shp(contour_layer)

        # Cut mesh
        cut_cmd = ["cut_mesh", temp_med_file, countour_shp_file, output_mesh_med_file]
        salome_return_code = run_q4ts_script_salome_env(cut_cmd, feedback)
        if salome_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by salome when cutting mesh. "
                    "Check logs for more informations.".format(salome_return_code)
                )
            )

        if not feedback.isCanceled():
            context_conversion = QgsProcessingContext()
            processing.run(
                "q4ts:med2slf",
                {"INPUT_MESH": output_mesh_med_file, "OUTPUT_MESH": output_mesh_file},
                context=context_conversion,
                feedback=feedback,
            )

        context.addLayerToLoadOnCompletion(
            output_mesh_file,
            QgsProcessingContext.LayerDetails(
                "Cut slf mesh",
                context.project(),
                "Cut slf mesh",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {
            self.OUTPUT_MESH: output_mesh_file,
            self.OUTPUT_MESH_MED: output_mesh_med_file,
        }
