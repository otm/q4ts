@ECHO OFF
@SETLOCAL

REM Get Telemac environnement script and wanted python script
set TELEMAC_ENV_SCRIPT=%1
set PYTHON_SCRIPT=%2
SHIFT
SHIFT

REM All other parts are argument that must be passed to the python script
SET ALL=%*

REM Remove telemac environnement and python script characters
call :strlen ALL_SIZE ALL
call :strlen TELEMAC_SIZE TELEMAC_ENV_SCRIPT
call :strlen PYTHON_SIZE PYTHON_SCRIPT
set /a SHIFT_SIZE=%TELEMAC_SIZE%+%PYTHON_SIZE%+2


CALL SET ALL_WITHOUT_SHIFT=%%ALL:~%SHIFT_SIZE%,%ALL_SIZE%%%
echo %ALL_WITHOUT_SHIFT%

call %TELEMAC_ENV_SCRIPT%
%PYTHON_SCRIPT% %ALL_WITHOUT_SHIFT%



REM ********* function *****************************
:strlen <resultVar> <stringVar>
(
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        set "len=1"
        for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
            if "!tmp:~%%P,1!" NEQ "" (
                set /a "len+=%%P"
                set "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        set len=0
    )
)
(
    endlocal
    set "%~1=%len%"
    exit /b
)
