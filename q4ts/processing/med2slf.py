"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProcessingUtils,
)
from qgis.PyQt.QtCore import QCoreApplication

from q4ts.processing.qgis_utils import (
    get_short_string,
    get_user_manual_url,
    run_python_script_telemac_env,
)


class Med2SlfAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    OUTPUT_MESH = "OUTPUT_MESH"

    def tr(self, string):
        return QCoreApplication.translate("Convert .med mesh to .slf mesh", string)

    def createInstance(self):
        return Med2SlfAlgorithm()

    def name(self):
        return "med2slf"

    def displayName(self):
        return self.tr("med2slf")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Convert .med mesh to .slf mesh")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_MESH,
                self.tr("Input .med mesh"),
                QgsProcessingParameterFile.File,
                "med",
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output .slf mesh"), "Mesh layer (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_file = self.parameterAsFile(parameters, self.INPUT_MESH, context)
        output_mesh_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH, context
        )

        commands = [
            "med2srfd",
            input_mesh_file,
            output_mesh_file,
        ]

        telemac_return_code = run_python_script_telemac_env(
            "converter.py", commands, feedback
        )
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when converting mesh from med to slf. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        context.addLayerToLoadOnCompletion(
            output_mesh_file,
            QgsProcessingContext.LayerDetails(
                "Converted slf mesh",
                context.project(),
                "Converted slf mesh",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))

        return {}
