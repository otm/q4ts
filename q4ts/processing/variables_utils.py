import sys
from dataclasses import dataclass, field
from enum import Enum
from typing import List

from processing.gui.wrappers import WidgetWrapper
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QComboBox
from qgis.core import (
    QgsColorRamp,
    QgsColorRampShader,
    QgsMeshDatasetGroupMetadata,
    QgsMeshDatasetIndex,
    QgsMeshLayer,
    QgsMeshRendererScalarSettings,
    QgsProcessingParameterString,
    QgsStyle,
)

from q4ts.toolbelt import PlgLogger


@dataclass
class VariableCustomColorRamp:
    # Color ramp name to get color
    ramp_name: str
    invert_ramp: bool = False
    # Color ramp shader is continuous or by interval
    continuous: bool = True
    # Color ramp shader is discrete or linear (exact not supported)
    discrete: bool = False
    # Steps for automatic values definition
    below_ten_step: float = 1.0
    above_ten_step: float = 10.0
    # Min value is transparent (opacity 0.0)
    min_is_transparent: bool = False
    # Fixed values for color ramp and used labels
    fixed_values: List = field(default_factory=lambda: [])
    fixed_values_labels: List = field(default_factory=lambda: [])
    # Remove some fixed value for color ramp
    unused_fixed_values: List = field(default_factory=lambda: [])


class VectorDatasetSplit(Enum):
    ALONG = "along"
    SUIVANT = "suivant"
    UV = "uv"


class VariablesUtils:
    SHIFT_MIN_VALUE = -sys.float_info.max
    SHIFT_MAX_VALUE = sys.float_info.max
    SHIFT_DECIMAL = 6

    VAL_MIN_VALUE = -sys.float_info.max
    VAL_MAX_VALUE = sys.float_info.max
    VAL_DECIMAL = 2

    # Maximum value supported for QSpinBox
    INT_MAX_VALUE = 2147483647

    def __init__(self):
        self.log = PlgLogger().log
        self.VAR_NAME_LIST = [
            self.tr("BOTTOM"),
            self.tr("BOTTOM FRICTION"),
            self.tr("FREE SURFACE"),
            self.tr("WATER DEPTH"),
            self.tr("VELOCITY U"),
            self.tr("VELOCITY V"),
            self.tr("WIND ALONG X"),
            self.tr("WIND ALONG Y"),
        ]

        self.VAR_UNIT_LIST = [self.tr("M"), self.tr("M1/3/S"), self.tr("M/S")]

        self.VAR_NAME_UNIT = {
            self.tr("BOTTOM"): self.tr("M"),
            self.tr("BOTTOM FRICTION"): self.tr("M1/3/S"),
            self.tr("FREE SURFACE"): self.tr("M"),
            self.tr("WATER DEPTH"): self.tr("M"),
            self.tr("VELOCITY U"): self.tr("M/S"),
            self.tr("VELOCITY V"): self.tr("M/S"),
            self.tr("WIND ALONG X"): self.tr("M/S"),
            self.tr("WIND ALONG Y"): self.tr("M/S"),
        }

        # Variable with custom styles
        self.VAR_STYLES = {
            self.tr("BOTTOM FRICTION"): ["Min", "Max", "Moy"]
        }

        # BOTTOM FRICTION specific color ramp with Min/Max/Moy style

        # Each style has same fixed values so the same color is used for the same value
        bottom_friction_fixed_values = [5.0, 8.0, 10.0, 12.0, 13.0, 15.0, 18.0, 20.0, float('inf')]

        # Define specific labels and unused values for each style
        min_bottom_friction_fixed_values_label = ["Zones à forte urbanisation : Ks = 5",
                                                  "Zones à faible urbanisation, d'arbustes, de sous-bois : Ks = 8",
                                                  "Zones de champs cultivés à végétation haute : Ks = 10",
                                                  "Zones d'arbustes, de sous-bois : Ks = 12",  # Not used
                                                  "Zones de champs cultivés à végétation haute : Ks = 13",  # Not used
                                                  "Zones de champs cultivés à végétation basse : Ks = 15",
                                                  "Zones de champs cultivés à végétation basse : Ks = 18",  # Not used
                                                  "Zones de champs, prairies sans culture : Ks = 20",
                                                  "Lit mineur"]
        min_bottom_friction_unused_fixed_values = [12.0, 13.0, 18.0]

        max_bottom_friction_fixed_values_label = ["Zones à forte urbanisation : Ks = 5",  # Not used
                                                  "Zones à forte urbanisation : Ks = 8",
                                                  "Zones à faible urbanisation : Ks = 10",
                                                  "Zones d'arbustes, de sous-bois : Ks = 12",
                                                  "Zones de champs cultivés à végétation haute : Ks = 13",  # Not used
                                                  "Zones de champs cultivés à végétation haute : Ks = 15",
                                                  "Zones de champs cultivés à végétation basse : Ks = 18",  # Not used
                                                  "Zones de champs cultivés à végétation basse et de champs, prairies sans culture : Ks = 20",
                                                  "Lit mineur"]
        max_bottom_friction_unused_fixed_values = [5.0, 13.0, 18.0]

        moy_bottom_friction_fixed_values_label = ["Zones à forte urbanisation : Ks = 5",
                                                  "Zones à faible urbanisation : Ks = 8",
                                                  "Zones d'arbustes, de sous-bois : Ks = 10",
                                                  "Zones d'arbustes, de sous-bois : Ks = 12",
                                                  "Zones de champs cultivés à végétation haute : Ks = 13",
                                                  "Zones de champs cultivés à végétation haute : Ks = 15",
                                                  "Zones de champs cultivés à végétation basse : Ks = 18",
                                                  "Zones de champs, prairies sans culture : Ks = 20",
                                                  "Lit mineur"]
        moy_bottom_friction_unused_fixed_values = [12.0, 15.0]

        self.VAR_CUSTOM_COLOR_RAMP_MAP = {
            self.tr("BOTTOM"): VariableCustomColorRamp(ramp_name="BrBG", invert_ramp=True,
                                                       continuous=True),
            self.tr("BOTTOM FRICTION" + "Min"): VariableCustomColorRamp(ramp_name="BrBG", invert_ramp=False,
                                                                        continuous=False, discrete=True,
                                                                        fixed_values=bottom_friction_fixed_values,
                                                                        fixed_values_labels=min_bottom_friction_fixed_values_label,
                                                                        unused_fixed_values=min_bottom_friction_unused_fixed_values),
            self.tr("BOTTOM FRICTION" + "Max"): VariableCustomColorRamp(ramp_name="BrBG", invert_ramp=False,
                                                                        continuous=False, discrete=True,
                                                                        fixed_values=bottom_friction_fixed_values,
                                                                        fixed_values_labels=max_bottom_friction_fixed_values_label,
                                                                        unused_fixed_values=max_bottom_friction_unused_fixed_values),
            self.tr("BOTTOM FRICTION" + "Moy"): VariableCustomColorRamp(ramp_name="BrBG", invert_ramp=False,
                                                                        continuous=False, discrete=True,
                                                                        fixed_values=bottom_friction_fixed_values,
                                                                        fixed_values_labels=moy_bottom_friction_fixed_values_label,
                                                                        unused_fixed_values=moy_bottom_friction_unused_fixed_values),
            self.tr("FREE SURFACE"): VariableCustomColorRamp(ramp_name="RdBu", invert_ramp=True,
                                                             continuous=True),
            self.tr("WATER DEPTH"): VariableCustomColorRamp(ramp_name="Blues", invert_ramp=False,
                                                            continuous=False,
                                                            fixed_values=[0.0, 0.01, 0.1, 0.2, 0.5, 0.7, 1.0],
                                                            below_ten_step=1, above_ten_step=5,
                                                            min_is_transparent=True),
            self.tr("VELOCITY U"): VariableCustomColorRamp(ramp_name="Turbo", invert_ramp=False,
                                                           continuous=False,
                                                           fixed_values=[0.0, 1e-6, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0],
                                                           below_ten_step=1, above_ten_step=1,
                                                           min_is_transparent=True),
            self.tr("VELOCITY V"): VariableCustomColorRamp(ramp_name="Turbo", invert_ramp=False,
                                                           continuous=False,
                                                           fixed_values=[0.0, 1e-6, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0],
                                                           below_ten_step=1, above_ten_step=1,
                                                           min_is_transparent=True),
            self.tr("WIND ALONG X"): VariableCustomColorRamp(ramp_name="Turbo", invert_ramp=False,
                                                             continuous=False, fixed_values=[0.0, 1e-6, 10.0],
                                                             below_ten_step=10, above_ten_step=10,
                                                             min_is_transparent=True),
            self.tr("WIND ALONG Y"): VariableCustomColorRamp(ramp_name="Turbo", invert_ramp=False,
                                                             continuous=False, fixed_values=[0.0, 1e-6, 10.0],
                                                             below_ten_step=10, above_ten_step=10,
                                                             min_is_transparent=True),
        }

    def tr(self, string):
        return QCoreApplication.translate("Q4TS VariablesUtils", string)

    def get_variable_custom_style(self) -> [str]:
        """
        Return list of available custom style for variable

        @return: ([str]) list of available custom style
        """
        res = []
        for variable, styles in self.VAR_STYLES.items():
            res += styles
        return res

    def add_variable_style(self, mesh_layer: QgsMeshLayer, variable: str, variable_unit: str) -> None:
        """
        Add style for variable to QgsMeshLayer

        @param mesh_layer:  (QgsMeshLayer) mesh layer to be updated
        @param variable:  (str) variable name (with style name for some variable)
        @param variable_unit:  (str) variable unit (needed to get dataset name)
        """
        if variable in self.VAR_CUSTOM_COLOR_RAMP_MAP:
            custom_ramp = self.VAR_CUSTOM_COLOR_RAMP_MAP[variable]

            # Transform variable name from MDAL selafin reader convention
            variable_used = variable
            adjust = 16
            if variable_used.startswith("WIND"):
                variable_used = variable_used[0: len(variable_used) - len("ALONG X")]
                adjust = adjust - len("ALONG X")
            elif variable_used.startswith("VELOCITY"):
                variable_used = variable_used[0: len(variable_used) - len(" U")]
                adjust = adjust - len(" U")
            elif variable_used.startswith("BOTTOM FRICTION"):
                variable_used = variable_used.replace("Min", "")
                variable_used = variable_used.replace("Max", "")
                variable_used = variable_used.replace("Moy", "")

            qgis_variable_name = variable_used.lower().ljust(adjust, ' ') + variable_unit.lower().replace("/", "")

            renderer_settings = mesh_layer.rendererSettings()
            dataset_index = self._get_dataset_index(mesh_layer, qgis_variable_name)

            # Get color ramp
            ramp = self._get_color_ramp(custom_ramp)
            scalar_setting = self._define_scalar_settings(custom_ramp, mesh_layer, dataset_index, ramp)
            renderer_settings.setScalarSettings(dataset_index.group(), scalar_setting)
            renderer_settings.setActiveScalarDatasetGroup(dataset_index.group())
            mesh_layer.setRendererSettings(renderer_settings)

    def _get_dataset_index(self, mesh_layer : QgsMeshLayer, qgis_variable_name : str) -> QgsMeshDatasetIndex:
        for index in mesh_layer.datasetGroupsIndexes():
            index_group = QgsMeshDatasetIndex(index)
            dataset_metadata = mesh_layer.datasetGroupMetadata(index_group)
            name = dataset_metadata.name()
            if name == qgis_variable_name:
                return index_group
        return QgsMeshDatasetIndex()

    def apply_mesh_symbology(self, mesh_layer: QgsMeshLayer, custom_style: str) -> None:
        """
        Apply Q4TS mesh symbology from current mesh layer dataset name

        @param mesh_layer: (QgsMeshLayer) input mesh layer
        @param custom_style: (str) custom style for variable
        """
        renderer_settings = mesh_layer.rendererSettings()

        for index in mesh_layer.datasetGroupsIndexes():
            index_group = QgsMeshDatasetIndex(index)
            dataset_metadata = mesh_layer.datasetGroupMetadata(index_group)

            # Convert dataset name to selafin variable
            selafin_variable = ""
            if dataset_metadata.isVector():
                for member in VectorDatasetSplit:
                    x_variable, y_variable = self.convert_vector_dataset_name_to_selafin_variables(dataset_metadata, member)
                    if x_variable.upper() in self.VAR_NAME_LIST:
                        selafin_variable = x_variable
                        break
                    if y_variable.upper() in self.VAR_NAME_LIST:
                        selafin_variable = y_variable
                        break
            else:
                selafin_variable = self.convert_dataset_name_to_selafin_variable(dataset_metadata)

            # Selafin variable stored uppercase in VariableUtils
            selafin_variable = selafin_variable.upper()
            if selafin_variable in self.VAR_NAME_LIST:

                # Add custom style to get custom color ramp
                if selafin_variable in self.VAR_STYLES:
                    # Use defined custom style or first available
                    variable_custom_style = self.VAR_STYLES[selafin_variable]
                    var_style = custom_style if custom_style in variable_custom_style else variable_custom_style[0]
                    # Add to selafin variable
                    selafin_variable = selafin_variable + var_style

                custom_ramp = self.VAR_CUSTOM_COLOR_RAMP_MAP[selafin_variable]

                # Get color ramp
                ramp = self._get_color_ramp(custom_ramp)
                scalar_setting = self._define_scalar_settings(custom_ramp, mesh_layer, index_group, ramp)
                renderer_settings.setScalarSettings(index_group.group(), scalar_setting)

        mesh_layer.setRendererSettings(renderer_settings)

    def convert_vector_dataset_name_to_selafin_variables(self, dataset_metadata: QgsMeshDatasetGroupMetadata,
                                                         vector_dataset_split: VectorDatasetSplit) -> (str, str):
        """
        Convert vector dataset name to selafin x and y variables.

        Remove unit added by MDAL to dataset name.
        Add a suffix to dataset name because MDAL is removing that suffix for a single dataset storage as vector dataset

        @param dataset_metadata: (QgsMeshDatasetGroupMetadata) dataset metadata
        @param vector_dataset_split: (VectorDatasetSplit) enum to define how to split vector dataset
        @return: (str,str) x variable, y variable
        """
        name = dataset_metadata.name()
        self.log(message=f"Original QgsMeshLayer dataset name : {name}", log_level=0)

        # Special treatment of name to remove units and split vector into variables
        if name.startswith("velocity"):
            name = "velocity"
            vector_dataset_split = VectorDatasetSplit.UV
        elif name.startswith("vitesse"):
            name = "vitesse"
            vector_dataset_split = VectorDatasetSplit.UV

        if vector_dataset_split == VectorDatasetSplit.UV:
            x_variable = "u"
            y_variable = "v"
        elif vector_dataset_split == VectorDatasetSplit.ALONG:
            x_variable = "along x"
            y_variable = "along y"
        else:
            x_variable = "suivant x"
            y_variable = "suivant y"

        # Restore original name by removing units

        # Add x_variable (same size as y)
        temp_name = x_variable + name
        # Remove unit by keeping only first 16 chars
        temp_varname = temp_name[0: min(len(temp_name), 16)]
        # Remove x_variable and extra spaces
        name = temp_varname.replace(x_variable, "").strip()

        self.log(message=f"Selafin variable used for x variable : {name} {x_variable}", log_level=0)
        self.log(message=f"Selafin variable used for y variable : {name} {y_variable}", log_level=0)
        return f"{name} {x_variable}", f"{name} {y_variable}"

    def convert_dataset_name_to_selafin_variable(self, dataset_metadata: QgsMeshDatasetGroupMetadata) -> str:
        """
        Convert dataset name to selafin variable.

        Remove unit added by MDAL to dataset name.

        @param dataset_metadata: (QgsMeshDatasetGroupMetadata) dataset metadata
        @return: (str) selafin variable
        """
        name = dataset_metadata.name()
        self.log(message=f"Original QgsMeshLayer dataset name : {name}", log_level=0)

        # Remove unit by keeping only first 16 chars
        name = name[0: min(len(name), 16)]
        # Remove extra spaces
        name = name.strip()

        self.log(message=f"Selafin variable used for x variable : {name}", log_level=0)
        return name

    def _define_scalar_settings(self, custom_ramp: VariableCustomColorRamp,
                                mesh_layer: QgsMeshLayer,
                                dataset_index: QgsMeshDatasetIndex,
                                ramp: QgsColorRamp):
        # Get current scalar settings
        scalar_setting = QgsMeshRendererScalarSettings()
        scalar_setting.setDataResamplingMethod(0)  # QgsMeshRendererScalarSettings.None

        # Update color ramp
        color_ramp = scalar_setting.colorRampShader()

        # Define min/max from value or fixed values
        metadata = mesh_layer.datasetGroupMetadata(dataset_index)
        if len(custom_ramp.fixed_values):
            min = custom_ramp.fixed_values[0]
        else:
            min = metadata.minimum()
        max = metadata.maximum()

        # Define classification mode
        if custom_ramp.continuous:
            color_ramp.setClassificationMode(QgsColorRampShader.Continuous)
        else:
            color_ramp.setClassificationMode(QgsColorRampShader.EqualInterval)

        scalar_setting.setClassificationMinimumMaximum(min, max)
        color_ramp.setMinimumValue(min)
        color_ramp.setMaximumValue(max)
        # Create color ramp item
        newLst = self._create_color_ramp_item(custom_ramp, max, min, ramp)
        color_ramp.setColorRampItemList(newLst)

        if custom_ramp.discrete:
            color_ramp.setColorRampType(QgsColorRampShader.Discrete)
        else:
            color_ramp.setColorRampType(QgsColorRampShader.Interpolated)

        color_ramp.setSourceColorRamp(ramp)
        scalar_setting.setColorRampShader(color_ramp)
        return scalar_setting

    def _create_color_ramp_item(self, custom_ramp: VariableCustomColorRamp,
                                max: float, min: float,
                                ramp: QgsColorRamp) -> [QgsColorRampShader.ColorRampItem]:
        if custom_ramp.continuous:
            # Use value from color ramp
            newLst = []
            nb_classes = ramp.count()
            for i in range(0, nb_classes):
                value_ramp = ramp.value(i)
                value = min + value_ramp * (max - min)
                color = ramp.color(value_ramp)
                newLst.append(QgsColorRampShader.ColorRampItem(value, color, f"{value}"))
        else:
            # Define values from fixed values
            newLst = []
            values = []

            fixed_values = custom_ramp.fixed_values
            below_ten_step = custom_ramp.below_ten_step
            above_ten_step = custom_ramp.above_ten_step

            values += fixed_values
            # If no fixed values, start at minimum value
            if len(fixed_values):
                val = fixed_values[len(fixed_values) - 1]
            else:
                val = min

            while val < max:
                step = below_ten_step if val < 10 else above_ten_step
                val = val + step
                values.append(val)

            for i, value in enumerate(values):
                if value not in custom_ramp.unused_fixed_values:
                    val = i / len(values)
                    color = ramp.color(val)
                    if len(custom_ramp.fixed_values_labels):
                        label = custom_ramp.fixed_values_labels[i]
                    else:
                        label = f"{value}"
                    newLst.append(QgsColorRampShader.ColorRampItem(value, color, label))

            if custom_ramp.min_is_transparent:
                newLst[0].color.setAlphaF(0.0)
        return newLst

    def _get_color_ramp(self, custom_ramp: VariableCustomColorRamp) -> QgsColorRamp:
        ramp_name = custom_ramp.ramp_name
        default_style = QgsStyle().defaultStyle()
        ramp = default_style.colorRamp(ramp_name)
        if custom_ramp.invert_ramp:
            ramp.invert()
        return ramp

    @staticmethod
    def addProcessingParameterStringCustomWidget(processing, name, display, widget):
        param = QgsProcessingParameterString(name, display)
        param.setMetadata({"widget_wrapper": {"class": widget}})
        processing.addParameter(param)

    class VariableNameWidget(WidgetWrapper):
        def createWidget(self):
            algo = VariablesUtils()
            self.combobox = QComboBox()
            self.combobox.setEditable(True)
            if algo.VAR_NAME_LIST:
                self.combobox.addItems(algo.VAR_NAME_LIST)

            return self.combobox

        def value(self):
            return self.combobox.currentText()

    class VariableUnitWidget(WidgetWrapper):
        def createWidget(self):
            algo = VariablesUtils()
            self.combobox = QComboBox()
            self.combobox.setEditable(True)
            if algo.VAR_UNIT_LIST:
                self.combobox.addItems(algo.VAR_UNIT_LIST)

            return self.combobox

        def value(self):
            return self.combobox.currentText()
