"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
import os
from pathlib import Path

from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterField,
    QgsProcessingParameterFile,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingUtils,
)
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    add_shift_parameters,
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_telemac_env,
    save_as_temp_shp,
)


class ProjectionCorineLandCoverAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    STRICKLER_TABLE = "STRICKLER_TABLE"
    CLC_DATA_FILE = "CLC_DATA_FILE"
    ZONE_SHP = "ZONE_SHP"
    ATTR_NAME = "ATTR_NAME"
    WRITE_CLC_CODE = "WRITE_CLC_CODE"
    ZONE_SHIFT = "ZONE_SHIFT"
    SHIFT_X = "SHIFT_X"
    SHIFT_Y = "SHIFT_Y"
    OUTPUT_MESH = "OUTPUT_MESH"

    current_dir = os.path.dirname(os.path.realpath(__file__))
    DEFAULT_STRICKLER_TABLE = str(
        Path(current_dir + "/../data/def_strickler_table.txt").resolve()
    )

    def tr(self, string):
        return QCoreApplication.translate(
            "Projection of Corine Land Cover data in mesh from friction table.", string
        )

    def createInstance(self):
        return ProjectionCorineLandCoverAlgorithm()

    def name(self):
        return "projection_corine_land_cover"

    def displayName(self):
        return self.tr("Projection Corine Land Cover")

    def icon(self) -> QIcon:
        return QIcon(
            str(
                DIR_PLUGIN_ROOT
                / "resources"
                / "images"
                / "projection-corine-land-cover-icon.png"
            )
        )

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(
            self.name(), "Corine Land Cover projection on mesh for Q4TS"
        )

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.STRICKLER_TABLE,
                self.tr("Friction table"),
                defaultValue=self.DEFAULT_STRICKLER_TABLE,
                fileFilter="Friction table (*.txt)",
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.CLC_DATA_FILE,
                self.tr("Corine Land Cover Data file"),
                types=[QgsProcessing.TypeVectorPolygon],
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.ZONE_SHP,
                self.tr("Area layer"),
                [QgsProcessing.TypeVectorPolygon],
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterField(
                self.ATTR_NAME,
                self.tr("Layer field"),
                None,
                self.ZONE_SHP,
                QgsProcessingParameterField.Any,
                optional=True,
            )
        )

        add_shift_parameters(self)

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.WRITE_CLC_CODE, self.tr("Write CLC Code")
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(self.ZONE_SHIFT, self.tr("Shift area"))
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "SERAFIN files (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        output_mesh = self.parameterAsFileOutput(parameters, self.OUTPUT_MESH, context)
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)
        clc_data_file_layer = self.parameterAsLayer(
            parameters, self.CLC_DATA_FILE, context
        )
        strickler_table_file_path = self.parameterAsFile(
            parameters, self.STRICKLER_TABLE, context
        )
        zone_shp = self.parameterAsVectorLayer(parameters, self.ZONE_SHP, context)
        attr_name = self.parameterAsString(parameters, self.ATTR_NAME, context)
        shift_x = self.parameterAsDouble(parameters, self.SHIFT_X, context)
        shift_y = self.parameterAsDouble(parameters, self.SHIFT_Y, context)
        write_clc_code = self.parameterAsBool(parameters, self.WRITE_CLC_CODE, context)
        zone_shift = self.parameterAsBool(parameters, self.ZONE_SHIFT, context)

        clc_data_file_path = self._get_layer_file(
            clc_data_file_layer, self.CLC_DATA_FILE
        )

        commands = [
            "clc",
            input_mesh_path,
            output_mesh,
            strickler_table_file_path,
            "--clc_data_file",
            clc_data_file_path,
        ]
        if zone_shp:
            temp_shp_file = save_as_temp_shp(zone_shp)
            commands += ["--zone", temp_shp_file]
            if attr_name:
                commands += ["--zone-attribute", attr_name]

        if shift_x or shift_y:
            commands += ["--offset", str(shift_x), str(shift_y)]

        if write_clc_code:
            commands += ["--write-clc-code"]
        if zone_shift:
            commands += ["--zone-offset"]

        telemac_return_code = run_q4ts_script_telemac_env(commands, feedback)
        if telemac_return_code:
            raise QgsProcessingException(
                self.tr(
                    "Error code '{}' returned by telemac when projecting cld data for mesh. "
                    "Check logs for more informations.".format(telemac_return_code)
                )
            )

        context.addLayerToLoadOnCompletion(
            output_mesh,
            QgsProcessingContext.LayerDetails(
                "Output mesh with CLC projection",
                context.project(),
                "CLC projection",
                QgsProcessingUtils.LayerHint.Mesh,
            ),
        )
        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh}

    def _get_layer_file(self, layer, layer_name):
        file_path = layer.dataProvider().dataSourceUri()
        index = file_path.rfind("|")
        if index != -1:
            file_path = file_path[:index]
        if not os.path.exists(file_path):
            raise QgsProcessingException(
                self.tr(
                    f"Layer for {layer_name} is not a inside a file. Memory or database "
                    "layer are not supported"
                )
            )
        return file_path
