import locale
import os
import subprocess
import sys
import tempfile
from pathlib import Path

from qgis.PyQt.QtWidgets import QFileDialog, QInputDialog
from processing.core.ProcessingConfig import ProcessingConfig
from qgis import processing
from qgis.core import (
    QgsProcessingException,
    QgsProcessingParameterNumber,
    QgsProject,
    QgsMeshLayer,
    QgsVectorLayer,
    QgsDataProvider
)

from q4ts.processing.settings import SalomeHydroSettings
from q4ts.processing.variables_utils import VariablesUtils
from q4ts.toolbelt import PlgTranslator

BASE_SETTINGS_KEY = "/Q4TS/plugin"
GPKG_FILENAME = "/gpkg"


def get_user_manual_url(processing_name: str) -> str:
    return f"https://otm.gitlab-pages.pam-retd.fr/q4ts/user_manual/processings.html#{processing_name}"


def get_short_string(processing_name: str, default_help_str: str) -> str:
    current_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    help_md = current_dir / ".." / "resources" / "help" / f"{processing_name}.md"
    help_str = default_help_str
    if os.path.exists(help_md):
        with open(help_md) as f:
            help_str = "".join(f.readlines())
    return help_str


def add_shift_parameters(p):
    p.addParameter(
        QgsProcessingParameterNumber(
            p.SHIFT_X,
            p.tr("Shift X"),
            optional=True,
            type=QgsProcessingParameterNumber.Double,
            minValue=VariablesUtils.SHIFT_MIN_VALUE,
            maxValue=VariablesUtils.SHIFT_MAX_VALUE,
        )
    )

    p.addParameter(
        QgsProcessingParameterNumber(
            p.SHIFT_Y,
            p.tr("Shift Y"),
            optional=True,
            type=QgsProcessingParameterNumber.Double,
            minValue=VariablesUtils.SHIFT_MIN_VALUE,
            maxValue=VariablesUtils.SHIFT_MAX_VALUE,
        )
    )


def get_selafin_layer_file_name(input_mesh_layer: QgsMeshLayer):
    input_mesh_path = input_mesh_layer.source().replace('SELAFIN:', '')
    input_mesh_path = input_mesh_path.replace('"', "")
    return input_mesh_path


def get_current_q4ts_gpkg() -> str:
    (gpkg_file_name, ok) = QgsProject.instance().readEntry(
        "Q4TS", BASE_SETTINGS_KEY + GPKG_FILENAME
    )
    if ok:
        return gpkg_file_name
    else:
        return ""


def get_current_q4ts_gpkg_folder() -> str:
    gpkg_file = get_current_q4ts_gpkg()
    if gpkg_file and Path(gpkg_file).exists():
        return str(Path(gpkg_file).parent) + "/"
    else:
        return ""


def get_or_create_q4ts_gpkg(parent) -> str:
    gpkg_file_name = get_current_q4ts_gpkg()
    if not gpkg_file_name:
        tr = PlgTranslator().tr
        layer_dir = Path().home() / '.q4ts'
        os.makedirs(layer_dir, exist_ok=True)

        gpkg_file_name, filter_used = QFileDialog.getSaveFileName(parent,
                                                                  tr("Select Q4TS geopackage"),
                                                                  str(Path(layer_dir / "new_package.gpkg")),
                                                                  tr("Geopackage (*.gpkg)")
                                                                  )
        if gpkg_file_name:
            if not Path(gpkg_file_name).suffix:
                gpkg_file_name = gpkg_file_name + ".gpkg"

            name, ok = QInputDialog.getText(parent, tr('New project'), tr("Name"))
            if ok and name:
                uri = f'geopackage:{gpkg_file_name}?projectName={name}'
                QgsProject.instance().write(uri)
            QgsProject.instance().writeEntry(
                "Q4TS", BASE_SETTINGS_KEY + GPKG_FILENAME, gpkg_file_name
            )
    return gpkg_file_name


def get_available_layer_name(layer_name):
    layers_name = []
    for layer in QgsProject.instance().mapLayers().values():
        layers_name.append(layer.name())
    new_layer_name = layer_name
    index = 0
    while new_layer_name in layers_name:
        index = index + 1
        new_layer_name = f"{layer_name}_{index}"
    return new_layer_name


def get_available_layer_name_in_gpkg(gpkg_filename: str, layer_name: str) -> str:
    layer = QgsVectorLayer(gpkg_filename, "test", "ogr")
    sub_layers = layer.dataProvider().subLayers()

    layers_name = []
    for subLayer in sub_layers:
        name = subLayer.split(QgsDataProvider.SUBLAYER_SEPARATOR)[1]
        layers_name.append(name)

    new_layer_name = layer_name
    index = 0
    while new_layer_name in layers_name:
        index = index + 1
        new_layer_name = f"{layer_name}_{index}"
    return new_layer_name


def get_available_file_name(directory: Path, layer_name: str, suffix: str):
    text_files = [f for f in os.listdir(directory) if f.endswith(suffix)]

    new_layer_name = layer_name
    index = 0
    while new_layer_name + ".shp" in text_files:
        index = index + 1
        new_layer_name = f"{layer_name}_{index}"

    return new_layer_name


def save_as_temp_shp(layer) -> str:
    temp_file_name = tempfile.NamedTemporaryFile(suffix=".shp").name
    processing.run(
        "native:savefeatures", {"INPUT": layer.source(), "OUTPUT": temp_file_name}
    )
    return temp_file_name


def get_layer(
        self, layer_name, context, parameters, new_layer_name=None, optional=True
):
    source_layer = self.parameterAsVectorLayer(parameters, layer_name, context)
    if source_layer:
        layer = source_layer.clone()
        if new_layer_name:
            layer.setName(new_layer_name)
        # TODO JMK : for now no clone use because of issue when creating tmp shp
        return source_layer
    elif not optional:
        raise QgsProcessingException(self.invalidSourceError(parameters, layer_name))


def run_python_script_telemac_env(python_script, commands, feedback) -> int:
    telemac_env_script = ProcessingConfig.getSetting(
        SalomeHydroSettings.TELEMAC_ENV_SCRIPT
    )
    if not os.path.exists(telemac_env_script):
        raise QgsProcessingException(
            f"Telemac environnement script '{telemac_env_script}' doesn't exists"
        )
    telemac_env_script = str(Path(telemac_env_script))

    current_dir = str(Path(os.path.dirname(os.path.realpath(__file__))))

    my_env = os.environ.copy()
    if sys.platform.startswith("win"):
        my_env["PYTHONHOME"] = ""
        my_env["GDAL_DRIVER_PATH"] = ""
        salome_script_commands = [
            str(Path(current_dir) / "script_telemac_env.bat"),
            telemac_env_script,
            python_script,
        ]
        encoding = "latin-1"
        shell = True
    else:
        salome_script_commands = [
            "bash",
            "script_telemac_env.sh",
            telemac_env_script,
            python_script,
        ]
        encoding = locale.getpreferredencoding(False)
        shell = False

    salome_script_commands += commands
    feedback.pushInfo("Telemac script call")
    feedback.pushInfo(" ".join(salome_script_commands))

    process = subprocess.Popen(
        salome_script_commands,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        cwd=current_dir,
        universal_newlines=True,
        env=my_env,
        encoding=encoding,
        shell=shell
    )
    while True:
        output = process.stdout.readline()
        if output:
            feedback.pushInfo(output.rstrip())
        if not check_process_and_feedback(feedback, process):
            break

    return process.returncode


def check_process_and_feedback(feedback, process, check_feedback: bool = True):
    result = True
    if check_feedback and feedback.isCanceled():
        feedback.reportError("Processing canceled")
        process.kill()
        result = False
    if process.poll() is not None:
        result = False
    return result


def run_q4ts_script_telemac_env(commands, feedback) -> int:
    commands_ = [get_salome_script()] + commands
    # Need to use python because q4ts processing scripts are not executable
    # Don't use "python3" in windows, it will use python from QGIS installation
    # use "python" to use python from telemac installation
    python_executable = "python" if sys.platform.startswith("win") else "python3"
    return run_python_script_telemac_env(python_executable, commands_, feedback)


def run_python_script_salome_env(python_script, commands, feedback) -> int:
    salome_bin = get_salome_bin()
    if sys.platform.startswith("win"):
        salome_script_commands = [get_python_salome(), salome_bin, "-t", python_script]
    else:
        salome_script_commands = [salome_bin, "-t", python_script]

    salome_bin_commands = "args:" + commands[0]
    for i in range(1, len(commands)):
        salome_bin_commands += "," + str(commands[i])
    salome_script_commands.append(salome_bin_commands)
    return_code = run_cmd_with_log_file(feedback, salome_script_commands)

    # Force salome stop for omniNames process
    if sys.platform.startswith("win"):
        kill_all_salome_session(feedback)

    return return_code


def run_q4ts_script_salome_env(commands, feedback) -> int:
    return run_python_script_salome_env(get_salome_script(), commands, feedback)


def kill_all_salome_session(feedback):
    salome_bin = get_salome_bin()

    if sys.platform.startswith("win"):
        salome_commands = [get_python_salome(), salome_bin, "killall"]
    else:
        salome_commands = [salome_bin, "killall"]

    run_cmd_with_log_file(feedback, salome_commands, False)


def run_cmd_with_log_file(feedback, commands, check_feedback: bool = True) -> int:
    feedback.pushInfo("Salome call")
    feedback.pushInfo(" ".join(commands))
    current_dir = os.path.dirname(os.path.realpath(__file__))

    my_env = os.environ
    if sys.platform.startswith("win"):
        encoding = "latin-1"
        my_env["PYTHONHOME"] = ""
        my_env["GDAL_DRIVER_PATH"] = ""
        shell = True
    else:
        encoding = locale.getpreferredencoding(False)
        shell = False

    stdout_logfile_temp = tempfile.NamedTemporaryFile(mode="wb", delete=False)
    stdout_logfile = open(stdout_logfile_temp.name, errors="replace")

    process = subprocess.Popen(
        commands,
        cwd=current_dir,
        stdout=stdout_logfile_temp,
        stderr=subprocess.STDOUT,
        universal_newlines=True,
        env=my_env,
        encoding=encoding,
        shell=shell
    )
    last_out, last_err = None, None
    while True:
        val_out = read_log_file(stdout_logfile, last_out)
        if val_out:
            last_out = val_out
            feedback.pushInfo(val_out)
        if not check_process_and_feedback(feedback, process, check_feedback):
            break

    stdout_logfile_temp.close()

    stdout_logfile.close()
    return process.returncode


def get_salome_script() -> str:
    current_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    path = current_dir / ".." / "salome_script" / "salome_script.py"
    return str(path.resolve())


def get_salome_bin():
    salome_bin = ProcessingConfig.getSetting(SalomeHydroSettings.SALOME_BIN)
    if not os.path.exists(salome_bin):
        raise QgsProcessingException(f"Salome executable '{salome_bin}' doesn't exists")
    return str(Path(salome_bin))


def get_python_salome():
    python_salome = ProcessingConfig.getSetting(SalomeHydroSettings.PYTHON_SALOME)
    if not os.path.exists(python_salome):
        raise QgsProcessingException(f"Python salome '{python_salome}' doesn't exists")
    return str(Path(python_salome))


def read_log_file(logfile, last_val: str) -> str:
    read_val = None
    val = logfile.readline()
    if val is not None:
        val = val.rstrip()
        if val != last_val:
            read_val = val
    return read_val
