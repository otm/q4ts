"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""
import sys
import typing

from qgis import processing
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingException,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterFileDestination,
    QgsProcessingParameterMeshLayer,
    QgsProcessingParameterNumber,
)
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.processing.qgis_utils import (
    get_layer,
    get_selafin_layer_file_name,
    get_short_string,
    get_user_manual_url,
    run_q4ts_script_salome_env,
    run_q4ts_script_telemac_env,
    save_as_temp_shp,
)


class RefineAlgorithm(QgsProcessingAlgorithm):
    INPUT_MESH = "INPUT_MESH"
    DOMAIN = "DOMAIN"
    TEMP_MED = "TEMP_MED"
    OUTPUT_MESH = "OUTPUT_MESH"

    REFINE_LEVEL = "REFINE_LEVEL"
    METHOD = "METHOD"

    REFINE_LEVEL_DEFAULT_VALUE = 1
    REFINE_LEVEL_MIN_VALUE = 1
    REFINE_LEVEL_MAX_VALUE = 10

    if sys.platform == "win32":
        METHOD_ENUM = ["stbtel"]
    else:
        METHOD_ENUM = ["homard", "stbtel"]

    def tr(self, string):
        return QCoreApplication.translate("Refine mesh for Q4TS", string)

    def createInstance(self):
        return RefineAlgorithm()

    def name(self):
        return "refine"

    def displayName(self):
        return self.tr("refine mesh")

    def icon(self) -> QIcon:
        return QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "refine-icon.png"))

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def helpUrl(self):
        return get_user_manual_url(self.name())

    def shortHelpString(self):
        return get_short_string(self.name(), "Refine a mesh layer")

    def checkParameterValues(
        self, parameters: typing.Dict[str, typing.Any], context: QgsProcessingContext
    ) -> typing.Tuple[bool, str]:
        res, error = super().checkParameterValues(parameters, context)
        # For stbtel option, if domain is used, refine level is limited to 1.
        if res:
            method = self.METHOD_ENUM[
                self.parameterAsEnum(parameters, self.METHOD, context)
            ]
            layer = get_layer(self, self.DOMAIN, context, parameters)
            refine_level = self.parameterAsInt(parameters, self.REFINE_LEVEL, context)

            if layer and method == "stbtel" and refine_level > 1:
                res = False
                error = self.tr(
                    "Refinement level with stbtel in a polygon is limited to 1\n Actual value : {0}".format(
                        refine_level
                    )
                )
        return res, error

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterMeshLayer(self.INPUT_MESH, self.tr("Input mesh"))
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.REFINE_LEVEL,
                self.tr("Refine level"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=self.REFINE_LEVEL_DEFAULT_VALUE,
                minValue=self.REFINE_LEVEL_MIN_VALUE,
                maxValue=self.REFINE_LEVEL_MAX_VALUE,
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.DOMAIN,
                self.tr("Refine domain"),
                [QgsProcessing.TypeVectorPolygon],
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.METHOD,
                self.tr("Method"),
                self.METHOD_ENUM,
                defaultValue=0,
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.TEMP_MED, self.tr("Temporary .med file"), "Mesh layer (*.med)"
            )
        )

        self.addParameter(
            QgsProcessingParameterFileDestination(
                self.OUTPUT_MESH, self.tr("Output mesh"), "Mesh layer (*.slf)"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_mesh_layer = self.parameterAsMeshLayer(
            parameters, self.INPUT_MESH, context
        )
        input_mesh_path = get_selafin_layer_file_name(input_mesh_layer)

        output_mesh_file = self.parameterAsFileOutput(
            parameters, self.OUTPUT_MESH, context
        )

        method = self.METHOD_ENUM[
            self.parameterAsEnum(parameters, self.METHOD, context)
        ]

        temp_med_file = self.parameterAsFileOutput(parameters, self.TEMP_MED, context)

        if method == "homard":
            # Convert to .med
            processing.run(
                "q4ts:slf2med",
                {"INPUT_MESH": input_mesh_path, "OUTPUT_MESH": temp_med_file},
                context=context,
                feedback=feedback,
            )

            input_refine = temp_med_file
            output_refine = temp_med_file
        else:
            input_refine = input_mesh_path
            output_refine = output_mesh_file

        refine_level = self.parameterAsInt(parameters, self.REFINE_LEVEL, context)

        cmd = [
            "refine",
            input_refine,
            output_refine,
            "-l",
            str(refine_level),
            "--method",
            method,
        ]
        layer = get_layer(self, self.DOMAIN, context, parameters)
        if layer:
            domain_shp_file = save_as_temp_shp(layer)
            cmd += ["--domain", domain_shp_file]

        if method == "homard":
            salome_return_code = run_q4ts_script_salome_env(cmd, feedback)
            if salome_return_code:
                raise QgsProcessingException(
                    self.tr(
                        "Error code '{}' returned by salome when refining mesh. "
                        "Check logs for more informations.".format(salome_return_code)
                    )
                )
        else:
            telemac_return_code = run_q4ts_script_telemac_env(cmd, feedback)
            if telemac_return_code:
                raise QgsProcessingException(
                    self.tr(
                        "Error code '{}' returned by telemac when refining mesh. "
                        "Check logs for more informations.".format(telemac_return_code)
                    )
                )

        if not feedback.isCanceled() and method == "homard":
            processing.run(
                "q4ts:med2slf",
                {"INPUT_MESH": temp_med_file, "OUTPUT_MESH": output_mesh_file},
                context=context,
                feedback=feedback,
            )

        feedback.pushInfo(self.tr("Processing {} finished".format(self.name())))
        return {"OUTPUT_MESH": output_mesh_file}
