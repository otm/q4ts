- Description :

Create boundary on a mesh from polygon

- Parameters :

|Input                  | Parameter          | Description                                                |
|-----------------------|--------------------|------------------------------------------------------------|
|Input mesh             | `INPUT_MESH`       | `QgsMeshLayer` in `.slf` format                            |
|Contour layer          | `CONTOUR_LAYER`    | `QgsVectorLayer` of mesh contour                           |
|Boundaries layer       | `BOUNDARIES_LAYER` | `QgsVectorLayer` use for boundaries                        |
|Default bound value    | `DEFAULT_BND`      | Default `bnd_type` value                                   |
|Check boundaries layer | `CHECK_BOUNDARIES` | Boolean for check of boundaries inclusion in contour layer |
|Output mesh            | `OUTPUT_MESH`      | Output `.slf` file (a corresponding `.cli` is also created |

- Processing name: `q4ts:create_boundary`
