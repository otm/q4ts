- Description :

Interpolate a xyz on the whole mesh or within a polygon

- Parameters :

| Input              | Parameter          | Description                                                                                                                                                                      |
|--------------------|--------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Input mesh         | `INPUT_MESH`       | `QgsMeshLayer` in `.slf` format                                                                                                                                                  |
| Input point cloud  | `CLOUD_POINT_FILE` | Name of the xyz like file or laz file that contains the points and value to interpolate                                                                                          |
| Delimiter          | `DELIMITER`        | Delimiter of columns in the points file                                                                                                                                          |
| Variable name      | `VAR_NAME`         | Name of the variable in the output mesh                                                                                                                                          |
| Variable unit      | `VAR_UNIT`         | Unit of the variable in the output mesh                                                                                                                                          |
| Area layer         | `ZONE_SHP`         | `QgsVectorLayer` containing zones and values to define field                                                                                                                     |
| Apply outside area | `OUTSIDE_ZONE`     | If a zone polygon file is given, the interpolation is done by default inside the polygone zone. If this option is activated, the interpolation is done outside the polygone zone |
| Default value      | `DEFAULT_VALUE`    | In case the variable does not already exists in the file this value will be applied to points not within the polygons if polygons were given                                     |
| Interpolation      | `KIND`             | Type of interpolation (nearest, linear, cubic)                                                                                                                                   |
| Shift X            | `SHIFT_X`          | Translation along with x-axis                                                                                                                                                    |
| Shift Y            | `SHIFT_Y`          | Translation along with y-axis                                                                                                                                                    |
| Output mesh        | `OUTPUT_MESH`      | Output `.slf` file                                                                                                                                                               |

- Advanced Parameters :

| Input                | Parameter           | Description                                                                                                                                                                                                  |
|----------------------|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Record               | `RECORD`            | Number of the record (time step) to change in the mesh (-1 is the last record)                                                                                                                               |
| Use memory map numpy | `NMAP`              | If activated, it converts the points file into a numpy object and create a memory map to avoid a full loading of the file in the memory                                                                      |
| Chunk interpolation  | `CHUNK_INTERP`      | This option allow to interpolate by chunk which is usefull for large datasets which lead to memory problems                                                                                                  |
| Chunk x partition    | `CHUNK_X_PARTITION` | Number of partitions in x-direction. If None, a default is calculated according to the number of blockpts                                                                                                    |
| Chunk y partition    | `CHUNK_Y_PARTITION` | Number of partitions in y-direction. If None, a default is calculated according to the number of blockpts                                                                                                    |
| Chunk overlap        | `CHUNK_OVERLAP`     | Proportion of block length to overlap on other blocks.  For example, if pe=0.25, the block will be extended 25% on both the left and right sides of px to overlap on successive blocks.                      |
| Block size           | `BLOCK_PTS`         | Approximate number of interpolation points within each partition block. Defaults to 300*300. blockpts is used to automatically size either chunk x partitions or chunk y partitions if these are set to None |

Processing name: `q4ts:interp_cloud_point`

:::{warning}
Please check that Input Mesh and Input point cloud file share the same CRS. No reprojection is done.
:::