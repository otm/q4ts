- Description :

Cut a mesh by removing faces and vertices inside a contour layer.

- Parameters :

|Input                   | Parameter         | Description                                    |
|------------------------|-------------------|------------------------------------------------|
| Input mesh             | `INPUT_MESH`      | `QgsMeshLayer` in `.slf` format                |
|Input contour layer     | `INPUT_CONTOUR`   | `QgsVectorLayer` with contour                  |
|Temporary .med file     | `TEMP_MED`        | Temporary `.med` file created by salome_script |
|Output mesh .med        | `OUTPUT_MESH_MED` | Output `.med` file                             |
|Output mesh             | `OUTPUT_MESH`     | Output `.slf` file                             |

Processing name: `q4ts:cut_mesh`
