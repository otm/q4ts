- Description :

Create a `.shp` file containing mesh contour.

- Parameters :

|Input           | Parameter         | Description                     |
|----------------|-------------------|---------------------------------|
|Input mesh      | `INPUT_MESH`      | `QgsMeshLayer` in `.slf` format |
|Output countour | `OUTPUT_COUNTOUR` | Output `.shp` file              |

- Processing name: `q4ts:contour`
