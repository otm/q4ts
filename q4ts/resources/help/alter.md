- Description :

Alter a SELAFIN mesh file.

- Input/Output file parameters :


| Input       | Parameter     | Description                     |
|-------------|---------------|---------------------------------|
| Input mesh  | `INPUT_MESH`  | `QgsMeshLayer` in `.slf` format |
| Output mesh | `OUTPUT_MESH` | Output `.slf` file              |

- Variable modification :

| Input     | Parameter | Description                                                                                        |
|-----------|-----------|----------------------------------------------------------------------------------------------------|
| Variables | `VARS`    | Define variables to keep. Each variable must be separated by `,`. Default : all variables are kept |

- Time modification :

| Input     | Parameter | Description                                                                                         |
|-----------|-----------|-----------------------------------------------------------------------------------------------------|
| Times     | `TIMES`   | Define time index to keep. Each index must be separated by `,`. Default : all time indexes are kept |
| Time from | `TFROM`   | First time index keep                                                                               |
| Time end  | `TEND`    | Last time index keep                                                                                |
| Time step | `TSTEP`   | Time index step                                                                                     |

:::{note}
For `TIMES` input use, telemac-mascaret must be on the main branch.
:::

- Coordinates modification :

| Input   | Parameter | Description                   |
|---------|-----------|-------------------------------|
| Shift X | `SHIFT_X` | Translation along with x-axis |
| Shift Y | `SHIFT_Y` | Translation along with y-axis |
| Rotate  | `ROTATE`  | Angle of rotation (counter clockwise) to apply in degree |

- Processing name: `q4ts:alter`
