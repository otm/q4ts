- Description :

Extend a mesh from an new extent layer and optional other mesh.

- Parameters :

|Input                   | Parameter               | Description                                                                                   |
|------------------------|-------------------------|-----------------------------------------------------------------------------------------------|
| Input mesh    | `INPUT_MESH`    | `QgsMeshLayer` in `.slf` format                                                                                                              |
| Other mesh    | `OTHER_MESH`    | Other `QgsMeshLayer` in `.slf` format to be used in extension                                                                                                              |
|Extended contour layer |`EXTENDED_CONTOUR_LAYER` | `QgsVectorLayer` with extended polygon  |
|Contour layer           | `CONTOUR_LAYER`         | `QgsVectorLayer` with contour polygon                                                           |
|Other contour layer           | `OTHER_CONTOUR_LAYER`         | `QgsVectorLayer` with contour polygon for other mesh                                                          |
|Island layer            | `ISLAND_LAYER`          | List of `QgsVectorLayer` with island polygons                                                         |
|Constrainte lines layer | `CONSTRAINT_LINE_LAYER` | List of `QgsVectorLayer` with constraint lines                                                        |
|Refinement area         | `REFINE_LAYER`          | List of `QgsVectorLayer` with refinement polygon  (must contain  `min_size/phy_size/max_size` fields) |
|Mesh size               | `MESH_SIZE`             | Physical size of a mesh element                                                              |
|Mesh min. size          | `MESH_MIN_SIZE`         | Minimum size of a mesh element                                                        |
|Mesh max. size          | `MESH_MAX_SIZE`         | Maximum size of a mesh element                                                        |
|Mesh growth rate          | `MESH_GROWTH_RATE`         | Define the growth rate allowed between cells                                                      |
|Mesh engine             | `MESH_ENGINE`           | Option to choose the mesh generator engine between `netgen_1d_2d / mg_cadsurf`                |
|Temporary .med file     | `TEMP_MED`              | Temporary `.med` file created by salome_script                                                |
|Other mesh temporary .med file     | `OTHER_TEMP_MED`              | Temporary `.med` file created by salome_script for other mesh                                               |
|Output mesh             | `OUTPUT_MESH`           | Output `.slf` file                                                                            |

> `mg_cadsurf` option need a licence and must be configured by user before use. Q4TS doesn't check mg_cadsurf availability

> `MESH_GROWTH_RATE` the meaning, differ for Netgen1D2D and for MGCadSurf. For NETGEN1D2D, it defines how much the linear dimensions of two adjecent cells can differ (e.g. 0.3 which is default value means 30%). For MGCadSurf, it is the maximum ratio between two adjacent edges (default value is 1.3)

Processing name: `q4ts:extend_mesh`
