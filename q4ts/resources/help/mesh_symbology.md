- Description :

Apply Q4TS symbology to a QgsMeshLayer depending on layer dataset names.

Supported selafin variables are :

- `BOTTOM`
- `BOTTOM FRICTION`
- `FREE SURFACE`
- `WATER DEPTH`
- `VELOCITY U`
- `VELOCITY V`
- `WIND ALONG X`
- `WIND ALONG Y`


- Parameters :

| Input        | Parameter      | Description                                            |
|--------------|----------------|--------------------------------------------------------|
| Input mesh   | `INPUT_MESH`   | `QgsMeshLayer`                                         |
| Custom style | `CUSTOM_STYLE` | Custom style to apply for bottom friction (Min/Moy/Max |

- Processing name: `q4ts:mesh_symbology`
