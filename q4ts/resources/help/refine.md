- Description :

Refine a mesh globally or within a domain layer

- Parameters :

| Input               | Parameter      | Description                                                         |
|---------------------|----------------|---------------------------------------------------------------------|
| Input mesh          | `INPUT_MESH`   | `QgsMeshLayer` in `.slf` format                                     |
| Domain layer        | `DOMAIN`       | `QgsVectorLayer` for refinement in specific area (optional)         |
| Refine level        | `REFINE_LEVEL` | Level of refinement (positive value)                                |
| Method              | `METHOD`       | Option to choose the method of refinement between `homard / stbtel` |
| Temporary .med file | `TEMP_MED`     | Temporary `.med` file created by salome_script                      |
| Output mesh         | `OUTPUT_MESH`  | Output `.slf` file                                                  |

Processing name: `q4ts:refine`

:::{note}
For now `homard` method is not available on Windows.

For `stbtel` telemac-mascaret must be on the main branch.

:::