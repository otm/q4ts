- Description :

Convert mesh in `.med` format to `.slf` format.

- Parameters :

| Input            | Parameter     | Description                  |
|------------------|---------------|------------------------------|
| Input .med mesh  | `INPUT_MESH`  | Mesh in `.med` format        |
| Output .slf mesh | `OUTPUT_MESH` | Output mesh in `.slf` format |

- Processing name: `q4ts:med2slf`
