- Description :

Convert mesh in `.slf` format to `.med` format.

- Parameters :

| Input                      | Parameter     | Description                   |
|----------------------------|---------------|-------------------------------|
| Input .slf mesh            | `INPUT_MESH`  | Mesh in `.slf` format         |
| Input .cli boundaries file | `INPUT_BND`   | Optional .cli boundaries file |
| Output .med mesh           | `OUTPUT_MESH` | Output mesh in `.med` format  |

- Processing name: `q4ts:slf2med`
