- Description :

Field a xyz on the whole mesh or within a polygon

- Parameters :

| Input         | Parameter       | Description                                                                                                                                  |
|---------------|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| Input mesh    | `INPUT_MESH`    | `QgsMeshLayer` in `.slf` format                                                                                                              |
| Variable name | `VAR_NAME`      | Name of the variable in the output mesh                                                                                                      |
| Variable unit | `VAR_UNIT`      | Unit of the variable in the output mesh                                                                                                      |
| Area layer    | `ZONE_SHP`      | `QgsVectorLayer` containing zones and values to define field                                                                                 |
| Layer field   | `ATTR_NAME`     | Name of the attribute to consider                                                                                                            |
| Field value   | `FIELD_VALUE`   | Field value used to define the field within the polygon(s) if here is no attributes defined                                                  |
| Default value | `DEFAULT_VALUE` | In case the variable does not already exists in the file this value will be applied to points not within the polygons if polygons were given |
| CRS           | `EPSG`          | EPSG code of the input mesh file                                                                                                             |
| Shift X       | `SHIFT_X`       | Translation along with x-axis                                                                                                                |
| Shift Y       | `SHIFT_Y`       | Translation along with y-axis                                                                                                                |
| Output mesh   | `OUTPUT_MESH`   | Output `.slf` file                                                                                                                           |

- Advanced Parameters :

| Input  | Parameter | Description                                                                    |
|--------|-----------|--------------------------------------------------------------------------------|
| Record | `RECORD`  | Number of the record (time step) to change in the mesh (-1 is the last record) |

Processing name: `q4ts:projection_field`
