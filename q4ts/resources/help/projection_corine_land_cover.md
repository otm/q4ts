- Description :

Projection of Corine Land Cover data in mesh from friction table.

- Parameters :

| Input                         | Parameter         | Description                                                                                                      |
|-------------------------------|-------------------|------------------------------------------------------------------------------------------------------------------|
| Input mesh                    | `INPUT_MESH`      | `QgsMeshLayer` in `.slf` format                                                                                  |
| Friction table                | `STRICKLER_TABLE` | Path to the clc <-> Friction correspondance table file                                                          |
| Corine Land Cover Data file   | `CLC_DATA_FILE`   | `QgsVectorLayer` containing Corine Land Cover data                                                               |
| Area layer                    | `ZONE_SHP`        | `QgsVectorLayer` containing zones and values with friction values                                               |
| Layer field                   | `ATTR_NAME`       | Name of the attribute to consider                                                                                |
| Shift X                       | `SHIFT_X`         | Translation along with x-axis                                                                                    |
| Shift Y                       | `SHIFT_Y`         | Translation along with y-axis                                                                                    |
| Write CLC Code                | `WRITE_CLC_CODE`  | If true an additional field called CODE CLC will be written containing the CLC code (-1 for custom defined area) |
| Shift area                    | `ZONE_SHIFT`      | Option should be added if the zone file is in the same projection as the mesh file                               |
| Output mesh                   | `OUTPUT_MESH`     | Output `.slf` file                                                                                               |

Processing name: `q4ts:projection_corine_land_cover`

:::{warning}
Please check that Input Mesh and Corine Land Cover Data file share the same CRS. No reprojection is done.
:::
