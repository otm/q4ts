<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" maxScale="0" minScale="1e+08" hasScaleBasedVisibilityFlag="0" version="3.22.3-Białowieża">
  <renderer-3d layer="BEL_Q14086_1CL_Lm_T2DDampAmont_acBR_ea681ca1_d3d4_4fc9_80de_9e596689792d" type="mesh">
    <symbol type="mesh">
      <data alt-clamping="relative" height="0" add-back-faces="0"/>
      <material diffuse="178,178,178,255" specular="255,255,255,255" shininess="0" ambient="26,26,26,255">
        <data-defined-properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data-defined-properties>
      </material>
      <advanced-settings vertical-relative="0" arrows-spacing="25" max-color-ramp-shader="255" texture-single-color="0,128,0,255" arrows-fixed-size="0" vertical-scale="1" level-of-detail="-99" arrows-enabled="0" smoothed-triangle="1" vertical-group-index="-1" texture-type="0" renderer-3d-enabled="0" wireframe-line-width="1" wireframe-enabled="0" wireframe-line-color="128,128,128,255" min-color-ramp-shader="0">
        <colorrampshader clip="0" minimumValue="0" labelPrecision="6" classificationMode="1" maximumValue="255" colorRampType="INTERPOLATED">
          <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
            <numericFormat id="basic">
              <Option type="Map">
                <Option type="QChar" name="decimal_separator" value=""/>
                <Option type="int" name="decimals" value="6"/>
                <Option type="int" name="rounding_type" value="0"/>
                <Option type="bool" name="show_plus" value="false"/>
                <Option type="bool" name="show_thousand_separator" value="true"/>
                <Option type="bool" name="show_trailing_zeros" value="false"/>
                <Option type="QChar" name="thousand_separator" value=""/>
              </Option>
            </numericFormat>
          </rampLegendSettings>
        </colorrampshader>
      </advanced-settings>
      <data-defined-properties>
        <Option type="Map">
          <Option type="QString" name="name" value=""/>
          <Option name="properties"/>
          <Option type="QString" name="type" value="collection"/>
        </Option>
      </data-defined-properties>
    </symbol>
  </renderer-3d>
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>0</Searchable>
    <Private>0</Private>
  </flags>
  <temporal temporal-active="1" reference-time="1900-01-01T00:00:00Z" start-time-extent="1900-01-01T00:00:00Z" matching-method="0" end-time-extent="1900-01-12T00:00:00Z"/>
  <customproperties>
    <Option/>
  </customproperties>
  <mesh-renderer-settings>
    <active-dataset-group vector="-1" scalar="1"/>
    <scalar-settings interpolation-method="none" group="2" max-val="203.84999999999999" min-val="119.73999999999999" opacity="1">
      <colorrampshader clip="0" minimumValue="119.73553466796875" labelPrecision="6" classificationMode="1" maximumValue="203.84530639648438" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="5,113,176,255"/>
            <Option type="QString" name="color2" value="202,0,32,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;16,120,180,255:0.0392157;27,126,183,255:0.0588235;38,133,187,255:0.0784314;49,139,190,255:0.0980392;60,146,194,255:0.117647;71,153,198,255:0.137255;82,159,201,255:0.156863;93,166,205,255:0.176471;104,172,208,255:0.196078;116,179,212,255:0.215686;127,185,216,255:0.235294;138,192,219,255:0.254902;148,198,222,255:0.27451;156,202,224,255:0.294118;164,206,226,255:0.313725;172,210,228,255:0.333333;180,214,230,255:0.352941;188,218,232,255:0.372549;195,221,234,255:0.392157;203,225,236,255:0.411765;211,229,238,255:0.431373;219,233,240,255:0.45098;227,237,242,255:0.470588;235,241,244,255:0.490196;243,245,246,255:0.509804;247,244,242,255:0.529412;247,237,233,255:0.54902;246,231,224,255:0.568627;246,225,215,255:0.588235;246,218,206,255:0.607843;246,212,197,255:0.627451;245,205,187,255:0.647059;245,199,178,255:0.666667;245,192,169,255:0.686275;245,186,160,255:0.705882;245,179,151,255:0.72549;244,173,141,255:0.745098;244,167,132,255:0.764706;242,155,124,255:0.784314;238,142,117,255:0.803922;235,129,109,255:0.823529;232,117,101,255:0.843137;228,104,94,255:0.862745;225,91,86,255:0.882353;222,78,78,255:0.901961;218,65,70,255:0.921569;215,52,63,255:0.941176;212,39,55,255:0.960784;209,26,47,255:0.980392;205,13,40,255"/>
          </Option>
          <prop k="color1" v="5,113,176,255"/>
          <prop k="color2" v="202,0,32,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;16,120,180,255:0.0392157;27,126,183,255:0.0588235;38,133,187,255:0.0784314;49,139,190,255:0.0980392;60,146,194,255:0.117647;71,153,198,255:0.137255;82,159,201,255:0.156863;93,166,205,255:0.176471;104,172,208,255:0.196078;116,179,212,255:0.215686;127,185,216,255:0.235294;138,192,219,255:0.254902;148,198,222,255:0.27451;156,202,224,255:0.294118;164,206,226,255:0.313725;172,210,228,255:0.333333;180,214,230,255:0.352941;188,218,232,255:0.372549;195,221,234,255:0.392157;203,225,236,255:0.411765;211,229,238,255:0.431373;219,233,240,255:0.45098;227,237,242,255:0.470588;235,241,244,255:0.490196;243,245,246,255:0.509804;247,244,242,255:0.529412;247,237,233,255:0.54902;246,231,224,255:0.568627;246,225,215,255:0.588235;246,218,206,255:0.607843;246,212,197,255:0.627451;245,205,187,255:0.647059;245,199,178,255:0.666667;245,192,169,255:0.686275;245,186,160,255:0.705882;245,179,151,255:0.72549;244,173,141,255:0.745098;244,167,132,255:0.764706;242,155,124,255:0.784314;238,142,117,255:0.803922;235,129,109,255:0.823529;232,117,101,255:0.843137;228,104,94,255:0.862745;225,91,86,255:0.882353;222,78,78,255:0.901961;218,65,70,255:0.921569;215,52,63,255:0.941176;212,39,55,255:0.960784;209,26,47,255:0.980392;205,13,40,255"/>
        </colorramp>
        <item label="120" alpha="255" color="#0571b0" value="119.73553466796875"/>
        <item label="121" alpha="255" color="#1078b4" value="121.38474225006713"/>
        <item label="123" alpha="255" color="#1b7eb7" value="123.0339582431427"/>
        <item label="125" alpha="255" color="#2685bb" value="124.68316582524109"/>
        <item label="126" alpha="255" color="#318bbe" value="126.33238181831665"/>
        <item label="128" alpha="255" color="#3c92c2" value="127.98158940041503"/>
        <item label="130" alpha="255" color="#4799c6" value="129.63079698251343"/>
        <item label="131" alpha="255" color="#529fc9" value="131.28002138656615"/>
        <item label="133" alpha="255" color="#5da6cd" value="132.9292457906189"/>
        <item label="135" alpha="255" color="#68acd0" value="134.57847019467164"/>
        <item label="136" alpha="255" color="#74b3d4" value="136.22761048895262"/>
        <item label="138" alpha="255" color="#7fb9d8" value="137.87683489300537"/>
        <item label="140" alpha="255" color="#8ac0db" value="139.5260592970581"/>
        <item label="141" alpha="255" color="#94c6de" value="141.17528370111086"/>
        <item label="143" alpha="255" color="#9ccae0" value="142.82450810516357"/>
        <item label="144" alpha="255" color="#a4cee2" value="144.47373250921632"/>
        <item label="146" alpha="255" color="#acd2e4" value="146.1228728034973"/>
        <item label="148" alpha="255" color="#b4d6e6" value="147.77209720755005"/>
        <item label="149" alpha="255" color="#bcdae8" value="149.4213216116028"/>
        <item label="151" alpha="255" color="#c3ddea" value="151.0705460156555"/>
        <item label="153" alpha="255" color="#cbe1ec" value="152.71977041970825"/>
        <item label="154" alpha="255" color="#d3e5ee" value="154.368994823761"/>
        <item label="156" alpha="255" color="#dbe9f0" value="156.01821922781372"/>
        <item label="158" alpha="255" color="#e3edf2" value="157.66735952209473"/>
        <item label="159" alpha="255" color="#ebf1f4" value="159.31658392614747"/>
        <item label="161" alpha="255" color="#f3f5f6" value="160.9658083302002"/>
        <item label="163" alpha="255" color="#f7f4f2" value="162.61503273425294"/>
        <item label="164" alpha="255" color="#f7ede9" value="164.26425713830565"/>
        <item label="166" alpha="255" color="#f6e7e0" value="165.9134815423584"/>
        <item label="168" alpha="255" color="#f6e1d7" value="167.5626218366394"/>
        <item label="169" alpha="255" color="#f6dace" value="169.21184624069213"/>
        <item label="171" alpha="255" color="#f6d4c5" value="170.86107064474487"/>
        <item label="173" alpha="255" color="#f5cdbb" value="172.51029504879762"/>
        <item label="174" alpha="255" color="#f5c7b2" value="174.15951945285036"/>
        <item label="176" alpha="255" color="#f5c0a9" value="175.80874385690308"/>
        <item label="177" alpha="255" color="#f5baa0" value="177.4579682609558"/>
        <item label="179" alpha="255" color="#f5b397" value="179.10710855523683"/>
        <item label="181" alpha="255" color="#f4ad8d" value="180.75633295928955"/>
        <item label="182" alpha="255" color="#f4a784" value="182.4055573633423"/>
        <item label="184" alpha="255" color="#f29b7c" value="184.054781767395"/>
        <item label="186" alpha="255" color="#ee8e75" value="185.70400617144776"/>
        <item label="187" alpha="255" color="#eb816d" value="187.3532305755005"/>
        <item label="189" alpha="255" color="#e87565" value="189.0023708697815"/>
        <item label="191" alpha="255" color="#e4685e" value="190.65159527383423"/>
        <item label="192" alpha="255" color="#e15b56" value="192.30081967788698"/>
        <item label="194" alpha="255" color="#de4e4e" value="193.95004408193972"/>
        <item label="196" alpha="255" color="#da4146" value="195.59926848599244"/>
        <item label="197" alpha="255" color="#d7343f" value="197.24849289004516"/>
        <item label="199" alpha="255" color="#d42737" value="198.89763318432617"/>
        <item label="201" alpha="255" color="#d11a2f" value="200.54685758837888"/>
        <item label="202" alpha="255" color="#cd0d28" value="202.19608199243163"/>
        <item label="204" alpha="255" color="#ca0020" value="203.84530639648438"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="3" max-val="203.84999999999999" min-val="119.25" opacity="1">
      <colorrampshader clip="0" minimumValue="119.25" labelPrecision="6" classificationMode="1" maximumValue="203.84999999999999" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="1,133,113,255"/>
            <Option type="QString" name="color2" value="166,97,26,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.25;128,205,193,255:0.5;245,245,245,255:0.75;223,194,125,255"/>
          </Option>
          <prop k="color1" v="1,133,113,255"/>
          <prop k="color2" v="166,97,26,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.25;128,205,193,255:0.5;245,245,245,255:0.75;223,194,125,255"/>
        </colorramp>
        <item label="119" alpha="255" color="#018571" value="119.24710083007813"/>
        <item label="121" alpha="255" color="#0b8b77" value="120.90588552518311"/>
        <item label="123" alpha="255" color="#15907e" value="122.56467868010864"/>
        <item label="124" alpha="255" color="#1f9684" value="124.22346337521363"/>
        <item label="126" alpha="255" color="#299c8a" value="125.88225653013916"/>
        <item label="128" alpha="255" color="#33a190" value="127.54104122524414"/>
        <item label="129" alpha="255" color="#3da797" value="129.19982592034913"/>
        <item label="131" alpha="255" color="#47ad9d" value="130.85862753509522"/>
        <item label="133" alpha="255" color="#51b2a3" value="132.51742914984132"/>
        <item label="134" alpha="255" color="#5bb8a9" value="134.1762307645874"/>
        <item label="136" alpha="255" color="#65bdb0" value="135.83494778112794"/>
        <item label="137" alpha="255" color="#6fc3b6" value="137.49374939587403"/>
        <item label="139" alpha="255" color="#79c9bc" value="139.15255101062013"/>
        <item label="141" alpha="255" color="#82cec2" value="140.81135262536623"/>
        <item label="142" alpha="255" color="#8bd1c6" value="142.4701542401123"/>
        <item label="144" alpha="255" color="#95d4ca" value="144.1289558548584"/>
        <item label="146" alpha="255" color="#9ed7ce" value="145.7876728713989"/>
        <item label="147" alpha="255" color="#a7dad2" value="147.446474486145"/>
        <item label="149" alpha="255" color="#b0ddd6" value="149.1052761008911"/>
        <item label="151" alpha="255" color="#b9e1da" value="150.7640777156372"/>
        <item label="152" alpha="255" color="#c3e4df" value="152.4228793303833"/>
        <item label="154" alpha="255" color="#cce7e3" value="154.0816809451294"/>
        <item label="156" alpha="255" color="#d5eae7" value="155.7404825598755"/>
        <item label="157" alpha="255" color="#deedeb" value="157.399199576416"/>
        <item label="159" alpha="255" color="#e7f0ef" value="159.0580011911621"/>
        <item label="161" alpha="255" color="#f0f3f3" value="160.7168028059082"/>
        <item label="162" alpha="255" color="#f4f3f0" value="162.3756044206543"/>
        <item label="164" alpha="255" color="#f2efe7" value="164.0344060354004"/>
        <item label="166" alpha="255" color="#f1ebdd" value="165.6932076501465"/>
        <item label="167" alpha="255" color="#efe7d4" value="167.351924666687"/>
        <item label="169" alpha="255" color="#ede3cb" value="169.0107262814331"/>
        <item label="171" alpha="255" color="#ecdfc1" value="170.6695278961792"/>
        <item label="172" alpha="255" color="#eadbb8" value="172.3283295109253"/>
        <item label="174" alpha="255" color="#e8d7ae" value="173.9871311256714"/>
        <item label="176" alpha="255" color="#e6d3a5" value="175.6459327404175"/>
        <item label="177" alpha="255" color="#e5cf9c" value="177.3047343551636"/>
        <item label="179" alpha="255" color="#e3cb92" value="178.9634513717041"/>
        <item label="181" alpha="255" color="#e1c789" value="180.62225298645018"/>
        <item label="182" alpha="255" color="#dfc37f" value="182.2810546011963"/>
        <item label="184" alpha="255" color="#dcbc77" value="183.93985621594237"/>
        <item label="186" alpha="255" color="#d7b56f" value="185.59865783068847"/>
        <item label="187" alpha="255" color="#d3ad68" value="187.25745944543456"/>
        <item label="189" alpha="255" color="#cea560" value="188.9161764619751"/>
        <item label="191" alpha="255" color="#ca9e58" value="190.5749780767212"/>
        <item label="192" alpha="255" color="#c59650" value="192.2337796914673"/>
        <item label="194" alpha="255" color="#c18f49" value="193.8925813062134"/>
        <item label="196" alpha="255" color="#bc8741" value="195.55138292095947"/>
        <item label="197" alpha="255" color="#b87f39" value="197.21018453570557"/>
        <item label="199" alpha="255" color="#b37831" value="198.8689015522461"/>
        <item label="201" alpha="255" color="#af702a" value="200.52770316699218"/>
        <item label="202" alpha="255" color="#aa6922" value="202.18650478173828"/>
        <item label="204" alpha="255" color="#a6611a" value="203.84530639648438"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="0" max-val="6.75" min-val="0" opacity="1">
      <colorrampshader clip="0" minimumValue="0" labelPrecision="6" classificationMode="2" maximumValue="6.75" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="255,0,255,0"/>
            <Option type="QString" name="color2" value="122,4,3,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="1e-07;48,18,59,255:0.01;70,98,216,255:0.02;53,171,248,255:0.03;27,229,181,255:0.04;116,254,93,255:0.05;201,239,52,255:0.1;251,185,56,255:0.2;245,105,24,255:0.5;201,41,3,255"/>
          </Option>
          <prop k="color1" v="255,0,255,0"/>
          <prop k="color2" v="122,4,3,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="1e-07;48,18,59,255:0.01;70,98,216,255:0.02;53,171,248,255:0.03;27,229,181,255:0.04;116,254,93,255:0.05;201,239,52,255:0.1;251,185,56,255:0.2;245,105,24,255:0.5;201,41,3,255"/>
        </colorramp>
        <item label="0,000000" alpha="0" color="#ff00ff" value="0"/>
        <item label="0,000001" alpha="255" color="#30123b" value="1e-06"/>
        <item label="0,100000" alpha="255" color="#4662d8" value="0.1"/>
        <item label="0,200000" alpha="255" color="#35abf8" value="0.2"/>
        <item label="0,300000" alpha="255" color="#1be5b5" value="0.3"/>
        <item label="0,400000" alpha="255" color="#74fe5d" value="0.4"/>
        <item label="0,500000" alpha="255" color="#c9ef34" value="0.5"/>
        <item label="1,000000" alpha="255" color="#fbb938" value="1"/>
        <item label="2,000000" alpha="255" color="#f56918" value="2"/>
        <item label="5,000000" alpha="255" color="#c92903" value="5"/>
        <item label="10,000000" alpha="255" color="#7a0403" value="10"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="1" max-val="14.74" min-val="0" opacity="1">
      <colorrampshader clip="0" minimumValue="0" labelPrecision="6" classificationMode="2" maximumValue="14.74" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="247,251,255,0"/>
            <Option type="QString" name="color2" value="8,48,107,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0002;236,244,252,255:0.002;227,238,248,255:0.004;217,231,245,255:0.01;207,225,242,255:0.014;197,218,238,255:0.02;181,212,233,255:0.04;165,205,227,255:0.06;146,195,222,255:0.08;125,184,218,255:0.1;105,173,213,255:0.12;88,161,207,255:0.14;72,150,200,255:0.16;57,137,193,255:0.18;44,124,187,255:0.2;31,110,179,255:0.3;20,96,168,255:0.4;9,82,157,255:0.5;8,65,133,255"/>
          </Option>
          <prop k="color1" v="247,251,255,0"/>
          <prop k="color2" v="8,48,107,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0002;236,244,252,255:0.002;227,238,248,255:0.004;217,231,245,255:0.01;207,225,242,255:0.014;197,218,238,255:0.02;181,212,233,255:0.04;165,205,227,255:0.06;146,195,222,255:0.08;125,184,218,255:0.1;105,173,213,255:0.12;88,161,207,255:0.14;72,150,200,255:0.16;57,137,193,255:0.18;44,124,187,255:0.2;31,110,179,255:0.3;20,96,168,255:0.4;9,82,157,255:0.5;8,65,133,255"/>
        </colorramp>
        <item label="0,000000" alpha="0" color="#f7fbff" value="0"/>
        <item label="0,010000" alpha="255" color="#ecf4fc" value="0.01"/>
        <item label="0,100000" alpha="255" color="#e3eef8" value="0.1"/>
        <item label="0,200000" alpha="255" color="#d9e7f5" value="0.2"/>
        <item label="0,500000" alpha="255" color="#cfe1f2" value="0.5"/>
        <item label="0,700000" alpha="255" color="#c5daee" value="0.7"/>
        <item label="1,000000" alpha="255" color="#b5d4e9" value="1"/>
        <item label="2,000000" alpha="255" color="#a5cde3" value="2"/>
        <item label="3,000000" alpha="255" color="#92c3de" value="3"/>
        <item label="4,000000" alpha="255" color="#7db8da" value="4"/>
        <item label="5,000000" alpha="255" color="#69add5" value="5"/>
        <item label="6,000000" alpha="255" color="#58a1cf" value="6"/>
        <item label="7,000000" alpha="255" color="#4896c8" value="7"/>
        <item label="8,000000" alpha="255" color="#3989c1" value="8"/>
        <item label="9,000000" alpha="255" color="#2c7cbb" value="9"/>
        <item label="10,000000" alpha="255" color="#1f6eb3" value="10"/>
        <item label="15,000000" alpha="255" color="#1460a8" value="15"/>
        <item label="20,000000" alpha="255" color="#09529d" value="20"/>
        <item label="25,000000" alpha="255" color="#084185" value="25"/>
        <item label="50,000000" alpha="255" color="#08306b" value="50"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="6" max-val="203.84999999999999" min-val="119.25" opacity="1">
      <colorrampshader clip="0" minimumValue="119.25" labelPrecision="6" classificationMode="1" maximumValue="203.84999999999999" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="5,113,176,255"/>
            <Option type="QString" name="color2" value="202,0,32,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;16,120,180,255:0.0392157;27,126,183,255:0.0588235;38,133,187,255:0.0784314;49,139,190,255:0.0980392;60,146,194,255:0.117647;71,153,198,255:0.137255;82,159,201,255:0.156863;93,166,205,255:0.176471;104,172,208,255:0.196078;116,179,212,255:0.215686;127,185,216,255:0.235294;138,192,219,255:0.254902;148,198,222,255:0.27451;156,202,224,255:0.294118;164,206,226,255:0.313725;172,210,228,255:0.333333;180,214,230,255:0.352941;188,218,232,255:0.372549;195,221,234,255:0.392157;203,225,236,255:0.411765;211,229,238,255:0.431373;219,233,240,255:0.45098;227,237,242,255:0.470588;235,241,244,255:0.490196;243,245,246,255:0.509804;247,244,242,255:0.529412;247,237,233,255:0.54902;246,231,224,255:0.568627;246,225,215,255:0.588235;246,218,206,255:0.607843;246,212,197,255:0.627451;245,205,187,255:0.647059;245,199,178,255:0.666667;245,192,169,255:0.686275;245,186,160,255:0.705882;245,179,151,255:0.72549;244,173,141,255:0.745098;244,167,132,255:0.764706;242,155,124,255:0.784314;238,142,117,255:0.803922;235,129,109,255:0.823529;232,117,101,255:0.843137;228,104,94,255:0.862745;225,91,86,255:0.882353;222,78,78,255:0.901961;218,65,70,255:0.921569;215,52,63,255:0.941176;212,39,55,255:0.960784;209,26,47,255:0.980392;205,13,40,255"/>
          </Option>
          <prop k="color1" v="5,113,176,255"/>
          <prop k="color2" v="202,0,32,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;16,120,180,255:0.0392157;27,126,183,255:0.0588235;38,133,187,255:0.0784314;49,139,190,255:0.0980392;60,146,194,255:0.117647;71,153,198,255:0.137255;82,159,201,255:0.156863;93,166,205,255:0.176471;104,172,208,255:0.196078;116,179,212,255:0.215686;127,185,216,255:0.235294;138,192,219,255:0.254902;148,198,222,255:0.27451;156,202,224,255:0.294118;164,206,226,255:0.313725;172,210,228,255:0.333333;180,214,230,255:0.352941;188,218,232,255:0.372549;195,221,234,255:0.392157;203,225,236,255:0.411765;211,229,238,255:0.431373;219,233,240,255:0.45098;227,237,242,255:0.470588;235,241,244,255:0.490196;243,245,246,255:0.509804;247,244,242,255:0.529412;247,237,233,255:0.54902;246,231,224,255:0.568627;246,225,215,255:0.588235;246,218,206,255:0.607843;246,212,197,255:0.627451;245,205,187,255:0.647059;245,199,178,255:0.666667;245,192,169,255:0.686275;245,186,160,255:0.705882;245,179,151,255:0.72549;244,173,141,255:0.745098;244,167,132,255:0.764706;242,155,124,255:0.784314;238,142,117,255:0.803922;235,129,109,255:0.823529;232,117,101,255:0.843137;228,104,94,255:0.862745;225,91,86,255:0.882353;222,78,78,255:0.901961;218,65,70,255:0.921569;215,52,63,255:0.941176;212,39,55,255:0.960784;209,26,47,255:0.980392;205,13,40,255"/>
        </colorramp>
        <item label="120" alpha="255" color="#0571b0" value="119.73553466796875"/>
        <item label="121" alpha="255" color="#1078b4" value="121.38474225006713"/>
        <item label="123" alpha="255" color="#1b7eb7" value="123.0339582431427"/>
        <item label="125" alpha="255" color="#2685bb" value="124.68316582524109"/>
        <item label="126" alpha="255" color="#318bbe" value="126.33238181831665"/>
        <item label="128" alpha="255" color="#3c92c2" value="127.98158940041503"/>
        <item label="130" alpha="255" color="#4799c6" value="129.63079698251343"/>
        <item label="131" alpha="255" color="#529fc9" value="131.28002138656615"/>
        <item label="133" alpha="255" color="#5da6cd" value="132.9292457906189"/>
        <item label="135" alpha="255" color="#68acd0" value="134.57847019467164"/>
        <item label="136" alpha="255" color="#74b3d4" value="136.22761048895262"/>
        <item label="138" alpha="255" color="#7fb9d8" value="137.87683489300537"/>
        <item label="140" alpha="255" color="#8ac0db" value="139.5260592970581"/>
        <item label="141" alpha="255" color="#94c6de" value="141.17528370111086"/>
        <item label="143" alpha="255" color="#9ccae0" value="142.82450810516357"/>
        <item label="144" alpha="255" color="#a4cee2" value="144.47373250921632"/>
        <item label="146" alpha="255" color="#acd2e4" value="146.1228728034973"/>
        <item label="148" alpha="255" color="#b4d6e6" value="147.77209720755005"/>
        <item label="149" alpha="255" color="#bcdae8" value="149.4213216116028"/>
        <item label="151" alpha="255" color="#c3ddea" value="151.0705460156555"/>
        <item label="153" alpha="255" color="#cbe1ec" value="152.71977041970825"/>
        <item label="154" alpha="255" color="#d3e5ee" value="154.368994823761"/>
        <item label="156" alpha="255" color="#dbe9f0" value="156.01821922781372"/>
        <item label="158" alpha="255" color="#e3edf2" value="157.66735952209473"/>
        <item label="159" alpha="255" color="#ebf1f4" value="159.31658392614747"/>
        <item label="161" alpha="255" color="#f3f5f6" value="160.9658083302002"/>
        <item label="163" alpha="255" color="#f7f4f2" value="162.61503273425294"/>
        <item label="164" alpha="255" color="#f7ede9" value="164.26425713830565"/>
        <item label="166" alpha="255" color="#f6e7e0" value="165.9134815423584"/>
        <item label="168" alpha="255" color="#f6e1d7" value="167.5626218366394"/>
        <item label="169" alpha="255" color="#f6dace" value="169.21184624069213"/>
        <item label="171" alpha="255" color="#f6d4c5" value="170.86107064474487"/>
        <item label="173" alpha="255" color="#f5cdbb" value="172.51029504879762"/>
        <item label="174" alpha="255" color="#f5c7b2" value="174.15951945285036"/>
        <item label="176" alpha="255" color="#f5c0a9" value="175.80874385690308"/>
        <item label="177" alpha="255" color="#f5baa0" value="177.4579682609558"/>
        <item label="179" alpha="255" color="#f5b397" value="179.10710855523683"/>
        <item label="181" alpha="255" color="#f4ad8d" value="180.75633295928955"/>
        <item label="182" alpha="255" color="#f4a784" value="182.4055573633423"/>
        <item label="184" alpha="255" color="#f29b7c" value="184.054781767395"/>
        <item label="186" alpha="255" color="#ee8e75" value="185.70400617144776"/>
        <item label="187" alpha="255" color="#eb816d" value="187.3532305755005"/>
        <item label="189" alpha="255" color="#e87565" value="189.0023708697815"/>
        <item label="191" alpha="255" color="#e4685e" value="190.65159527383423"/>
        <item label="192" alpha="255" color="#e15b56" value="192.30081967788698"/>
        <item label="194" alpha="255" color="#de4e4e" value="193.95004408193972"/>
        <item label="196" alpha="255" color="#da4146" value="195.59926848599244"/>
        <item label="197" alpha="255" color="#d7343f" value="197.24849289004516"/>
        <item label="199" alpha="255" color="#d42737" value="198.89763318432617"/>
        <item label="201" alpha="255" color="#d11a2f" value="200.54685758837888"/>
        <item label="202" alpha="255" color="#cd0d28" value="202.19608199243163"/>
        <item label="204" alpha="255" color="#ca0020" value="203.84530639648438"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="7" max-val="950400" min-val="0" opacity="1">
      <colorrampshader clip="0" minimumValue="0" labelPrecision="6" classificationMode="1" maximumValue="950400" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="5,5,5,255"/>
            <Option type="QString" name="color2" value="250,250,250,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
          </Option>
          <prop k="color1" v="5,5,5,255"/>
          <prop k="color2" v="250,250,250,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
        </colorramp>
        <item label="0" alpha="255" color="#050505" value="0"/>
        <item label="18635.3" alpha="255" color="#0a0a0a" value="18635.25312"/>
        <item label="37270.6" alpha="255" color="#0f0f0f" value="37270.60128"/>
        <item label="55905.9" alpha="255" color="#131313" value="55905.854400000004"/>
        <item label="74541.2" alpha="255" color="#181818" value="74541.20256"/>
        <item label="93176.5" alpha="255" color="#1d1d1d" value="93176.45568000001"/>
        <item label="111812" alpha="255" color="#222222" value="111811.70880000001"/>
        <item label="130447" alpha="255" color="#272727" value="130447.15199999999"/>
        <item label="149083" alpha="255" color="#2b2b2b" value="149082.5952"/>
        <item label="167718" alpha="255" color="#303030" value="167718.0384"/>
        <item label="186353" alpha="255" color="#353535" value="186352.5312"/>
        <item label="204988" alpha="255" color="#3a3a3a" value="204987.97439999998"/>
        <item label="223623" alpha="255" color="#3f3f3f" value="223623.41760000002"/>
        <item label="242259" alpha="255" color="#434343" value="242258.86080000002"/>
        <item label="260894" alpha="255" color="#484848" value="260894.30399999997"/>
        <item label="279530" alpha="255" color="#4d4d4d" value="279529.7472"/>
        <item label="298164" alpha="255" color="#525252" value="298164.24"/>
        <item label="316800" alpha="255" color="#575757" value="316799.68319999997"/>
        <item label="335435" alpha="255" color="#5b5b5b" value="335435.1264"/>
        <item label="354071" alpha="255" color="#606060" value="354070.56960000005"/>
        <item label="372706" alpha="255" color="#656565" value="372706.01279999997"/>
        <item label="391341" alpha="255" color="#6a6a6a" value="391341.456"/>
        <item label="409977" alpha="255" color="#6f6f6f" value="409976.8992"/>
        <item label="428611" alpha="255" color="#737373" value="428611.392"/>
        <item label="447247" alpha="255" color="#787878" value="447246.83520000003"/>
        <item label="465882" alpha="255" color="#7d7d7d" value="465882.2784"/>
        <item label="484518" alpha="255" color="#828282" value="484517.72160000005"/>
        <item label="503153" alpha="255" color="#878787" value="503153.16479999997"/>
        <item label="521789" alpha="255" color="#8c8c8c" value="521788.60799999995"/>
        <item label="540423" alpha="255" color="#909090" value="540423.1008"/>
        <item label="559059" alpha="255" color="#959595" value="559058.544"/>
        <item label="577694" alpha="255" color="#9a9a9a" value="577693.9872"/>
        <item label="596329" alpha="255" color="#9f9f9f" value="596329.4304"/>
        <item label="614965" alpha="255" color="#a4a4a4" value="614964.8736"/>
        <item label="633600" alpha="255" color="#a8a8a8" value="633600.3168"/>
        <item label="652236" alpha="255" color="#adadad" value="652235.76"/>
        <item label="670870" alpha="255" color="#b2b2b2" value="670870.2528"/>
        <item label="689506" alpha="255" color="#b7b7b7" value="689505.696"/>
        <item label="708141" alpha="255" color="#bcbcbc" value="708141.1392000001"/>
        <item label="726777" alpha="255" color="#c0c0c0" value="726776.5824"/>
        <item label="745412" alpha="255" color="#c5c5c5" value="745412.0255999999"/>
        <item label="764047" alpha="255" color="#cacaca" value="764047.4688"/>
        <item label="782682" alpha="255" color="#cfcfcf" value="782681.9615999999"/>
        <item label="801317" alpha="255" color="#d4d4d4" value="801317.4048"/>
        <item label="819953" alpha="255" color="#d8d8d8" value="819952.848"/>
        <item label="838588" alpha="255" color="#dddddd" value="838588.2912000001"/>
        <item label="857224" alpha="255" color="#e2e2e2" value="857223.7344"/>
        <item label="875859" alpha="255" color="#e7e7e7" value="875859.1775999999"/>
        <item label="894494" alpha="255" color="#ececec" value="894493.6704000001"/>
        <item label="913129" alpha="255" color="#f0f0f0" value="913129.1135999999"/>
        <item label="931765" alpha="255" color="#f5f5f5" value="931764.5568"/>
        <item label="950400" alpha="255" color="#fafafa" value="950400"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="4" max-val="71.060000000000002" min-val="0" opacity="1">
      <colorrampshader clip="0" minimumValue="0" labelPrecision="6" classificationMode="2" maximumValue="71.060000000000002" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="255,0,255,0"/>
            <Option type="QString" name="color2" value="122,4,3,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="1e-08;48,18,59,255:0.01;70,98,216,255:0.02;53,171,248,255:0.05;27,229,181,255:0.1;116,254,93,255:0.2;201,239,52,255:0.3;251,185,56,255:0.5;245,105,24,255:0.75;201,41,3,255"/>
          </Option>
          <prop k="color1" v="255,0,255,0"/>
          <prop k="color2" v="122,4,3,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="1e-08;48,18,59,255:0.01;70,98,216,255:0.02;53,171,248,255:0.05;27,229,181,255:0.1;116,254,93,255:0.2;201,239,52,255:0.3;251,185,56,255:0.5;245,105,24,255:0.75;201,41,3,255"/>
        </colorramp>
        <item label="0,000000" alpha="0" color="#ff00ff" value="0"/>
        <item label="0,000001" alpha="255" color="#30123b" value="1e-06"/>
        <item label="1,000000" alpha="255" color="#4662d8" value="1"/>
        <item label="2,000000" alpha="255" color="#35abf8" value="2"/>
        <item label="5,000000" alpha="255" color="#1be5b5" value="5"/>
        <item label="10,000000" alpha="255" color="#74fe5d" value="10"/>
        <item label="20,000000" alpha="255" color="#c9ef34" value="20"/>
        <item label="30,000000" alpha="255" color="#fbb938" value="30"/>
        <item label="50,000000" alpha="255" color="#f56918" value="50"/>
        <item label="75,000000" alpha="255" color="#c92903" value="75"/>
        <item label="100,000000" alpha="255" color="#7a0403" value="100"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <scalar-settings interpolation-method="none" group="5" max-val="43" min-val="5" opacity="1">
      <colorrampshader clip="0" minimumValue="5" labelPrecision="6" classificationMode="2" maximumValue="43" colorRampType="DISCRETE">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="190,138,68,255"/>
            <Option type="QString" name="color2" value="1,133,113,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.285714;190,138,68,255:0.428571;226,201,142,255:0.571429;242,238,228,255:0.714286;206,242,214,255:0.857143;92,184,170,255"/>
          </Option>
          <prop k="color1" v="190,138,68,255"/>
          <prop k="color2" v="1,133,113,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.285714;190,138,68,255:0.428571;226,201,142,255:0.571429;242,238,228,255:0.714286;206,242,214,255:0.857143;92,184,170,255"/>
        </colorramp>
        <item label="Zones à forte urbanisation : Ks = 8" alpha="255" color="#be8a44" value="15.857142857142858"/>
        <item label="Zones à faible urbanisation : Ks = 10" alpha="255" color="#e2c98e" value="21.285714285714285"/>
        <item label="Zones d'arbustes, de sous-bois : Ks = 12" alpha="255" color="#f2eee4" value="26.714285714285715"/>
        <item label="Zones de champs cultivés à végétation haute : Ks = 15" alpha="255" color="#cef2d6" value="32.142857142857146"/>
        <item label="Zones de champs cultivés à végétation basse et de champs, prairies sans culture : Ks = 20" alpha="255" color="#5cb8aa" value="37.57142857142857"/>
        <item label="Lit mineur" alpha="255" color="#018571" value="inf"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <edge-settings stroke-width-unit="0">
        <mesh-stroke-width maximum-value="10" minimum-width="0.26000000000000001" ignore-out-of-range="0" maximum-width="3" use-absolute-value="0" fixed-width="0.26000000000000001" width-varying="0" minimum-value="0"/>
      </edge-settings>
    </scalar-settings>
    <vector-settings line-width="0.26000000000000001" user-grid-width="10" group="0" user-grid-enabled="0" symbology="0" user-grid-height="10" filter-max="10" coloring-method="0" color="0,0,0,255" filter-min="-1">
      <colorrampshader clip="0" minimumValue="0" labelPrecision="6" classificationMode="1" maximumValue="6.75" colorRampType="INTERPOLATED">
        <colorramp type="gradient" name="[source]">
          <Option type="Map">
            <Option type="QString" name="color1" value="13,8,135,255"/>
            <Option type="QString" name="color2" value="240,249,33,255"/>
            <Option type="QString" name="discrete" value="0"/>
            <Option type="QString" name="rampType" value="gradient"/>
            <Option type="QString" name="stops" value="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
          </Option>
          <prop k="color1" v="13,8,135,255"/>
          <prop k="color2" v="240,249,33,255"/>
          <prop k="discrete" v="0"/>
          <prop k="rampType" v="gradient"/>
          <prop k="stops" v="0.0196078;27,6,141,255:0.0392157;38,5,145,255:0.0588235;47,5,150,255:0.0784314;56,4,154,255:0.0980392;65,4,157,255:0.117647;73,3,160,255:0.137255;81,2,163,255:0.156863;89,1,165,255:0.176471;97,0,167,255:0.196078;105,0,168,255:0.215686;113,0,168,255:0.235294;120,1,168,255:0.254902;128,4,168,255:0.27451;135,7,166,255:0.294118;142,12,164,255:0.313725;149,17,161,255:0.333333;156,23,158,255:0.352941;162,29,154,255:0.372549;168,34,150,255:0.392157;174,40,146,255:0.411765;180,46,141,255:0.431373;186,51,136,255:0.45098;191,57,132,255:0.470588;196,62,127,255:0.490196;201,68,122,255:0.509804;205,74,118,255:0.529412;210,79,113,255:0.54902;214,85,109,255:0.568627;218,91,105,255:0.588235;222,97,100,255:0.607843;226,102,96,255:0.627451;230,108,92,255:0.647059;233,114,87,255:0.666667;237,121,83,255:0.686275;240,127,79,255:0.705882;243,133,75,255:0.72549;245,140,70,255:0.745098;247,147,66,255:0.764706;249,154,62,255:0.784314;251,161,57,255:0.803922;252,168,53,255:0.823529;253,175,49,255:0.843137;254,183,45,255:0.862745;254,190,42,255:0.882353;253,198,39,255:0.901961;252,206,37,255:0.921569;251,215,36,255:0.941176;248,223,37,255:0.960784;246,232,38,255:0.980392;243,240,39,255"/>
        </colorramp>
        <item label="0,000000" alpha="255" color="#0d0887" value="0"/>
        <item label="0,132353" alpha="255" color="#1b068d" value="0.13235265"/>
        <item label="0,264706" alpha="255" color="#260591" value="0.264705975"/>
        <item label="0,397059" alpha="255" color="#2f0596" value="0.397058625"/>
        <item label="0,529412" alpha="255" color="#38049a" value="0.52941195"/>
        <item label="0,661765" alpha="255" color="#41049d" value="0.6617646"/>
        <item label="0,794117" alpha="255" color="#4903a0" value="0.79411725"/>
        <item label="0,926471" alpha="255" color="#5102a3" value="0.92647125"/>
        <item label="1,058825" alpha="255" color="#5901a5" value="1.05882525"/>
        <item label="1,191179" alpha="255" color="#6100a7" value="1.19117925"/>
        <item label="1,323527" alpha="255" color="#6900a8" value="1.3235265"/>
        <item label="1,455880" alpha="255" color="#7100a8" value="1.4558805"/>
        <item label="1,588234" alpha="255" color="#7801a8" value="1.5882345"/>
        <item label="1,720589" alpha="255" color="#8004a8" value="1.7205885"/>
        <item label="1,852942" alpha="255" color="#8707a6" value="1.8529425"/>
        <item label="1,985297" alpha="255" color="#8e0ca4" value="1.9852965"/>
        <item label="2,117644" alpha="255" color="#9511a1" value="2.11764375"/>
        <item label="2,249998" alpha="255" color="#9c179e" value="2.24999775"/>
        <item label="2,382352" alpha="255" color="#a21d9a" value="2.38235175"/>
        <item label="2,514706" alpha="255" color="#a82296" value="2.51470575"/>
        <item label="2,647060" alpha="255" color="#ae2892" value="2.64705975"/>
        <item label="2,779414" alpha="255" color="#b42e8d" value="2.77941375"/>
        <item label="2,911768" alpha="255" color="#ba3388" value="2.91176775"/>
        <item label="3,044115" alpha="255" color="#bf3984" value="3.044115"/>
        <item label="3,176469" alpha="255" color="#c43e7f" value="3.176469"/>
        <item label="3,308823" alpha="255" color="#c9447a" value="3.308823"/>
        <item label="3,441177" alpha="255" color="#cd4a76" value="3.441177"/>
        <item label="3,573531" alpha="255" color="#d24f71" value="3.573531"/>
        <item label="3,705885" alpha="255" color="#d6556d" value="3.705885"/>
        <item label="3,838232" alpha="255" color="#da5b69" value="3.83823225"/>
        <item label="3,970586" alpha="255" color="#de6164" value="3.97058625"/>
        <item label="4,102940" alpha="255" color="#e26660" value="4.102940250000001"/>
        <item label="4,235294" alpha="255" color="#e66c5c" value="4.23529425"/>
        <item label="4,367648" alpha="255" color="#e97257" value="4.36764825"/>
        <item label="4,500002" alpha="255" color="#ed7953" value="4.50000225"/>
        <item label="4,632356" alpha="255" color="#f07f4f" value="4.63235625"/>
        <item label="4,764704" alpha="255" color="#f3854b" value="4.7647035"/>
        <item label="4,897057" alpha="255" color="#f58c46" value="4.8970575"/>
        <item label="5,029412" alpha="255" color="#f79342" value="5.0294115"/>
        <item label="5,161765" alpha="255" color="#f99a3e" value="5.1617655"/>
        <item label="5,294119" alpha="255" color="#fba139" value="5.2941195"/>
        <item label="5,426474" alpha="255" color="#fca835" value="5.4264735"/>
        <item label="5,558821" alpha="255" color="#fdaf31" value="5.55882075"/>
        <item label="5,691175" alpha="255" color="#feb72d" value="5.69117475"/>
        <item label="5,823529" alpha="255" color="#febe2a" value="5.82352875"/>
        <item label="5,955883" alpha="255" color="#fdc627" value="5.955882750000001"/>
        <item label="6,088237" alpha="255" color="#fcce25" value="6.08823675"/>
        <item label="6,220591" alpha="255" color="#fbd724" value="6.22059075"/>
        <item label="6,352938" alpha="255" color="#f8df25" value="6.352938"/>
        <item label="6,485292" alpha="255" color="#f6e826" value="6.485292"/>
        <item label="6,617646" alpha="255" color="#f3f027" value="6.617646000000001"/>
        <item label="6,750000" alpha="255" color="#f0f921" value="6.75"/>
        <rampLegendSettings maximumLabel="" direction="0" orientation="2" useContinuousLegend="1" minimumLabel="" suffix="" prefix="">
          <numericFormat id="basic">
            <Option type="Map">
              <Option type="QChar" name="decimal_separator" value=""/>
              <Option type="int" name="decimals" value="6"/>
              <Option type="int" name="rounding_type" value="0"/>
              <Option type="bool" name="show_plus" value="false"/>
              <Option type="bool" name="show_thousand_separator" value="true"/>
              <Option type="bool" name="show_trailing_zeros" value="false"/>
              <Option type="QChar" name="thousand_separator" value=""/>
            </Option>
          </numericFormat>
        </rampLegendSettings>
      </colorrampshader>
      <vector-arrow-settings arrow-head-length-ratio="0.40000000000000002" arrow-head-width-ratio="0.14999999999999999">
        <shaft-length method="minmax" max="10" min="0.80000000000000004"/>
      </vector-arrow-settings>
      <vector-streamline-settings seeding-method="0" seeding-density="0.14999999999999999"/>
      <vector-traces-settings maximum-tail-length="100" particles-count="1000" maximum-tail-length-unit="0"/>
    </vector-settings>
    <mesh-settings-native line-width="0.26000000000000001" enabled="0" color="0,0,0,255" line-width-unit="MM"/>
    <mesh-settings-edge line-width="0.26000000000000001" enabled="0" color="0,0,0,255" line-width-unit="MM"/>
    <mesh-settings-triangular line-width="0.26000000000000001" enabled="0" color="0,0,0,255" line-width-unit="MM"/>
    <averaging-3d method="0">
      <multi-vertical-layers-settings start-layer-index="1" end-layer-index="1"/>
    </averaging-3d>
  </mesh-renderer-settings>
  <mesh-simplify-settings enabled="0" mesh-resolution="5" reduction-factor="10"/>
  <blendMode>0</blendMode>
  <layerOpacity>1</layerOpacity>
</qgis>
