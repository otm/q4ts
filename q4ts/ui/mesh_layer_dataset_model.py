from enum import Enum

from qgis.core import QgsMeshDatasetIndex, QgsMeshLayer
from qgis.PyQt import QtCore
from qgis.PyQt.QtCore import Qt, QVariant
from qgis.PyQt.QtGui import QStandardItemModel

from q4ts.processing.variables_utils import VariablesUtils, VectorDatasetSplit
from q4ts.toolbelt import PlgLogger


class MeshDatasetListModel(QStandardItemModel):
    def __init__(self, parent):
        super().__init__(parent)
        self.log = PlgLogger().log
        self.setHorizontalHeaderLabels([self.tr("Variable")])
        self.upper_variable = False
        self.vector_dataset_split = VectorDatasetSplit.ALONG
        self._mesh_layer = QgsMeshLayer()

    def check_all_dataset(self, check: bool) -> None:
        state = Qt.Checked if check else Qt.Unchecked
        for row in range(0, self.rowCount()):
            self.setData(self.index(row, 0), state, QtCore.Qt.CheckStateRole)

    def set_upper_variable(self, enable: bool) -> None:
        if self.upper_variable != enable:
            self.upper_variable = enable
            self.dataChanged.emit(self.index(0, 0), self.index(self.rowCount(), 0))

    def set_vector_dataset_split(self, vector_dataset_split: VectorDatasetSplit) -> None:
        self.vector_dataset_split = vector_dataset_split
        self.set_mesh_layer(self._mesh_layer)

    def set_mesh_layer(self, mesh_layer: QgsMeshLayer) -> None:
        while self.rowCount():
            self.removeRow(0)

        self._mesh_layer = mesh_layer
        if not self._mesh_layer.isValid():
            return

        for index in mesh_layer.datasetGroupsIndexes():
            index_group = QgsMeshDatasetIndex(index)
            dataset_metadata = mesh_layer.datasetGroupMetadata(index_group)

            if dataset_metadata.isVector():
                x_variable, y_variable = VariablesUtils().convert_vector_dataset_name_to_selafin_variables(dataset_metadata,
                                                                                                           self.vector_dataset_split)

                self._add_dataset_name(x_variable)
                self._add_dataset_name(y_variable)
            else:

                variable = VariablesUtils().convert_dataset_name_to_selafin_variable(dataset_metadata)
                self._add_dataset_name(variable)

    def data(self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole) -> QVariant:
        """
        Override QStandardItemModel data() for upper variable name

        Args:
            index:
            role:

        Returns:

        """
        res = super().data(index, role)
        if role == QtCore.Qt.DisplayRole:
            res = str(res).upper() if self.upper_variable else res
        return res

    def _add_dataset_name(self, name):
        self.insertRow(self.rowCount())
        row = self.rowCount() - 1
        self.setData(self.index(row, 0), name)
        self.setData(self.index(row, 0), Qt.Checked, QtCore.Qt.CheckStateRole)

    def get_dataset_name(self, only_checked: bool = False) -> [str]:
        res = []
        for row in range(0, self.rowCount()):
            if not only_checked:
                res.append(str(self.data(self.index(row, 0))))
            elif self.data(self.index(row, 0), QtCore.Qt.CheckStateRole) == Qt.Checked:
                res.append(str(self.data(self.index(row, 0))))

        return res

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlags:
        """
        Override QStandardItemModel flags.

        Args:
            index: QModelIndex

        Returns: index flags

        """
        # All item are enabled and selectable
        flags = Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable
        # All item should be checkable
        flags = flags | Qt.ItemIsUserCheckable
        return flags
