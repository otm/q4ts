from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel

from qgis.core import (
    QgsMeshDatasetIndex,
    QgsMeshLayer
)


class MeshTimeListModel(QStandardItemModel):
    def __init__(self, parent):
        super().__init__(parent)
        self._mesh_layer = None
        self._dataset_index = -1
        self.setHorizontalHeaderLabels([self.tr("Times")])

        # Need a bool to enable or disable check
        # because even with NoEditTriggers, QTableView still accepts CheckStateRole modification
        self.check_enable = True

    def set_mesh_layer(self, mesh_layer: QgsMeshLayer) -> None:
        self._mesh_layer = mesh_layer
        while self.rowCount():
            self.removeRow(0)

        dataset_index = self.get_used_dataset_index()
        if dataset_index >= 0:
            index_group = QgsMeshDatasetIndex(dataset_index)
            for index_row in range(0, self._mesh_layer.datasetCount(index_group)):
                index_time = QgsMeshDatasetIndex(dataset_index, index_row)
                time_ms = self._mesh_layer.datasetRelativeTimeInMilliseconds(index_time)

                self.insertRow(self.rowCount())
                row = self.rowCount() - 1
                self.setData(self.index(row, 0), time_ms / 1000.0)  ## Display time in second
                self.setData(self.index(row, 0), Qt.Checked, QtCore.Qt.CheckStateRole)

    def set_dataset_index(self, index: int) -> None:
        self._dataset_index = index
        self.set_mesh_layer(self._mesh_layer)

    def check_all_layer(self, check: bool) -> None:
        state = Qt.Checked if check else Qt.Unchecked
        for row in range(0, self.rowCount()):
            self.setData(self.index(row, 0), state, QtCore.Qt.CheckStateRole)

    def check_last_row_only(self) -> None:
        for row in range(0, self.rowCount()):
            state = Qt.Checked if row == self.rowCount() - 1 else Qt.Unchecked
            self.setData(self.index(row, 0), state, QtCore.Qt.CheckStateRole)

    def select_date_from_specific_time_range(self, start: int, stop: int, step: int):
        for row in range(0, self.rowCount()):
            self.setData(self.index(row, 0), Qt.Unchecked, QtCore.Qt.CheckStateRole)
        for row in range(start, stop + 1, step):
            self.setData(self.index(row, 0), Qt.Checked, QtCore.Qt.CheckStateRole)

    def get_selected_times(self) -> [str]:
        res = []
        for row in range(0, self.rowCount()):
            if self.data(self.index(row, 0), QtCore.Qt.CheckStateRole) == Qt.Checked:
                res.append(str(self.data(self.index(row, 0))))
        return res

    def get_available_time_str(self) -> [str]:
        res = []
        for row in range(0, self.rowCount()):
            res.append(str(self.data(self.index(row, 0))))
        return res

    def get_used_dataset_index(self) -> int:
        dataset_index = self._dataset_index
        if not self._is_dataset_index_valid(dataset_index):
            dataset_index = self.get_default_dataset_index()
        return dataset_index

    def _is_dataset_index_valid(self, dataset_index: int) -> bool:
        res = False
        if self._mesh_layer and dataset_index >= 0:
            dataset_size = len(self._mesh_layer.datasetGroupsIndexes())
            res = dataset_index < dataset_size
        return res

    def get_default_dataset_index(self) -> int:
        res = -1
        if self._mesh_layer and len(self._mesh_layer.datasetGroupsIndexes()):
            res = self._mesh_layer.datasetGroupsIndexes()[0]
        return res

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlags:
        """
        Override QAbstractListModel flags.

        Args:
            index: QModelIndex

        Returns: index flags

        """
        # All item are enabled and selectable
        flags = Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable
        if self.check_enable:
            flags = flags | Qt.ItemIsUserCheckable
        return flags
