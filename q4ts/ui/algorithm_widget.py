import os
from functools import partial

from qgis import processing
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsCoordinateTransform,
    QgsMapLayer,
    QgsMeshLayer,
    QgsMessageLog,
    QgsProcessingAlgRunnerTask,
    QgsProcessingContext,
    QgsProject,
    QgsVectorLayer,
)
from qgis.gui import QgsMapLayerComboBox
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QMessageBox, QWidget

from q4ts.processing.contour import CountourAlgorithm
from q4ts.processing.qgis_utils import (
    get_available_layer_name,
    get_available_layer_name_in_gpkg,
    get_or_create_q4ts_gpkg,
)
from q4ts.ui.layer_selection_creation import LayerSelectionCreation
from q4ts.ui.processing_feedback import QTextEditProcessingFeedBack
from q4ts.ui.ui_utils import get_or_create_group


class AlgorithmWidget(QWidget):
    def __init__(self, parent=None, iface=None, ui_path: str = None):
        self._iface = iface
        self._task = None
        self._feedback = None
        super().__init__(parent)
        if ui_path:
            uic.loadUi(ui_path, self)

    def _set_running(self, running: bool, create_action=None):
        try:
            self.createButton.disconnect()
        except Exception:
            pass

        if running:
            self.createButton.setIcon(QIcon(QgsApplication.iconPath("mActionStop.svg")))
            self.createButton.clicked.connect(self._cancel_task)
        else:
            self.createButton.setIcon(
                QIcon(QgsApplication.iconPath("mActionStart.svg"))
            )
            if create_action:
                self.createButton.clicked.connect(create_action)

    def _cancel_task(self):
        if self._task:
            self._task.cancel()

    def _run_alg(self, alg, params, executed_callback, *args):
        context = QgsProcessingContext()
        res, error = alg.checkParameterValues(params, context)
        if res:
            self.resultLogEdit.clear()
            self._feedback = QTextEditProcessingFeedBack(self.resultLogEdit)
            self._task = QgsProcessingAlgRunnerTask(
                alg, params, context, self._feedback
            )
            self._task.executed.connect(partial(executed_callback, context, *args))
            QgsApplication.taskManager().addTask(self._task)
            self._set_running(True)
        else:
            QMessageBox.critical(
                self,
                self.tr("Can't run {0}".format(alg.name())),
                self.tr("Invalid parameters : {0}.".format(error)),
            )

    def _check_if_layer_saved(self, display_name: str, layer: QgsMapLayer) -> bool:
        if layer.isEditable():
            saved = False
            reply = QMessageBox.question(
                self,
                self.tr("Save layer"),
                self.tr(
                    "{} '{}' not saved.\nSave needed before run. Do you want to save layer ?".format(
                        display_name, layer.name()
                    )
                ),
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No,
            )
            if reply == QMessageBox.Yes:
                if isinstance(layer, QgsMeshLayer):
                    tr = QgsCoordinateTransform(
                        layer.crs(),
                        self._iface.mapCanvas().mapSettings().destinationCrs(),
                        QgsProject.instance(),
                    )
                    layer.commitFrameEditing(tr, False)
                else:
                    layer.commitChanges()
                saved = True
        else:
            saved = True
        return saved

    def create_contour(
        self,
        mapLayerCombobox: QgsMapLayerComboBox,
        layer_selection_cbx: LayerSelectionCreation,
    ):
        params = {
            CountourAlgorithm.INPUT_MESH: mapLayerCombobox.currentLayer(),
            CountourAlgorithm.OUTPUT_COUNTOUR: "TEMPORARY_OUTPUT",
        }

        algo_str = "q4ts:contour"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)
        if alg:
            self._run_alg(alg, params, self.contour_created, layer_selection_cbx)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def contour_created(
        self,
        context,
        layer_selection_cbx: LayerSelectionCreation,
        successful: bool,
        results,
    ):
        self._set_not_running()
        self.check_inout_file()
        if not successful:
            QgsMessageLog.logMessage(
                "Create boundary finished unsucessfully",
                "Create boundary",
                Qgis.Warning,
            )
        else:
            contour_layer_file = results[CountourAlgorithm.OUTPUT_COUNTOUR]
            if contour_layer_file:
                layer_name = get_available_layer_name("ContourMesh")

                gpkg_file_name = get_or_create_q4ts_gpkg(self)
                if gpkg_file_name:
                    layer_name = get_available_layer_name_in_gpkg(
                        gpkg_file_name, "ContourMesh"
                    )

                contour_layer = QgsVectorLayer(contour_layer_file, layer_name, "ogr")

                # Need to load style before packaging to save style in .gpkg
                contour_layer.loadNamedStyle(
                    os.path.dirname(os.path.realpath(__file__))
                    + "/../style/ContourMesh.qml"
                )

                if gpkg_file_name:
                    processing.run(
                        "native:package",
                        {"LAYERS": [contour_layer], "OUTPUT": gpkg_file_name},
                    )

                    uri = "%s|layername=%s" % (
                        gpkg_file_name,
                        layer_name,
                    )
                    layer = QgsVectorLayer(uri, layer_name, "ogr")
                else:
                    layer = contour_layer

                # Check CRS
                crs = layer.crs()
                if not crs.isValid() or not crs.authid():
                    layer.setCrs(QgsProject.instance().crs())

                group = get_or_create_group("contour_mesh")
                QgsProject().instance().addMapLayer(layer, False)
                group.addLayer(layer)
                layer_selection_cbx.mMapLayerComboBox.setLayer(layer)
            else:
                QMessageBox.critical(
                    self,
                    self.tr("Error in processing"),
                    self.tr("Contour not available, check processing logs"),
                )
