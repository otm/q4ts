import os

from qgis.core import (
    Qgis,
    QgsApplication,
    QgsMapLayerProxyModel,
    QgsMeshDatasetIndex,
    QgsMessageLog,
)
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAbstractItemView, QFileDialog, QMessageBox

from q4ts.processing.alter import AlterAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.processing.variables_utils import VariablesUtils, VectorDatasetSplit
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.mesh_layer_dataset_model import MeshDatasetListModel
from q4ts.ui.mesh_layer_time_model import MeshTimeListModel
from q4ts.ui.ui_utils import (
    connect_zoom_selection,
    copy_input_mesh_renderer_settings,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class AlterWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__)) + "/alter_widget.ui",
        )
        self.setWindowIcon(AlterAlgorithm().icon())

        # Mesh layer selection
        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)
        self.mMapLayerComboBox.layerChanged.connect(self._update_mesh_dataset)
        connect_zoom_selection(self.mMapLayerComboBox, self.zoomMeshButton, self._iface)

        # Output mesh definition
        self.outputMeshSelectionButton.clicked.connect(self.select_result_mesh)
        self.outputMeshEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.outputMeshEdit.editingFinished.connect(self.check_inout_file)
        self.createButton.setEnabled(False)
        self.createButton.setIcon(QIcon(QgsApplication.iconPath("mActionStart.svg")))

        # Tablemodel for mesh dataset display
        self._layer_dataset_model = MeshDatasetListModel(self)
        self.variableTableView.setModel(self._layer_dataset_model)
        self.variableTableView.horizontalHeader().setStretchLastSection(True)

        # Options for mesh dataset extract
        # Upper dataset name
        self.upper_variable_checkbox.setChecked(True)
        self._layer_dataset_model.set_upper_variable(True)
        self.upper_variable_checkbox.clicked.connect(
            lambda clicked: self._layer_dataset_model.set_upper_variable(clicked)
        )
        # Vector dataset split option
        for e in VectorDatasetSplit:
            self.vector_dataset_split_combobox.addItem(e.value)
        self.vector_dataset_split_combobox.setCurrentText(
            VectorDatasetSplit.ALONG.value
        )
        self._layer_dataset_model.set_vector_dataset_split(VectorDatasetSplit.ALONG)

        self.vector_dataset_split_combobox.currentTextChanged.connect(
            lambda text: self._layer_dataset_model.set_vector_dataset_split(
                VectorDatasetSplit(text)
            )
        )

        self.uncheck_all_variable_button.clicked.connect(
            lambda clicked: self._layer_dataset_model.check_all_dataset(False)
        )
        self.check_all_variable_button.clicked.connect(
            lambda clicked: self._layer_dataset_model.check_all_dataset(True)
        )

        # Tablemodel for mesh time display
        self._layer_time_model = MeshTimeListModel(self)
        self.timeTableView.setModel(self._layer_time_model)
        self.timeTableView.horizontalHeader().setStretchLastSection(True)

        # Options for times selection
        # Specific values
        self.last_time_button.clicked.connect(
            self._layer_time_model.check_last_row_only
        )
        self.uncheck_all_time_button.clicked.connect(
            lambda clicked: self._layer_time_model.check_all_layer(False)
        )
        self.check_all_time_button.clicked.connect(
            lambda clicked: self._layer_time_model.check_all_layer(True)
        )

        # Specific times range
        self.specific_time_rang_gb.clicked.connect(self._specific_time_range_changed)
        self.first_time_combobox.currentIndexChanged.connect(
            self._select_date_from_specific_time_range
        )
        self.last_time_combobox.currentIndexChanged.connect(
            self._select_date_from_specific_time_range
        )
        self.time_step_spinbox.setMinimum(1)
        self.time_step_spinbox.valueChanged.connect(
            self._select_date_from_specific_time_range
        )

        self._define_shift_spin_box_min_max()
        self.rotateSpinBox.setMinimum(AlterAlgorithm.ROTATE_MIN_VALUE)
        self.rotateSpinBox.setMaximum(AlterAlgorithm.ROTATE_MAX_VALUE)
        self._set_not_running()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.alter)

    def _define_shift_spin_box_min_max(self):
        def _define_shift_spin_box_min_max(spinbox):
            spinbox.setMinimum(VariablesUtils.SHIFT_MIN_VALUE)
            spinbox.setMaximum(VariablesUtils.SHIFT_MAX_VALUE)
            spinbox.setDecimals(VariablesUtils.SHIFT_DECIMAL)

        _define_shift_spin_box_min_max(self.shiftXSpinBox)
        _define_shift_spin_box_min_max(self.shiftYSpinBox)

    def select_result_mesh(self):
        filename, filter_use = QFileDialog.getSaveFileName(
            self,
            "Select file",
            get_current_q4ts_gpkg_folder() + "alter.slf",
            "Mesh file (*.slf)",
        )

        self.outputMeshEdit.setText(filename)
        self.check_inout_file()

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr("Input mesh must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(
                self.tr("Input mesh"), self.mMapLayerComboBox.currentLayer()
            )

        if self.specific_time_rang_gb.isChecked():
            start = self.first_time_combobox.currentIndex()
            stop = self.last_time_combobox.currentIndex()
            if start > stop:
                tooltip = self.tr("Start time index must be before end time index")
                enable = False
        return enable, tooltip

    def alter(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)

        if not enable:
            return

        params = {
            AlterAlgorithm.INPUT_MESH: self.mMapLayerComboBox.currentLayer(),
            AlterAlgorithm.SHIFT_X: self.shiftXSpinBox.value(),
            AlterAlgorithm.SHIFT_Y: self.shiftYSpinBox.value(),
            AlterAlgorithm.ROTATE: self.rotateSpinBox.value(),
            AlterAlgorithm.OUTPUT_MESH: get_filepath_or_temp_output(
                self.outputMeshEdit
            ),
        }
        mesh_layer = self.mMapLayerComboBox.currentLayer()
        selected_datasets = self._layer_dataset_model.get_dataset_name(
            only_checked=True
        )
        nb_dataset = len(self._layer_dataset_model.get_dataset_name(only_checked=False))

        if len(selected_datasets) != nb_dataset:
            params[AlterAlgorithm.VARS] = ",".join(selected_datasets)

        if nb_dataset != 0:
            if self.specific_time_rang_gb.isChecked():
                params[
                    AlterAlgorithm.TIME_FROM
                ] = self.first_time_combobox.currentIndex()
                params[AlterAlgorithm.TIME_END] = self.last_time_combobox.currentIndex()
                params[AlterAlgorithm.TIME_STEP] = self.time_step_spinbox.value()
            else:
                selected_times = self._layer_time_model.get_selected_times()

                # Compare with current used dataset time to add option only if needed (some time index are unchecked)
                dataset_index = self._layer_time_model.get_used_dataset_index()
                index_group = QgsMeshDatasetIndex(dataset_index)
                nb_time = mesh_layer.datasetCount(index_group)

                if len(selected_times) != nb_time:
                    time_str = " ".join(selected_times)
                    params[AlterAlgorithm.TIMES] = time_str

        algo_str = "q4ts:alter"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)

        if alg:
            self._run_alg(alg, params, self.alter_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def alter_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()

        if not successful:
            QgsMessageLog.logMessage(
                "Alteration of mesh finished unsucessfully", "Alter mesh", Qgis.Warning
            )
        else:
            mesh_layer = import_mesh_layer(
                self, results["OUTPUT_MESH"], "alter_mesh", self.outputMeshEdit, "alter"
            )
            if mesh_layer:
                copy_input_mesh_renderer_settings(
                    self.mMapLayerComboBox.currentLayer(), mesh_layer
                )

                # Need to define reference time or loaded style will override temporal properties
                ref_time = (
                    self.mMapLayerComboBox.currentLayer()
                    .temporalProperties()
                    .referenceTime()
                )
                if ref_time.isValid():
                    mesh_layer.setReferenceTime(ref_time)

    def _specific_time_range_changed(self):
        enabled = not self.specific_time_rang_gb.isChecked()

        # Enable/Disable quick index selection button
        self.last_time_button.setEnabled(enabled)
        self.uncheck_all_time_button.setEnabled(enabled)
        self.check_all_time_button.setEnabled(enabled)

        # Enable/Disable time table view edition
        self._layer_time_model.check_enable = enabled
        if enabled:
            self.timeTableView.setEditTriggers(QAbstractItemView.NoEditTriggers)
            self._select_date_from_specific_time_range()
        else:
            self.timeTableView.setEditTriggers(QAbstractItemView.SelectedClicked)

    def _select_date_from_specific_time_range(self):
        self._layer_time_model.select_date_from_specific_time_range(
            self.first_time_combobox.currentIndex(),
            self.last_time_combobox.currentIndex(),
            self.time_step_spinbox.value(),
        )
        # Check if processing can be run
        self.check_inout_file()

    def _update_mesh_dataset(self):
        if self.mMapLayerComboBox.currentLayer():
            self._layer_time_model.set_mesh_layer(self.mMapLayerComboBox.currentLayer())
            self._layer_dataset_model.set_mesh_layer(
                self.mMapLayerComboBox.currentLayer()
            )

            self.first_time_combobox.clear()
            self.last_time_combobox.clear()

            available_time_str = self._layer_time_model.get_available_time_str()
            self.first_time_combobox.addItems(available_time_str)
            self.last_time_combobox.addItems(available_time_str)
