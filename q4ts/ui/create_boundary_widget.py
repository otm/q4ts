import os

from qgis.core import (
    Qgis,
    QgsApplication,
    QgsMapLayerProxyModel,
    QgsMessageLog,
    QgsWkbTypes,
)
from qgis.PyQt.QtWidgets import QFileDialog, QMessageBox

from q4ts.processing.create_boundary import CreateBoundaryAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.ui_utils import connect_zoom_selection, get_filepath_or_temp_output


class CreateBoundaryWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__)) + "/create_boundary_widget.ui",
        )

        self.setWindowIcon(CreateBoundaryAlgorithm().icon())

        self.boundarySelectionWidget.set_iface(iface)
        self.boundarySelectionWidget.setLayerWkb(QgsWkbTypes.LineString)
        self.boundarySelectionWidget.setFields(
            CreateBoundaryAlgorithm.BOUNDARIES_FIELDS
        )
        self.boundarySelectionWidget.setLayerStyle("Boundaries_v2.qml")
        self.boundarySelectionWidget.setEditForm(
            "boundary_feature_widget.ui", "boundary_feature_widget.py"
        )
        self.boundarySelectionWidget.layer_create_name = "boundaries"
        self.boundarySelectionWidget.directory = "boundaries"
        self.boundarySelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.check_inout_file
        )

        self.contourSelectionWidget.set_iface(iface)
        self.contourSelectionWidget.setLayerWkb(QgsWkbTypes.Polygon)
        self.contourSelectionWidget.setLayerStyle("Contour_v2.qml")
        self.contourSelectionWidget.define_custom_creation_callback(
            lambda: self.create_contour(
                self.mMapLayerComboBox, self.contourSelectionWidget
            )
        )
        self.contourSelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.check_inout_file
        )

        # Auto update of snapping when layer is selected
        self.contourSelectionWidget.enable_snapping(True)
        self.contourSelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.contourSelectionWidget.define_snapping_config_from_layer
        )

        self.defaultBoundValueSpinBox.setMinimum(
            CreateBoundaryAlgorithm.DEFAULT_BND_MIN_VAL
        )
        self.defaultBoundValueSpinBox.setMaximum(
            CreateBoundaryAlgorithm.DEFAULT_BND_MAX_VAL
        )
        self.defaultBoundValueSpinBox.setValue(
            CreateBoundaryAlgorithm.DEFAULT_BND_INIT_VAL
        )

        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)

        self.outputMeshSelectionButton.clicked.connect(self.select_result_mesh)
        self.outputMeshEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.outputMeshEdit.editingFinished.connect(self.check_inout_file)

        connect_zoom_selection(self.mMapLayerComboBox, self.zoomMeshButton, self._iface)

        self._set_not_running()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.create_boundaries)

    def select_result_mesh(self):
        filename, _ = QFileDialog.getSaveFileName(
            self,
            "Select file",
            get_current_q4ts_gpkg_folder() + "create_boundary.slf",
            "Mesh file (*.slf)",
        )

        self.outputMeshEdit.setText(filename)
        self.check_inout_file()

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr("Input mesh must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(
                self.tr("Input mesh"), self.mMapLayerComboBox.currentLayer()
            )

        if not self.contourSelectionWidget.currentLayer():
            tooltip = self.tr("Contour layer must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(
                self.tr("Contour layer"), self.contourSelectionWidget.currentLayer()
            )

        if not self.boundarySelectionWidget.currentLayer():
            tooltip = self.tr("Boundary layer must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(
                self.tr("Boundary layer"), self.boundarySelectionWidget.currentLayer()
            )
        return enable, tooltip

    def create_boundaries(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)
        if not enable:
            return

        params = {
            CreateBoundaryAlgorithm.CONTOUR_LAYER: self.contourSelectionWidget.currentLayer(),
            CreateBoundaryAlgorithm.BOUNDARIES_LAYER: self.boundarySelectionWidget.currentLayer(),
            CreateBoundaryAlgorithm.INPUT_MESH: self.mMapLayerComboBox.currentLayer(),
            CreateBoundaryAlgorithm.OUTPUT_MESH: get_filepath_or_temp_output(
                self.outputMeshEdit
            ),
            CreateBoundaryAlgorithm.DEFAULT_BND: self.defaultBoundValueSpinBox.value(),
        }

        algo_str = "q4ts:create_boundary"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)
        if alg:
            self._run_alg(alg, params, self.boundaries_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def boundaries_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()
        if not successful:
            QgsMessageLog.logMessage(
                "Create boundary finished unsucessfully",
                "Create boundary",
                Qgis.Warning,
            )
