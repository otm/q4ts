from PyQt5.QtWidgets import QDialogButtonBox, QMessageBox
from qgis.PyQt.QtWidgets import QComboBox, QLineEdit, QCheckBox
from qgis.core import QgsFeature

liquid_boundary_codes = {
    "Prescribed Q": 455,
    "Prescribed H": 544,
    "Prescribed Q and H": 555,
    "Prescribed UV": 466,
    "Prescribed UV and H": 566,
    "Prescribed incident Waves": 111
}

tracer_codes = {
    "Wall for tracer": 2,
    "Boundary with prescribed Tracer": 5,
    "Boundary with free Tracer ": 4
}


def formOpen(dialog, layerid, featureid: QgsFeature):
    liquid_cb = dialog.findChild(QComboBox, "liquidCodeComboBox")
    tracer_cb = dialog.findChild(QComboBox, "tracerCodeComboBox")
    bnd_type = dialog.findChild(QLineEdit, "bnd_type")
    free_tb = dialog.findChild(QCheckBox, "freeTypeCheckBox")

    if not liquid_cb.count():
        for liquid_boundary_code in liquid_boundary_codes:
            liquid_cb.addItem(liquid_boundary_code, liquid_boundary_codes[liquid_boundary_code])
        liquid_cb.currentIndexChanged.connect(
            lambda index, f=free_tb, l=liquid_cb, t=tracer_cb, b=bnd_type: code_changed(f, l, t, b))

    if not tracer_cb.count():
        for tracer_code in tracer_codes:
            tracer_cb.addItem(tracer_code, tracer_codes[tracer_code])
        tracer_cb.currentIndexChanged.connect(
            lambda index, f=free_tb, l=liquid_cb, t=tracer_cb, b=bnd_type: code_changed(f, l, t, b))

    free_tb.clicked.connect(
        lambda clicked, l=liquid_cb, t=tracer_cb, b=bnd_type: free_type_changed(clicked, l, t, b))

    bnd_type.setEnabled(False)

    code_changed(free_tb, liquid_cb, tracer_cb, bnd_type)
    button_box = dialog.findChild(QDialogButtonBox, "buttonBox")
    # Disconnect the signal that QGIS has wired up for the dialog to the button box.
    button_box.button(QDialogButtonBox.Ok).clicked.disconnect()
    button_box.button(QDialogButtonBox.Ok).clicked.connect(lambda clicked, b=bnd_type, d=dialog: validate(b, dialog))

    index = featureid.fieldNameIndex("bnd_type")
    if index != -1 and featureid.attribute("bnd_type"):
        bnd_type.setText(str(featureid.attribute("bnd_type")))


def free_type_changed(clicked: bool, liquid_cb: QComboBox, tracer_cb: QComboBox, bnd_type: QLineEdit):
    liquid_cb.setEnabled(not clicked)
    tracer_cb.setEnabled(not clicked)
    bnd_type.setEnabled(clicked)


def code_changed(free_tb: QCheckBox, liquid_cb: QComboBox, tracer_cb: QComboBox,
                 bnd_type: QLineEdit):
    if not free_tb.isChecked():
        boundary_code = liquid_cb.currentData()
        tracer_code = tracer_cb.currentData()
        bnd_type.setText(f'{boundary_code}{tracer_code}')


def validate(bnd_type, dialog):
    if len(bnd_type.text()) != 4:
        dialog.resetValues()
        msg_box = QMessageBox()
        msg_box.setText("Boundary Type must be four digits long")
        msg_box.exec_()
    else:
        dialog.parent().accept()
