import os
import sys
import tempfile

from PyQt5 import uic
from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QColor, QIcon, QStandardItemModel
from PyQt5.QtWidgets import (
    QColorDialog,
    QDialog,
    QDoubleSpinBox,
    QFileDialog,
    QSpinBox,
    QStyledItemDelegate,
)
from qgis.core import QgsApplication, QgsMapLayerProxyModel
from qgis.PyQt import QtCore
from qgis.PyQt.QtWidgets import QWidget

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.datamodel.strickler_table import StricklerTable, StricklerTableRow
from q4ts.processing.projection_clc import ProjectionCorineLandCoverAlgorithm
from q4ts.ui.ui_utils import (
    add_strickler_style
)


class HtmlColorItemDelegate(QStyledItemDelegate):
    def setEditorData(self, editor: QWidget, index: QtCore.QModelIndex) -> None:
        editor.setCurrentColor(QColor(index.data(QtCore.Qt.DisplayRole)))

    def __init__(self, parent) -> None:
        """
        QStyledItemDelegate for HTML color string definition

        Args:
            parent:
        """
        super().__init__(parent)

    def createEditor(self, parent, option, index) -> QWidget:
        """
        Create a QColorDialog for color definition

        Args:
            parent: QWidget
            option: QStyleOptionViewItem
            index: QModelIndex

        Returns: QDoubleSpinBox

        """
        editor = QColorDialog(parent)
        editor.setOptions(editor.options() | QColorDialog.DontUseNativeDialog)
        return editor

    def setModelData(
        self,
        editor: QWidget,
        model: QtCore.QAbstractItemModel,
        index: QtCore.QModelIndex,
    ) -> None:
        if editor.result() == QDialog.Accepted:
            model.setData(index, editor.selectedColor().name())


class StrickerItemDelegate(QStyledItemDelegate):
    def __init__(self, parent) -> None:
        """
        QStyledItemDelegate for strickler value definition

        Args:
            parent:
        """
        super().__init__(parent)

    def createEditor(self, parent, option, index) -> QWidget:
        """
        Create a QDoubleSpinBox for strickler value definition

        Args:
            parent: QWidget
            option: QStyleOptionViewItem
            index: QModelIndex

        Returns: QDoubleSpinBox

        """
        editor = QDoubleSpinBox(parent)
        editor.setMaximum(sys.float_info.max)
        editor.setMinimum(0.0)
        return editor


class CodeItemDelegate(QStyledItemDelegate):
    def __init__(self, parent) -> None:
        """
        QStyledItemDelegate for CLC code value definition

        Args:
            parent:
        """
        super().__init__(parent)

    def createEditor(self, parent, option, index) -> QWidget:
        """
        Create a QSpinBox for clc code value definition

        Args:
            parent: QWidget
            option: QStyleOptionViewItem
            index: QModelIndex

        Returns: QSpinBox

        """
        editor = QSpinBox(parent)
        editor.setMaximum(2147483647)
        editor.setMinimum(0)
        return editor


class StricklerTableModel(QStandardItemModel):
    NAME_COL = 0
    STRICKLER_COL = 1
    HTML_COLOR_COL = 2
    CLC_CODE_COL = 3

    def __init__(self, parent=None) -> None:
        """
        QStandardItemModel for strickler table display and edition

        Args:
            parent: QWidget
        """
        super().__init__(parent)
        self.setHorizontalHeaderLabels(
            [
                self.tr("Name"),
                self.tr("Friction Coefficient"),
                self.tr("HTML Color"),
                self.tr("CLC Code"),
            ]
        )

    def data(
        self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole
    ) -> QVariant:
        """
        Override QStandardItemModel data() for background color on HTML Color column

        Args:
            index:
            role:

        Returns:

        """
        result = super().data(index, role)
        if role == QtCore.Qt.BackgroundRole:
            if index.column() == self.HTML_COLOR_COL:
                html_color = str(self.data(index, QtCore.Qt.DisplayRole))
                result = QColor(html_color)

        return result

    def set_strickler_table(self, strickler_table: StricklerTable) -> None:
        while self.rowCount():
            self.removeRow(0)
        for r in strickler_table.rows:
            self.insertRow(self.rowCount())
            row = self.rowCount() - 1
            self.setData(self.index(row, self.NAME_COL), r.name)
            self.setData(self.index(row, self.HTML_COLOR_COL), f"#{r.html_color}")
            self.setData(self.index(row, self.CLC_CODE_COL), r.code)
            self.setData(self.index(row, self.STRICKLER_COL), r.value)

    def get_strickler_table(self) -> StricklerTable:
        res = StricklerTable()
        for i in range(0, self.rowCount()):
            res.rows.append(
                StricklerTableRow(
                    name=str(self.data(self.index(i, self.NAME_COL))),
                    html_color=str(self.data(self.index(i, self.HTML_COLOR_COL))),
                    code=int(self.data(self.index(i, self.CLC_CODE_COL))),
                    value=float(self.data(self.index(i, self.STRICKLER_COL))),
                )
            )
        return res


class StricklerTableWidget(QWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(parent)
        uic.loadUi(
            os.path.dirname(os.path.realpath(__file__)) + "/strickler_table_widget.ui",
            self,
        )
        self.setWindowIcon(
            QIcon(
                str(
                    DIR_PLUGIN_ROOT
                    / "resources"
                    / "images"
                    / "strickler-table-edition-icon.png"
                )
            )
        )
        self._iface = iface

        self.set_iface(iface)

        self.selectFileButton.clicked.connect(self.select_strickler_table)

        self.saveAsButton.setIcon(
            QIcon(QgsApplication.iconPath("mActionFileSaveAs.svg"))
        )
        self.saveAsButton.clicked.connect(self.save_strickler_table)

        self.saveButton.setIcon(QIcon(QgsApplication.iconPath("mActionFileSave.svg")))
        self.saveButton.clicked.connect(self.save_current_strickler_table)

        self.strickler_table_model = StricklerTableModel(self)
        self.tableView.setModel(self.strickler_table_model)
        self.tableView.setItemDelegateForColumn(
            self.strickler_table_model.HTML_COLOR_COL, HtmlColorItemDelegate(self)
        )
        self.tableView.setItemDelegateForColumn(
            self.strickler_table_model.STRICKLER_COL, StrickerItemDelegate(self)
        )
        self.tableView.setItemDelegateForColumn(
            self.strickler_table_model.CLC_CODE_COL, CodeItemDelegate(self)
        )

        self.addButton.setIcon(QIcon(QgsApplication.iconPath("mActionAdd.svg")))
        self.addButton.clicked.connect(self._add_line)

        self.removeButton.setIcon(QIcon(QgsApplication.iconPath("mActionRemove.svg")))
        self.removeButton.clicked.connect(self._remove_lines)        

        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self._update_apply_button)
        self.colorRampButton.colorRampChanged.connect(self._update_apply_button)
        self.strickler_table_model.dataChanged.connect(self._update_apply_button)

        self.applySymbologyButton.setIcon(QIcon(QgsApplication.iconPath("mActionReload.svg")))
        self.applySymbologyButton.clicked.connect(self._apply_symbology)

    def set_iface(self, iface):
        self._iface = iface

    def select_strickler_table(self):
        default_strickler_table = (
            ProjectionCorineLandCoverAlgorithm.DEFAULT_STRICKLER_TABLE
        )
        filename, filter_used = QFileDialog.getOpenFileName(
            self, "Select file", default_strickler_table, "Friction table (*.txt)"
        )

        if filename:
            self.stricklerTableLineEdit.setText(filename)
            self._read_strickler_table_file(filename)

    def save_strickler_table(self):
        filename, filter_used = QFileDialog.getSaveFileName(
            self, "Write file", "def_strickler_table.txt", "Friction table (*.txt)"
        )

        if filename:
            self.stricklerTableLineEdit.setText(filename)
            self._write_strickler_table_file(filename)

    def save_current_strickler_table(self):
        self._write_strickler_table_file(self.stricklerTableLineEdit.text())

    def _read_strickler_table_file(self, filename):
        strickler_table = StricklerTable()
        strickler_table.read(filename)
        strickler_table.sort_by_value()
        self.strickler_table_model.set_strickler_table(strickler_table)
        self.tableView.resizeColumnsToContents()

    def _write_strickler_table_file(self, filename: str) -> None:
        strickler_table = self.strickler_table_model.get_strickler_table()
        strickler_table.write(filename)

    def _add_line(self):
        self.strickler_table_model.insertRow(self.strickler_table_model.rowCount())

    def _remove_lines(self):
        while self.tableView.selectionModel().selectedRows():
            self.strickler_table_model.removeRow(
                self.tableView.selectionModel().selectedRows()[0].row()
            )

    def _update_apply_button(self) -> None:
        enable = True
        tooltip = ""
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr("Input mesh must be defined")
            enable = False
        elif not self.colorRampButton.colorRamp():  
            tooltip = self.tr("Color ramp must be defined")
            enable = False
        elif self.strickler_table_model.rowCount() == 0:            
            tooltip = self.tr("At least one friction value must be defined")
            enable = False

        self.applySymbologyButton.setEnabled(enable)
        self.applySymbologyButton.setToolTip(tooltip)

    def _apply_symbology(self) -> None:
        tmp_file = tempfile.NamedTemporaryFile(suffix=".txt")
        temp_file_name = tmp_file.name
        tmp_file.close()
        self._write_strickler_table_file(temp_file_name)

        add_strickler_style(
            self.mMapLayerComboBox.currentLayer(),
            self.mMapLayerComboBox.currentLayer(),
            temp_file_name,
            self.colorRampButton.colorRamp(),
        )
        
