import os
from pathlib import Path

from qgis.core import (
    QgsApplication,
    QgsColorRamp,
    QgsColorRampShader,
    QgsMapLayer,
    QgsMeshDatasetIndex,
    QgsMeshLayer,
    QgsProject,
    QgsUnitTypes,
)
from qgis.gui import QgsMapLayerComboBox
from qgis.PyQt.QtGui import QColor, QIcon
from qgis.PyQt.QtWidgets import QMessageBox, QTextEdit, QToolButton

from q4ts.datamodel.strickler_table import StricklerTable
from q4ts.processing.qgis_utils import get_available_layer_name


def get_or_create_group(directory):
    root = QgsProject.instance().layerTreeRoot()
    group = root.findGroup(directory)
    if not group:
        temp = root.addGroup(directory)
        group = temp.clone()
        root.insertChildNode(0, group)
        root.removeChildNode(temp)
    return group


def get_filepath_or_temp_output(textedit: QTextEdit) -> str:
    output = 'TEMPORARY_OUTPUT'
    if textedit.text():
        output = textedit.text()
    return output


def zoom_current_layer(maplayer_combobox: QgsMapLayerComboBox, iface):
    layer = maplayer_combobox.currentLayer()
    if layer and iface:
        iface.mapCanvas().setExtent(layer.extent())
        iface.mapCanvas().refresh()


def connect_zoom_selection(maplayer_combobox: QgsMapLayerComboBox, button: QToolButton, iface):
    button.clicked.connect(lambda clicked, cb=maplayer_combobox, i=iface: zoom_current_layer(cb, i))
    button.setIcon(QIcon(QgsApplication.iconPath('mActionZoomToLayer.svg')))


def import_mesh_layer(w, result_path: str, default_base_name, textedit: QTextEdit, group_name: str) -> QgsMeshLayer:
    mesh_layer = None
    if os.path.isfile(result_path):

        base_name = default_base_name
        if textedit.text():
            base_name = Path(result_path).stem
        layer_name = get_available_layer_name(base_name)
        mesh_layer = QgsMeshLayer(result_path, layer_name, "mdal")
        # Check CRS
        crs = mesh_layer.crs()
        if not crs.isValid():
            mesh_layer.setCrs(QgsProject.instance().crs())
        group = get_or_create_group(group_name)
        QgsProject().instance().addMapLayer(mesh_layer, False)
        group.addLayer(mesh_layer)
    else:
        QMessageBox.critical(w,
                             w.tr("Error in processing"),
                             w.tr("Result mesh not available, check processing logs"))
    return mesh_layer


def copy_input_mesh_renderer_settings(origin_mesh_layer: QgsMeshLayer, mesh_layer: QgsMeshLayer) -> None:
    variable_settings_map = {}
    # Copy settings from origin layer
    origin_render_settings = origin_mesh_layer.rendererSettings()
    for index in origin_mesh_layer.datasetGroupsIndexes():
        index_group = QgsMeshDatasetIndex(index)
        dataset_metadata = mesh_layer.datasetGroupMetadata(index_group)
        name = dataset_metadata.name()
        variable_settings_map[name] = origin_render_settings.scalarSettings(index)

    renderer_settings = mesh_layer.rendererSettings()
    for index in mesh_layer.datasetGroupsIndexes():
        index_group = QgsMeshDatasetIndex(index)
        dataset_metadata = mesh_layer.datasetGroupMetadata(index_group)
        name = dataset_metadata.name()
        if name in variable_settings_map:
            renderer_settings.setScalarSettings(index, variable_settings_map[name])
    mesh_layer.setRendererSettings(renderer_settings)


def add_mesh_display_style(mesh_layer: QgsMeshLayer, color: QColor, line_size_mm: float = 0.2) -> None:
    render_settings = mesh_layer.rendererSettings()
    native_mesh_settings = render_settings.nativeMeshSettings()
    native_mesh_settings.setEnabled(True)
    native_mesh_settings.setColor(color)
    native_mesh_settings.setLineWidth(line_size_mm)
    native_mesh_settings.setLineWidthUnit(QgsUnitTypes.RenderMillimeters)
    render_settings.setNativeMeshSettings(native_mesh_settings)
    mesh_layer.setRendererSettings(render_settings)


def add_strickler_style(origin_mesh_layer: QgsMeshLayer,
                        mesh_layer: QgsMeshLayer,
                        strickler_table_file: str,
                        ramp: QgsColorRamp) -> None:
    # Copy settings from origin layer
    copy_input_mesh_renderer_settings(origin_mesh_layer, mesh_layer)
    define_renderer_settings_for_bottom_friction(mesh_layer, ramp, strickler_table_file)


def define_renderer_settings_for_bottom_friction(mesh_layer, ramp, strickler_table_file):
    renderer_settings = mesh_layer.rendererSettings()
    for index in mesh_layer.datasetGroupsIndexes():
        index_group = QgsMeshDatasetIndex(index)
        dataset_metadata = mesh_layer.datasetGroupMetadata(index_group)
        name = dataset_metadata.name()
        if name.startswith("bottom friction"):
            scalar_setting = renderer_settings.scalarSettings(index)
            scalar_setting.setDataResamplingMethod(0)  # QgsMeshRendererScalarSettings.None
            color_ramp = scalar_setting.colorRampShader()
            strickler_table = StricklerTable()
            strickler_table.read(strickler_table_file)
            strickler_table.sort_by_value()

            min = strickler_table.rows[0].value
            max = strickler_table.rows[len(strickler_table.rows) - 1].value
            lst = []
            for r in strickler_table.rows:
                val = (r.value - min) / (max - min)
                color = ramp.color(val)
                lst.append(QgsColorRampShader.ColorRampItem(r.value, color, f"{r.name}: K = {r.value}"))

            color_ramp.setColorRampItemList(lst)
            color_ramp.setColorRampType(QgsColorRampShader.Discrete)
            color_ramp.setSourceColorRamp(ramp)
            scalar_setting.setColorRampShader(color_ramp)
            renderer_settings.setScalarSettings(index, scalar_setting)
            renderer_settings.setActiveScalarDatasetGroup(index)
    mesh_layer.setRendererSettings(renderer_settings)
