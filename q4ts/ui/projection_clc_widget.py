import os

from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsField,
    QgsMapLayerProxyModel,
    QgsMessageLog,
    QgsWkbTypes,
)

from q4ts.processing.projection_clc import ProjectionCorineLandCoverAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.processing.variables_utils import VariablesUtils
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.ui_utils import (
    copy_input_mesh_renderer_settings,
    connect_zoom_selection,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class ProjectionCorineLandCoverWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__)) + "/projection_clc_widget.ui",
        )

        self.setWindowIcon(ProjectionCorineLandCoverAlgorithm().icon())

        self.areaLayerSelectionWidget.set_iface(iface)
        self.areaLayerSelectionWidget.setLayerWkb(QgsWkbTypes.Polygon)
        self.areaLayerSelectionWidget.setFields([QgsField("value", QVariant.Double)])
        self.areaLayerSelectionWidget.layer_create_name = "area"
        self.areaLayerSelectionWidget.directory = "area"
        self.areaLayerSelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.area_layer_changed
        )

        self.stricklerTableLineEdit.setText(
            ProjectionCorineLandCoverAlgorithm.DEFAULT_STRICKLER_TABLE
        )
        self.stricklerTableSelectButton.clicked.connect(self.select_strickler_table)
        self.stricklerTableLineEdit.editingFinished.connect(self.check_inout_file)

        self.clcLayerSelectionWidget.set_iface(iface)
        self.clcLayerSelectionWidget.setLayerWkb(QgsWkbTypes.Polygon)
        self.clcLayerSelectionWidget.setCreationEnabled(False)
        self.clcLayerSelectionWidget.setLayerStyle("CorineLandCover_fr.qml")
        self.clcLayerSelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.check_inout_file
        )

        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)

        self.outputMeshSelectionButton.clicked.connect(self.select_result_mesh)
        self.outputMeshEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.outputMeshEdit.editingFinished.connect(self.check_inout_file)
        self.createButton.setEnabled(False)
        self.createButton.setIcon(QIcon(QgsApplication.iconPath("mActionStart.svg")))

        self._define_shift_spin_box_min_max()

        connect_zoom_selection(self.mMapLayerComboBox, self.zoomMeshButton, self._iface)

        self._set_not_running()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.projection_corine_land_cover)

    def _define_shift_spin_box_min_max(self):
        def _define_shift_spin_box_min_max(spinbox):
            spinbox.setMinimum(VariablesUtils.SHIFT_MIN_VALUE)
            spinbox.setMaximum(VariablesUtils.SHIFT_MAX_VALUE)
            spinbox.setDecimals(VariablesUtils.SHIFT_DECIMAL)

        _define_shift_spin_box_min_max(self.shiftXSpinBox)
        _define_shift_spin_box_min_max(self.shiftYSpinBox)

    def select_strickler_table(self):
        filename, filter_use = QFileDialog.getOpenFileName(
            self, "Select file", "strickler.txt", "Friction table (*.txt)"
        )
        if filename:
            self.stricklerTableLineEdit.setText(filename)
            self.check_inout_file()

    def select_result_mesh(self):
        filename, filter_use = QFileDialog.getSaveFileName(
            self,
            "Select file",
            get_current_q4ts_gpkg_folder() + "projection_corine_land_cover.slf",
            "Mesh file (*.slf)"
        )

        self.outputMeshEdit.setText(filename)
        self.check_inout_file()

    def area_layer_changed(self):
        area_layer = self.areaLayerSelectionWidget.currentLayer()
        if area_layer:
            self.areaLayerFieldComboBox.setLayer(area_layer)
        self.check_inout_file()

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr("Input mesh must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(self.tr("Input mesh"), self.mMapLayerComboBox.currentLayer())

        if not self.stricklerTableLineEdit.text():
            tooltip = self.tr("Friction table file must be defined")
            enable = False
        if self.stricklerTableLineEdit.text() and not os.path.exists(
                self.stricklerTableLineEdit.text()
        ):
            tooltip = self.tr("Friction table file must exists")
            enable = False
        if not self.clcLayerSelectionWidget.currentLayer():
            tooltip = self.tr("CLC Data file layer must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(self.tr("CLC layer"), self.clcLayerSelectionWidget.currentLayer())

        if check_unsaved_layer and self.areaLayerSelectionWidget.currentLayer():
            enable &= self._check_if_layer_saved(self.tr("Area layer"),self.areaLayerSelectionWidget.currentLayer())

        return enable, tooltip

    def projection_corine_land_cover(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)

        if not enable:
            return

        params = {
            ProjectionCorineLandCoverAlgorithm.INPUT_MESH: self.mMapLayerComboBox.currentLayer(),
            ProjectionCorineLandCoverAlgorithm.STRICKLER_TABLE: self.stricklerTableLineEdit.text(),
            ProjectionCorineLandCoverAlgorithm.CLC_DATA_FILE: self.clcLayerSelectionWidget.currentLayer(),
            ProjectionCorineLandCoverAlgorithm.ZONE_SHP: self.areaLayerSelectionWidget.currentLayer(),
            ProjectionCorineLandCoverAlgorithm.ATTR_NAME: self.areaLayerFieldComboBox.currentField(),
            ProjectionCorineLandCoverAlgorithm.OUTPUT_MESH: get_filepath_or_temp_output(
                self.outputMeshEdit
            ),
            ProjectionCorineLandCoverAlgorithm.SHIFT_X: self.shiftXSpinBox.value(),
            ProjectionCorineLandCoverAlgorithm.SHIFT_Y: self.shiftYSpinBox.value(),
            ProjectionCorineLandCoverAlgorithm.ZONE_SHIFT: self.shiftAreaCheckBox.isChecked(),
            ProjectionCorineLandCoverAlgorithm.WRITE_CLC_CODE: self.writeCLCCodeCheckBox.isChecked(),
        }

        algo_str = "q4ts:projection_corine_land_cover"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)
        if alg:
            self._run_alg(alg, params, self.interp_clc_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def interp_clc_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()
        if not successful:
            QgsMessageLog.logMessage(
                "Projection Corine Land Cover finished unsucessfully",
                "Projection Corine Land Cover",
                Qgis.Warning,
            )
        else:
            mesh_layer = import_mesh_layer(
                self,
                results["OUTPUT_MESH"],
                "projection_clc",
                self.outputMeshEdit,
                "projection",
            )
            if mesh_layer:
                copy_input_mesh_renderer_settings(self.mMapLayerComboBox.currentLayer(), mesh_layer)
