import os

from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsField,
    QgsMapLayerProxyModel,
    QgsMessageLog,
    QgsWkbTypes,
)

from q4ts.processing.projection_field import ProjectionFieldAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.processing.variables_utils import VariablesUtils
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.ui_utils import (
    connect_zoom_selection,
    copy_input_mesh_renderer_settings,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class ProjectionFieldWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__)) + "/projection_field_widget.ui",
        )

        self.setWindowIcon(ProjectionFieldAlgorithm().icon())
        alg = VariablesUtils()
        self.variableNameComboBox.setEditable(True)
        self.variableNameComboBox.addItems(alg.VAR_NAME_LIST)
        self.variableNameComboBox.currentTextChanged.connect(self.variable_name_changed)
        self.variableUnitComboBox.setEditable(True)
        self.variableUnitComboBox.addItems(alg.VAR_UNIT_LIST)

        self.areaLayerSelectionWidget.set_iface(iface)
        self.areaLayerSelectionWidget.setLayerWkb(QgsWkbTypes.Polygon)
        self.areaLayerSelectionWidget.setFields([QgsField("value", QVariant.Double)])
        self.areaLayerSelectionWidget.layer_create_name = "area"
        self.areaLayerSelectionWidget.directory = "area"
        self.areaLayerSelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.area_layer_changed
        )

        self.areaLayerFieldComboBox.setAllowEmptyFieldName(True)
        self.areaLayerFieldComboBox.fieldChanged.connect(self.area_field_changed)

        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)

        self.outputMeshSelectionButton.clicked.connect(self.select_result_mesh)
        self.outputMeshEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.outputMeshEdit.editingFinished.connect(self.check_inout_file)
        self.createButton.setEnabled(False)
        self.createButton.setIcon(QIcon(QgsApplication.iconPath("mActionStart.svg")))

        self._define_values_spin_box_min_max()
        self._define_shift_spin_box_min_max()

        connect_zoom_selection(self.mMapLayerComboBox, self.zoomMeshButton, self._iface)

        self._set_not_running()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.projection_field)

    def _define_shift_spin_box_min_max(self):
        def _define_shift_spin_box_min_max(spinbox):
            spinbox.setMinimum(VariablesUtils.SHIFT_MIN_VALUE)
            spinbox.setMaximum(VariablesUtils.SHIFT_MAX_VALUE)
            spinbox.setDecimals(VariablesUtils.SHIFT_DECIMAL)

        _define_shift_spin_box_min_max(self.shiftXSpinBox)
        _define_shift_spin_box_min_max(self.shiftYSpinBox)

    def _define_values_spin_box_min_max(self):
        def _define_values_spin_box_min_max(spinbox):
            spinbox.setMinimum(VariablesUtils.VAL_MIN_VALUE)
            spinbox.setMaximum(VariablesUtils.VAL_MAX_VALUE)
            spinbox.setDecimals(VariablesUtils.VAL_DECIMAL)

        _define_values_spin_box_min_max(self.defaultValueSpinBox)
        _define_values_spin_box_min_max(self.fieldValueSpinBox)
        self.defaultValueSpinBox.setValue(None)
        self.defaultValueSpinBox.setNullValueText(self.tr("Use available value."))

    def select_result_mesh(self):
        filename, filter_use = QFileDialog.getSaveFileName(
            self,
            "Select file",
            get_current_q4ts_gpkg_folder() + "projection_field.slf",
            "Mesh file (*.slf)",
        )

        self.outputMeshEdit.setText(filename)
        self.check_inout_file()

    def variable_name_changed(self):
        variable_name = self.variableNameComboBox.currentText()
        alg = VariablesUtils()
        if variable_name in alg.VAR_NAME_UNIT:
            variable_unit = alg.VAR_NAME_UNIT[variable_name]
            self.variableUnitComboBox.setCurrentText(variable_unit)

        self.variableStyleComboBox.clear()
        if variable_name in alg.VAR_STYLES:
            self.variableStyleComboBox.setEnabled(True)
            self.variableStyleComboBox.addItems(alg.VAR_STYLES[variable_name])
        else:
            self.variableStyleComboBox.setEnabled(False)

    def area_layer_changed(self):
        area_layer = self.areaLayerSelectionWidget.currentLayer()
        if area_layer:
            self.areaLayerFieldComboBox.setLayer(area_layer)
        self.check_inout_file()

    def area_field_changed(self):
        self.fieldValueSpinBox.setEnabled(
            not self.areaLayerFieldComboBox.currentField()
        )

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr("Input mesh must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(
                self.tr("Input mesh"), self.mMapLayerComboBox.currentLayer()
            )

        if check_unsaved_layer and self.areaLayerSelectionWidget.currentLayer():
            enable &= self._check_if_layer_saved(
                self.tr("Area layer"), self.areaLayerSelectionWidget.currentLayer()
            )

        return enable, tooltip

    def projection_field(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)
        if not enable:
            return

        params = {
            ProjectionFieldAlgorithm.INPUT_MESH: self.mMapLayerComboBox.currentLayer(),
            ProjectionFieldAlgorithm.VAR_NAME: self.variableNameComboBox.currentText(),
            ProjectionFieldAlgorithm.VAR_UNIT: self.variableUnitComboBox.currentText(),
            ProjectionFieldAlgorithm.ZONE_SHP: self.areaLayerSelectionWidget.currentLayer(),
            ProjectionFieldAlgorithm.ATTR_NAME: self.areaLayerFieldComboBox.currentField(),
            ProjectionFieldAlgorithm.OUTPUT_MESH: get_filepath_or_temp_output(
                self.outputMeshEdit
            ),
            ProjectionFieldAlgorithm.FIELD_VALUE: self.fieldValueSpinBox.value(),
            ProjectionFieldAlgorithm.EPSG: self.mQgsProjectionSelectionWidget.crs(),
            ProjectionFieldAlgorithm.SHIFT_X: self.shiftXSpinBox.value(),
            ProjectionFieldAlgorithm.SHIFT_Y: self.shiftYSpinBox.value(),
        }

        if self.defaultValueSpinBox.value() is not None:
            params[
                ProjectionFieldAlgorithm.DEFAULT_VALUE
            ] = self.defaultValueSpinBox.value()

        algo_str = "q4ts:projection_field"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)
        if alg:
            self._run_alg(alg, params, self.projection_field_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def projection_field_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()
        if not successful:
            QgsMessageLog.logMessage(
                "Projection field finished unsucessfully",
                "Projection field",
                Qgis.Warning,
            )
        else:
            mesh_layer = import_mesh_layer(
                self,
                results["OUTPUT_MESH"],
                "projection_field",
                self.outputMeshEdit,
                "projection",
            )
            if mesh_layer:
                copy_input_mesh_renderer_settings(
                    self.mMapLayerComboBox.currentLayer(), mesh_layer
                )
                variable = self.variableNameComboBox.currentText()
                if self.variableStyleComboBox.isEnabled():
                    variable += self.variableStyleComboBox.currentText()
                VariablesUtils().add_variable_style(
                    mesh_layer, variable, self.variableUnitComboBox.currentText()
                )
