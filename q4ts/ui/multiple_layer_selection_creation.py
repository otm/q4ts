import os

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QToolButton
from qgis.PyQt.QtWidgets import QWidget
from qgis.PyQt import uic, QtCore
from qgis.core import QgsApplication
from qgis.core import QgsWkbTypes

from q4ts.ui.layer_selection_creation import LayerSelectionCreation


class MultipleLayerSelectionCreation(QWidget):

    def __init__(self, parent=None, iface=None,
                 layer_create_name="temporary", layer_style=None,
                 layer_wkb=QgsWkbTypes.Polygon,
                 fields=None,
                 optional=True,
                 directory='temporary'):
        super().__init__(parent)
        self._iface = iface
        self._layer_create_name = layer_create_name
        self._layer_style = layer_style
        self._layer_wkb = layer_wkb
        self._fields = fields
        self._optional = optional
        self.directory = directory

        self._edit_form = None
        self._edit_form_init = None

        uic.loadUi(os.path.dirname(os.path.realpath(__file__)) + "/multiple_layer_selection_creation.ui", self)
        self._widgets = []

        self.set_iface(iface)

    def set_iface(self, iface):
        self._iface = iface
        if iface and len(self._widgets) == 0:
            self._add_layer()

    def setFields(self, fields):
        self._fields = fields
        for widget in self._widgets:
            widget.setFields(fields)

    def setEditForm(self, edit_form: str, edit_form_init: str = ""):
        self._edit_form = edit_form
        self._edit_form_init = edit_form_init
        for widget in self._widgets:
            widget.setEditForm(edit_form, edit_form_init)

    def currentLayerList(self):
        layer_list = []
        for widget in self._widgets:
            layer = widget.mMapLayerComboBox.currentLayer()
            if layer:
                layer_list.append(layer)
        return layer_list

    layerChanged = QtCore.pyqtSignal()

    def _add_layer(self):
        row = self.grid.rowCount()

        widget = LayerSelectionCreation(self, self._iface, self._layer_create_name,
                                        self._layer_style,
                                        self._layer_wkb,
                                        optional=self._optional,
                                        directory=self.directory)
        if self._fields:
            widget.setFields(self._fields)

        if self._edit_form:
            widget.setEditForm(self._edit_form, self._edit_form_init)

        widget.mMapLayerComboBox.layerChanged.connect(self.layerChanged.emit)

        if len(self._widgets) == 0:
            button = QToolButton(self)
            button.setIcon(QIcon(QgsApplication.iconPath('mActionAdd.svg')))
            button.clicked.connect(self._add_layer)
        else:
            button = QToolButton(self)
            button.setIcon(QIcon(QgsApplication.iconPath('mActionRemove.svg')))
            button.clicked.connect(lambda clicked, w=widget, b=button: self._remove_layer(w, b))

        self.grid.addWidget(button, row, 0)
        self.grid.addWidget(widget, row, 1)
        self._widgets.append(widget)

    def _remove_layer(self, widget, button):
        self._widgets.remove(widget)
        self.grid.removeWidget(widget)
        self.grid.removeWidget(button)
