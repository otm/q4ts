import os
from pathlib import Path

from qgis.core import Qgis, QgsApplication, QgsMessageLog
from qgis.gui import QgsCollapsibleGroupBox
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import QFileDialog, QLabel, QMessageBox, QVBoxLayout

from q4ts.processing.create_mesh import CreateMeshAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.layer_selection_creation import LayerSelectionCreation
from q4ts.ui.multiple_layer_selection_creation import MultipleLayerSelectionCreation
from q4ts.ui.ui_utils import (
    add_mesh_display_style,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class CreateMeshWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__)) + "/create_mesh_widget.ui",
        )

        self.setWindowIcon(CreateMeshAlgorithm().icon())

        self.meshEngineComboBox.addItems(CreateMeshAlgorithm.MESH_ENGINE_ENUM)

        self.input_layers = CreateMeshAlgorithm().input_layers
        self.multiple_input_layers = CreateMeshAlgorithm().multiple_input_layers
        self.layer_combo = {}
        self.multiple_layer_widget = {}

        self.input_layer_layout.setColumnStretch(1, 1)
        for layer_name, input_layer in self.input_layers.items():
            row = self.input_layer_layout.rowCount()
            widget = LayerSelectionCreation(
                self,
                self._iface,
                layer_name,
                input_layer["style"],
                input_layer["type_wkb"],
                optional=input_layer["optional"],
                directory=input_layer["directory"],
            )
            if "fields" in input_layer:
                widget.setFields(input_layer["fields"])
            if "edit_form" in input_layer:
                widget.setEditForm(input_layer["edit_form"])
            widget.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)
            self.layer_combo[layer_name] = widget
            self.input_layer_layout.addWidget(QLabel(input_layer["display"]), row, 0)
            self.input_layer_layout.addWidget(widget, row, 1)

        for layer_name, input_layer in self.multiple_input_layers.items():
            row = self.input_layer_layout.rowCount()
            widget = MultipleLayerSelectionCreation(
                self,
                self._iface,
                layer_name,
                input_layer["style"],
                input_layer["type_wkb"],
                optional=input_layer["optional"],
                directory=input_layer["directory"],
            )
            if "fields" in input_layer:
                widget.setFields(input_layer["fields"])
            if "edit_form" in input_layer:
                widget.setEditForm(input_layer["edit_form"])

            widget.layerChanged.connect(self.check_inout_file)
            self.multiple_layer_widget[layer_name] = widget

            gp = QgsCollapsibleGroupBox(input_layer["display"], self)
            layout = QVBoxLayout()
            layout.addWidget(widget)
            gp.setLayout(layout)
            self.input_layer_layout.addWidget(
                gp, row, 0, 1, self.input_layer_layout.columnCount()
            )

        self.selectTempMedButton.clicked.connect(self.select_temp_med)
        self.selectResultSlfButton.clicked.connect(self.select_result_slf)

        self.tempMedEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.tempMedEdit.editingFinished.connect(self.check_inout_file)
        self.resultSlfEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.resultSlfEdit.editingFinished.connect(self.check_inout_file)

        self._define_mesh_size_spin_box_min_max()
        self._define_mesh_size_spin_box_default_values()

        self._set_not_running()
        self.check_inout_file()

        self.meshEngineComboBox.currentIndexChanged.connect(self._mesh_engine_changed)
        self._mesh_engine_changed()

    def _mesh_engine_changed(self) -> None:
        if self.meshEngineComboBox.currentIndex() == 0:
            self.meshSizeSpinBox.setEnabled(False)
            self.meshSizeSpinBox.setToolTip(
                self.tr("Mesh size can be defined only with mg_cadsurf")
            )
        else:
            self.meshSizeSpinBox.setEnabled(True)
            self.meshSizeSpinBox.setToolTip("")

    def _set_not_running(self):
        self._set_running(False, self.create_mesh)

    def _define_mesh_size_spin_box_default_values(self):
        self.meshSizeSpinBox.setValue(CreateMeshAlgorithm.MESH_SIZE_DEFAULT_VALUE)
        self.meshMinSizeSpinBox.setValue(
            CreateMeshAlgorithm.MESH_MIN_SIZE_DEFAULT_VALUE
        )
        self.meshMaxSizeSpinBox.setValue(
            CreateMeshAlgorithm.MESH_MAX_SIZE_DEFAULT_VALUE
        )
        self.meshGrowthRateSpinBox.setValue(
            CreateMeshAlgorithm.MESH_GROWTH_RATE_DEFAULT_VALUE
        )

    def _define_mesh_size_spin_box_min_max(self):
        def _define_mesh_size_spin_box_min_max(spinbox):
            spinbox.setMinimum(CreateMeshAlgorithm.MESH_SIZE_MIN_VALUE)
            spinbox.setMaximum(CreateMeshAlgorithm.MESH_SIZE_MAX_VALUE)
            spinbox.setDecimals(CreateMeshAlgorithm.MESH_SIZE_DECIMAL)

        _define_mesh_size_spin_box_min_max(self.meshSizeSpinBox)
        _define_mesh_size_spin_box_min_max(self.meshMinSizeSpinBox)
        _define_mesh_size_spin_box_min_max(self.meshMaxSizeSpinBox)

        self.meshGrowthRateSpinBox.setMinimum(
            CreateMeshAlgorithm.MESH_GROWTH_RATE_MIN_VALUE
        )
        self.meshGrowthRateSpinBox.setMaximum(
            CreateMeshAlgorithm.MESH_GROWTH_RATE_MAX_VALUE
        )

    def select_temp_med(self):
        default_dir = "temp.med"
        if self.resultSlfEdit.text():
            default_dir = str(Path(self.resultSlfEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(
            self, self.tr("Select file"), default_dir, self.tr("Mesh file (*.med)")
        )

        self.tempMedEdit.setText(filename)
        self.check_inout_file()

    def select_result_slf(self):
        default_dir = "create_mesh.slf"
        if self.tempMedEdit.text():
            default_dir = str(Path(self.tempMedEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(
            self, self.tr("Select file"), default_dir, self.tr("Mesh file (*.slf)")
        )

        self.resultSlfEdit.setText(filename)
        self.check_inout_file()

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        for layer_name, input_layer in self.input_layers.items():
            layer = self.layer_combo[layer_name].currentLayer()
            display_name = input_layer["display"]
            if not input_layer["optional"] and not layer:
                tooltip = self.tr(f"{display_name} must be defined")
                enable = False
            elif layer and check_unsaved_layer:
                enable &= self._check_if_layer_saved(display_name, layer)

        if check_unsaved_layer:
            for layer_name, input_layer in self.multiple_input_layers.items():
                for layer in self.multiple_layer_widget[layer_name].currentLayerList():
                    display_name = input_layer["display"]
                    enable &= self._check_if_layer_saved(display_name, layer)
        return enable, tooltip

    def create_mesh(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)
        if not enable:
            return

        params = {}
        for layer_name in self.input_layers.keys():
            params[layer_name] = self.layer_combo[layer_name].currentLayer()

        for layer_name in self.multiple_input_layers.keys():
            params[layer_name] = self.multiple_layer_widget[
                layer_name
            ].currentLayerList()

        params[CreateMeshAlgorithm.TEMP_MED] = get_filepath_or_temp_output(
            self.tempMedEdit
        )
        params[CreateMeshAlgorithm.OUTPUT_MESH] = get_filepath_or_temp_output(
            self.resultSlfEdit
        )
        params[CreateMeshAlgorithm.MESH_SIZE] = self.meshSizeSpinBox.value()
        params[CreateMeshAlgorithm.MESH_MIN_SIZE] = self.meshMinSizeSpinBox.value()
        params[CreateMeshAlgorithm.MESH_MAX_SIZE] = self.meshMaxSizeSpinBox.value()
        params[
            CreateMeshAlgorithm.MESH_GROWTH_RATE
        ] = self.meshGrowthRateSpinBox.value()
        params[CreateMeshAlgorithm.MESH_ENGINE] = self.meshEngineComboBox.currentIndex()

        algo_str = "q4ts:create_mesh"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)

        if alg:
            self._run_alg(alg, params, self.mesh_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def mesh_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()

        if not successful:
            QgsMessageLog.logMessage(
                "Create mesh finished unsucessfully", "Create mesh", Qgis.Warning
            )
        else:
            mesh_layer = import_mesh_layer(
                self,
                results["OUTPUT_MESH"],
                "create_mesh",
                self.resultSlfEdit,
                "creation",
            )
            add_mesh_display_style(mesh_layer, QColor("black"))
