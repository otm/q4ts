import os
from pathlib import Path

from qgis.core import (
    Qgis,
    QgsApplication,
    QgsMapLayerProxyModel,
    QgsMessageLog,
    QgsWkbTypes,
)
from qgis.PyQt.QtGui import QColor, QIcon
from qgis.PyQt.QtWidgets import QFileDialog, QMessageBox

from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.processing.refine import RefineAlgorithm
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.ui_utils import (
    add_mesh_display_style,
    connect_zoom_selection,
    copy_input_mesh_renderer_settings,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class RefineWidget(AlgorithmWidget):

    def __init__(self, parent=None, iface=None):
        super().__init__(parent, iface, os.path.dirname(os.path.realpath(__file__)) + "/refine_widget.ui")
        self.setWindowIcon(RefineAlgorithm().icon())
        self.methodComboBox.addItems(RefineAlgorithm.METHOD_ENUM)

        self.domainLayerSelectionWidget.set_iface(iface)
        self.domainLayerSelectionWidget.setLayerWkb(QgsWkbTypes.Polygon)
        self.domainLayerSelectionWidget.layer_create_name = 'refine_domain'
        self.domainLayerSelectionWidget.directory = 'refine_domain'
        self.domainLayerSelectionWidget.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)

        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)

        self.tempMedEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.tempMedEdit.editingFinished.connect(self.check_inout_file)

        self.outputMeshSelectionButton.clicked.connect(self.select_result_mesh)
        self.outputMeshEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.outputMeshEdit.editingFinished.connect(self.check_inout_file)
        self.createButton.setEnabled(False)
        self.createButton.setIcon(QIcon(QgsApplication.iconPath('mActionStart.svg')))

        self.refinementLevelSpinBox.setMinimum(RefineAlgorithm.REFINE_LEVEL_MIN_VALUE)
        self.refinementLevelSpinBox.setMaximum(RefineAlgorithm.REFINE_LEVEL_MAX_VALUE)
        self.refinementLevelSpinBox.setValue(RefineAlgorithm.REFINE_LEVEL_DEFAULT_VALUE)

        connect_zoom_selection(self.mMapLayerComboBox, self.zoomMeshButton, self._iface)

        self.selectTempMedButton.clicked.connect(self.select_temp_med)

        self._set_not_running()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.refine)

    def select_temp_med(self):
        default_dir = "temp.med"
        if self.outputMeshEdit.text():
            default_dir = str(Path(self.outputMeshEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(self,
                                                            self.tr("Select file"), default_dir,
                                                            self.tr("Mesh file (*.med)"))

        self.tempMedEdit.setText(filename)
        self.check_inout_file()

    def select_result_mesh(self):
        default_dir = "refine.slf"
        if self.tempMedEdit.text():
            default_dir = str(Path(self.tempMedEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(self,
                                                            self.tr("Select file"), default_dir,
                                                            self.tr("Mesh file (*.slf)"))

        self.outputMeshEdit.setText(filename)
        self.check_inout_file()

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ''
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr('Input mesh must be defined')
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(self.tr("Input mesh"), self.mMapLayerComboBox.currentLayer())

        if check_unsaved_layer and self.domainLayerSelectionWidget.currentLayer():
            enable &= self._check_if_layer_saved(self.tr("Domain mesh"),
                                                 self.domainLayerSelectionWidget.currentLayer())

        return enable, tooltip

    def refine(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)
        if not enable:
            return

        params = {RefineAlgorithm.INPUT_MESH: self.mMapLayerComboBox.currentLayer(),
                  RefineAlgorithm.DOMAIN: self.domainLayerSelectionWidget.currentLayer(),
                  RefineAlgorithm.TEMP_MED: get_filepath_or_temp_output(self.tempMedEdit),
                  RefineAlgorithm.OUTPUT_MESH: get_filepath_or_temp_output(self.outputMeshEdit),
                  RefineAlgorithm.REFINE_LEVEL: self.refinementLevelSpinBox.value(),
                  RefineAlgorithm.METHOD: self.methodComboBox.currentIndex()}

        algo_str = 'q4ts:refine'

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)
        if alg:
            self._run_alg(alg, params, self.refine_created)
        else:
            QMessageBox.critical(self,
                                 self.tr("Processing not available"),
                                 self.tr(f"{algo_str} processing not available."))

    def refine_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()
        if not successful:
            QgsMessageLog.logMessage('Refine finished unsucessfully',
                                     "Refine", Qgis.Warning)
        else:
            mesh_layer = import_mesh_layer(self, results["OUTPUT_MESH"], 'refine', self.outputMeshEdit, 'refine')
            if mesh_layer:
                copy_input_mesh_renderer_settings(self.mMapLayerComboBox.currentLayer(), mesh_layer)
                add_mesh_display_style(mesh_layer, QColor("blue"), 0.1)
