from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QDoubleSpinBox, QToolButton
from qgis.core import QgsApplication


class DoubleSpinBoxNullable(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.spin_box = QDoubleSpinBox(self)
        layout.addWidget(self.spin_box)
        clean_button = QToolButton(self)
        clean_button.setIcon(QIcon(QgsApplication.iconPath('mIconClearText.svg')))
        layout.addWidget(clean_button)
        clean_button.clicked.connect(self.set_null_val)

        self.setLayout(layout)

    def set_null_val(self):
        self.spin_box.setValue(self.spin_box.minimum())

    def setMinimum(self, min_val: float):
        self.spin_box.setMinimum(min_val)

    def setMaximum(self, max_val: float):
        self.spin_box.setMaximum(max_val)

    def setDecimals(self, decimal: int):
        self.spin_box.setDecimals(decimal)

    def setNullValueText(self, text: str):
        self.spin_box.setSpecialValueText(text)

    def value(self):
        current_val = self.spin_box.value()
        if current_val == self.spin_box.minimum():
            return None
        else:
            return current_val

    def setValue(self, val: float):
        if val:
            self.spin_box.setValue(val)
        else:
            self.spin_box.setValue(self.spin_box.minimum())
