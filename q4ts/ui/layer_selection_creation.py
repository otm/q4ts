import os
from pathlib import Path

from qgis import processing
from qgis.core import (
    QgsApplication,
    QgsEditFormConfig,
    QgsField,
    QgsFields,
    QgsMapLayerProxyModel,
    QgsProject,
    QgsProviderRegistry,
    QgsSnappingConfig,
    QgsTolerance,
    QgsVectorLayer,
    QgsWkbTypes,
)
from qgis.gui import QgsMapCanvasTracer
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QVariant
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QFileDialog, QToolButton, QWidget

from q4ts.processing.qgis_utils import (
    get_available_layer_name,
    get_available_layer_name_in_gpkg,
    get_or_create_q4ts_gpkg,
)
from q4ts.ui.ui_utils import connect_zoom_selection, get_or_create_group


class LayerSelectionCreation(QWidget):
    def __init__(
        self,
        parent=None,
        iface=None,
        layer_create_name="temporary",
        layer_style=None,
        layer_wkb=QgsWkbTypes.Polygon,
        fields=None,
        optional=True,
        directory="temporary",
    ):
        super().__init__(parent)
        self.iface = iface
        self.layer_create_name = layer_create_name
        self.directory = directory
        self._layer_style = layer_style
        uic.loadUi(
            os.path.dirname(os.path.realpath(__file__))
            + "/layer_selection_creation.ui",
            self,
        )
        self.set_iface(iface)

        self._layer_wkb = layer_wkb
        self.setLayerWkb(layer_wkb)
        self._fields = fields

        self.mMapLayerComboBox.setAllowEmptyLayer(optional)
        if optional:
            self.mMapLayerComboBox.setLayer(None)

        self.createButton.clicked.connect(self.create_layer)
        self.createButton.setIcon(QIcon(QgsApplication.iconPath("mActionAdd.svg")))

        self.selectButton.clicked.connect(self.select_layer)

        self.refreshStyleButton.clicked.connect(self.refresh_layer_style)
        self.refreshStyleButton.setIcon(
            QIcon(QgsApplication.iconPath("mActionReload.svg"))
        )

        self.snapButton.clicked.connect(self.define_snapping_config_from_layer)
        self.snapButton.setIcon(QIcon(QgsApplication.iconPath("mIconSnapping.svg")))
        self.snapButton.setVisible(False)

        self._edit_form = None
        self._edit_form_init = None

    def set_iface(self, iface):
        self.iface = iface
        connect_zoom_selection(self.mMapLayerComboBox, self.zoomButton, self.iface)

    def enable_snapping(self, enable: bool) -> None:
        self.snapButton.setVisible(enable)

    def setLayerStyle(self, layer_style):
        self._layer_style = layer_style

    def setFields(self, fields):
        self._fields = fields

    def setEditForm(self, edit_form: str, edit_form_init: str = ""):
        self._edit_form = edit_form
        self._edit_form_init = edit_form_init

    def setCreationEnabled(self, enabled: bool):
        self.createButton.setVisible(enabled)

    def setLayerWkb(self, layer_wkb):
        self._layer_wkb = layer_wkb

        layer_filter = None
        if self._layer_wkb == QgsWkbTypes.Polygon:
            layer_filter = QgsMapLayerProxyModel.PolygonLayer
        elif self._layer_wkb == QgsWkbTypes.LineString:
            layer_filter = QgsMapLayerProxyModel.LineLayer
        elif self._layer_wkb == QgsWkbTypes.Point:
            layer_filter = QgsMapLayerProxyModel.PointLayer

        if layer_filter:
            self.mMapLayerComboBox.setFilters(layer_filter)

    def addActionButton(self, action: QAction) -> None:
        button = QToolButton(self)
        button.setDefaultAction(action)
        self.additionnal_option_layout.addWidget(button)

    def currentLayer(self):
        return self.mMapLayerComboBox.currentLayer()

    def define_custom_creation_callback(self, callback):
        self.createButton.clicked.disconnect(self.create_layer)
        self.createButton.clicked.connect(callback)

    def refresh_layer_style(self):
        layer = self.mMapLayerComboBox.currentLayer()
        if layer:
            self._update_layer_style(layer)
            self.iface.mapCanvas().redrawAllLayers()

    def select_layer(self):
        vector_file_filter = QgsProviderRegistry.instance().fileVectorFilters()
        file_path, filter_used = QFileDialog.getOpenFileName(
            self, "Select file", "", vector_file_filter
        )

        if file_path:
            file_name = Path(file_path).stem
            layer_name = get_available_layer_name(file_name)
            layer = QgsVectorLayer(file_path, layer_name)
            self._update_layer_style(layer)
            QgsProject().instance().addMapLayer(layer)
            self.mMapLayerComboBox.setLayer(layer)

    def create_layer(self):
        gpkg_file_name = get_or_create_q4ts_gpkg(self)

        if gpkg_file_name:
            layer_wkb_str = QgsWkbTypes.displayString(self._layer_wkb)
            layer_name = get_available_layer_name_in_gpkg(
                gpkg_file_name, self.layer_create_name
            )
            layer = QgsVectorLayer(
                f"{layer_wkb_str}?crs={QgsProject.instance().crs().authid()}",
                layer_name,
                "memory",
            )
            layer.dataProvider().addAttributes(self._get_qgs_fields())

            # Need to load style before packaging to save style in .gpkg
            self._update_layer_style(layer)

            processing.run(
                "native:package", {"LAYERS": [layer], "OUTPUT": gpkg_file_name}
            )

            uri = "%s|layername=%s" % (
                gpkg_file_name,
                layer_name,
            )
            layer = QgsVectorLayer(uri, layer_name, "ogr")

            group = get_or_create_group(self.directory)
            QgsProject().instance().addMapLayer(layer, False)
            group.addLayer(layer)
            layer.startEditing()
            self.iface.layerTreeView().setCurrentLayer(layer)
            self.iface.actionAddFeature().trigger()
            self.mMapLayerComboBox.setLayer(layer)

    def _get_qgs_fields(self):
        fields = QgsFields()
        if self._fields:
            for field in self._fields:
                fields.append(field)
        # Need to have at least one fields or editing is disabled on QGIS 3.16
        if fields.count() == 0:
            fields.append(QgsField("name", QVariant.String))

        return fields

    def _update_layer_style(self, layer):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        if self._layer_style:
            layer.loadNamedStyle(current_dir + f"/../style/{self._layer_style}")
        if self._edit_form:
            edit_config = QgsEditFormConfig()
            edit_config.setLayout(QgsEditFormConfig.EditorLayout.UiFileLayout)
            edit_config.setUiForm(current_dir + os.sep + self._edit_form)
            if self._edit_form_init:
                edit_config.setInitCodeSource(
                    QgsEditFormConfig.PythonInitCodeSource.CodeSourceFile
                )
                edit_config.setInitFilePath(current_dir + os.sep + self._edit_form_init)
                edit_config.setInitFunction("formOpen")
            layer.setEditFormConfig(edit_config)

    def define_snapping_config_from_layer(self) -> None:
        if self.currentLayer():
            proj = QgsProject.instance()
            config = proj.snappingConfig()
            config.setEnabled(True)
            config.clearIndividualLayerSettings()
            config.setMode(QgsSnappingConfig.AdvancedConfiguration)
            config.setIndividualLayerSettings(
                self.currentLayer(),
                QgsSnappingConfig.IndividualLayerSettings(
                    enabled=True,
                    type=QgsSnappingConfig.Vertex,
                    tolerance=10,
                    units=QgsTolerance.Pixels,
                ),
            )
            proj.setSnappingConfig(config)

            tracer = QgsMapCanvasTracer.tracerForCanvas(self.iface.mapCanvas())
            tracer.actionEnableTracing().setChecked(True)
            tracer.setOffset(0.0)

            node = (
                QgsProject.instance()
                .layerTreeRoot()
                .findLayer(self.currentLayer().id())
            )
            if node:
                node.setItemVisibilityChecked(True)
