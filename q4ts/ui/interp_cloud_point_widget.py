import os

from qgis.core import (
    Qgis,
    QgsApplication,
    QgsMapLayerProxyModel,
    QgsMessageLog,
    QgsWkbTypes,
)
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QFileDialog, QMessageBox

from q4ts.processing.interp_cloud_point import InterpolationCloudPointAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.processing.variables_utils import VariablesUtils
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.ui_utils import (
    connect_zoom_selection,
    copy_input_mesh_renderer_settings,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class InterpPointCloudWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__))
            + "/interp_cloud_point_widget.ui",
        )
        self.setWindowIcon(InterpolationCloudPointAlgorithm().icon())

        self.kindComboBox.addItems(InterpolationCloudPointAlgorithm.KIND_ENUM)

        alg = VariablesUtils()
        self.variableNameComboBox.setEditable(True)
        self.variableNameComboBox.addItems(alg.VAR_NAME_LIST)
        self.variableNameComboBox.currentTextChanged.connect(self.variable_name_changed)
        self.variableUnitComboBox.setEditable(True)
        self.variableUnitComboBox.addItems(alg.VAR_UNIT_LIST)

        self.areaLayerSelectionWidget.set_iface(iface)
        self.areaLayerSelectionWidget.setLayerWkb(QgsWkbTypes.Polygon)
        self.areaLayerSelectionWidget.layer_create_name = "area"
        self.areaLayerSelectionWidget.directory = "area"
        self.areaLayerSelectionWidget.mMapLayerComboBox.layerChanged.connect(
            self.area_layer_changed
        )

        self.pointCloudSelectButton.clicked.connect(self.select_point_cloud)

        self.mMapLayerComboBox.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)

        self.outputMeshSelectionButton.clicked.connect(self.select_result_mesh)
        self.outputMeshEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.outputMeshEdit.editingFinished.connect(self.check_inout_file)
        self.createButton.setEnabled(False)
        self.createButton.setIcon(QIcon(QgsApplication.iconPath("mActionStart.svg")))

        self._chunk_spin_box = [
            self.blockPtsSpinBox,
            self.chunkXSpinBox,
            self.chunkYSpinBox,
            self.chunkOverlapSpinBox,
        ]

        self._define_values_spin_box_min_max()
        self._define_shift_spin_box_min_max()

        connect_zoom_selection(self.mMapLayerComboBox, self.zoomMeshButton, self._iface)

        self.chunkRadioButton.toggled.connect(self._check_chunk_spinbox_enabled)
        self.blockPtsSpinBox.valueChanged.connect(self._check_chunk_spinbox_enabled)

        self._set_not_running()
        self.area_layer_changed()
        self._check_chunk_spinbox_enabled()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.interp_cloud_point)

    def _check_chunk_spinbox_enabled(self):
        # Enable chunk spinbox if chunk enabled
        for spin_box in self._chunk_spin_box:
            enabled = self.chunkRadioButton.isChecked()
            spin_box.setEnabled(enabled)
            # Enable chunk spinbox partitions if blocks pts is 0
            if (
                enabled
                and spin_box != self.blockPtsSpinBox
                and spin_box != self.chunkOverlapSpinBox
            ):
                spin_box.setEnabled(self.blockPtsSpinBox.value() == 0)

        self.check_inout_file()

    def _define_shift_spin_box_min_max(self):
        def _define_shift_spin_box_min_max(spinbox):
            spinbox.setMinimum(VariablesUtils.SHIFT_MIN_VALUE)
            spinbox.setMaximum(VariablesUtils.SHIFT_MAX_VALUE)
            spinbox.setDecimals(VariablesUtils.SHIFT_DECIMAL)

        _define_shift_spin_box_min_max(self.shiftXSpinBox)
        _define_shift_spin_box_min_max(self.shiftYSpinBox)

    def _define_values_spin_box_min_max(self):
        def _define_values_spin_box_min_max(spinbox):
            spinbox.setMinimum(VariablesUtils.VAL_MIN_VALUE)
            spinbox.setMaximum(VariablesUtils.VAL_MAX_VALUE)
            spinbox.setDecimals(VariablesUtils.VAL_DECIMAL)

        def _define_positive_values_spin_box(spinbox):
            spinbox.setMinimum(0)
            spinbox.setMaximum(VariablesUtils.INT_MAX_VALUE)

        _define_values_spin_box_min_max(self.defaultValueSpinBox)
        self.defaultValueSpinBox.setValue(None)
        self.defaultValueSpinBox.setNullValueText(self.tr("Use available value."))

        for spin_box in self._chunk_spin_box:
            _define_positive_values_spin_box(spin_box)

    def select_point_cloud(self):
        filename, filter_use = QFileDialog.getOpenFileName(
            self, "Select file", "", "Point cloud file (*.xyz)"
        )
        if filename:
            self.pointCloudLineEdit.setText(filename)
            self.check_inout_file()

    def select_result_mesh(self):
        filename, filter_use = QFileDialog.getSaveFileName(
            self, "Select file",
            get_current_q4ts_gpkg_folder() + "interp_cloud_point.slf",
            "Mesh file (*.slf)"
        )

        self.outputMeshEdit.setText(filename)
        self.check_inout_file()

    def area_layer_changed(self):
        area_layer = self.areaLayerSelectionWidget.currentLayer()
        if area_layer:
            self.defaultValueSpinBox.setEnabled(True)
        else:
            self.defaultValueSpinBox.setEnabled(False)
        self.check_inout_file()

    def variable_name_changed(self):
        variable_name = self.variableNameComboBox.currentText()
        alg = VariablesUtils()
        if variable_name in alg.VAR_NAME_UNIT:
            variable_unit = alg.VAR_NAME_UNIT[variable_name]
            self.variableUnitComboBox.setCurrentText(variable_unit)

        self.variableStyleComboBox.clear()
        if variable_name in alg.VAR_STYLES:
            self.variableStyleComboBox.setEnabled(True)
            self.variableStyleComboBox.addItems(alg.VAR_STYLES[variable_name])
        else:
            self.variableStyleComboBox.setEnabled(False)

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        if not self.mMapLayerComboBox.currentLayer():
            tooltip = self.tr("Input mesh must be defined")
            enable = False
        elif check_unsaved_layer:
            enable &= self._check_if_layer_saved(self.tr("Input mesh"), self.mMapLayerComboBox.currentLayer())

        if not self.pointCloudLineEdit.text():
            tooltip = self.tr("Point cloud file must be defined")
            enable = False
        if check_unsaved_layer and self.areaLayerSelectionWidget.currentLayer():
            enable &= self._check_if_layer_saved(self.tr("Area layer"), self.areaLayerSelectionWidget.currentLayer())

        return enable, tooltip

    def interp_cloud_point(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)
        if not enable:
            return

        params = {
            InterpolationCloudPointAlgorithm.INPUT_MESH: self.mMapLayerComboBox.currentLayer(),
            InterpolationCloudPointAlgorithm.CLOUD_POINT_FILE: self.pointCloudLineEdit.text(),
            InterpolationCloudPointAlgorithm.VAR_NAME: self.variableNameComboBox.currentText(),
            InterpolationCloudPointAlgorithm.VAR_UNIT: self.variableUnitComboBox.currentText(),
            InterpolationCloudPointAlgorithm.ZONE_SHP: self.areaLayerSelectionWidget.currentLayer(),
            InterpolationCloudPointAlgorithm.OUTPUT_MESH: get_filepath_or_temp_output(
                self.outputMeshEdit
            ),
            InterpolationCloudPointAlgorithm.SHIFT_X: self.shiftXSpinBox.value(),
            InterpolationCloudPointAlgorithm.SHIFT_Y: self.shiftYSpinBox.value(),
            InterpolationCloudPointAlgorithm.KIND: self.kindComboBox.currentIndex(),
            InterpolationCloudPointAlgorithm.DELIMITER: self.delimiterLineEdit.text(),
            InterpolationCloudPointAlgorithm.RECORD: self.recordSpinBox.value(),
            InterpolationCloudPointAlgorithm.OUTSIDE_ZONE: self.outsideAreaCheckBox.isChecked(),
            InterpolationCloudPointAlgorithm.NMAP: self.nmapRadioButton.isChecked(),
            InterpolationCloudPointAlgorithm.CHUNK_INTERP: self.chunkRadioButton.isChecked(),
            InterpolationCloudPointAlgorithm.CHUNK_X_PARTITION: self.chunkXSpinBox.value(),
            InterpolationCloudPointAlgorithm.CHUNK_Y_PARTITION: self.chunkYSpinBox.value(),
            InterpolationCloudPointAlgorithm.CHUNK_OVERLAP: self.chunkOverlapSpinBox.value(),
            InterpolationCloudPointAlgorithm.BLOCK_PTS: self.blockPtsSpinBox.value(),
        }

        if self.defaultValueSpinBox.value() is not None:
            params[
                InterpolationCloudPointAlgorithm.DEFAULT_VALUE
            ] = self.defaultValueSpinBox.value()

        algo_str = "q4ts:interp_cloud_point"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)
        if alg:
            self._run_alg(alg, params, self.interp_point_cloud_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def interp_point_cloud_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()
        if not successful:
            QgsMessageLog.logMessage(
                "Interpolation point cloud finished unsucessfully",
                "Interpolation point cloud",
                Qgis.Warning,
            )
        else:
            mesh_layer = import_mesh_layer(
                self,
                results["OUTPUT_MESH"],
                "interp_point_cloud",
                self.outputMeshEdit,
                "interpolation",
            )
            if mesh_layer:
                copy_input_mesh_renderer_settings(self.mMapLayerComboBox.currentLayer(), mesh_layer)
                variable = self.variableNameComboBox.currentText()
                if self.variableStyleComboBox.isEnabled():
                    variable += self.variableStyleComboBox.currentText()
                VariablesUtils().add_variable_style(mesh_layer, variable, self.variableUnitComboBox.currentText())
