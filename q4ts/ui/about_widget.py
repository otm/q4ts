import os

from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QLabel, QSizePolicy

from q4ts.__about__ import __title__, __summary__, __uri__, __version__


class AboutWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi(os.path.dirname(os.path.realpath(__file__)) + "/about_widget.ui", self)

        label = QLabel(self.tr("Name"),self)
        label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)

        self.layoutGrid.addWidget( label, 0, 0)
        self.layoutGrid.addWidget(QLabel(__title__, self), 0, 1)

        self.layoutGrid.addWidget(QLabel(self.tr("Description"), self), 1, 0)
        self.layoutGrid.addWidget(QLabel(__summary__, self), 1, 1)

        self.layoutGrid.addWidget(QLabel(self.tr("Version"), self), 2, 0)
        self.layoutGrid.addWidget(QLabel(__version__, self), 2, 1)

        self.layoutGrid.addWidget(QLabel(self.tr("Repository"), self), 3, 0)
        self.layoutGrid.addWidget(QLabel(__uri__, self), 3, 1)
