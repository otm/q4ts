import os
from pathlib import Path

from qgis.core import Qgis, QgsApplication, QgsMapLayerProxyModel, QgsMessageLog
from qgis.gui import QgsCollapsibleGroupBox
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import QFileDialog, QLabel, QMessageBox, QVBoxLayout

from q4ts.processing.extend_mesh import ExtendMeshAlgorithm
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg_folder
from q4ts.ui.algorithm_widget import AlgorithmWidget
from q4ts.ui.layer_selection_creation import LayerSelectionCreation
from q4ts.ui.multiple_layer_selection_creation import MultipleLayerSelectionCreation
from q4ts.ui.ui_utils import (
    add_mesh_display_style,
    connect_zoom_selection,
    get_filepath_or_temp_output,
    import_mesh_layer,
)


class ExtendMeshWidget(AlgorithmWidget):
    def __init__(self, parent=None, iface=None):
        super().__init__(
            parent,
            iface,
            os.path.dirname(os.path.realpath(__file__)) + "/extend_mesh_widget.ui",
        )

        self.setWindowIcon(ExtendMeshAlgorithm().icon())

        self.inputMeshLayerCbx.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.inputMeshLayerCbx.layerChanged.connect(self.check_inout_file)
        connect_zoom_selection(self.inputMeshLayerCbx, self.zoomMeshButton, self._iface)

        self.otherMeshLayerCbx.setFilters(QgsMapLayerProxyModel.MeshLayer)
        self.otherMeshLayerCbx.layerChanged.connect(self.check_inout_file)
        connect_zoom_selection(
            self.otherMeshLayerCbx, self.zoomOtherMeshButton, self._iface
        )

        self.meshEngineComboBox.addItems(ExtendMeshAlgorithm.MESH_ENGINE_ENUM)

        self.input_layers = ExtendMeshAlgorithm().input_layers
        self.multiple_input_layers = ExtendMeshAlgorithm().multiple_input_layers
        self.layer_combo = {}
        self.multiple_layer_widget = {}

        self.input_layer_layout.setColumnStretch(1, 1)
        for layer_name, input_layer in self.input_layers.items():
            row = self.input_layer_layout.rowCount()
            widget = LayerSelectionCreation(
                self,
                self._iface,
                layer_name,
                input_layer["style"],
                input_layer["type_wkb"],
                optional=input_layer["optional"],
                directory=input_layer["directory"],
            )
            self.layer_combo[layer_name] = widget
            if "fields" in input_layer:
                widget.setFields(input_layer["fields"])
            if "edit_form" in input_layer:
                widget.setEditForm(input_layer["edit_form"])

            if layer_name == ExtendMeshAlgorithm.CONTOUR_LAYER:
                widget.define_custom_creation_callback(
                    lambda: self.create_contour(
                        self.inputMeshLayerCbx,
                        self.layer_combo[ExtendMeshAlgorithm.CONTOUR_LAYER],
                    )
                )
                widget.enable_snapping(True)
            elif layer_name == ExtendMeshAlgorithm.OTHER_CONTOUR_LAYER:
                widget.define_custom_creation_callback(
                    lambda: self.create_contour(
                        self.otherMeshLayerCbx,
                        self.layer_combo[ExtendMeshAlgorithm.OTHER_CONTOUR_LAYER],
                    )
                )
                widget.enable_snapping(True)
            widget.mMapLayerComboBox.layerChanged.connect(self.check_inout_file)
            self.input_layer_layout.addWidget(QLabel(input_layer["display"]), row, 0)
            self.input_layer_layout.addWidget(widget, row, 1)

        for layer_name, input_layer in self.multiple_input_layers.items():
            row = self.input_layer_layout.rowCount()
            widget = MultipleLayerSelectionCreation(
                self,
                self._iface,
                layer_name,
                input_layer["style"],
                input_layer["type_wkb"],
                optional=input_layer["optional"],
                directory=input_layer["directory"],
            )
            if "fields" in input_layer:
                widget.setFields(input_layer["fields"])
            if "edit_form" in input_layer:
                widget.setEditForm(input_layer["edit_form"])

            widget.layerChanged.connect(self.check_inout_file)
            self.multiple_layer_widget[layer_name] = widget

            gp = QgsCollapsibleGroupBox(input_layer["display"], self)
            layout = QVBoxLayout()
            layout.addWidget(widget)
            gp.setLayout(layout)
            self.input_layer_layout.addWidget(
                gp, row, 0, 1, self.input_layer_layout.columnCount()
            )

        self.selectTempMedButton.clicked.connect(self.select_temp_med)
        self.selectOtherTempMedButton.clicked.connect(self.select_other_temp_med)
        self.selectResultSlfButton.clicked.connect(self.select_result_slf)

        self.tempMedEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.tempMedEdit.editingFinished.connect(self.check_inout_file)
        self.otherTempMedEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.otherTempMedEdit.editingFinished.connect(self.check_inout_file)
        self.resultSlfEdit.setPlaceholderText(self.tr("[Save to temporary file]"))
        self.resultSlfEdit.editingFinished.connect(self.check_inout_file)

        self._define_mesh_size_spin_box_min_max()
        self._define_mesh_size_spin_box_default_values()

        self._set_not_running()
        self.check_inout_file()

    def _set_not_running(self):
        self._set_running(False, self.extend_mesh)

    def _define_mesh_size_spin_box_default_values(self):
        self.meshSizeSpinBox.setValue(ExtendMeshAlgorithm.MESH_SIZE_DEFAULT_VALUE)
        self.meshMinSizeSpinBox.setValue(
            ExtendMeshAlgorithm.MESH_MIN_SIZE_DEFAULT_VALUE
        )
        self.meshMaxSizeSpinBox.setValue(
            ExtendMeshAlgorithm.MESH_MAX_SIZE_DEFAULT_VALUE
        )
        self.meshGrowthRateSpinBox.setValue(
            ExtendMeshAlgorithm.MESH_GROWTH_RATE_DEFAULT_VALUE
        )

    def _define_mesh_size_spin_box_min_max(self):
        def _define_mesh_size_spin_box_min_max(spinbox):
            spinbox.setMinimum(ExtendMeshAlgorithm.MESH_SIZE_MIN_VALUE)
            spinbox.setMaximum(ExtendMeshAlgorithm.MESH_SIZE_MAX_VALUE)
            spinbox.setDecimals(ExtendMeshAlgorithm.MESH_SIZE_DECIMAL)

        _define_mesh_size_spin_box_min_max(self.meshSizeSpinBox)
        _define_mesh_size_spin_box_min_max(self.meshMinSizeSpinBox)
        _define_mesh_size_spin_box_min_max(self.meshMaxSizeSpinBox)

        self.meshGrowthRateSpinBox.setMinimum(
            ExtendMeshAlgorithm.MESH_GROWTH_RATE_MIN_VALUE
        )
        self.meshGrowthRateSpinBox.setMaximum(
            ExtendMeshAlgorithm.MESH_GROWTH_RATE_MAX_VALUE
        )

    def select_temp_med(self):
        default_dir = "temp.med"
        if self.resultSlfEdit.text():
            default_dir = str(Path(self.resultSlfEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(
            self, self.tr("Select file"), default_dir, self.tr("Mesh file (*.med)")
        )

        self.tempMedEdit.setText(filename)
        self.check_inout_file()

    def select_other_temp_med(self):
        default_dir = "other_temp.med"
        if self.resultSlfEdit.text():
            default_dir = str(Path(self.resultSlfEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(
            self, self.tr("Select file"), default_dir, self.tr("Mesh file (*.med)")
        )

        self.otherTempMedEdit.setText(filename)
        self.check_inout_file()

    def select_result_slf(self):
        default_dir = "extend_mesh.slf"
        if self.tempMedEdit.text():
            default_dir = str(Path(self.tempMedEdit.text()).parent / default_dir)
        else:
            default_dir = get_current_q4ts_gpkg_folder() + default_dir
        filename, filter_used = QFileDialog.getSaveFileName(
            self, self.tr("Select file"), default_dir, self.tr("Mesh file (*.slf)")
        )

        self.resultSlfEdit.setText(filename)
        self.check_inout_file()

    def check_inout_file(self):
        enable, tooltip = self._define_algorithm_status()

        self.createButton.setEnabled(enable)
        self.createButton.setToolTip(tooltip)

    def _define_algorithm_status(self, check_unsaved_layer: bool = False):
        enable = True
        tooltip = ""
        for layer_name, input_layer in self.input_layers.items():
            layer = self.layer_combo[layer_name].currentLayer()
            display_name = input_layer["display"]
            if not input_layer["optional"] and not layer:
                tooltip = self.tr(f"{display_name} must be defined")
                enable = False
            elif layer and check_unsaved_layer:
                enable &= self._check_if_layer_saved(display_name, layer)

        if check_unsaved_layer:
            for layer_name, input_layer in self.multiple_input_layers.items():
                for layer in self.multiple_layer_widget[layer_name].currentLayerList():
                    display_name = input_layer["display"]
                    enable &= self._check_if_layer_saved(display_name, layer)
        return enable, tooltip

    def extend_mesh(self):
        enable, _ = self._define_algorithm_status(check_unsaved_layer=True)
        if not enable:
            return

        params = {}
        for layer_name in self.input_layers.keys():
            params[layer_name] = self.layer_combo[layer_name].currentLayer()

        for layer_name in self.multiple_input_layers.keys():
            params[layer_name] = self.multiple_layer_widget[
                layer_name
            ].currentLayerList()

        params[ExtendMeshAlgorithm.INPUT_MESH] = self.inputMeshLayerCbx.currentLayer()
        params[ExtendMeshAlgorithm.OTHER_MESH] = self.otherMeshLayerCbx.currentLayer()

        params[ExtendMeshAlgorithm.TEMP_MED] = get_filepath_or_temp_output(
            self.tempMedEdit
        )
        params[ExtendMeshAlgorithm.OTHER_TEMP_MED] = get_filepath_or_temp_output(
            self.otherTempMedEdit
        )
        params[ExtendMeshAlgorithm.OUTPUT_MESH] = get_filepath_or_temp_output(
            self.resultSlfEdit
        )
        params[ExtendMeshAlgorithm.MESH_SIZE] = self.meshSizeSpinBox.value()
        params[ExtendMeshAlgorithm.MESH_MIN_SIZE] = self.meshMinSizeSpinBox.value()
        params[ExtendMeshAlgorithm.MESH_MAX_SIZE] = self.meshMaxSizeSpinBox.value()
        params[
            ExtendMeshAlgorithm.MESH_GROWTH_RATE
        ] = self.meshGrowthRateSpinBox.value()
        params[ExtendMeshAlgorithm.MESH_ENGINE] = self.meshEngineComboBox.currentIndex()

        algo_str = "q4ts:extend_mesh"

        alg = QgsApplication.processingRegistry().algorithmById(algo_str)

        if alg:
            self._run_alg(alg, params, self.mesh_created)
        else:
            QMessageBox.critical(
                self,
                self.tr("Processing not available"),
                self.tr(f"{algo_str} processing not available."),
            )

    def mesh_created(self, context, successful, results):
        self._set_not_running()
        self.check_inout_file()

        if not successful:
            QgsMessageLog.logMessage(
                "Extend mesh finished unsucessfully", "Extend mesh", Qgis.Warning
            )
        else:
            mesh_layer = import_mesh_layer(
                self,
                results["OUTPUT_MESH"],
                "extend_mesh",
                self.resultSlfEdit,
                "creation",
            )
            add_mesh_display_style(mesh_layer, QColor("black"))
