import sys

from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QTextEdit
from qgis.core import QgsProcessingFeedback


class QTextEditProcessingFeedBack(QgsProcessingFeedback):
    insert_text_color = pyqtSignal(str, QColor)

    def __init__(self, text_edit: QTextEdit):
        super().__init__()
        self._text_edit = text_edit
        if sys.platform.startswith("win"):
            self._text_edit.setFontFamily('Courier New')
        else:
            self._text_edit.setFontFamily('monospace')

        self._text_edit.setReadOnly(True)
        self._text_edit.setUndoRedoEnabled(False)

        self.insert_text_color.connect(self._change_color_and_insert_text)

    def setProgressText(self, text: str):
        self.pushInfo(text)

    def pushWarning(self, warning: str):
        self.insert_text_color.emit(warning, QColor("orange"))

    def pushInfo(self, info: str):
        self.insert_text_color.emit(info, QColor("black"))

    def pushCommandInfo(self, info: str):
        self.pushInfo(info)

    def pushDebugInfo(self, info):
        self.pushInfo(info)

    def pushConsoleInfo(self, info: str):
        self.pushInfo(info)

    def reportError(self, error: str, fatalError=False):
        self.insert_text_color.emit(error, QColor("red"))

    @pyqtSlot(str, QColor)
    def _change_color_and_insert_text(self, text: str, color: QColor):
        self._text_edit.setTextColor(color)
        self._text_edit.append(text)
        sb = self._text_edit.verticalScrollBar()
        sb.setValue(sb.maximum())
