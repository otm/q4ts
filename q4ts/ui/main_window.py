"""Main dialog of Q4TS"""
import os

from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QDialog
from qgis.utils import iface as iface_import

from q4ts.__about__ import DIR_PLUGIN_ROOT
from q4ts.ui.about_widget import AboutWidget
from q4ts.ui.alter_widget import AlterWidget
from q4ts.ui.create_boundary_widget import CreateBoundaryWidget
from q4ts.ui.create_mesh_widget import CreateMeshWidget
from q4ts.ui.extend_mesh_widget import ExtendMeshWidget
from q4ts.ui.interp_cloud_point_widget import InterpPointCloudWidget
from q4ts.ui.projection_clc_widget import ProjectionCorineLandCoverWidget
from q4ts.ui.projection_field_widget import ProjectionFieldWidget
from q4ts.ui.refine_widget import RefineWidget
from q4ts.ui.strickler_table_widget import StricklerTableWidget


class MainWindow(QDialog):
    """Main class about the dialog of the plugin"""

    def __init__(self, iface=None, parent=None):
        super().__init__(parent)
        uic.loadUi(
            os.path.dirname(os.path.realpath(__file__)) + "/main_window.ui", self
        )
        self.setWindowIcon(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "q4ts_32.png"))
        )
        self._iface = iface

        panels = [
            {"tool_name": "Create Mesh", "widget": CreateMeshWidget(self, iface)},
            {"tool_name": "Extend Mesh", "widget": ExtendMeshWidget(self, iface)},
            {
                "tool_name": "Create Boundary",
                "widget": CreateBoundaryWidget(self, iface),
            },
            {
                "tool_name": "Interpolation point cloud",
                "widget": InterpPointCloudWidget(self, iface),
            },
            {
                "tool_name": "Projection Corine Land Cover",
                "widget": ProjectionCorineLandCoverWidget(self, iface),
            },
            {
                "tool_name": "Projection field",
                "widget": ProjectionFieldWidget(self, iface),
            },
            {
                "tool_name": "Refine",
                "widget": RefineWidget(self, iface),
            },
            {
                "tool_name": "Mesh transform",
                "widget": AlterWidget(self, iface),
            },
            {
                "tool_name": "Friction table edition",
                "widget": StricklerTableWidget(self, iface),
            },
            {"tool_name": "About", "widget": AboutWidget(self)},
        ]

        for panel in panels:
            self.add_tab(panel["tool_name"], panel["widget"])

    def add_tab(self, tool_name, widget):
        self.tabWidget.addTab(widget, widget.windowIcon(), tool_name)

    @property
    def iface(self):
        """Get iface."""
        if self._iface:
            return self._iface
        return iface_import
