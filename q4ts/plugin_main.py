#! python3  # noqa: E265

"""
    Main plugin module.
"""
from qgis.core import (
    QgsApplication,
    QgsMapLayer,
    QgsMeshLayer,
    QgsProcessingContext,
    QgsProcessingFeedback,
    QgsProject,
    QgsProviderRegistry,
    QgsSettings,
)

# PyQGIS
from qgis.PyQt.QtCore import QCoreApplication, Qt, QTimer
from qgis.PyQt.QtWidgets import QAction, QDockWidget, QMenu, QMessageBox

from q4ts.processing.mesh_symbology import MeshSymbologyAlgorithm

# project
from q4ts.processing.provider import Provider
from q4ts.processing.qgis_utils import get_current_q4ts_gpkg
from q4ts.toolbelt import PlgLogger, PlgTranslator
from q4ts.ui.alter_widget import AlterWidget
from q4ts.ui.create_boundary_widget import CreateBoundaryWidget
from q4ts.ui.create_mesh_widget import CreateMeshWidget
from q4ts.ui.extend_mesh_widget import ExtendMeshWidget
from q4ts.ui.interp_cloud_point_widget import InterpPointCloudWidget
from q4ts.ui.main_window import MainWindow
from q4ts.ui.projection_clc_widget import ProjectionCorineLandCoverWidget
from q4ts.ui.projection_field_widget import ProjectionFieldWidget
from q4ts.ui.refine_widget import RefineWidget
from q4ts.ui.strickler_table_widget import StricklerTableWidget


class SalomeHydroPlugin:
    def __init__(self, iface):
        self.iface = iface
        self.log = PlgLogger().log
        self.provider = None

        self.main_window = None
        self.action = None
        self.menu = None
        self.docks = []

        # translation
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr

        self.layer_delete_connect = None
        self.layer_added_connect = None
        self.layer_added_timer = None

        # Update settings for digitizing line
        s = QgsSettings()
        s.setValue("qgis/digitizing/line_width", 3)

    def initProcessing(self):
        self.provider = Provider()
        QgsApplication.processingRegistry().addProvider(self.provider)

    def initGui(self):
        self.initProcessing()

        self.main_window = MainWindow(self.iface, self.iface.mainWindow())

        self.action = QAction(
            self.main_window.windowIcon(), "Q4TS", self.iface.mainWindow()
        )
        self.menu = QMenu()
        self.action.setMenu(self.menu)
        self.action.triggered.connect(self.show_all_tools)

        actions = [
            {
                "name": "Create Mesh",
                "widget": CreateMeshWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Extend Mesh",
                "widget": ExtendMeshWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Create Boundary",
                "widget": CreateBoundaryWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Interpolation point cloud",
                "widget": InterpPointCloudWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Projection Corine Land Cover",
                "widget": ProjectionCorineLandCoverWidget(
                    self.iface.mainWindow(), self.iface
                ),
            },
            {
                "name": "Projection field",
                "widget": ProjectionFieldWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Refine",
                "widget": RefineWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Mesh transform",
                "widget": AlterWidget(self.iface.mainWindow(), self.iface),
            },
            {
                "name": "Friction table edition",
                "widget": StricklerTableWidget(self.iface.mainWindow(), self.iface),
            },
        ]

        for action in actions:
            self.add_action(action["name"], action["widget"])

        self.iface.addToolBarIcon(self.action)
        self.layer_delete_connect = QgsProject.instance().layersWillBeRemoved.connect(
            self._layers_will_be_removed
        )
        self.layer_added_connect = QgsProject.instance().layersAdded.connect(
            self._project_layer_loaded
        )

    def _layers_will_be_removed(self, layer_ids: [str]) -> None:
        gpkg_filename = get_current_q4ts_gpkg()
        if gpkg_filename:
            for layer_id in layer_ids:
                layer = QgsProject.instance().mapLayer(layer_id)
                source = layer.source()
                source_split = source.split("|layername=")
                if source.startswith(gpkg_filename) and len(source_split) > 1:
                    geopackage_layername = source_split[1]
                    reply = QMessageBox.question(
                        self.iface.mainWindow(),
                        self.tr("Remove layer"),
                        self.tr(
                            "Do you want to remove {} layer from Q4TS geopackage ?".format(
                                layer.name()
                            )
                        ),
                        QMessageBox.Yes | QMessageBox.No,
                        QMessageBox.No,
                    )
                    if reply == QMessageBox.Yes:
                        md = QgsProviderRegistry.instance().providerMetadata("ogr")
                        con = md.createConnection(gpkg_filename, {})
                        con.executeSql("DROP TABLE {0}".format(geopackage_layername))

    def _project_layer_loaded(self, layers: [QgsMapLayer]) -> None:
        # Need to use a timer or the style is not updated.
        # It seems that QGIS add style after emitting layersAdded signal
        self.layer_added_timer = QTimer(self.iface.mainWindow())
        self.layer_added_timer.setSingleShot(True)
        self.layer_added_timer.timeout.connect(
            lambda: self._project_layer_loaded_run(layers)
        )
        self.layer_added_timer.start(100)

    def _project_layer_loaded_run(self, layers: [QgsMapLayer]) -> None:
        mesh_layer = [layer for layer in layers if isinstance(layer, QgsMeshLayer)]
        for layer in mesh_layer:
            context = QgsProcessingContext()
            feedback = QgsProcessingFeedback()
            params = {MeshSymbologyAlgorithm.INPUT_MESH: layer}
            MeshSymbologyAlgorithm().run(params, context, feedback)

    def show_all_tools(self):
        self.main_window.show()

    def add_action(self, name, widget):
        dock = QDockWidget(name, self.iface.mainWindow())
        dock.setWindowIcon(widget.windowIcon())
        dock.setWidget(widget)
        self.iface.addDockWidget(Qt.RightDockWidgetArea, dock)
        dock.close()
        self.docks.append(dock)
        dock.toggleViewAction().setIcon(widget.windowIcon())
        self.menu.addAction(dock.toggleViewAction())

    def unload(self):
        QgsApplication.processingRegistry().removeProvider(self.provider)
        if self.action:
            self.iface.removeToolBarIcon(self.action)
            del self.action
        if self.main_window:
            self.main_window.deleteLater()
            self.main_window = None
        if self.menu:
            del self.menu
        for _dock in self.docks:
            self.iface.removeDockWidget(_dock)
            _dock.deleteLater()
        self.docks.clear()

        if self.layer_delete_connect:
            QgsProject.instance().layersWillBeRemoved.disconnect(
                self.layer_delete_connect
            )

        if self.layer_added_connect:
            QgsProject.instance().layersAdded.disconnect(self.layer_added_connect)
