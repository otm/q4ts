import re
from dataclasses import dataclass


@dataclass
class StricklerTableRow:
    name: str
    code: int
    html_color: str
    value: float


class StricklerTable:

    def __init__(self):
        self.rows = []

    def sort_by_value(self) -> None:
        self.rows.sort(key=lambda r: r.value)

    def read(self, filename: str) -> None:
        self.rows.clear()
        with open(filename, 'r') as f:
            for line in f.readlines():
                match = re.match(
                    r'"(?P<name>[^"]+)"\s+(?P<value>[^ ]+)\s+(?P<html_color>[^ ]+)'
                    r'\s+(?P<code>[^ ]+)', line)
                if match:
                    try:
                        html_color = match.group('html_color')
                        if len(html_color) != 6:
                            html_color = html_color.zfill(6)

                        row = StricklerTableRow(name=str(match.group('name')),
                                                code=int(match.group('code')),
                                                html_color=html_color,
                                                value=float(match.group('value')))
                        self.rows.append(row)
                    except ValueError:
                        print("invalid values")

    def write(self, filename: str) -> None:
        with open(filename, 'w') as f:
            f.write('CODE CLC 12\n')
            for r in self.rows:
                line = f'"{r.name}" {r.value} {r.html_color.replace("#", "")} {r.code}\n'
                f.write(line)
