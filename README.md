# Q4TS

QGIS for TELEMAC-SALOME is a pre-processing of open-TELEMAC meshes : mesh creation, mesh modification, mesh interpolation, creation of boundary condition file.

The documentation is available [here](https://otm.gitlab-pages.pam-retd.fr/q4ts/index.html)

## Install

### Nightly build

:warning: The nightly build is not a production version and is aimed at alpha testers and developers only.

In the `Settings` tab of the QGIS `Plugins Manager` window (see [official documentation](https://docs.qgis.org/3.16/en/docs/user_manual/plugins/plugins.html#the-settings-tab)):

1. Add ```https://otm.gitlab-pages.pam-retd.fr/q4ts/plugins.xml``` to the QGIS extensions repository list
2. Hit the `Reload all Repositories` button.

Finally, go to `Plugins` -> `Manage and Install Plugins` -> `All` search for "Q4TS" and hit `Install Plugin`.
