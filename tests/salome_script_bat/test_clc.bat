@ECHO OFF
@SETLOCAL

call env_file.bat

if not defined SALOME_BIN (
	echo Error : SALOME_BIN must be set to salome binary
	exit \b 0)

if not defined PYTHON_SALOME (
	echo Error : PYTHON_SALOME must be set to Python binary ^(embeded in Salome installation^)
	exit \b 0)

set SALOME_SCRIPT=..\..\q4ts\salome_script\salome_script.py
set CLC_DATA_FILE=..\data\Garonne\garonne_1_clc.gpkg

REM Input data
set MESH_FILE=..\data\Garonne\garonne_1.slf
set CORRESP_TABLE=..\..\q4ts\data\def_strickler_table.txt

REM Output data
set OUTPUT_FILE=garonne_with_clc.slf
set FIG_NAME=test_clc.png

set OUTPUT_FILE2=garonne_with_clc_code.slf
set FIG_NAME2=test_clc_code.png

call %ENV_TELEMAC%

%SALOME_SCRIPT% clc %MESH_FILE% %OUTPUT_FILE% %CORRESP_TABLE% --offset 430000.0 6350000.0
plot.py var -v "BOTTOM FRICTION" %OUTPUT_FILE% -f %FIG_NAME%

%SALOME_SCRIPT% clc %MESH_FILE% %OUTPUT_FILE2% %CORRESP_TABLE% --offset 430000.0 6350000.0 --write-clc-code
plot.py var -v "BOTTOM FRICTION" %OUTPUT_FILE2% -f %FIG_NAME2%
