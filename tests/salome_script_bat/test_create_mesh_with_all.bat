@ECHO OFF
@SETLOCAL

call env_file.bat

if not defined SALOME_BIN (
	echo Error : SALOME_BIN must be set to salome binary
	exit \b 0)

if not defined PYTHON_SALOME (
	echo Error : PYTHON_SALOME must be set to Python binary ^(embeded in Salome installation^)
	exit \b 0)

set CONTOUR=..\data\Garonne\garonne_countour.shp
set CONSTRAINT1=..\data\Garonne\constraints1.shp
set CONSTRAINT2=..\data\Garonne\constraints2.shp
set ISLE1=..\data\Garonne\isle1.shp
set ISLE2=..\data\Garonne\isle2.shp
set REFINE1=..\data\Garonne\refinement1.shp
set REFINE2=..\data\Garonne\refinement2.shp
set MED_MESH=Garonne_test_with_all.med
set SLF_MESH=Garonne_test_with_all.slf
set ENGINE=netgen_1d_2d

%PYTHON_SALOME% %SALOME_BIN% -t ..\..\q4ts\salome_script\salome_script.py args:create_mesh,%CONTOUR%,-dx,100.0,-min,50.0,-max,100.0,%MED_MESH%,--mesh_engine,%ENGINE%,--constraints,%CONSTRAINT1%,%CONSTRAINT2%,--isles,%ISLE1%,%ISLE2%,--refine,%REFINE1%,%REFINE2%

call %ENV_TELEMAC%
converter.py med2srf %MED_MESH% %SLF_MESH%
plot.py mesh2d %SLF_MESH% -f Garonne_test_with_all.png

REM # To compare with ref\ref_Garonne_test_with_all.png
