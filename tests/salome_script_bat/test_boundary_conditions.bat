@ECHO OFF
@SETLOCAL

call env_file.bat

if not defined SALOME_BIN (
	echo Error : SALOME_BIN must be set to salome binary
	exit \b 0)

if not defined PYTHON_SALOME (
	echo Error : PYTHON_SALOME must be set to Python binary ^(embeded in Salome installation^)
	exit \b 0)

set SALOME_SCRIPT=..\..\q4ts\salome_script\salome_script.py

REM Input data
set MESH_FILE=../data/Garonne/garonne_1.slf
set BND=../data/Garonne/boundaries.shp

REM Output data
set COUNTOUR_FILE=garonne_countour.shp
set OUTPUT_FILE=garonne_with_bnd.slf
set OUTPUT_BND_FILE=garonne_with_bnd.cli
set FIG_NAME=test_boundary_conditions.png

call %ENV_TELEMAC%

%SALOME_SCRIPT% contour %MESH_FILE% %COUNTOUR_FILE%
%SALOME_SCRIPT% create_boundary %MESH_FILE% %OUTPUT_FILE% %COUNTOUR_FILE% --boundaries %BND%

plot.py mesh2d %OUTPUT_FILE% -b %OUTPUT_BND_FILE% --bnd -f %FIG_NAME%
