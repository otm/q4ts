#!/bin/bash

DATA_DIR=../data

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

cp $DATA_DIR/Garonne/garonne_1.slf .
cp $DATA_DIR/Garonne/garonne_poly.shp .
cp $DATA_DIR/Garonne/garonne_poly.cpg .
cp $DATA_DIR/Garonne/garonne_poly.dbf .
cp $DATA_DIR/Garonne/garonne_poly.shx .
cp $DATA_DIR/Garonne/garonne_poly.prj .

cp $DATA_DIR/Garonne/garonne_multi_poly.shp .
cp $DATA_DIR/Garonne/garonne_multi_poly.cpg .
cp $DATA_DIR/Garonne/garonne_multi_poly.dbf .
cp $DATA_DIR/Garonne/garonne_multi_poly.shx .
cp $DATA_DIR/Garonne/garonne_multi_poly.prj .

. $ENV_TELEMAC

../../q4ts/salome_script/salome_script.py field "garonne_1.slf" "garonne_strick_minor_0.slf" --varname "BOTTOM FRICTION" --varunit "M1/3/S" --default-value 15.0 --epsg "epsg:2154" --shift-x 430000.0 --shift-y 6350000.0

../../q4ts/salome_script/salome_script.py field "garonne_1.slf" "garonne_strick_minor_1.slf" --varname "BOTTOM FRICTION" --varunit "M1/3/S" --zone-shp "garonne_poly.shp" --field-value 40.0 --default-value 20.0 --epsg "epsg:2154" --shift-x 430000.0 --shift-y 6350000.0

../../q4ts/salome_script/salome_script.py field "garonne_1.slf" "garonne_strick_minor_2.slf" --varname "BOTTOM FRICTION" --varunit "M1/3/S" --zone-shp "garonne_poly.shp" --attr-name 'Strickler' --default-value 25.0 --epsg "epsg:2154" --shift-x 430000.0 --shift-y 6350000.0

../../q4ts/salome_script/salome_script.py field "garonne_1.slf" "garonne_strick_minor_3.slf" --varname "BOTTOM FRICTION" --varunit "M1/3/S" --zone-shp "garonne_multi_poly.shp" --attr-name 'Strickler' --default-value 25.0 --epsg "epsg:2154" --shift-x 430000.0 --shift-y 6350000.0

../../q4ts/salome_script/salome_script.py field "garonne_1.slf" "garonne_water_depth_minor.slf" --varname "WATER DEPTH" --varunit "M" --zone-shp "garonne_poly.shp" --field-value 2.0 --default-value 0.0 --epsg "epsg:2154" --shift-x 430000.0 --shift-y 6350000.0
