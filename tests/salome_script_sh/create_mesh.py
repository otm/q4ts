#/usr/bin/env python3

from os import path, remove
import numpy as np
from data_manip.extraction.telemac_file import TelemacFile

def create_file_from_scratch(file_name, bnd_file=None, nvar=3, ntime=10):
    """
    Example of creation of mesh from stracth

    @param (str) bnd_file If not None creating boundary info
    @param (bool) var If true creating a var in the file
    """
    print("Creating from scratch {}".format(file_name))

    step = 0.5
    points = np.array(\
            [[step*0, step*0],
             [step*2, step*0],
             [step*4, step*0],
             [step*1, step*2],
             [step*3, step*2],
             [step*2, step*4]])
    ikle = [[0, 1, 3],
            [1, 4, 3],
            [1, 2, 4],
            [3, 4, 5],
            ]

    if path.exists(file_name):
        remove(file_name)
    if bnd_file is not None and path.exists(bnd_file):
        remove(bnd_file)

    res = TelemacFile(file_name, bnd_file=bnd_file, access='w')
    res.add_header('Zelda triforce', date=[1986, 2, 21, 0, 0, 0])
    res.add_mesh(points[:, 0], points[:, 1], ikle)
    for ivar in range(nvar):
        res.add_variable('variable {}'.format(ivar), '')
    for itime in range(1, ntime+1):
        res.add_time_step(itime*1.0)


    if bnd_file is not None:
        if file_name.endswith("med"):
            ikle_bnd = [[0, 3], [3, 5], [5, 4], [4, 2], [2, 1], [1, 0]]
            lihbor = [2, 2, 2, 2, 4, 4]
            liubor = [2, 2, 2, 2, 5, 5]
            livbor = [2, 2, 2, 2, 5, 5]
        else:
            ikle_bnd = [0, 3, 5, 4, 2, 1]
            lihbor = [4, 2, 2, 2, 4, 4]
            liubor = [5, 2, 2, 2, 5, 5]
            livbor = [5, 2, 2, 2, 5, 5]

        res.add_bnd(ikle_bnd, lihbor=lihbor, liubor=liubor, livbor=livbor)

    res.write()
    res.close()


if __name__ == "__main__":
    create_file_from_scratch("mesh_with_result.med", "mesh_with_result.bnd", 5, 10)
