#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
# Input data
MESH_FILE="../data/simple_mesh/mesh_with_result.slf"

# Output data
OUTPUT_FILE1="mesh_translated.slf"
OUTPUT_FILE2="mesh_less_var.slf"
OUTPUT_FILE3="mesh_less_time.slf"
OUTPUT_FILE4="mesh_select_time.slf"
OUTPUT_FILE5="mesh_less_time_neg_index.slf"

. $ENV_TELEMAC && \
python3 create_mesh.py \
../../q4ts/salome_script/salome_script.py alter \
-X+? 66.6 -Y+? 666.666 --force \
$MESH_FILE $OUTPUT_FILE1 && \
../../q4ts/salome_script/salome_script.py alter \
--vars "variable 1,variable3,variable 4" --force \
$MESH_FILE $OUTPUT_FILE2 && \
../../q4ts/salome_script/salome_script.py alter \
-f 2 -s 7 -d 2 --force \
$MESH_FILE $OUTPUT_FILE3 &&\
../../q4ts/salome_script/salome_script.py alter \
-f -7 -s -2 -d 2 --force \
$MESH_FILE $OUTPUT_FILE5 &&\
../../q4ts/salome_script/salome_script.py alter \
$MESH_FILE $OUTPUT_FILE4 --force \
--times 2.0 3.0 3.6 \
