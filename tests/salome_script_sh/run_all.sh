#!/bin/bash

RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

let "nb_test=0"
let "nb_failed=0"


for mytest in `ls test_*.sh`; do
  echo "Testing $mytest"
  log_file="${mytest}.log"
  bash $mytest >$log_file 2>&1
  if [[ $? -eq 0 ]]; then
    echo -e "${GREEN}passed${ENDCOLOR}"
  else
    echo -e "${RED}failed${ENDCOLOR}"
    let "nb_failed++"
  fi
  let "nb_test++"
done

if [[ "nb_failed" -eq 0 ]]; then
  echo "All ${nb_test} tests passed"
  exit 0
else
  echo "${nb_failed}/${nb_test} failed"
  exit 1
fi
