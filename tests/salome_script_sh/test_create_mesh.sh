#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binaru"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
CONTOUR="../data/Garonne/garonne_countour.shp"
MED_MESH="Garonne_test.med"
SLF_MESH="Garonne_test.slf"
ENGINE="netgen_1d_2d"

$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:create_mesh,$CONTOUR,-dx,50.0,-min,25.0,-max,100.0,$MED_MESH,--mesh_engine,$ENGINE && \
. $ENV_TELEMAC && \
converter.py med2srf $MED_MESH $SLF_MESH && \
plot.py mesh2d $SLF_MESH -f Garonne_test.png

# To compare with ref/ref_Garonne_test.png
