#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binary"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
INPUT_MESH_SLF="../data/Garonne/garonne_1.slf"
INPUT_MESH_MED="garonne_1.med"
INPUT_CONTOUR="garonne_1.shp"
OTHER_MESH_SLF="../data/Garonne/garonne_other3.slf"
OTHER_MESH_MED="garonne_other3.med"
OTHER_CONTOUR="garonne_other3.shp"
EXTENDED_CONTOUR="../data/Garonne/garonne_join_contour.shp"
REFINE_CONTOUR="../data/Garonne/garonne_join_refine.shp"
JOINED_MESH_MED="Garonne_extension_test.med"
JOINED_MESH_SLF="Garonne_extension_test.slf"
ENGINE="netgen_1d_2d"

. $ENV_TELEMAC && \
run_telfile.py contour $INPUT_MESH_SLF $INPUT_CONTOUR && \
run_telfile.py contour $OTHER_MESH_SLF $OTHER_CONTOUR && \
converter.py srf2med $INPUT_MESH_SLF $INPUT_MESH_MED && \
converter.py srf2med $OTHER_MESH_SLF $OTHER_MESH_MED && \
$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:extend_mesh,$INPUT_MESH_MED,$INPUT_CONTOUR,$EXTENDED_CONTOUR,-dx,50.0,-min,100.0,-max,200.0,$JOINED_MESH_MED,--other-mesh,$OTHER_MESH_MED,--other-contour-shp,$OTHER_CONTOUR,--mesh_engine,$ENGINE,--refine,$REFINE_CONTOUR && \
converter.py med2srf $JOINED_MESH_MED $JOINED_MESH_SLF && \
plot.py mesh2d $JOINED_MESH_SLF -f Garonne_join_test_with_refinement.png

# To compare with ref/ref_Garonne_join_test_with_refinement.png
