#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
export CLC_DATA_FILE="../data/Garonne/garonne_1_clc.gpkg"

# Input data
MESH_FILE="../data/Garonne/garonne_1.slf"
CORRESP_TABLE="../../q4ts/data/def_strickler_table.txt"
ZONE_CLC="../data/Garonne/zone_clc.shp"
ATTR_NAME="strickler"

# Output data
OUTPUT_FILE="garonne_with_zone_clc.slf"
FIG_NAME="test_clc_with_zone.png"

OUTPUT_FILE2="garonne_with_zone_clc_code.slf"
FIG_NAME2="test_clc_with_zone_code.png"


. $ENV_TELEMAC && \
../../q4ts/salome_script/salome_script.py clc $MESH_FILE $OUTPUT_FILE $CORRESP_TABLE --offset 430000.0 6350000.0 --zone $ZONE_CLC --zone-attribute $ATTR_NAME --zone-offset && \
# Only works with trunk version
#plot.py poly garonne_countour.shp -f test_countour.png
plot.py var -v "BOTTOM FRICTION" $OUTPUT_FILE -f $FIG_NAME && \
../../q4ts/salome_script/salome_script.py clc $MESH_FILE $OUTPUT_FILE2 $CORRESP_TABLE --offset 430000.0 6350000.0 --write-clc-code --zone $ZONE_CLC --zone-attribute $ATTR_NAME --zone-offset && \
# Only works with trunk version
#plot.py poly garonne_countour.shp -f test_countour.png
plot.py var -v "CODE CLC" $OUTPUT_FILE2 -f $FIG_NAME2

# To compare with ref/ref_countour.png

# clean up
#rm  garonne_1.slf
#rm garonne_countour.*
