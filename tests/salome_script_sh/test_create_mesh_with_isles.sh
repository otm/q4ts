#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binaru"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

CONTOUR="../data/Garonne/garonne_countour.shp"
ISLE1="../data/Garonne/isle1.shp"
ISLE2="../data/Garonne/isle2.shp"
MED_MESH="Garonne_test_with_isles.med"
SLF_MESH="Garonne_test_with_isles.slf"
ENGINE="netgen_1d_2d"

if [[ -e $MED_MESH ]]; then
  rm $MED_MESH $SLF_MESH
fi
$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:create_mesh,$CONTOUR,-dx,100.0,-min,50.0,-max,100.0,$MED_MESH,--mesh_engine,$ENGINE,--isles,$ISLE1,$ISLE2 && \
. $ENV_TELEMAC &&\
converter.py med2srf $MED_MESH $SLF_MESH &&\
plot.py mesh2d $SLF_MESH -f Garonne_test_with_isles.png

# To compare with ref/ref_Garonne_test_with_isles.png
