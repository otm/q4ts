#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

# Input data
MESH_FILE="../data/Garonne/garonne_1.slf"
REFINED_MESH_MED="garonne_refined_stbtel_local.slf"
REFINEMENT_LEVEL="1"
REFINEMENT_ZONE="../data/Garonne/RefinmentZone_L93TR.shp"

# Output data
COUNTOUR_FILE="garonne_countour.shp"


. $ENV_TELEMAC && \
../../q4ts/salome_script/salome_script.py refine $MESH_FILE $REFINED_MESH_MED --domain $REFINEMENT_ZONE -l $REFINEMENT_LEVEL --method "stbtel"
# Only works with trunk version
#plot.py poly $COUNTOUR_FILE

# To compare with

# clean up
#rm  garonne_refined_stbtel_local.slf
