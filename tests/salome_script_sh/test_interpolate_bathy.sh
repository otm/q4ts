#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

INPUT_MESH="../data/Garonne/garonne_1.slf"
OUTPUT_MESH="garonne_1_interp.slf"
OUTPUT_MESH2="garonne_2_interp.slf"
OUTPUT_MESH3="garonne_3_interp.slf"
OUTPUT_MESH4="garonne_4_interp.slf"
OUTPUT_MESH5="garonne_5_interp.slf"
OUTPUT_MESH6="garonne_6_interp.slf"
POINTS_FILE="../data/Garonne/Cloud_02.xyz"
POLY_SHP="../data/Garonne/garonne_poly.shp"

. $ENV_TELEMAC

../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $OUTPUT_MESH $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'nearest' --record -1 --shift-x 430000.0 --shift-y 6350000.0

../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $OUTPUT_MESH2 $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'linear' --record -1 --shift-x 430000.0 --shift-y 6350000.0

../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $OUTPUT_MESH3 $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'linear' --record -1 --shift-x 430000.0 --shift-y 6350000.0 --zone $POLY_SHP --default-value 100.0

../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $OUTPUT_MESH4 $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'linear' --record -1 --shift-x 430000.0 --shift-y 6350000.0 --zone $POLY_SHP --default-value 10.0 --chunk-interp --blockpts 10000

../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $OUTPUT_MESH5 $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'linear' --record -1 --shift-x 430000.0 --shift-y 6350000.0 --mmap

../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $OUTPUT_MESH6 $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'linear' --record -1 --shift-x 430000.0 --shift-y 6350000.0 --zone $POLY_SHP --outside-zone --default-value 50.0 --chunk-interp --chunk-x-partitions 5 --chunk-y-partitions 5 --chunk-overlap 0.2
