#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
SLF_MESH="../data/Garonne/garonne_1.slf"
SLF_BND="../data/Garonne/garonne_1.cli"
MED_MESH="garonne_1_conv.med"
MED_BND="garonne_1_conv.bnd"

# Not working be cause requires medcoupling to have boundaries on segment
. $ENV_TELEMAC && \
converter.py srf2med $SLF_MESH -b $SLF_BND $MED_MESH
