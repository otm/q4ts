#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binaru"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

MED_MESH="../data/Garonne/garonne_1.med"
MED_BND="../data/Garonne/garonne_1.bnd"
REFINED_MESH_MED="garonne_lrefined.med"
REFINED_MESH_SLF="garonne_lrefined.slf"
REFINEMENT_LEVEL="5"
REFINEMENT_ZONE="../data/Garonne/RefinmentZone_L93TR.shp"


# Mesh must be in MED format with boundary on segment so that homard handles it properly
# This requires MEDCoupling in telemac
# See test_refine00.sh for the conversion
# Conversion as to be done in another script as we can not load ENV_TELEMAC before running salome

$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:refine,--domain=$REFINEMENT_ZONE,-l,5,$MED_MESH,$REFINED_MESH_MED && \
. $ENV_TELEMAC && \
converter.py med2srf $REFINED_MESH_MED -b $MED_BND $REFINED_MESH_SLF && \
plot.py mesh2d $MED_MESH -f Garonne_non_refined.png && \
plot.py mesh2d $REFINED_MESH_SLF -f Garonne_lrefined.png &&\
rm -rf I0* maill_*.med
