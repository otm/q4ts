#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

# Input data
MESH_FILE="../data/Garonne/garonne_1.slf"
REFINED_MESH_MED="garonne_refined_stbtel.slf"
REFINEMENT_LEVEL="2"

# Output data
COUNTOUR_FILE="garonne_countour.shp"


. $ENV_TELEMAC && \
../../q4ts/salome_script/salome_script.py refine $MESH_FILE $REFINED_MESH_MED -l $REFINEMENT_LEVEL --method "stbtel"
# Only works with trunk version
#plot.py poly $COUNTOUR_FILE

# To compare with

# clean up
#rm  garonne_refined_stbtel.slf
