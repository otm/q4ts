#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binary"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
INPUT_MESH_SLF="../data/Garonne/garonne_1.slf"
INPUT_MESH_MED="garonne_1.med"
INPUT_CONTOUR="garonne_1.shp"
EXTENDED_CONTOUR="../data/Garonne/garonne_partial_extension.shp"
EXTENDED_MESH_MED="Garonne_partial_extension_test.med"
EXTENDED_MESH_SLF="Garonne_partial_extension_test.slf"
ENGINE="netgen_1d_2d"

. $ENV_TELEMAC && \
run_telfile.py contour $INPUT_MESH_SLF $INPUT_CONTOUR && \
converter.py srf2med $INPUT_MESH_SLF $INPUT_MESH_MED && \
$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:extend_mesh,$INPUT_MESH_MED,$INPUT_CONTOUR,$EXTENDED_CONTOUR,-dx,50.0,-min,25.0,-max,100.0,$EXTENDED_MESH_MED,--mesh_engine,$ENGINE && \
converter.py med2srf $EXTENDED_MESH_MED $EXTENDED_MESH_SLF && \
plot.py mesh2d $EXTENDED_MESH_SLF -f Garonne_partial_extension_test.png

# To compare with ref/ref_Garonne_partial_extension_test.png
