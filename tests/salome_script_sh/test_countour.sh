#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

# Input data
MESH_FILE="../data/Garonne/garonne_1.slf"

# Output data
COUNTOUR_FILE="garonne_countour.shp"


. $ENV_TELEMAC && \
../../q4ts/salome_script/salome_script.py contour $MESH_FILE $COUNTOUR_FILE && \
# Only works with trunk version
#plot.py poly garonne_countour.shp -f test_countour.png
plot.py poly $COUNTOUR_FILE

# To compare with ref/ref_countour.png

# clean up
#rm  garonne_1.slf
#rm garonne_countour.*
