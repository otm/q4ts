#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binaru"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

INPUT_MESH="../data/Garonne/garonne_1.med"
POINTS_FILE="../data/Garonne/Cloud_02.xyz"
POLY_SHP="../data/Garonne/garonne_poly.shp"
MED_MESH="./garonne_1_interp.med"
MED_BND="../data/Garonne/garonne_1.bnd"
REFINED_MESH_MED="garonne_refined.med"
REFINED_MESH_SLF="garonne_refined.slf"
REFINEMENT_LEVEL="2"

if [[ ! -f "$MED_MESH" ]]; then
  echo "Generating a mesh with field"
  . $ENV_TELEMAC
  ../../q4ts/salome_script/salome_script.py interpolate $INPUT_MESH $MED_MESH $POINTS_FILE --delimiter " " --varname "BOTTOM" --varunit "M" --kind 'nearest' --record -1 --shift-x 430000.0 --shift-y 6350000.0
else
  echo "$MED_MESH exists"
fi


# Mesh must be in MED format with boundary on segment so that homard handles it properly
# This requires MEDCoupling in telemac
# See test_refine00.sh for the conversion
# Conversion as to be done in another script as we can not load ENV_TELEMAC before running salome

$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:refine,$MED_MESH,$REFINED_MESH_MED,-l,$REFINEMENT_LEVEL && \
. $ENV_TELEMAC && \
converter.py med2srf $REFINED_MESH_MED -b $MED_BND $REFINED_MESH_SLF && \
plot.py mesh2d $MED_MESH -f Garonne_non_refined.png && \
plot.py mesh2d $REFINED_MESH_SLF -f Garonne_refined.png &&\
rm -rf I0* maill_*.med
