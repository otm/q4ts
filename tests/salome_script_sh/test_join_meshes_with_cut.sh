#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binary"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi
INPUT_MESH_SLF="../data/Garonne/garonne_1.slf"
INPUT_MESH_MED="garonne_1.med"
INPUT_CONTOUR="garonne_1.shp"
OTHER_MESH_SLF="../data/Garonne/garonne_other2.slf"
OTHER_MESH_MED="garonne_other.med"
CUT_MESH_MED="garonne_cut.med"
CUT_CONTOUR="garonne_cut.shp"
EXTENDED_CONTOUR="../data/Garonne/garonne_join_contour.shp"
JOINED_MESH_MED="Garonne_extension_test.med"
JOINED_MESH_SLF="Garonne_extension_test.slf"
ENGINE="netgen_1d_2d"

. $ENV_TELEMAC && \
converter.py srf2med $INPUT_MESH_SLF $INPUT_MESH_MED && \
run_telfile.py contour $INPUT_MESH_MED $INPUT_CONTOUR && \
converter.py srf2med $OTHER_MESH_SLF $OTHER_MESH_MED && \
$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:cut_mesh,$OTHER_MESH_MED,$INPUT_CONTOUR,$CUT_MESH_MED
run_telfile.py contour $CUT_MESH_MED $CUT_CONTOUR && \
$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:extend_mesh,$INPUT_MESH_MED,$INPUT_CONTOUR,$EXTENDED_CONTOUR,-dx,50.0,-min,100.0,-max,150.0,$JOINED_MESH_MED,--other-mesh,$CUT_MESH_MED,--other-contour-shp,$CUT_CONTOUR,--mesh_engine,$ENGINE && \
converter.py med2srf $JOINED_MESH_MED $JOINED_MESH_SLF && \
plot.py mesh2d $JOINED_MESH_SLF -f Garonne_join_with_cut_test.png

# To compare with ref/ref_Garonne_join_with_cut_test.png
