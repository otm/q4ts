#!/bin/bash

if [[ ! -v SALOME_BIN ]]; then
  echo "SALOME_BIN must be set to salome binaru"
  exit 1
fi
if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

CONTOUR="../data/Garonne/garonne_countour.shp"
CONSTRAINT1="../data/Garonne/constraints_merged.shp"
MED_MESH="Garonne_test_with_constraints_merged.med"
SLF_MESH="Garonne_test_with_constraints_merged.slf"
ENGINE="netgen_1d_2d"

if [[ -e $MED_MESH ]]; then
  rm $MED_MESH $SLF_MESH
fi
$SALOME_BIN -t ../../q4ts/salome_script/salome_script.py args:create_mesh,$CONTOUR,-dx,100.0,-min,50.0,-max,100.0,-gr,1.1,$MED_MESH,--mesh_engine,$ENGINE,--constraints,$CONSTRAINT1 && \
. $ENV_TELEMAC &&\
converter.py med2srf $MED_MESH $SLF_MESH &&\
plot.py mesh2d $SLF_MESH -f Garonne_test_with_constraints_merged_growth_rate.png

# To compare with ref/ref_Garonne_test_with_constraints.png
