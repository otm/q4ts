#!/bin/bash

if [[ ! -v ENV_TELEMAC ]]; then
  echo "ENV_TELEMAC must be set to telemac source file"
  exit 1
fi

# Input data
MESH_FILE="../data/Garonne/garonne_1.slf"
BND="../data/Garonne/boundaries.shp"

# Output data
COUNTOUR_FILE="garonne_countour.shp"
OUTPUT_FILE="garonne_with_bnd.slf"
OUTPUT_BND_FILE="garonne_with_bnd.cli"
FIG_NAME="test_boundary_conditions.png"


. $ENV_TELEMAC && \
../../q4ts/salome_script/salome_script.py contour $MESH_FILE $COUNTOUR_FILE && \
../../q4ts/salome_script/salome_script.py create_boundary $MESH_FILE $OUTPUT_FILE $COUNTOUR_FILE --boundaries $BND && \
plot.py mesh2d $OUTPUT_FILE -b $OUTPUT_BND_FILE --bnd -f $FIG_NAME

# To compare with ref/ref_boundary_conditions.png

# clean up
#rm  garonne_1.slf boundaries.*
#rm garonne_countour.*
#rm garonne_with_bnd.*
