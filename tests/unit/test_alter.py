import filecmp
from pathlib import Path

import pytest

from qgis.core import (
    QgsMeshLayer,
    QgsProcessingContext,
    QgsApplication
)

from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


MESH_WITH_RESULT_SLF = Path("simple_mesh") / "mesh_with_result.slf"
ALTER_REF_PATH = Path("ref") / "alter"

alter_parameters_names = "input_mesh, ref_mesh, " \
                         "variables, " \
                         "times ,t_from, t_end, t_step, " \
                         "shift_x, shift_y"
alter_parameters_values = [
    (MESH_WITH_RESULT_SLF, ALTER_REF_PATH / "mesh_less_time.slf",
     [],
     None, 2, 7, 2,
     0.0, 0.0),
    (MESH_WITH_RESULT_SLF, ALTER_REF_PATH / "mesh_less_time_neg_index.slf",
     [],
     None, -7, -2, 2,
     0.0, 0.0),
    (MESH_WITH_RESULT_SLF, ALTER_REF_PATH / "mesh_less_var.slf",
     "variable 1,variable 3,variable 4",
     None, None, None, None,
     0.0, 0.0),
    (MESH_WITH_RESULT_SLF, ALTER_REF_PATH / "mesh_translated.slf",
     "",
     None, None, None, None,
     66.6, 666.666),

]


@pytest.mark.parametrize(
    alter_parameters_names,
    alter_parameters_values,
)
@pytest.mark.v8p5
def test_alter(test_data_path: Path, input_mesh: Path, ref_mesh: Path,
               variables: str,
               times: str, t_from: int, t_end: int, t_step: int,
               shift_x: float, shift_y: float):
    from q4ts.processing.alter import AlterAlgorithm

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    output_ref_mesh = test_data_path / ref_mesh

    params = {AlterAlgorithm.INPUT_MESH: mesh_layer,
              AlterAlgorithm.VARS: variables,
              AlterAlgorithm.SHIFT_Y: shift_y,
              AlterAlgorithm.SHIFT_X: shift_x,
              AlterAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT"}

    if times:
        params[AlterAlgorithm.TIMES] = times

    if t_from:
        params[AlterAlgorithm.TIME_FROM] = t_from
        params[AlterAlgorithm.TIME_END] = t_end
        params[AlterAlgorithm.TIME_STEP] = t_step

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:alter'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[AlterAlgorithm.OUTPUT_MESH]
    # assert filecmp.cmp(output_ref_mesh, output_mesh)
    assert output_mesh
    assert Path(output_mesh).exists()
