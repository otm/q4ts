import filecmp
from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsMeshLayer, QgsProcessingContext

from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import exit_qgis, setup_qgis_processing


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


EPSG_2154 = "epsg:2154"
GARONNE_POLY = Path("Garonne") / "garonne_poly.shp"
GARONNE_1_SLF = Path("Garonne") / "garonne_1.slf"
PROJECTION_FIELD_REF_PATH = Path("ref") / "projection_field"

projection_field_parameters = (
    "input_mesh, ref_mesh, epsg, "
    "shift_x, shift_y, "
    "zone, field_value, default_value, attr_name, "
    "var_name, var_unit"
)

projection_field_parameters_values = [
    (
        GARONNE_1_SLF,
        PROJECTION_FIELD_REF_PATH / "garonne_strick_minor_0.slf",
        EPSG_2154,
        430000.0,
        6350000.0,
        None,
        None,
        15.0,
        None,
        "BOTTOM FRICTION",
        "M1/3/S",
    ),
    (
        GARONNE_1_SLF,
        PROJECTION_FIELD_REF_PATH / "garonne_strick_minor_1.slf",
        EPSG_2154,
        430000.0,
        6350000.0,
        GARONNE_POLY,
        40.0,
        20.0,
        None,
        "BOTTOM FRICTION",
        "M1/3/S",
    ),
    (
        GARONNE_1_SLF,
        PROJECTION_FIELD_REF_PATH / "garonne_strick_minor_2.slf",
        EPSG_2154,
        430000.0,
        6350000.0,
        GARONNE_POLY,
        None,
        25.0,
        "Strickler",
        "BOTTOM FRICTION",
        "M1/3/S",
    ),
    (
        GARONNE_1_SLF,
        PROJECTION_FIELD_REF_PATH / "garonne_strick_minor_3.slf",
        EPSG_2154,
        430000.0,
        6350000.0,
        Path("Garonne") / "garonne_multi_poly.shp",
        None,
        25.0,
        "Strickler",
        "BOTTOM FRICTION",
        "M1/3/S",
    ),
    (
        GARONNE_1_SLF,
        PROJECTION_FIELD_REF_PATH / "garonne_water_depth_minor.slf",
        EPSG_2154,
        430000.0,
        6350000.0,
        GARONNE_POLY,
        2.0,
        0.0,
        None,
        "WATER DEPTH",
        "M",
    ),
]


@pytest.mark.parametrize(
    projection_field_parameters,
    projection_field_parameters_values,
)
def test_projection_field(
    test_data_path: Path,
    input_mesh: Path,
    ref_mesh: Path,
    epsg: str,
    shift_x: float,
    shift_y: float,
    zone: Path,
    field_value: float,
    default_value: float,
    attr_name: str,
    var_name: str,
    var_unit: str,
):
    from q4ts.processing.projection_field import ProjectionFieldAlgorithm

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    output_ref_mesh = test_data_path / ref_mesh
    zone_layer = None
    if zone:
        zone_layer = str(test_data_path / zone)

    params = {
        ProjectionFieldAlgorithm.INPUT_MESH: mesh_layer,
        ProjectionFieldAlgorithm.EPSG: epsg,
        ProjectionFieldAlgorithm.SHIFT_Y: shift_y,
        ProjectionFieldAlgorithm.SHIFT_X: shift_x,
        ProjectionFieldAlgorithm.FIELD_VALUE: field_value,
        ProjectionFieldAlgorithm.DEFAULT_VALUE: default_value,
        ProjectionFieldAlgorithm.ZONE_SHP: zone_layer,
        ProjectionFieldAlgorithm.ATTR_NAME: attr_name,
        ProjectionFieldAlgorithm.VAR_NAME: var_name,
        ProjectionFieldAlgorithm.VAR_UNIT: var_unit,
        ProjectionFieldAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:projection_field"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[ProjectionFieldAlgorithm.OUTPUT_MESH]
    assert filecmp.cmp(output_ref_mesh, output_mesh)
