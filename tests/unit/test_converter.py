
import shutil
from pathlib import Path
import pytest
from qgis.core import QgsProcessingContext
from qgis.core import QgsApplication
from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


SLF2MED_REF_PATH = Path("ref") / "slf2med"
GARONNE_1_SLF = Path("Garonne") / "garonne_1.slf"
GARONNE_1_CLI = Path("Garonne") / "garonne_1.cli"


def test_slf2med(test_data_path: Path):
    from q4ts.processing.slf2med import Slf2MedAlgorithm

    mesh_input_path = test_data_path / GARONNE_1_SLF

    specific_tmp = Path('/tmp')
    input_mesh = specific_tmp / 'garonne_1.slf'
    shutil.copyfile(mesh_input_path, input_mesh)
    output_mesh = specific_tmp / 'garonne_1_conv.med'

    params = {Slf2MedAlgorithm.INPUT_MESH: str(input_mesh),
              Slf2MedAlgorithm.OUTPUT_MESH: str(output_mesh)}

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:slf2med'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[Slf2MedAlgorithm.OUTPUT_MESH]
    assert output_mesh
    assert Path(output_mesh).exists()


def test_slf2med_with_bnd(test_data_path: Path):
    from q4ts.processing.slf2med import Slf2MedAlgorithm

    mesh_input_path = test_data_path / GARONNE_1_SLF
    bnd_input_path = test_data_path / GARONNE_1_CLI

    # Need to run in specific directory to avoid directory length errors
    specific_tmp = Path('/tmp')
    input_mesh = specific_tmp / 'garonne_1.slf'
    shutil.copyfile(mesh_input_path, input_mesh)
    input_bnd = specific_tmp / 'garonne_1.cli'
    shutil.copyfile(bnd_input_path, input_bnd)
    output_mesh = specific_tmp / 'garonne_1_conv_bnd.med'

    params = {Slf2MedAlgorithm.INPUT_MESH: str(input_mesh),
              Slf2MedAlgorithm.INPUT_BND: str(input_bnd),
              Slf2MedAlgorithm.OUTPUT_MESH: str(output_mesh)}

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:slf2med'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[Slf2MedAlgorithm.OUTPUT_MESH]
    assert output_mesh
    assert Path(output_mesh).exists()
