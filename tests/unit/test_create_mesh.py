from pathlib import Path
from typing import List

import pytest
import filecmp
from qgis.core import QgsVectorLayer
from qgis.core import QgsProcessingContext
from qgis.core import QgsApplication
from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


GARONNE_1_CONTOUR = Path("Garonne") / "garonne_countour.shp"
CREATE_MESH_REF_PATH = Path("ref") / "create_mesh"

create_mesh_parameters_names = "contour_path, ref_mesh, " \
                               "mesh_size, mesh_min_size, mesh_max_size, " \
                               "island_layers, constraint_line_layers, refine_layers"
create_mesh_parameters_values = [
    (
        GARONNE_1_CONTOUR, CREATE_MESH_REF_PATH / 'Garonne_test.slf',
        50.0, 25.0, 100.0,
        None, None, None
    ),
    (
        GARONNE_1_CONTOUR, CREATE_MESH_REF_PATH / 'Garonne_test_with_constraints.slf',
        100.0, 50.0, 100.0,
        None,
        [Path("Garonne") / "constraints1.shp", Path("Garonne") / "constraints2.shp"],
        None
    ),
    (
        GARONNE_1_CONTOUR, CREATE_MESH_REF_PATH / 'Garonne_test_with_isles.slf',
        100.0, 50.0, 100.0,
        [Path("Garonne") / "isle1.shp", Path("Garonne") / "isle2.shp"],
        None,
        None
    ),
    (
        GARONNE_1_CONTOUR, CREATE_MESH_REF_PATH / 'Garonne_test_with_refines.slf',
        100.0, 50.0, 100.0,
        None,
        None,
        [Path("Garonne") / "refinement1.shp", Path("Garonne") / "refinement2.shp"]
    ),
]


@pytest.mark.parametrize(
    create_mesh_parameters_names,
    create_mesh_parameters_values,
)
def test_create_mesh(test_data_path: Path, contour_path: Path, ref_mesh: Path,
                     mesh_size: float, mesh_min_size: float, mesh_max_size: float,
                     island_layers: List[Path], constraint_line_layers: List[Path], refine_layers: List[Path]):
    from q4ts.processing.create_mesh import CreateMeshAlgorithm

    contour_input_path = test_data_path / contour_path
    contour_layer = QgsVectorLayer(str(contour_input_path))

    specific_tmp = Path('/tmp')

    temp_med = specific_tmp / 'temp.med'
    output_mesh = specific_tmp / 'temp.slf'
    output_ref_mesh = test_data_path / ref_mesh

    params = {CreateMeshAlgorithm.CONTOUR_LAYER: contour_layer,
              CreateMeshAlgorithm.TEMP_MED: str(temp_med),
              CreateMeshAlgorithm.OUTPUT_MESH: str(output_mesh),
              CreateMeshAlgorithm.MESH_SIZE: mesh_size,
              CreateMeshAlgorithm.MESH_MIN_SIZE: mesh_min_size,
              CreateMeshAlgorithm.MESH_MAX_SIZE: mesh_max_size,
              CreateMeshAlgorithm.MESH_ENGINE: 0,
              CreateMeshAlgorithm.CONSTRAINT_LINE_LAYER: create_layer_list(constraint_line_layers, test_data_path),
              CreateMeshAlgorithm.ISLAND_LAYER: create_layer_list(island_layers, test_data_path),
              CreateMeshAlgorithm.REFINE_LAYER: create_layer_list(refine_layers, test_data_path)}

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:create_mesh'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[CreateMeshAlgorithm.OUTPUT_MESH]
    #assert filecmp.cmp(output_ref_mesh, output_mesh)
    assert output_mesh
    assert Path(output_mesh).exists()


def create_layer_list(layers, test_data_path):
    layer_list = []
    if layers:
        for layer in layers:
            layer_list.append(QgsVectorLayer(str(test_data_path / layer)))
    return layer_list
