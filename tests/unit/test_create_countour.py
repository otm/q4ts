from pathlib import Path
import pytest
from qgis.core import QgsVectorLayer
from qgis.core import QgsMeshLayer
from qgis.core import QgsProcessingContext
from qgis.core import QgsApplication
from qgis.testing import TestCase
from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


GARONNE_1_SLF = Path("Garonne") / "garonne_1.slf"


def test_contour(test_data_path: Path):
    from q4ts.processing.contour import CountourAlgorithm

    mesh_input_path = test_data_path / GARONNE_1_SLF
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    output_ref_contour = test_data_path / 'ref' / 'contour' / 'garonne_1' / 'OUTPUT_CONTOUR.shp'

    params = {CountourAlgorithm.INPUT_MESH: mesh_layer,
              CountourAlgorithm.OUTPUT_COUNTOUR: 'TEMPORARY_OUTPUT'}

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:contour'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_layer = QgsVectorLayer(result[CountourAlgorithm.OUTPUT_COUNTOUR])
    ref_layer = QgsVectorLayer(str(output_ref_contour))

    TestCase().assertLayersEqual(output_layer, ref_layer)
