import pytest
from qgis.core import QgsApplication
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


def test_alg_create_mesh_available():
    algo_str = 'q4ts:create_mesh'
    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    assert alg


def test_alg_not_available():
    algo_str = 'q4ts:invalid_alg'
    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    assert not alg
