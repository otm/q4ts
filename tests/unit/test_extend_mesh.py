import filecmp
from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsMeshLayer, QgsProcessingContext, QgsVectorLayer

from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import exit_qgis, setup_qgis_processing


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


GARONNE_EXTENSION_CONTOUR = Path("Garonne") / "garonne_extension.shp"
GARONNE_PARTIAL_EXTENSION_CONTOUR = Path("Garonne") / "garonne_partial_extension.shp"
GARONNE_JOIN_CONTOUR = Path("Garonne") / "garonne_join_contour.shp"

INPUT_MESH_SLF = Path("Garonne") / "garonne_1.slf"
OTHER_MESH_SLF = Path("Garonne") / "garonne_other.slf"
OTHER_WITH_CUT_MESH_SLF = Path("Garonne") / "garonne_other2.slf"

EXTEND_MESH_REF_PATH = Path("ref") / "extend_mesh"

extend_mesh_parameters_names = (
    "input_mesh, input_contour, other_mesh, ref_mesh, "
    "mesh_size, mesh_min_size, mesh_max_size"
)
extend_mesh_parameters_values = [
    (
        INPUT_MESH_SLF,
        GARONNE_EXTENSION_CONTOUR,
        None,
        EXTEND_MESH_REF_PATH / "Garonne_extension.slf",
        50.0,
        25.0,
        100.0,
    ),
    (
        INPUT_MESH_SLF,
        GARONNE_PARTIAL_EXTENSION_CONTOUR,
        None,
        EXTEND_MESH_REF_PATH / "Garonne_partial_extension.slf",
        100.0,
        50.0,
        100.0,
    ),
    (
        INPUT_MESH_SLF,
        GARONNE_JOIN_CONTOUR,
        OTHER_MESH_SLF,
        EXTEND_MESH_REF_PATH / "Garonne_join_no_cut_extension.slf",
        100.0,
        50.0,
        100.0,
    ),
    (
        INPUT_MESH_SLF,
        GARONNE_JOIN_CONTOUR,
        OTHER_WITH_CUT_MESH_SLF,
        EXTEND_MESH_REF_PATH / "Garonne_join_cut_extension.slf",
        100.0,
        50.0,
        100.0,
    ),
]


@pytest.mark.parametrize(
    extend_mesh_parameters_names,
    extend_mesh_parameters_values,
)
def test_extend_mesh(
    test_data_path: Path,
    input_mesh: Path,
    input_contour: Path,
    other_mesh: Path,
    ref_mesh: Path,
    mesh_size: float,
    mesh_min_size: float,
    mesh_max_size: float,
):
    from q4ts.processing.contour import CountourAlgorithm
    from q4ts.processing.extend_mesh import ExtendMeshAlgorithm

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:contour"

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")
    alg_contour = QgsApplication.processingRegistry().algorithmById(algo_str)

    params = {
        CountourAlgorithm.INPUT_MESH: mesh_layer,
        CountourAlgorithm.OUTPUT_COUNTOUR: "TEMPORARY_OUTPUT",
    }
    result, success = alg_contour.run(params, context, feedback)
    assert success

    mesh_contour_layer = QgsVectorLayer(result[CountourAlgorithm.OUTPUT_COUNTOUR])

    contour_input_path = test_data_path / input_contour
    contour_layer = QgsVectorLayer(str(contour_input_path))

    params = {
        ExtendMeshAlgorithm.INPUT_MESH: mesh_layer,
        ExtendMeshAlgorithm.CONTOUR_LAYER: mesh_contour_layer,
        ExtendMeshAlgorithm.EXTENDED_CONTOUR_LAYER: contour_layer,
        ExtendMeshAlgorithm.TEMP_MED: "TEMPORARY_OUTPUT",
        ExtendMeshAlgorithm.OTHER_TEMP_MED: "TEMPORARY_OUTPUT",
        ExtendMeshAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT",
        ExtendMeshAlgorithm.MESH_SIZE: mesh_size,
        ExtendMeshAlgorithm.MESH_MIN_SIZE: mesh_min_size,
        ExtendMeshAlgorithm.MESH_MAX_SIZE: mesh_max_size,
        ExtendMeshAlgorithm.MESH_ENGINE: 0,
    }

    if other_mesh:
        other_mesh_input_path = test_data_path / other_mesh
        other_mesh_layer = QgsMeshLayer(
            str(other_mesh_input_path), "input_mesh", "mdal"
        )
        params[ExtendMeshAlgorithm.OTHER_MESH] = other_mesh_layer

        contour_params = {
            CountourAlgorithm.INPUT_MESH: other_mesh_layer,
            CountourAlgorithm.OUTPUT_COUNTOUR: "TEMPORARY_OUTPUT",
        }
        result, success = alg_contour.run(contour_params, context, feedback)
        assert success

        other_contour_layer = QgsVectorLayer(result[CountourAlgorithm.OUTPUT_COUNTOUR])
        params[ExtendMeshAlgorithm.OTHER_CONTOUR_LAYER] = other_contour_layer

    algo_str = "q4ts:extend_mesh"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[ExtendMeshAlgorithm.OUTPUT_MESH]
    assert Path(output_mesh).exists()
    output_ref_mesh = test_data_path / ref_mesh
    assert filecmp.cmp(output_ref_mesh, output_mesh)
    assert output_mesh
