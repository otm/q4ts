from pathlib import Path

import pytest
import filecmp
from qgis.core import QgsVectorLayer
from qgis.core import QgsMeshLayer
from qgis.core import QgsProcessingContext
from qgis.core import QgsApplication
from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


def test_create_boundaries(test_data_path: Path):
    from q4ts.processing.create_boundary import CreateBoundaryAlgorithm

    contour_input_path = test_data_path / 'Garonne' / 'garonne_countour.shp'
    contour_layer = QgsVectorLayer(str(contour_input_path))

    bnd_input_path = test_data_path / 'Garonne' / 'boundaries.shp'
    bnd_layer = QgsVectorLayer(str(bnd_input_path))

    mesh_input_path = test_data_path / 'Garonne' / 'garonne_1.slf'
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    params = {CreateBoundaryAlgorithm.CONTOUR_LAYER: contour_layer,
              CreateBoundaryAlgorithm.BOUNDARIES_LAYER: bnd_layer,
              CreateBoundaryAlgorithm.CHECK_BOUNDARIES: False,
              CreateBoundaryAlgorithm.INPUT_MESH: mesh_layer,
              CreateBoundaryAlgorithm.OUTPUT_MESH: '/home/tm/test_bound.slf'}

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:create_boundary'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success


    output_mesh = result[CreateBoundaryAlgorithm.OUTPUT_MESH]
    #output_ref_mesh = test_data_path / 'ref' / 'create_boundary' / 'garonne_with_bnd.slf'
    #assert filecmp.cmp(output_ref_mesh, output_mesh)
    assert output_mesh
    assert Path(output_mesh).exists()


    output_mesh_cli = output_mesh[:len(output_mesh) - 3] + "cli"
    #output_ref_mesh_cli = test_data_path / 'ref' / 'create_boundary' / 'garonne_with_bnd.cli'
    #assert filecmp.cmp(output_ref_mesh_cli, output_mesh_cli)
    assert Path(output_mesh_cli).exists()
