import filecmp
import shutil
from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsProcessingContext, QgsVectorLayer

from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import exit_qgis, setup_qgis_processing


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


CUT_MESH_REF_PATH = Path("ref") / "cut_mesh"
CONTOUR_PATH = Path("Garonne") / "garonne_countour.shp"
GARONNE_OTHER_2_SLF = Path("Garonne") / "garonne_other2.slf"


def test_cut_mesh(test_data_path: Path):
    from q4ts.processing.cut_mesh import CutMeshAlgorithm

    mesh_input_path = test_data_path / GARONNE_OTHER_2_SLF

    specific_tmp = Path("/tmp")
    input_mesh = specific_tmp / "garonne_other_2.slf"
    shutil.copyfile(mesh_input_path, input_mesh)

    contour_input_path = test_data_path / CONTOUR_PATH
    contour_layer = QgsVectorLayer(str(contour_input_path))

    params = {
        CutMeshAlgorithm.INPUT_MESH: str(input_mesh),
        CutMeshAlgorithm.INPUT_CONTOUR: contour_layer,
        CutMeshAlgorithm.TEMP_MED: "TEMPORARY_OUTPUT",
        CutMeshAlgorithm.OUTPUT_MESH_MED: "TEMPORARY_OUTPUT",
        CutMeshAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:cut_mesh"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_ref_mesh = test_data_path / CUT_MESH_REF_PATH / "garonne_other_2_cut.slf"
    output_mesh = result[CutMeshAlgorithm.OUTPUT_MESH]
    assert filecmp.cmp(output_ref_mesh, output_mesh)
