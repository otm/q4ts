import filecmp
from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsMeshLayer, QgsProcessingContext, QgsVectorLayer

from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import exit_qgis, setup_qgis_processing


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


GARONNE_POLY_SHAPE = Path("Garonne") / "garonne_poly.shp"
GARONNE_1_SLF = Path("Garonne") / "garonne_1.slf"
CLOUD_02_XYZ = Path("Garonne") / "Cloud_02.xyz"
INTERP_CLOUD_REF_PATH = Path("ref") / "interp_cloud_point"

interp_cloud_point_parameters_names = (
    "input_mesh, cloud_point_file, delimiter, ref_mesh, "
    "kind, shift_x, shift_y, "
    "zone_shp, outside_zone, default_value, "
    "nmap "
)
interp_cloud_point_parameters_values = [
    (
        GARONNE_1_SLF,
        CLOUD_02_XYZ,
        " ",
        INTERP_CLOUD_REF_PATH / "garonne_1_interp.slf",
        "nearest",
        430000.0,
        6350000.0,
        None,
        False,
        None,
        False,
    ),
    (
        GARONNE_1_SLF,
        CLOUD_02_XYZ,
        " ",
        INTERP_CLOUD_REF_PATH / "garonne_2_interp.slf",
        "linear",
        430000.0,
        6350000.0,
        None,
        False,
        None,
        False,
    ),
    (
        GARONNE_1_SLF,
        CLOUD_02_XYZ,
        " ",
        INTERP_CLOUD_REF_PATH / "garonne_3_interp.slf",
        "linear",
        430000.0,
        6350000.0,
        GARONNE_POLY_SHAPE,
        False,
        100.0,
        False,
    ),
    (
        GARONNE_1_SLF,
        CLOUD_02_XYZ,
        " ",
        INTERP_CLOUD_REF_PATH / "garonne_5_interp.slf",
        "linear",
        430000.0,
        6350000.0,
        None,
        False,
        None,
        True,
    ),
]


@pytest.mark.parametrize(
    interp_cloud_point_parameters_names,
    interp_cloud_point_parameters_values,
)
def test_interp_cloud_point(
    test_data_path: Path,
    input_mesh: Path,
    cloud_point_file: Path,
    delimiter: str,
    ref_mesh: Path,
    kind: str,
    shift_x: float,
    shift_y: float,
    zone_shp: Path,
    outside_zone: bool,
    default_value: float,
    nmap: bool,
):
    from q4ts.processing.interp_cloud_point import InterpolationCloudPointAlgorithm

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    cloud_point_file_path = test_data_path / cloud_point_file
    output_ref_mesh = test_data_path / ref_mesh

    zone_shp_layer = None
    if zone_shp:
        zone_shp_layer = QgsVectorLayer(
            str(test_data_path / zone_shp), "input_area", "ogr"
        )

    params = create_interp_cloud_point_params(
        cloud_point_file_path,
        default_value,
        delimiter,
        kind,
        mesh_layer,
        outside_zone,
        shift_x,
        shift_y,
        zone_shp_layer,
    )

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:interp_cloud_point"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[InterpolationCloudPointAlgorithm.OUTPUT_MESH]
    assert filecmp.cmp(output_ref_mesh, output_mesh)


interp_cloud_point_parameters_chunk = (
    "input_mesh, cloud_point_file, delimiter, ref_mesh, "
    "kind, shift_x, shift_y, "
    "zone_shp, outside_zone, default_value, "
    "block_pts, chunk_x, chunk_y, chunk_overlap "
)
interp_cloud_point_parameters_chunk_values = [
    (
        GARONNE_1_SLF,
        CLOUD_02_XYZ,
        " ",
        INTERP_CLOUD_REF_PATH / "garonne_6_interp.slf",
        "linear",
        430000.0,
        6350000.0,
        GARONNE_POLY_SHAPE,
        True,
        50.0,
        None,
        5,
        5,
        0.2,
    ),
    (
        GARONNE_1_SLF,
        CLOUD_02_XYZ,
        " ",
        INTERP_CLOUD_REF_PATH / "garonne_4_interp.slf",
        "linear",
        430000.0,
        6350000.0,
        GARONNE_POLY_SHAPE,
        False,
        10.0,
        10000,
        None,
        None,
        None,
    ),
]


@pytest.mark.parametrize(
    interp_cloud_point_parameters_chunk,
    interp_cloud_point_parameters_chunk_values,
)
def test_interp_cloud_point_interp_chunk(
    test_data_path: Path,
    input_mesh: Path,
    cloud_point_file: Path,
    delimiter: str,
    ref_mesh: Path,
    kind: str,
    shift_x: float,
    shift_y: float,
    zone_shp: Path,
    outside_zone: bool,
    default_value: float,
    block_pts: int,
    chunk_x: int,
    chunk_y: int,
    chunk_overlap: float,
):
    from q4ts.processing.interp_cloud_point import InterpolationCloudPointAlgorithm

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    cloud_point_file_path = test_data_path / cloud_point_file
    output_ref_mesh = test_data_path / ref_mesh

    zone_shp_layer = None
    if zone_shp:
        zone_shp_layer = QgsVectorLayer(
            str(test_data_path / zone_shp), "input_area", "ogr"
        )

    params = create_interp_cloud_point_params(
        cloud_point_file_path,
        default_value,
        delimiter,
        kind,
        mesh_layer,
        outside_zone,
        shift_x,
        shift_y,
        zone_shp_layer,
    )

    params[InterpolationCloudPointAlgorithm.CHUNK_INTERP] = True
    params[InterpolationCloudPointAlgorithm.BLOCK_PTS] = block_pts
    params[InterpolationCloudPointAlgorithm.CHUNK_X_PARTITION] = chunk_x
    params[InterpolationCloudPointAlgorithm.CHUNK_Y_PARTITION] = chunk_y
    params[InterpolationCloudPointAlgorithm.CHUNK_OVERLAP] = chunk_overlap

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:interp_cloud_point"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[InterpolationCloudPointAlgorithm.OUTPUT_MESH]
    assert filecmp.cmp(output_ref_mesh, output_mesh)


def create_interp_cloud_point_params(
    cloud_point_file_path: Path,
    default_value: float,
    delimiter: str,
    kind: str,
    mesh_layer: QgsMeshLayer,
    outside_zone: bool,
    shift_x: float,
    shift_y: float,
    zone_shp_layer: QgsVectorLayer,
):
    from q4ts.processing.interp_cloud_point import InterpolationCloudPointAlgorithm

    params = {
        InterpolationCloudPointAlgorithm.INPUT_MESH: mesh_layer,
        InterpolationCloudPointAlgorithm.CLOUD_POINT_FILE: str(cloud_point_file_path),
        InterpolationCloudPointAlgorithm.DELIMITER: delimiter,
        InterpolationCloudPointAlgorithm.VAR_NAME: "BOTTOM",
        InterpolationCloudPointAlgorithm.VAR_UNIT: "M",
        InterpolationCloudPointAlgorithm.KIND: InterpolationCloudPointAlgorithm.KIND_ENUM.index(
            kind
        ),
        InterpolationCloudPointAlgorithm.SHIFT_Y: shift_y,
        InterpolationCloudPointAlgorithm.SHIFT_X: shift_x,
        InterpolationCloudPointAlgorithm.ZONE_SHP: zone_shp_layer,
        InterpolationCloudPointAlgorithm.OUTSIDE_ZONE: outside_zone,
        InterpolationCloudPointAlgorithm.DEFAULT_VALUE: default_value,
        InterpolationCloudPointAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT",
    }
    return params
