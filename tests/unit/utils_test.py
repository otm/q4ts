from qgis.core import QgsApplication

prov = None


def setup_qgis_processing(salome_bin, telemac_env_script):
    from q4ts.processing.provider import Provider
    from q4ts.processing.settings import SalomeHydroSettings
    from processing.core.ProcessingConfig import ProcessingConfig, Setting

    global prov
    prov = Provider()
    prov.load()
    ProcessingConfig.setSettingValue(SalomeHydroSettings.SALOME_BIN, salome_bin)
    ProcessingConfig.setSettingValue(SalomeHydroSettings.TELEMAC_ENV_SCRIPT, telemac_env_script)
    ProcessingConfig.readSettings()

    QgsApplication.processingRegistry().addProvider(prov)

def exit_qgis():
    QgsApplication.processingRegistry().removeProvider(prov)