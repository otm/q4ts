import shutil
from pathlib import Path

import pytest

from qgis.core import QgsMeshLayer
from qgis.core import QgsProcessingContext
from qgis.core import QgsApplication
from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import setup_qgis_processing, exit_qgis


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


GARONNE_1_SLF = Path("Garonne") / "garonne_1.slf"

refine_parameters_names = "input_mesh, refine_level, method, domain"
refine_parameters_values = [
    (GARONNE_1_SLF, 2, 0, None),
    (GARONNE_1_SLF, 2, 0, Path("Garonne") / "RefinmentZone_L93TR.shp"),
    (GARONNE_1_SLF, 2, 1, None),
    (GARONNE_1_SLF, 1, 1, Path("Garonne") / "RefinmentZone_L93TR.shp"),
]


@pytest.mark.parametrize(
    refine_parameters_names,
    refine_parameters_values,
)
def test_refine(test_data_path: Path, input_mesh: Path,
                refine_level: int, method: int, domain: Path):
    from q4ts.processing.refine import RefineAlgorithm

    specific_tmp = Path('/tmp')

    # Need to run in specific directory to avoid directory length errors
    mesh_input_path = test_data_path / input_mesh
    input_mesh_tmp = specific_tmp / 'input_temp.slf'
    shutil.copyfile(mesh_input_path, input_mesh_tmp)

    mesh_layer = QgsMeshLayer(str(input_mesh_tmp), "input_mesh", "mdal")

    init_face_count = mesh_layer.meshFaceCount()
    init_vertex_count = mesh_layer.meshVertexCount()

    temp_med = specific_tmp / 'temp.med'
    output_mesh = specific_tmp / 'temp.slf'

    params = {RefineAlgorithm.INPUT_MESH: mesh_layer,
              RefineAlgorithm.TEMP_MED: str(temp_med),
              RefineAlgorithm.OUTPUT_MESH: str(output_mesh),
              RefineAlgorithm.REFINE_LEVEL: refine_level,
              RefineAlgorithm.METHOD: method}

    if domain:
        # Add test data path
        params[RefineAlgorithm.DOMAIN] = str(test_data_path / domain)

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = 'q4ts:refine'

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[RefineAlgorithm.OUTPUT_MESH]
    assert output_mesh
    assert Path(output_mesh).exists()

    # Check that faces and vertex are added
    output_mesh_layer = QgsMeshLayer(output_mesh, "output_mesh", "mdal")
    result_face_count = output_mesh_layer.meshFaceCount()
    result_vertex_count = output_mesh_layer.meshVertexCount()

    assert result_face_count > init_face_count
    assert result_vertex_count > init_vertex_count
