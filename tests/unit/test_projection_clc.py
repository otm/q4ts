import filecmp
from pathlib import Path

import pytest
from qgis.core import QgsApplication, QgsMeshLayer, QgsProcessingContext

from .console_processing_feedback import ConsoleProcessingFeedback
from .utils_test import exit_qgis, setup_qgis_processing


@pytest.fixture(autouse=True)
def setup(qgis_app, qgis_new_project, qgis_processing, salome_bin, telemac_env_script):
    setup_qgis_processing(salome_bin, telemac_env_script)


def teardown():
    exit_qgis()


GARONNE_1_SLF = Path("Garonne") / "garonne_1.slf"
GARONNE_1_CLC = Path("Garonne") / "garonne_1_clc.gpkg"
INTERP_CLC_REF_PATH = Path("ref") / "interp_clc"

interp_clc_parameters = (
    "input_mesh, clc_layer, ref_mesh, " "shift_x, shift_y, " "write_clc_code"
)
interp_clc_parameters_values = [
    (
        GARONNE_1_SLF,
        GARONNE_1_CLC,
        INTERP_CLC_REF_PATH / "garonne_with_clc.slf",
        430000.0,
        6350000.0,
        False,
    ),
    (
        GARONNE_1_SLF,
        GARONNE_1_CLC,
        INTERP_CLC_REF_PATH / "garonne_with_clc_code.slf",
        430000.0,
        6350000.0,
        True,
    ),
]


@pytest.mark.parametrize(
    interp_clc_parameters,
    interp_clc_parameters_values,
)
def test_interp_clc(
    test_data_path: Path,
    input_mesh: Path,
    clc_layer: Path,
    ref_mesh: Path,
    shift_x: float,
    shift_y: float,
    write_clc_code: bool,
):
    from q4ts.processing.projection_clc import ProjectionCorineLandCoverAlgorithm

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    clc_layer_file_path = test_data_path / clc_layer
    output_ref_mesh = test_data_path / ref_mesh

    params = {
        ProjectionCorineLandCoverAlgorithm.INPUT_MESH: mesh_layer,
        ProjectionCorineLandCoverAlgorithm.CLC_DATA_FILE: str(clc_layer_file_path),
        ProjectionCorineLandCoverAlgorithm.SHIFT_Y: shift_y,
        ProjectionCorineLandCoverAlgorithm.SHIFT_X: shift_x,
        ProjectionCorineLandCoverAlgorithm.WRITE_CLC_CODE: write_clc_code,
        ProjectionCorineLandCoverAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:projection_corine_land_cover"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[ProjectionCorineLandCoverAlgorithm.OUTPUT_MESH]
    # assert filecmp.cmp(output_ref_mesh, output_mesh)
    assert output_mesh


interp_clc_area_parameters = (
    "input_mesh, clc_layer, ref_mesh, "
    "shift_x, shift_y, "
    "zone, zone_attribute, zone_offset, "
    "write_clc_code"
)
interp_clc_area_parameters_values = [
    (
        GARONNE_1_SLF,
        GARONNE_1_CLC,
        INTERP_CLC_REF_PATH / "garonne_with_zone_clc.slf",
        430000.0,
        6350000.0,
        Path("Garonne") / "zone_clc.shp",
        "strickler",
        True,
        False,
    ),
    (
        GARONNE_1_SLF,
        GARONNE_1_CLC,
        INTERP_CLC_REF_PATH / "garonne_with_zone_clc_code.slf",
        430000.0,
        6350000.0,
        Path("Garonne") / "zone_clc.shp",
        "strickler",
        True,
        True,
    ),
]


@pytest.mark.parametrize(
    interp_clc_area_parameters,
    interp_clc_area_parameters_values,
)
def test_interp_clc_with_area(
    test_data_path: Path,
    input_mesh: Path,
    clc_layer: Path,
    ref_mesh: Path,
    shift_x: float,
    shift_y: float,
    zone: Path,
    zone_attribute: str,
    zone_offset: bool,
    write_clc_code: bool,
):
    from q4ts.processing.projection_clc import ProjectionCorineLandCoverAlgorithm

    mesh_input_path = test_data_path / input_mesh
    mesh_layer = QgsMeshLayer(str(mesh_input_path), "input_mesh", "mdal")

    clc_layer_file_path = test_data_path / clc_layer
    output_ref_mesh = test_data_path / ref_mesh
    zone_layer = str(test_data_path / zone)

    params = {
        ProjectionCorineLandCoverAlgorithm.INPUT_MESH: mesh_layer,
        ProjectionCorineLandCoverAlgorithm.CLC_DATA_FILE: str(clc_layer_file_path),
        ProjectionCorineLandCoverAlgorithm.SHIFT_Y: shift_y,
        ProjectionCorineLandCoverAlgorithm.SHIFT_X: shift_x,
        ProjectionCorineLandCoverAlgorithm.WRITE_CLC_CODE: write_clc_code,
        ProjectionCorineLandCoverAlgorithm.ZONE_SHP: zone_layer,
        ProjectionCorineLandCoverAlgorithm.ATTR_NAME: zone_attribute,
        ProjectionCorineLandCoverAlgorithm.ZONE_SHIFT: zone_offset,
        ProjectionCorineLandCoverAlgorithm.OUTPUT_MESH: "TEMPORARY_OUTPUT",
    }

    context = QgsProcessingContext()
    feedback = ConsoleProcessingFeedback()
    algo_str = "q4ts:projection_corine_land_cover"

    alg = QgsApplication.processingRegistry().algorithmById(algo_str)
    result, success = alg.run(params, context, feedback)
    assert success

    output_mesh = result[ProjectionCorineLandCoverAlgorithm.OUTPUT_MESH]
    # assert filecmp.cmp(output_ref_mesh, output_mesh)
    assert output_mesh
    assert Path(output_mesh).exists()
