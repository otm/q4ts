from pathlib import Path

import pytest


def pytest_addoption(parser):
    parser.addoption("--salome_bin", action="store",
                     default='/home/tm/salome/SALOME-9.9.0-native-DB11-SRC/salome')
    parser.addoption("--telemac_env_script", action="store",
                     default='/home/tm/telemac-mascaret/git_main/configs/pysource.ubuntu.gfortran.dyn.sh')


@pytest.fixture()
def salome_bin(request):
    return request.config.getoption("--salome_bin")


@pytest.fixture()
def telemac_env_script(request):
    return request.config.getoption("--telemac_env_script")


@pytest.fixture()
def test_data_path() -> Path:
    return Path(__file__).parent / '..' / 'data'
